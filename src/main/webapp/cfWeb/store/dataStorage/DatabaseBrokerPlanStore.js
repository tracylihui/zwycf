Ext.define('cfWeb.store.dataStorage.DatabaseBrokerPlanStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.databaseBrokerPlanStore',
	autoLoad:false,
	fields:[
	        {name:'name',type:'String'},
	        {name:'public',type:'String'},
	        {name:'guid',type:'String'}
	]
	
});
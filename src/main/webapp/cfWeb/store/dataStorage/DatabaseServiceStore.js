Ext.define('cfWeb.store.dataStorage.DatabaseServiceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.databaseServiceStore',
	autoLoad:false,
	fields:[
	        {name:'label',type:'String'},
	        {name:'plans'}
	],
	proxy:{
		type:'rest',
		url:'rest/service/listforbroker',
		reader : {
	 			type : 'json',
	 			root : 'data',
	 			successProperty : 'success'
	 		}
	}

});

Ext.define('cfWeb.store.dataStorage.DataStorageStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.datbaseListStore',
	autoLoad : false,
	fields : [
	          {name : 'version' , type : 'string'},
	          {name:'title',mapping:'serviceInstanceName'},
	          {name : 'plan' , type : 'string'},
	          {name : 'service' , type : 'string',mapping:'service'},
	          {name : 'itemImg', mapping : 'serviceTypeImage', type : 'string'},
	          {name : 'itemId' , mapping : 'serviceTypeId' ,type:'int'},
	          {name : 'host' , mapping : 'host' ,type:'string'},
	          {name : 'port' , mapping : 'port' ,type:'int'},
	          {name : 'user' , mapping : 'user' ,type:'string'}
//	          {name : 'itemColor', defaultValue : 'blue'},
//			  {name : 'instance', type : 'int'},
//	          {name : 'title', type : 'string'},
//			  {name : 'status', type : 'string'},
//			  {name : 'memory', type : 'int'},
//	          {name : 'space',  type : 'double'},
//	          {name : 'runtime',  type : 'string'},
//	          {name : 'service',  type : 'string'}
	],
//	data: [{'template':'test1', 'instance':1, 'title':'test1', 'status':'test1', 'memory':1, 'space':1.0, 'runtime':'test1', 'service':'test1'}]
	proxy : {
		type : 'rest',
		url : 'rest/service/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

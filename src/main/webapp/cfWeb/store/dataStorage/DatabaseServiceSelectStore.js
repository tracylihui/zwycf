Ext.define('cfWeb.store.dataStorage.DatabaseServiceSelectStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.databaseServiceSelectStore',
	autoLoad : false,
	fields : [
	          {name : 'version' , type : 'string'},
	          {name:'title',mapping:'serviceInstanceName'},
	          {name : 'plan' , type : 'string'},
	          {name : 'label' , type : 'string'},
	          {name : 'itemImg', mapping : 'serviceTypeImage', type : 'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/service/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.appService.AppServiceListStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appServiceListStore',
	autoLoad : false,
	fields : [
	          {name : 'version' , type : 'string'},
	          {name:'title',mapping:'serviceInstanceName'},
	          {name : 'plan' , type : 'string'},
	          {name : 'label' , type : 'string'},
	          {name : 'itemImg', mapping : 'serviceTypeImage', type : 'string'},
	          {name : 'itemId' , mapping : 'serviceTypeId' ,type:'int'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/service/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

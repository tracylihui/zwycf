Ext.define('cfWeb.store.appService.AppServiceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appServiceStore',
	autoLoad : true,
	fields : [
	          {name : 'itemId', mapping : 'id', type : 'int'},
	          {name : 'itemName', mapping : 'serName', type : 'String',
	        	  convert : function(v, record){
	        		  return cfWebLang.Service[v];}
	          },
	          {name : 'itemIco', mapping : 'serTemImg', type : 'String'},
	          {name : 'type',type : 'int'},
	          {name : 'serPrefix'},
	          {name : 'brokers'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/service/getDatabaseSer',
		extraParams : {
			type : 3
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

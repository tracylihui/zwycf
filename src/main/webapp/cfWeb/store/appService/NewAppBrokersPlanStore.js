Ext.define('cfWeb.store.appService.NewAppBrokersPlanStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.newAppBrokersPlanStore',
	autoLoad:false,
	fields:[
	        {name:'name',type:'String'},
	        {name:'public',type:'String'},
	        {name:'guid',type:'String'}
	]
	
});
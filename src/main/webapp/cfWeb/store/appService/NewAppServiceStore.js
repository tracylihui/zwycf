Ext.define('cfWeb.store.appService.NewAppServiceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.newAppServiceStore',
	autoLoad:false,
	fields:[
	        {name:'label',type:'String'},
	        {name:'plans'}
	],
	proxy:{
		type:'rest',
		url:'rest/service/listforbroker',
		reader : {
	 			type : 'json',
	 			root : 'data',
	 			successProperty : 'success'
	 		}
	}
});

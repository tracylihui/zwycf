Ext.define('cfWeb.store.util.MemoryStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.memoryStore',
	autoLoad :false,
	autoSync: false,
	fields :['memoryKey','memoryValue'],
	data :[
		{'memoryKey':"512M",'memoryValue':'512'},
		{'memoryKey':"1G",'memoryValue':'1024'},
		{'memoryKey':"2G",'memoryValue':'2048'},
		{'memoryKey':"4G",'memoryValue':'4096'},
		{'memoryKey':"8G",'memoryValue':'8192'},
		{'memoryKey':"16G",'memoryValue':'16348'},
	]
});

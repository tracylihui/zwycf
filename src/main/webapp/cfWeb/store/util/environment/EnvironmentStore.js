Ext.define('cfWeb.store.util.environment.EnvironmentStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.environmentStore',
	autoLoad : false,
	fields : [
	          {name : 'itemId', mapping : 'id', type : 'int'},
	          {name : 'itemName', mapping : 'envName', type : 'String'},
	          {name : 'itemIco', mapping : 'envImg', type : 'String'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/info/cfEnv',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.util.dashboard.ServiceUsedStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.serviceUsedStore',
	autoLoad : false,
	fields : [
	          {name : 'itemId', type : 'int'},
	          {name : 'itemName', type : 'String'},
	          {name : 'itemCount', mapping : 'serviceNum', type : 'int'},
	          {name : 'total', type : 'int'}
	]
//	proxy : {
//		type : 'rest',
//		url : 'rest/info/basicInfo',
//		reader : {
//			type : 'json',
//			successProperty : 'success'
//		}
//	}
});
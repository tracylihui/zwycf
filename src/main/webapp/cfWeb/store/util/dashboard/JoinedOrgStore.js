Ext.define('cfWeb.store.util.dashboard.JoinedOrgStore', {
	extend: 'Ext.data.Store',
	alias: 'widget.joinedOrgStore',
	autoLoad: false,
	fields: [
		{name: 'itemId', type: 'int'},
		{name: 'itemName', type: 'String'},
		{name: 'itemCount', mapping: 'joinedOrg', type: 'double'},
		{name: 'total', mapping: 'allOrg', type: 'int'}
	]
//	proxy : {
//		type : 'rest',
//		url : 'rest/info/basicInfo',
//		reader : {
//			type : 'json',
//			successProperty : 'success'
//		}
//	}
});
Ext.define('cfWeb.store.util.dashboard.MemoryUsedStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.memoryUsedStore',
	autoLoad : false,
	fields : [
	          {name : 'itemId', type : 'int'},
	          {name : 'itemName', type : 'String'},
	          {name : 'itemCount', mapping : 'memoryUsed', type : 'double'},
	          {name : 'total', mapping : 'memoryAll', type : 'int'}
	]
//	proxy : {
//		type : 'rest',
//		url : 'rest/info/basicInfo',
//		reader : {
//			type : 'json',
//			successProperty : 'success'
//		}
//	}
});
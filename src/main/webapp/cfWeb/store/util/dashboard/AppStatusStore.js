Ext.define('cfWeb.store.util.dashboard.AppStatusStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appStatusStore',
	autoLoad : false,
	fields : [
	          {name : 'itemId', type : 'int'},
	          {name : 'itemName', type : 'String'},
	          {name : 'itemCount', type : 'int'},
	          {name : 'total', type : 'int'}
	]
//	proxy : {
//		type : 'rest',
//		url : 'rest/info/basicInfo',
//		reader : {
//			type : 'json',
//			successProperty : 'success'
//		}
//	}
});
Ext.define('cfWeb.store.util.dashboard.RouteUsedStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.routeUsedStore',
	autoLoad : false,
	fields : [
	          {name : 'itemId', type : 'int'},
	          {name : 'itemName', type : 'String'},
	          {name : 'itemCount', mapping : 'applicationNum', type : 'int'},
	          {name : 'total', mapping : 'applicationAll', type : 'int'}
	]
//	proxy : {
//		type : 'rest',
//		url : 'rest/info/basicInfo',
//		reader : {
//			type : 'json',
//			successProperty : 'success'
//		}
//	}
});
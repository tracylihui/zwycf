Ext.define('cfWeb.store.accountSetting.QuotaStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.quotaStore',
	autoLoad :	false,
	fields :[
	        {name : 'guid',type : 'String',mapping:'metadata.guid'},
			{name : 'name',type : 'String',mapping:'entity.name'},
			{name : 'services',type : 'String',mapping:'entity.total_services'},
			{name : 'routes',type : 'String',mapping:'entity.total_routes'},
			{name : 'memeory',type : 'String',mapping:'entity.memory_limit'},
			{name : 'instancememeory',type : 'String',mapping:'entity.instance_memory_limit'},
		],
	proxy : {
		type : 'rest',
		url : 'rest/quota/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

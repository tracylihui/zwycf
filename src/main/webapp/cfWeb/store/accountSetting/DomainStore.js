Ext.define('cfWeb.store.accountSetting.DomainStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.domainStore',
	autoLoad :	false,
	fields :[
			{name : 'name',type : 'String'},
			{name : 'status',type : 'String'}
		],
	proxy : {
		type : 'rest',
		url : 'rest/org/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

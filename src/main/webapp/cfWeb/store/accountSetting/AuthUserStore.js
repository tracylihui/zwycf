Ext.define('cfWeb.store.accountSetting.AuthUserStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.authUserStore',
	autoLoad :false,
	fields :[
			{name : 'userName',type : 'String'},
			{name : 'userRole',type : 'int'}

			],
	proxy : {
		type : 'rest',
		url : 'rest/user/orgUser',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

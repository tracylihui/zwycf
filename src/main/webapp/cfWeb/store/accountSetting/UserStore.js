Ext.define('cfWeb.store.accountSetting.UserStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.userStore',
	autoLoad :	false,
	fields :[
			{name : 'userName',type : 'String'},
			{name : 'id',type : 'int'},
			{name : 'userPass',type : 'String'}

			],
	proxy : {
		type : 'rest',
		url : 'rest/user/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.accountSetting.BuildpackStore', {
	extend : 'Ext.data.Store',
	alias : 'widget.buildpackStore',
	fields : [ {
		name : 'id',
		type : 'int'
	}, {
		name : 'envName',
		type : 'String'
	}, {
		name : 'envDescChn',
		type : 'String'
	}, {
		name : 'envDescEng',
		type : 'String'
	}, {
		name : 'envImg',
		type : 'String'
	} ],
	proxy : {
		type : 'rest',
		url : 'rest/buildpack/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	},
	autoLoad : true
});

Ext.define('cfWeb.store.accountSetting.SpaceRoleStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.spaceRoleStore',
	autoLoad :false, 
	fields :[
			{name : 'name',type : 'String'},
			{name : 'displayname',type : 'String'}
		],
	data:[{name:'SpaceManager',displayname:cfWebLang.permission.SpaceManager},
	      {name:'SpaceDeveloper',displayname:cfWebLang.permission.SpaceDeveloper},
	      {name:'SpaceAuditor',displayname:cfWebLang.permission.SpaceAuditor}]
});
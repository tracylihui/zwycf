
Ext.define('cfWeb.store.accountSetting.RouteStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.routeStore',
	autoLoad :	false,
	fields :[
			{name : 'host',type : 'String'},
			{name : 'domain',type : 'String'},
			{name : 'apps', type: 'auto'}
		],
	proxy : {
		type : 'rest',
		url : 'rest/route/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.accountSetting.SpaceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.spaceStore',
	autoLoad :	false,
	fields :[
			{name : 'name',type : 'String'},
			{name : 'orgName',type : 'String'}
		],
	proxy : {
		type : 'rest',
		url : 'rest/space/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

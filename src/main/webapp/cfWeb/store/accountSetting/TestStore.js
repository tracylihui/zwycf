Ext.define('cfWeb.store.accountSetting.TestStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.TestStore',
	autoLoad :	false, 
	fields: [{name : 'name',type : 'String'}],
	data: [{name:'test1'},
		      {name:'test2'},
		      {name:'test3'}]
});
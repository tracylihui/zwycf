Ext.define('cfWeb.store.accountSetting.AppNameStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appNameStore',
	autoLoad :false,
	autoSync: false,

	fields :[
			{name : 'name',type : 'String'}
			],
	proxy : {
		type : 'rest',
		url : 'rest/app/listAtOrgSpa',
//		isSynchronous:true,
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.accountSetting.OrgStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.orgStore',
	autoLoad :	false,
	fields :[
	        {name : 'guid',type : 'String'},
	        {name : 'quota',type : 'String'},
			{name : 'name',type : 'String'},
			{name : 'cloud',type : 'String'},
			{name : 'operation',type : 'String'}
		],
	proxy : {
		type : 'rest',
		url : 'rest/org/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

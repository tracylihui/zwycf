Ext.define('cfWeb.store.accountSetting.OrgRoleStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.orgRoleStore',
	autoLoad :	false, 
	fields :[
			{name : 'name',type : 'String'},
			{name : 'displayname',type : 'String'}
		],
	data:[{name:'OrgManager',displayname:cfWebLang.permission.OrgManager},
	      {name:'BillingManager',displayname:cfWebLang.permission.BillingManager},
	      {name:'OrgAuditor',displayname:cfWebLang.permission.OrgAuditor}]  
});
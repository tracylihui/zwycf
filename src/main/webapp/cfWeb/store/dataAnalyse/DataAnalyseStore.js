Ext.define('cfWeb.store.dataAnalyse.DataAnalyseStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.dataAnalyseStore',
	autoLoad : true,
	fields : [
	          {name : 'itemId', mapping : 'id', type : 'int'},
	          {name : 'itemName', mapping : 'serName', type : 'String',
	        	  convert : function(v, record){
	        		  return cfWebLang.Service[v];}
	          },
	          {name : 'itemIco', mapping : 'serTemImg', type : 'String'},
	          {name : 'brokers'},
	          {name : 'type',type:'int'},
	          {name: 'serPrefix'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/service/getDatabaseSer',
		extraParams : {
			type : 2
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}

});

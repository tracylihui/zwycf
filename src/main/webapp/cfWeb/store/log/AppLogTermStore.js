Ext.define('cfWeb.store.log.AppLogTermStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appLogTermStore',
	autoLoad : false,
	fields : [
	          {name : 'name'},
	          {name : 'count'}
	]
});
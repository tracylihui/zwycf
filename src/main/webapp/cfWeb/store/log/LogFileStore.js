Ext.define('cfWeb.store.log.LogFileStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.logFileStore',
	autoLoad : false,
	fields : [
	          {name : 'fileName'},
	          {name : 'errorCount'},
	          {name : 'exceptionCount'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/log/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

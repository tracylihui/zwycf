Ext.define('cfWeb.store.log.AppLogFieldStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appLogFieldStore',
	fields :[
	 		{name: 'patternId',type :'int'},
	 		{name: 'patternField',type: 'string'}
	 	],
	 	proxy : {
	 		type : 'rest',
	 		url : 'rest/app/getFields',
	 		reader : {
	 			type : 'json',
	 			root : 'data',
	 			successProperty : 'success'
	 		}
	 	}
});

Ext.define('cfWeb.store.log.AppLogConfigStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appLogConfigStore',
	autoLoad : false,
	fields :[
		{name: 'appId',type :'int'},
		{name: 'configJson',type: 'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/app/getconfigs',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

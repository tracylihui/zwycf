Ext.define('cfWeb.store.appManage.StorageComboStore', {
    extend: 'Ext.data.Store',
    alias : 'widget.storageComboStore',
    autoLoad: false,
    fields: [
        {name: 'id', type: 'String'},
        {name: 'name', type: 'String'}
    ],
    data :[
    	{'id': 'c1','name': 'c1'},
    	{'id': 'c2','name': 'c2'},
    	{'id': 'c3','name': 'c3'},
    	{'id': 'c4','name': 'c4'}
    ]
//    proxy: {
//    	type : 'ajax',
//		url : 'rest/dossiers/getCategoryLarge',
//		api : {
//			read : 'rest/dossiers/getCategoryLarge'
//		},
//		reader : {
//			type : 'json',
//			root : 'data',
//			successProperty : 'success'
//		}
//    }
//     fields:['appSource', 'cloudSource'],
//    data:[
//    { 'appSource': 'Oracle1',  "cloudSource":"C1" },
//    { 'appSource': 'Oracle2',  "cloudSource":"C2"},
//    { 'appSource': 'Oracle3', "cloudSource":"C3"},
//    { 'appSource': 'Oracle4', "cloudSource":"C4"}
//     ]
});
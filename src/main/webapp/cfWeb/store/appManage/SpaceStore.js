Ext.define('cfWeb.store.appManage.SpaceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.spaceStore',
	autoLoad : false,
	fields :[
		{name: 'name',type :'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/space/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.appManage.AppHistStore',{
	extend : 'cfWeb.cfWebComponent.store.CfDataStore',
	alias : 'widget.appHistStore',
	autoLoad : false,
	fields : [{name:'name',mapping:'date'},
	          {name:'data',mapping:'value',type:'number'}],
	proxy : {
		type : 'rest',
		url : 'rest/log/latency',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

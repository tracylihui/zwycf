Ext.define('cfWeb.store.appManage.OrgStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.orgStore',
	autoLoad : false,
	fields :[
		{name: 'name',type :'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/org/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

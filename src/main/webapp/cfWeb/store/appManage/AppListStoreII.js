Ext.define('cfWeb.store.appManage.AppListStoreII', {
    extend: 'Ext.data.Store',
    alias: 'widget.appListStoreII',
    autoLoad: false,
    autoSync: true,
    fields: ['id','appName','templateId', 'instance', 'org', 'space','state','userName',
        {
            name: 'memory',
            convert: function (v) {
                if (v >= 1024) {
                    v = v / 1024 + 'G';
                } else v = v + "M";
                return v;
            }
        },
        {
            name:'date',
            convert:function(v){
                var d = new Date(v);
                return d.toLocaleDateString();
            }
        },
        {
            name:'status',
            convert:function(v,rec){
                v = rec.data.state;
                switch(v){
                    case 0: return '未审核';
                    case 1: return '未通过';
                    case 2: return '已批准';
                }
            }
        }
    ],
    proxy: {
        type: 'rest',
        url: 'rest/audit/listall',
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('cfWeb.store.appManage.DomainStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.domainStore',
	autoLoad : false,
	fields :[
		{name: 'name',type :'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/domain/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

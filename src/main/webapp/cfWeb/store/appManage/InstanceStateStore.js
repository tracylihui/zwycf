Ext.define('cfWeb.store.appManage.InstanceStateStore', {
			extend : 'Ext.data.Store',
			alias : 'widget.instanceStateStore',
			autoLoad : false,
			fields : [{
						name : 'date'
					}, {
						name : 'value'
					}]
});

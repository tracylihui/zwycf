Ext.define('cfWeb.store.appManage.DataAnalyseStore', {
	extend : 'Ext.data.Store',
	alias : 'widget.dataAnalyseStore',
	autoLoad : true,
	fields : [ {
		name : 'itemId',
		mapping : 'id',
		type : 'int'
	}, {
		name : 'itemName',
		mapping : 'serName',
		type : 'String',
		convert : function(v, record) {
			return cfWebLang.Service[v];
		}
	}, {
		name : 'itemIco',
		mapping : 'serImg',
		type : 'String'
	} ],
	proxy : {
		type : 'rest',
		url : 'rest/service/listall',
		extraParams : {
			type : 2
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

Ext.define('cfWeb.store.appManage.TemplateOrgStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.templateOrgStore',
	autoLoad : true,
	fields : [
	          {name : 'name', type : 'String'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/org/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

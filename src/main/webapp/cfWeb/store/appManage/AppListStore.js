Ext.define('cfWeb.store.appManage.AppListStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appListStore',
	autoLoad : false,
	fields : [
	          {name : 'template' ,mapping : 'cfapp.temName', type : 'string'},
	          {name : 'itemColor', mapping : 'cfapp.temColor'},
			  {name : 'instance', mapping : 'cloudApp.entity.instances', type : 'int'},
	          {name : 'title', mapping : 'cloudApp.entity.name', type : 'string'},
	          {name : 'relatedApp', mapping : 'cfapp.relatedApp', type : 'string'},
			  {name : 'status', mapping : 'cloudApp.entity.state', type : 'string'},
			  {name : 'memory', mapping : 'cloudApp.entity.memory', type : 'int',convert:function(value){
				  if(value!=null){
					  return value/1024.0;
				  }
			  }},
	          {name : 'space', mapping : 'cloudApp.entity.disk_quota/1024', type : 'double'},
	          {name : 'runtime', mapping : 'cloudApp.entity.buildpack', type : 'string'},
	          {name : 'itemImg', mapping : 'cfapp.temImg', type : 'string'},
	          {name : 'message', mapping : 'cloudApp.message', type : 'string'},
//	          {name : 'id', mapping : 'id', type : 'string'},
	          {name : 'service', mapping : 'cloudApp.service_instance', type : 'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/app/listall',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

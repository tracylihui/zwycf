Ext.define('cfWeb.store.appManage.StorageStore', {
        extend: 'Ext.data.Store',
        alias: 'widget.storageStore',
        autoLoad: false,
       fields: [
            {name: 'appSource', type: 'string'},
            {name: 'cloudSource', type: 'string'}
        ],
        data :[
        ]
});
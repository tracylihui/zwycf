Ext.define('cfWeb.store.appManage.AppListDetailsStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appListDetailsStore',
	autoLoad : true,
	fields : [
	          {name : 'itemImg', type : 'string'},
	          {name : 'title', type : 'string'},
	          {name : 'message', type : 'int'},
	          {name : 'itemColor', type : 'string'},
	          {name : 'status', type : 'string'},
	          {name : 'instance', type : 'int'},
	          {name : 'memory', type : 'int'},
	          {name : 'space', type : 'int'},
	          {name : 'runtime', type : 'string'},
	          {name : 'serviceName', type : 'string'},
	          {name : 'instanceStatus', type : 'string'},
	          {name : 'domain', type : 'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/app/details',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

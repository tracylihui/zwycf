Ext.define('cfWeb.store.appManage.AppDetailsEnvVarStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appDetailsEnvVarStore',
	autoLoad : false,
	fields : [
	          {name : 'key' ,  type : 'string'},
	          {name : 'value', type : 'string'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/app/getEnv',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

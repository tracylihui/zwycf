Ext.define('cfWeb.store.appManage.ServiceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.serviceStore',
	autoLoad : false,
	fields:[{name :'serName_cfWebLang',mapping :'serName',
			convert : function(v, record){
	        		  return cfWebLang.Service[v];}},
	        {name :'serName',mapping :'serName'},
	        {name :'brokers',mapping :'brokers'},
	        {name :'serPrefix',mapping :'serPrefix'}],
	proxy : {
			type : 'rest',
			url : 'rest/service/listall?type=1',
			reader : {
				type : 'json',
				root : 'data'
			}
	}
});

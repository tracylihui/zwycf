Ext.define('cfWeb.store.appManage.BuildpackStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.buildpackStore',
	autoLoad : false,
	fields : [
	          {name : 'itemId', mapping : 'id', type : 'int'},
	          {name : 'itemName', mapping : 'envName', type : 'String'},
	          {name : 'itemIco', mapping : 'envImg', type : 'String'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/info/cfEnv',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

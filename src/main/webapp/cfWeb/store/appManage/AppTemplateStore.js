Ext.define('cfWeb.store.appManage.AppTemplateStore', {
	extend: 'Ext.data.Store',
	alias: 'widget.appTemplateStore',
	autoLoad: true,
	fields: [{
		name: 'itemId',
		mapping: 'id',
		type: 'int'
	}, {
		name: 'itemName',
		mapping: 'temName',
		type: 'String'
	}, {
		name: 'itemIco',
		mapping: 'temImg',
		type: 'String'
	}, {
		name: 'type',
		type: 'int'
	}],
	
		proxy : {
			type : 'rest',
			url : 'rest/template/listTemplateOfCurrentUser',
			reader : {
				type : 'json',
				root : 'data',
				successProperty : 'success'
			}
		}
});
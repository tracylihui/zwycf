Ext.define('cfWeb.store.appManage.TemplateSpaceStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.templateSpaceStore',
	autoLoad : false,
	fields : [
	          {name : 'name', type : 'String'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/space/list',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

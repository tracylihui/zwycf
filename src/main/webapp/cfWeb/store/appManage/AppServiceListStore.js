Ext.define('cfWeb.store.appManage.AppServiceListStore',{
	extend : 'Ext.data.Store',
	alias : 'widget.appServiceListStore',
	autoLoad : false,
	fields : [
//	          {name : 'version' , type : 'string'},
	          {name:'serviceName',mapping:'serviceInstanceName'},
	          {name : 'plan' , mapping:'plan',type : 'string'},
	          {name : 'service' , type : 'string',mapping:'service',type : 'string'},
	          {name : 'itemImage', mapping : 'serviceTypeImage'},
	          {name : 'itemId' , mapping : 'serviceTypeId' ,type:'int'},
	          {name : 'createTime' , mapping : 'createTime' ,type:'long',convert : function(v,rec){
	        	  if(v==null){
	        		  return null;
	        	  }
	        	  var date = new Date(v);
	        	  console.log(v);
	        	  console.log(Ext.Date.format(date,'Y/m/d'));
	        	  //var time = date.getFullYear() +'/'+ date.getMonth() +'/' +date.getDay() ;

	        	  return Ext.Date.format(date,'Y/m/d');

	          }},
	          {name : 'updateTime' , mapping : 'updateTime' ,type:'long',convert : function(v,rec){
	        	  if(v){
		        	  var date = new Date(v);
		        	 // var time = date.getFullYear() +'/'+ date.getMonth() +'/' + date.getDay();
		        	  return Ext.Date.format(date,'Y/m/d');

	        	  }
	        	  else{
	        		  return '';
	        	  }
	          }},
	          {name : 'id' , mapping : 'id' ,type:'int'},
	          {name : 'detail', mapping : 'detail'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/app/services',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

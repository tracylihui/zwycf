Ext.define('cfWeb.store.appManage.AppAccessStore',{
	extend : 'cfWeb.cfWebComponent.store.CfDataStore',
	alias : 'widget.appAccessStore',
	autoLoad : false,
	fields : [{name:'name',mapping:'date'},
	          {name:'data',mapping:'value',type:'number'}
	],
	proxy : {
		type : 'rest',
		url : 'rest/log/pageview',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}
});

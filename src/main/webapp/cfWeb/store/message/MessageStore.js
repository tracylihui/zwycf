Ext.define('cfWeb.store.message.MessageStore', {
    extend: 'Ext.data.Store',
    alias : 'widget.messageStore',
    autoLoad: false,

   fields : ['fileName','errorCount','exceptionCount'],

   proxy : {
		type : 'rest',
		url : 'rest/log/listEE',
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success'
		}
	}

});

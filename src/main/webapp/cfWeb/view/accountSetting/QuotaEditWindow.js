Ext.define('cfWeb.view.accountSetting.QuotaEditWindow',{
	alias : 'widget.quotaEditWindow',
	extend : 'Ext.window.Window',
	cls: 'style-cfWindow',
	title : cfWebLang.QuotaManage.createTitle,
	layout : 'fit',
	height:300,
	width:500,
	items:[{
			xtype : 'form',
			cls: 'window-popup',
			border : 0,
			items:[{
				layout:'column',
				margin:'35 0 10 0',
				items:[{
					xtype: 'textfield',
			        name: 'name',
			        labelWidth : 150,
					labelAlign:'right',
			        fieldLabel: cfWebLang.Util.Name
				},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.Util.FieldRequired,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		},{
	    			xtype : 'hiddenfield',
	    			name : 'guid'
	    		},{
	    			xtype : 'displayfield',
	    			itemId : 'nameRepeatError',
	    			value :cfWebLang.QuotaManage.nameRepeat,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		}]
			},{
				layout:'column',
				margin:'10 0 10 0',
				items:[{
	    			xtype : 'numberfield',
	    			cls: 'appPanel-input',
	    			fieldLabel :cfWebLang.QuotaManage.services,
	    			labelAlign : 'right', 
	    			minValue : 1,
					maxValue: 10000,
					labelWidth : 150,
					labelStyle:'color:#2a3950;',
	    			name : 'services'
	    		},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.Util.FieldRequired,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		}]
			},{
				layout:'column',
				margin:'10 0 10 0',
				items:[{
	    			xtype : 'numberfield',
	    			cls: 'appPanel-input',
	    			lableStyle:{color:' #2a3950'},
	    			fieldLabel :cfWebLang.QuotaManage.routes,
	    			labelAlign : 'right', 
	    			minValue : 1,
					maxValue: 10000,
					labelWidth : 150,
					labelStyle:'color:#2a3950;',
	    			name : 'routes'
	    		},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.Util.FieldRequired,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		}]
			},{
				layout:'column',
				margin:'10 0 10 0',
				items:[{
					xtype: 'textfield',
			        name: 'memeory',
			        labelWidth : 150,
					labelAlign:'right',
					width:260,
					fieldLabel : cfWebLang.QuotaManage.memory,
					listeners:{
						'afterrender':function(obj){
							var value=obj.getValue();
							if(value==null||value==""){
								obj.setValue(value);
								return;
							}
							if(value<1024){
								obj.setValue(value+"M");
								return;
							}else{
								obj.setValue(Math.round(value/1024)+"G");
							}
							
						}
					}
					
				},{
	    			xtype : 'displayfield',
	    			value:'(e.g. 128M, 1G)'
			},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.Util.FieldRequired,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		}]
			},{
				layout:'column',
				margin:'10 0 10 0',
				items:[{
	    			xtype : 'combobox',
	    			store : Ext.create('cfWeb.store.util.MemoryStore'),
	    			editable:false,
	    			displayField : 'memoryKey',
	    			valueField : 'memoryValue',
	    			queryMode:'local',
	    			valueField : 'memoryValue',
	    			fieldLabel : cfWebLang.QuotaManage.instancesmemory,
	    			labelAlign : 'right',
	    			labelWidth : 150,
	    			name:'instancememeory',
	    			hidden:true
	    		}]
			},{
				xtype : 'fieldset',
				border : 0,
				margin:'20 0 0 120',
				items :[
				{
					xtype : 'button',
					text : cfWebLang.Util.Confirm,
					action : 'addQuota'
				},{
					xtype : 'button',
					text : cfWebLang.Util.Cancel,
					action : 'close'
				}]
			}]
		}],
	initComponent : function(){
		this.callParent(arguments);
	}
});

Ext.define('cfWeb.view.accountSetting.OrgManageView',{
	alias : 'widget.orgManageView',
	extend : 'Ext.panel.Panel',
	itemId : 'orgManageWindow',
	cls:"account-setting",
	overflowX:'hidden',
	overflowY:'auto',
	items :[{
		xtype : 'grid',
		itemId : 'orgManagePanel',
		store : Ext.create('cfWeb.store.accountSetting.OrgStore'),
		columns :[
		{header : cfWebLang.Util.Name,dataIndex : 'name',align : 'center',flex :1},
		{header : cfWebLang.SettingWindow.belongToCloud,dataIndex : 'cloud',align : 'center',flex : 1,renderer:function(){
			var nav= Ext.ComponentQuery.query('navView')[0];
			return nav.platform;
		}},
		{
		  header : cfWebLang.Util.Operate,
		  menuText:cfWebLang.Util.Operate,
	      xtype : 'actioncolumn',
	      align : 'center',
	      renderer : function(me,classes,data,row,col){
	      	var me=this;
	        me.items[0].iconCls = 'dis_n';
	      	me.items[1].iconCls = 'dis_n';
	      	me.items[2].iconCls = 'dis_n';
			var windowPanel = me.up('panel[itemId=orgManageWindow]');
			var grid = windowPanel.down('grid[itemId=orgManagePanel]');
			var button = windowPanel.down('button[itemId=addUser]');
			var isOrgRole = false;
			console.log(isAdmin);
	        if(isAdmin==1){
				me.items[0].iconCls = '';
				me.items[2].iconCls = '';
				button.setVisible(true);
			}
	      	if(isAdmin == false){
	      		var length = userRoles.length;
	      		var record =grid.getStore().getAt(row);
	      		var orgName = record.data.name;
	      		var j = 0;
	      		for(;j<length;j++){
	      			if(userRoles[j].orgName == orgName&&userRoles[j].authType == "OrgManager"){
	      				me.items[0].iconCls = '';
	      			}else{
	      				button.setVisible(false);
	      			}
	      		}
	      	}	   

	      },
	      flex : 1,
	      items : [ {
	      	xtype : 'actioncolumn',
	      	cls:'edit',
	        tooltip : cfWebLang.Util.Edit,
	        itemId : 'orgEdit',
	        handler : function(me,rindex){
	        	var routeView = me.up('SettingWindow').down('routeManageView');
	            var spaceView = me.up('SettingWindow').down('spaceManageView');
	            var domainView = me.up('SettingWindow').down('domainManageView');
	        	var window =Ext.create('cfWeb.view.accountSetting.OrgEditWindow',{
	        		rindexFlag : rindex,
	        		component : me
	        	});
	        	window.routeView = routeView;
	        	window.spaceView = spaceView;
	        	window.domainView = domainView;
	        	console.log(routeView);
	        	var record=me.getStore().getAt(rindex);
	        	window.down('form').loadRecord(record);
	        	window.quota=record.get('quota');
	        	window.show();
	        }
	      },{
	    	xtype : 'actioncolumn',
	      	icon : 'resources/images/user.png',
	        tooltip : cfWebLang.Util.UserAuth,
	        text : cfWebLang.Util.UserAuth,
	       	handler : function(){
	       		alert("you have click the userAuthorize button");
	       	}
	      },{
	    	  header :cfWebLang.Util.Delete,
	    	  xtype : 'actioncolumn',
	      	tooltip :cfWebLang.Util.Delete,
	      	handler : function(me,rindex){
	      		Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){  
	                if(btn == 'yes') { 
	                	var routeView = me.up('SettingWindow').down('routeManageView');
	                	var spaceView = me.up('SettingWindow').down('spaceManageView');
	                	var domainView = me.up('SettingWindow').down('domainManageView');
	                	var routeOrg = routeView.down('combobox[itemId=org]').getValue();
	                	var routeOrgStore = routeView.down('combobox[itemId=org]').getStore();
	                	var spaceOrg = spaceView.down('combobox[itemId=org]').getValue();
	                	var spaceOrgStore = spaceView.down('combobox[itemId=org]').getStore();
	                	var domainOrg = domainView.down('combobox[itemId=org]').getValue();
	                	var domainOrgStore = domainView.down('combobox[itemId=org]').getStore();
	                	var store=me.getStore();
	                	var record=store.getAt(rindex);
	                	var orgName = record.data.name;
	                	store.remove(record);
	                	Ext.Ajax.request({
	                		url : 'rest/org/delete',
	                		method : 'GET',
	                		params :{
	                			orgName : orgName
	                		},
	                		success : function(resp){
	                			var response=Ext.decode(resp.responseText);                			
	                			var flag =response.success;
	                			currentORG=response.data[0];
	  						  	currentSpace=response.data[1];
	                			if(flag == true){
	                				routeOrgStore.reload();
	                				spaceOrgStore.reload();
	                				domainOrgStore.reload();
	                				if(routeOrg==orgName){
	                					if(routeOrgStore.data.length>0){
	                						routeView.down('combobox[itemId=org]').setValue(routeOrgStore.data.items[0].data.name);
	                					}else{
	                						routeView.down('combobox[itemId=org]').setValue('');
	                					}	
	                				}
	                				if(spaceOrg==orgName){
	                					if(spaceOrgStore.data.length>0){
	                						spaceView.down('combobox[itemId=org]').setValue(spaceOrgStore.data.items[0].data.name);
	                					}else{
	                						spaceView.down('combobox[itemId=org]').setValue('');
	                					}	
	                				}
	                				if(domainOrg==orgName){
	                					if(domainOrgStore.data.length>0){
	                						domainView.down('combobox[itemId=org]').setValue(domainOrgStore.data.items[0].data.name);
	                					}else{
	                						domainView.down('combobox[itemId=org]').setValue('');
	                					}	
	                				}
	                				var curOrg=Ext.fly('selectOrg');
				    			    if(curOrg.getValue().trim()==orgName.trim()){
				    				   var obj=document.getElementById("selectOrg");
				    				   obj.options.remove(obj.selectedIndex);
				    				   Ext.getCmp('navView').orgOnChange();
				    			    }else{
				    				   var obj=document.getElementById("selectOrg");
				    		           for(var i=0;i<obj.length;i++){
				    		        	   if(orgName.trim()==obj.options[i].value){
				    		        		   obj.options.remove(i);
				    		        	   }
				    		           }
				    			    }
	                			}
	                		},
	                		failure : function(resp){
	                			var error = Ext.JSON.decode(resp.responseText);
				                if(error.data!=null&&error.data.source==1){
					                     Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				                }else{
					                    Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
				                }
	                		}
	                	});
	                }
	      		});
	       	}
	   }]
	  }
	]
	},{
		xtype : 'button',
		cls:'style-addButton icon-plus22',
		itemId : 'addUser',
		buttonAlign : 'right',
		hidden:true,
		style :{
			'float':'right',
			'margin-right':'4px',
			'line-height':'10px!important',
			'padding-left':'2px',
			//'margin-top':'-28px'
		}
	}
	],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});

var orgStore = Ext.create('cfWeb.store.accountSetting.OrgStore');
Ext.define('cfWeb.view.accountSetting.RouteManageView', {

	alias : 'widget.routeManageView',
	extend : 'Ext.panel.Panel',
	itemId : 'routeManagerWindow',
	overflowY : 'auto',
	overflowX: 'hidden',
	items : [
			{
				xtype : 'combobox',
				cls:'style-cfCombo',
				labelWidth :70,
				labelStyle:'color:#8d8d8d',
				labelAlign:'right',
				fieldLabel:cfWebLang.SettingWindow.OrgList,
				margin:'10 10 10 10',
				editable : false,
				itemId : 'org',
				store : 'accountSetting.OrgStore',
				style : "float:left;margin-right:10px;",
				emptyText : cfWebLang.SettingWindow.OrgChoose,
				displayField : 'name'
			},
			{
				xtype : 'combobox',
				labelWidth : 70,
				labelAlign:'right',
				labelStyle:'color:#8d8d8d',
				fieldLabel:cfWebLang.SettingWindow.SpaceList,
				editable : false,
				cls:'style-cfCombo',
				margin:'10 0 10 10',
				itemId : 'space',
				queryMode : 'local',
				store : 'appManage.SpaceStore',
				emptyText : cfWebLang.SettingWindow.SpaceChoose,
				displayField : 'name'

			},
			{
				xtype : 'grid',
				itemId : 'routerManagePanel',
				cls:"account-setting",
				viewConfig:{
                    loadMask: false
                },
				hidden : true,
				store : Ext.create('cfWeb.store.accountSetting.RouteStore'),
				columns : [
						{
							header :cfWebLang.Util.Name,
							dataIndex : 'host',
							align : 'center',
							flex : 2,
						},
						{
							header :cfWebLang.Util.Domain,
							dataIndex : 'domain',
							align : 'center',
							flex : 2
						},
						{
							header : cfWebLang.SettingWindow.BindApp ,
							dataIndex : 'apps',
							align : 'center',
							flex : 2
						},
						{
							header :cfWebLang.Util.Edit,
							xtype : 'actioncolumn',
							menuText:cfWebLang.Util.Edit,
							hidden: true,
							itemId: 'editRouter',
							align : 'center',
//							icon : 'resources/images/icon_edit.png',
							tooltip :cfWebLang.Util.Edit,
							flex : 1,
							handler:function(me, rowIndex){
								var apps = me.getStore().getAt(rowIndex).get('apps');
								console.log("edit apps:" + apps);
								var windowPanel = me.up('panel').up('panel');
								var orgName = windowPanel.down(
										'combobox[itemId=org]').getValue();
								console.log("edit orgName :" + orgName);
								var spaceName = windowPanel.down(
										'combobox[itemId=space]').getValue();
								console.log("edit spaceName :" + spaceName);
								
								var editWindow = Ext.widget("routeAppEditView");
								var itemSelector = editWindow.down('itemselector[itemId=appSelector]');
								var store=itemSelector.getStore();
								store.load({
									params: {
										org:orgName,
										space:spaceName,
									}
								});
								console.log(store);
								itemSelector.setValue(apps);
//								itemSelector.getValue();
								editWindow.show();
							}
						},
						{
							header :cfWebLang.Util.Delete,
							menuText:cfWebLang.Util.Delete,
//							icon : 'resources/images/icon_delete.png',
							tooltip : cfWebLang.Util.Delete,
							align : 'center',
							itemId:'deleteRoute',
							hidden: true,
							xtype : 'actioncolumn',
							flex : 1,
							handler : function(me, rindex) {
								Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){
									if(btn=='yes'){
										var store = me.getStore();
								        var record = store.getAt(rindex);
								        console.log("redcord data :" + record.data);
								        var host = record.data.host;
								        console.log("delete host:" + host);
								        var domainName = record.data.domain;
								        console.log("delete domain:" + domainName);
								        var windowPanel = me.up('panel').up('panel');
								        var orgName = windowPanel.down('combobox[itemId=org]').getValue();
								        console.log("delete orgName :" + orgName);
								        var spaceName = windowPanel.down('combobox[itemId=space]').getValue();
								        console.log("delete spaceName :" + spaceName);
								        store.remove(record);
								        Ext.Ajax.request({
									        url : 'rest/route/delete',
									        method : 'GET',
									        params : {
									           	orgName : orgName,
										        spaceName : spaceName,
										        host : host,
										        domainName : domainName
									        },
									        success : function(resp) {
										        console.log(resp);
										        console.log("delete router success");
										        routeOper = true;
									        },
									        failure : function(resp) {
										        var error = Ext.JSON.decode(resp.responseText);
				                                if(error.data!=null&&error.data.source==1){
					                               Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				                                }else{
					                               Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
				                                }
									        }
								       });
							       }  
								});
								}
								
						} ]
			}
			,
			{
				xtype : 'button',
				hidden : true,
				cls:'style-addButton icon-plus22',
				itemId : 'addRoute',
				buttonAlign : 'right',
				style :{
					'float':'right',
					'margin-right':'4px',
					'line-height':'10px!important',
					'padding-left':'2px',
					//'margin-top':'-28px'
				}
			}
			],

	initComponent : function() {
		this.callParent(arguments);
	}
});

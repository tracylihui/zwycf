Ext.define('cfWeb.view.accountSetting.AddBuildpackWindow',{
	alias : 'widget.addBuildpackWindow',
	extend : 'Ext.window.Window',
	title :cfWebLang.SettingWindow.NewBuildpack,
	modal : true,
	height : 200,
	header:{
		height:27
	},
	cls: 'style-cfWindow',
	width : 450,
	items :[{
		xtype : 'form',
		cls: 'window-popup',
		items :[{
			margin:'15 0 0 0',
			xtype : 'textfield',
			fieldLabel :cfWebLang.SettingWindow.InputBuildpackName,
			itemId : 'inputBuildpackName',
			name:'envName',
			width:362,
			labelWidth: 150,
			labelAlign : 'right',
			allowBlank : false
		},
		{
	        xtype: 'filefield',
	        margin:'10 0 0 0',
	        name: 'uploadFile',
	        fieldLabel: 'buildpack',
	        labelWidth: 150,
	        labelAlign : 'right',
	        msgTarget: 'side',
	        allowBlank: false,
	        anchor: '100%',
	        buttonText: 'Select ...'
	    },{
			xtype : 'fieldset',
			border : 0,
			margin:'20 0 0 80',
			items :[
			{
				xtype : 'button',
				text : cfWebLang.Util.Confirm,
				itemId : 'add'
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
			}]
		}]
	}
	],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});
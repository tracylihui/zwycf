Ext.define('cfWeb.view.accountSetting.AuthOrgUserWindow',{
	extend : 'Ext.window.Window',
	requires:['cfWeb.store.accountSetting.AuthUserStore'],
	alias : 'widget.authOrgUserWindow',
	modal : true,
	padding:20,
	width : 600,
	orgRole : [],
	spaceRole : [],
	header:{
		height:24
	},
	cls:'select-cls style-cfWindow window-popup',
	items : [{
		xtype : 'combobox',
		labelWidth:120,
		editable : false,
		itemId:'orgType',
		emptyText :cfWebLang.SettingWindow.OrgChoose,
		displayField : 'name',
		valueField : 'name',
		store :'accountSetting.OrgStore'
		},{
		hidden : false,
		height : 200,
		buttons:['add', 'remove'],
		width : '100%',
		xtype : 'itemselector',
		itemId : 'orgSelector',
		anchor : '100%',
		store :'accountSetting.OrgRoleStore',
		displayField : 'displayname',
		valueField : 'name',
		allowBlank : true,
		msgTarget : 'side',
		fromTitle :cfWebLang.SettingWindow.RoleList,
		toTitle : cfWebLang.SettingWindow.ChoseRole 
	},{
		xtype : 'combobox',
		labelWidth:120,
		editable : false,
		itemId:'spaceType',
		emptyText : cfWebLang.SettingWindow.SpaceChoose,
		displayField : 'name',
		valueField : 'name',
		store : Ext.create('cfWeb.store.accountSetting.SpaceStore')
		},{
			hidden : false,
			height : 200,
			width : '100%',
			buttons:['add', 'remove'],
			xtype : 'itemselector',
			//itemId : 'authOrgUserSelector',
			anchor : '100%',
			store :'accountSetting.SpaceRoleStore',
			itemId : 'spaceSelector',
			displayField : 'displayname',
			valueField : 'name',
			allowBlank : true,
			msgTarget : 'side',
			fromTitle : cfWebLang.SettingWindow.RoleList,
			toTitle : cfWebLang.SettingWindow.ChoseRole 
		},
		{
			xtype : 'fieldset',
			border : false,
			margin:'0 0 0 150',
			items :[
			{
				xtype : 'button',
				text : cfWebLang.Util.Confirm ,
				itemId : 'updateRole',
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel',
			}]
		}],	
	initComponent : function(){
		this.callParent(arguments);
	}
});
Ext.define('cfWeb.view.accountSetting.RouteAddWindow',{
	alias : 'widget.routeAddWindow',
	extend : 'Ext.window.Window',
	title : cfWebLang.SettingWindow.NewRoute,
	modal : true,
	height : 180,
	autoScroll : false,
	header:{
		height:27
	},
	cls: 'style-cfWindow',
	width : 400,
	items :[{
		xtype : 'form',
		cls:'window-popup',
		items :[{
			layout:'column',
			margin:'15 0 0 0',
			items:[{
			    xtype : 'textfield',
			fieldLabel : cfWebLang.SettingWindow.InputName ,
			itemId : 'inputHostName',
			name:'host',
			labelWidth:125,
			labelAlign : 'right',
			width : 270,
			listeners : {
					blur : function(me){
						var routeName = me.getValue().toLowerCase();
						var errorTip = me.up('routeAddWindow').down('displayfield[itemId=errorRouteTip]');
						if(routeName == ''){
							errorTip.setValue(cfWebLang.SettingWindow.RouteEmp);
							errorTip.setVisible(true);
						}
					},
					focus : function(me){
						var window = me.up('window');
						var errorTip = window.down('displayfield');
						errorTip.setVisible(false);
						me.setValue('');
					}
				}
			},{
    			xtype : 'displayfield',
    			itemId : 'errorRouteTip',
    			value :cfWebLang.SettingWindow.RouteEmp,
    			fieldStyle : {
    					'margin-left' : '5px',
    					
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:120,
    			hidden : true
    		}]
			
		},{
			margin:'10 0 0 0',
			xtype : 'combobox',
			fieldLabel : cfWebLang.SettingWindow.InputDomain ,
			itemId : 'inputDomainName',
			labelWidth:125,
			labelAlign : 'right',
			editable : false,
			store :'appManage.DomainStore',
			displayField : 'name',
			valueField : 'name',
			name : 'domain',
			queryMode : 'local',
			width : 270
		},{
			xtype : 'fieldset',
			border : 0,
			margin:'20 0 0 80',
			items :[
			{
				xtype : 'button',
				text :cfWebLang.Util.Confirm,
				itemId : 'addRoute'
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
			}]
		}]
	}
	],	
	initComponent : function(){
		this.callParent(arguments);
	}
});
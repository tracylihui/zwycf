Ext.define('cfWeb.view.accountSetting.RouteAppEditWindow', {
	extend : 'Ext.window.Window',
	// requires : [ 'cfWeb.store.accountSetting.AppNameStore' ],
	alias : 'widget.routeAppEditView',
	modal : true,
	width : 600,
	height: 360,
	header:{
		height:27
	},
	title:cfWebLang.SettingWindow.selectApp,
	cls:'select-cls style-cfWindow window-popup',
	items : [ {
			hidden : false,
			height : 240,
			buttons:['add', 'remove'],
			width : '100%',
			xtype : 'itemselector',
			itemId : 'appSelector',
			anchor : '100%',
			store :'accountSetting.AppNameStore',
			displayField : 'name',
			valueField : 'name',
//			value:['test1'],
			allowBlank : true,
			msgTarget : 'side',
			fromTitle : cfWebLang.SettingWindow.AppList,
			toTitle : cfWebLang.SettingWindow.BindApp
	},{
		    xtype : 'fieldset',
			border : 0,
			margin:'20 0 0 160',
		    items:[{
		       xtype : 'button',
			   itemId : 'editRouterSubmit',
			   text:cfWebLang.Util.Submit
		    },{
		    	xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
		    }]
	}
	],
	initComponent : function() {
		this.callParent(arguments);
	}
});
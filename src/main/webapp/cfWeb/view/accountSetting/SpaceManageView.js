Ext.define('cfWeb.view.accountSetting.SpaceManageView',{
	alias : 'widget.spaceManageView',
	extend : 'Ext.panel.Panel',
	itemId : 'spaceManageWindow',
	autoScroll : true,
	items :[{
		xtype : 'combobox',
		labelWidth:60,
		labelAlign:'right',
		fieldLabel:cfWebLang.SettingWindow.OrgList,
		margin:'10 0 10 10',
		editable : false,
		labelStyle:'color:#8d8d8d',
		cls:'style-cfCombo',
		itemId :'org',
		emptyText : cfWebLang.SettingWindow.OrgChoose,
		displayField : 'name',
		store : 'accountSetting.OrgStore',
		listeners : {
			'expand' : function(me){
				me.store.load();
			}
		}
	},{
		cls:"account-setting",
		xtype : 'grid',
		hidden : true,
		itemId : 'spaceManagePanel',
//		plugins: [
//		Ext.create('Ext.grid.plugin.CellEditing', {
//			clicksToEdit: 1
//		})
//	    ],
		store : Ext.create('cfWeb.store.accountSetting.SpaceStore'),
		columns :[
		{
			header : cfWebLang.Util.Name,
			dataIndex : 'name',
			align : 'center',
			flex :1
//			field :{
//				xtype : 'textfield',
//				listeners :{
//					'blur' : function(me){
//						var grid=me.up('grid[itemId=spaceManagePanel]');
//						var store=grid.getStore();
//						var window=grid.up('panel[itemId=spaceManageWindow]');
//						var orgName=window.down('combobox').getValue();
//						var spaceName=me.originalValue;
//						var newName=me.lastValue;
//						console.log('hellobutton');
//						console.log(me);
//						console.log('spaceName');
//						console.log(spaceName);
//						console.log('newName');
//						console.log(newName);
//						if(spaceName==""&&newName!=""){
//							Ext.Ajax.request({
//								url : 'rest/space/add',
//								method : 'POST',
//								params :{
//									orgName : orgName,
//									spaceName : newName
//								},
//								success : function(resp){
//									console.log(resp);
//									console.log('success');
//									store.load({
//										url : 'rest/space/list',
//										params :{
//											orgName : orgName
//										}
//									});
//								},
//								failure : function(resp){
//									console.log(resp);
//									console.log("failure");
//									store.load({
//										url : 'rest/space/list',
//										params :{
//											orgName : orgName
//										}
//									})
//								}
//							})
//						}else if(spaceName!=""&&newName!=""){
//							Ext.Ajax.request({
//								url : 'rest/space/rename',
//								method : 'POST',
//								params :{
//									orgName : orgName,
//									spaceName : spaceName,
//									newName : newName
//								},
//								success : function(resp){
//									console.log(resp);
//									console.log("rename success");
//									store.load({
//										url : 'rest/space/list',
//										params :{
//											orgName : orgName
//										}
//									});
//								},
//								failure : function(resp){
//									console.log(resp);
//									console.log("rename failure");
//									store.load({
//										url : 'rest/space/list',
//										params :{
//											orgName : orgName
//										}
//									});
//								}
//							})
//						};
//						
//					}
//				}
//			}
			
		},
		{header : cfWebLang.SettingWindow.belongToOrg ,dataIndex : 'orgName',align : 'center',flex : 2},
		{
		  header : cfWebLang.Util.Operate,
		  menuText:cfWebLang.Util.Operate,
	      xtype : 'actioncolumn',
	      align : 'center',
	      renderer : function(me,classes,data,row,col){
	      	console.log("this");
	      	console.log(this);
	      	console.log(classes);
	      	console.log(data);
	      	console.log(row);
	      	console.log(col);
	      	var me=this;
	      	me.items[0].iconCls = 'dis_n';
	      	me.items[1].iconCls = 'dis_n';
	      	me.items[2].iconCls = 'dis_n';
	      	me.items[3].iconCls = 'dis_n';
	      	var windowPanel = me.up('panel[itemId=spaceManageWindow]');
	      	var orgName=windowPanel.down('combobox').getValue();
	      	console.log(orgName);
	      	var button= windowPanel.down('button[itemId=addSpaceUser]');
	      	var orgAuthFlag = false;
	      	var spaceAuthFlag = undefined;
			
//	      	setTimeout(100,function(){
//	    				me.items[0].iconCls = '';
//	      				me.items[1].iconCls = '';
//	      				me.items[2].iconCls = '';
//	      	});
	      	
			if(isAdmin==1){
				me.items[1].iconCls = '';
				me.items[3].iconCls = '';
				button.setVisible(true);
			}
	      	if(isAdmin == false){
	      		Ext.Ajax.request({
	      			url : 'rest/user/currentuserOrgRole',
	      			method : 'GET',
	      			async : false,
	      			params :{
	      				orgName : orgName
	      			},
	      			success : function(resp){
	      				var data=Ext.decode(resp.responseText).data;
	      				var length=data.length;
	      				console.log(resp);
	      				console.log(" resp data success");
	      				console.log(length);
	      				console.log(data);
	      				var i=0;
	      				console.log("helloworld data");
	      				for(;i<length;i++){
	      					console.log(data[i].spaceName);
	      					if(data[i].spaceName == null|| data[i].spaceName == ''){
	      						console.log(data[i].spaceName);
	      						if(data[i].authType == 'OrgManager'){
	      							console.log(data[i].authType);
	      							orgAuthFlag = true;
	      						}
	      					}
	      				}
	      				console.log("orgAuthFlag = "+orgAuthFlag);
	      				if(orgAuthFlag == true){
	      					me.items[3].iconCls = '';
	      					button.setVisible(true);
	      				}
	      				var store = windowPanel.down('grid[itemId=spaceManagePanel]').getStore();
		      			var record = store.getAt(row);
		      			console.log(record);
		      			var spaceName = record.data.name;
		      			console.log(spaceName);
		      			i=0;
		      			for(;i<length;i++){
		      				if(data[i].spaceName == spaceName){
		      					if(data[i].authType == 'SpaceManager'){
		      						spaceAuthFlag = true;
		      						console.log('spaceAuthFlag = '+spaceAuthFlag);
		      					}
		      				}
	      				}
	      				if(orgAuthFlag == true){
	      					me.items[1].iconCls = '';
							me.items[3].iconCls = '';
	      				}
	      				
	      				if(spaceAuthFlag){
	      					me.items[1].iconCls = '';
	      				}
	      			},
	      			failure : function(resp){
	      				console.log(resp);
	      				console.log("resp failure");
	      			}
	      		})
	      	}
//	      	Ext.Ajax.request({
//	      		url : 'rest/user/userOrgRole',
//	      		method : 'GET',
//	      		async : false,
//	      		params : {
//	      			orgName : orgName
//	      		},
//	      		success : function(resp){
//	      			var data=Ext.decode(resp.responseText).data;
//	      			var length=data.length;
//	      			console.log(length);
//	      			console.log(data);
//	      			var i=0;
//	      			for(;i<length;i++){
//	      				if(data[i].spaceName == null){
//	      					if(data[i].authType == 'OrgManager'){
//	      						orgAuthFlag = true;
//	      					}
//	      				}
//	      			}
//	      			console.log("orgAuthFlag = "+orgAuthFlag);
//	      			if(orgAuthFlag == true){
//	      				
//	      				me.items[0].iconCls = '';
//	      				me.items[1].iconCls = '';
//	      				me.items[2].iconCls = '';
//	      				button.setVisible(true);
//	      			}
//	      			var store = windowPanel.down('grid[itemId=spaceManagePanel]').getStore();
//	      			var record = store.getAt(row);
//	      			console.log(record);
//	      			var spaceName = record.data.name;
//	      			console.log(spaceName);
//	      			i=0;
//	      			for(;i<length;i++){
//	      				if(data[i].spaceName == spaceName){
//	      					if(data[i].authType == 'SpaceManager'){
//	      						spaceAuthFlag = true;
//	      						console.log('spaceAuthFlag = '+spaceAuthFlag);
//	      					}
//	      				}
//	      			}
//	      			if(orgAuthFlag == false &&spaceAuthFlag == true){
//	      				me.items[1].iconCls = '';
//	      			}
//	      		},
//	      		failure : function(resp){
//	      			console.log(resp);
//	      			console.log("resp failure");
//	      		}
//	      	})
//	      	
//	      	if(row==0){
//	      		console.log("row=0");
//	      		console.log("编辑");
//	      		this.items[0].iconCls = 'dis_n';
//	      		this.items[1].iconCls = 'dis_n';
//	      	}
//	      	else if(row==1){
//	      		this.items[0].iconCls = '';
//	      	}
//	      	else if(row==2){
//	      		this.items[1].iconCls = '';
//	      	}
	      },
	      
	      items : [{
	      	icon : 'resources/images/detail.png',
	      	tooltip : cfWebLang.Util.Detail,
	      	handler : function(){
	      		alert("you have click the detail button");
	      	}
	      },{
//	      	icon : 'resources/images/icon_edit.png',
	        tooltip : cfWebLang.Util.Edit,
	        xtype : 'actioncolumn',
	        itemId : 'spaceEdit',
	        handler : function(me,rindex){
	        	var routePanel = me.up('SettingWindow').down('routeManageView');
	        	var window = Ext.create('cfWeb.view.accountSetting.SpaceEditWindow',{
	        		rindexFlag : rindex,
	        		component : me
	        	});
	        	window.routePanel = routePanel;
	        	window.down('form').loadRecord(me.getStore().getAt(rindex));
	        	window.show();
	        }
//	         handler : function(me,rindex){
//	         	var existFlag = false;
//	         	while(existFlag == true){
//	         		var msg = Ext.Msg.prompt(cfWebLang.SettingWindow.ChangeSpaceName, cfWebLang.SettingWindow.InputNewSpaceName, function(btn, text){
//				    if (btn == 'ok'&&text){
//				    	var store=me.getStore();
//				    	var record=store.getAt(rindex);
//				    	var spaceName=record.data.name;
//				    	var newName=text;
//				    	console.log(record.data.name);
//				    	console.log(text);
//				    	console.log(me);
//				    	var window=me.up('panel[itemId=spaceManageWindow]');
//				    	var orgName = window.down('combobox').getValue();
//				    	console.log(orgName);
//				    	Ext.Ajax.request({
//				    		url : 'rest/space/rename',
//				    		method : 'POST',
//				    		params :{
//				    			orgName : orgName,
//				    			spaceName : spaceName,
//				    			newName : newName
//				    		},
//				    		success : function(resp){
//				    			console.log("rename success");
//				    			
//				    			var nameExistFlag = false;
//				    			var i=0;
//				    			var length=store.data.length;
//				    			console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!");
//				    			console.log(store.data.length);
//				    			console.log("!!!!!!!!!!!!!!!!!!!!!!");
//				    			for(i=0;i<length;i++){
//				    				if(i!= rindex){
//				    					if(text == store.getAt(i).data.name){
//				    						nameExistFlag = true;
//				    					}
//				    				}
//				    			}
//				    			if(nameExistFlag == true){
//				    				Ext.MessageBox.alert(cfWebLang.Util.Tip,cfWebLang.Bug.SpaceNameExisted);
//				    			}else{
//				    			}
//				    			
//				    			
//				    			
//				    			store.load({
//				    				url : 'rest/space/list',
//				    				params :{
//				    					orgName : orgName
//				    				}
//				    			});
//				    			var curSpace=Ext.fly('selectSpace');
//				    			console.log(curSpace.getValue()+'  '+spaceName+'  '+newName);
//				    			if(curSpace.getValue().trim()==spaceName.trim()){
//				    				var obj=document.getElementById("selectSpace");
//				    				obj.options[obj.selectedIndex].innerHTML=newName;
//				    				obj.options[obj.selectedIndex].value=newName;
//				    				Ext.getCmp('navView').spaceOnChange();
//				    			}
//				    		},
//				    		failure : function(resp){
//				    			console.log("rename failure");
//				    		}
//				    	})
//				    }
//				});
//	        	msg.addCls("prompt");
//	         	}
//	        }
	      },{
	      	icon : 'resources/images/user.png',
	      	tooltip : cfWebLang.Util.UserAuth,
	      	handler : function(){
	      		alert("you have click the Auth button");
	      	}
	      },{
//	      	icon : 'resources/images/icon_delete.png',
	      	tooltip : cfWebLang.Util.Delete,
	      	handler : function(me,rindex){
	      		Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){  
	                if(btn == 'yes') { 
	                	var routeView = me.up('SettingWindow').down('routeManageView');
	                	var routeOrg = routeView.down('combobox[itemId=org]').getValue();
	                	var routeSpace = routeView.down('combobox[itemId=space]').getValue();
	                	var routeSpaceStore = routeView.down('combobox[itemId=space]').getStore();
	                	var store=me.getStore();
	                	console.log(store);
	                	var record=store.getAt(rindex);
	                	console.log(record.data);
	                	var spaceName = record.data.name;
	                	console.log(spaceName);
	                	var windowPanel=me.up('panel').up('panel');
	                	console.log(windowPanel);
	                	var orgName=windowPanel.down('combobox').getValue();
	                	console.log(orgName);
	                	store.remove(record);
	                	Ext.Ajax.request({
	                		url : 'rest/space/delete',
	                		method : 'GET',
	                		params :{
	                			orgName : orgName,
	                			spaceName : spaceName
	                		},
	                		success : function(resp){
	                			var response=Ext.decode(resp.responseText);                			
	                			var flag =response.success;
	                			currentORG=response.data[0];
	  						  	currentSpace=response.data[1];
	                			if(flag == true){
	                				routeSpaceStore.reload();
	                				if(orgName==routeOrg&&routeSpace==spaceName){
	                					if(routeSpaceStore.data.length>0){
	                						routeView.down('combobox[itemId=space]').setValue(routeSpaceStore.data.items[0].data.name);
	                					}else{
	                						routeView.down('combobox[itemId=space]').setValue('');
	                					}
	                				}
	                				var curSpace=Ext.fly('selectSpace');
	                			    var curOrg=Ext.fly('selectOrg');
				    			    console.log(curSpace.getValue()+'  '+spaceName);
				    			    if(curSpace.getValue().trim()==spaceName.trim()){
				    			    	var obj=document.getElementById("selectSpace");
				    				    obj.options.remove(obj.selectedIndex);
				    			    	Ext.getCmp('navView').spaceOnChange();
				    			    }
				    			    if(curOrg.getValue().trim()==orgName.trim()){
				    			    	var obj=document.getElementById("selectSpace");
				    		            for(var i=0;i<obj.length;i++){
				    		            	if(spaceName.trim()==obj.options[i].value){
				    		             		obj.options.remove(i);
				    		            	}
				    		            }
				    		    	}
	                			}
	                		},
	                		failure : function(resp){
	                			var error = Ext.JSON.decode(resp.responseText);
				                if(error.data!=null&&error.data.source==1){
					                    Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				                }else{
					                    Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
				                }
	                		}
	                	});
	                }
	      		});
	       	}
	      }],
	      flex : 2
	   }
//	   	{
//	   	  header : '详情',
//	   	  icon : 'resources/images/detail.png',
//	      tooltip : '详情',
//	      align : 'center',
//	      xtype : 'actioncolumn',
//	      flex : 1
//	   },{
//	   	  header : '删除',
//	   	  icon : 'resources/helpImage/deleteuser.png',
//	      tooltip : '删除',
//	      align : 'center',
//	      xtype : 'actioncolumn',
//	      flex :1,
//	      handler : function(me,rindex){
//	      		var store=me.getStore();
//	      		console.log(store);
//	      		var record=store.getAt(rindex);
//	      		console.log(record.data);
//	      		var spaceName = record.data.name;
//	      		console.log(spaceName);
//	      		var windowPanel=me.up('panel').up('panel');
//	      		console.log(windowPanel);
//	      		var orgName=windowPanel.down('combobox').getValue();
//	      		console.log(orgName);
//	      		store.remove(record);
//	      		Ext.Ajax.request({
//	      			url : 'rest/space/delete',
//	      			method : 'GET',
//	      			params :{
//	      				orgName : orgName,
//	      				spaceName : spaceName
//	      			},
//	      			success : function(resp){
//	      				console.log(resp);
//	      				console.log("success");
//	      			},
//	      			failure : function(resp){
//	      				console.log(resp);
//	      				console.log("failure");
//	      			}
//	      		})
//	       	}
//	   }
	]
	},
	{
		xtype : 'button',
		hidden : true,
		cls:'style-addButton icon-plus22',
		itemId : 'addSpaceUser',
		buttonAlign : 'right',
		style :{
			'float':'right',
			'margin-right':'4px',
			'line-height':'10px!important',
			'padding-left':'2px',
			//'margin-top':'-28px'
		}
	}
	],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});
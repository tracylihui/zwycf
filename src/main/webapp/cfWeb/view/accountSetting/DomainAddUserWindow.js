Ext.define('cfWeb.view.accountSetting.DomainAddUserWindow', {
	alias : 'widget.domainAddUserWindow',
	extend : 'Ext.window.Window',
	title : cfWebLang.SettingWindow.NewDomain,
	modal : true,
	height : 200,
	header:{
		height:27
	},
	cls : 'style-cfWindow domain-window',
	width : 400,
	items : [ {
		xtype : 'form',
		cls:'window-popup',
		height:110,
		items:[{
			margin:'15 0 0 0',
			layout : 'column',
    		items:[{
    		xtype : 'textfield',
			fieldLabel : cfWebLang.SettingWindow.InputDomainName,
			itemId : 'inputDomainName',
			labelWidth:125,
			labelAlign : 'right',
			width:265,
			listeners:{
				blur : function(me){
					var Domain=me.getValue().toLowerCase();
					var domainTip = me.up('domainAddUserWindow').down('displayfield[itemId=errorDomainTip]');
					if(Domain==''){
						domainTip.setValue(cfWebLang.SettingWindow.DomainEmp);
						domainTip.setVisible(true);
					}
				},
    			focus : function(me){
    				var tip=me.up('domainAddUserWindow').down('displayfield[itemId=errorDomainTip]');
    				tip.setVisible(false);
    			}
			}
    		},{
    			xtype : 'displayfield',
    			itemId : 'errorDomainTip',
    			value :cfWebLang.SettingWindow.DomainEmp,
    			fieldStyle : {
    					'margin-left' : '5px',
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:120,
    			hidden : true
    		}]
		},{
			margin:'5 0 0 0',
		    xtype : 'combobox',
			fieldLabel :cfWebLang.SettingWindow.DomainProperty,
			width:265,
			labelWidth : 125,
			labelAlign : 'right',
			editable : false,
			queryMode : 'local',
			itemId : 'property',
			emptyText : "lala"// cfWebLang.SettingWindow.DomainProperty,
		},{
			margin:'5 0 0 0',
		    xtype : 'displayfield',
			fieldLabel : cfWebLang.SettingWindow.DomainInOrg,
			itemId : 'whichOrg',
			labelWidth : 125,
			width:265,
			labelAlign : 'right',
			readOnly: true
		}]
	} ],
	buttons : [ {
		margin:'0 50 0 0',
		text : cfWebLang.Util.Confirm,
		itemId : 'add'
	}, {
		margin:'0 90 0 0',
		text : cfWebLang.Util.Cancel,
		itemId : 'cancel'
	} ],

	initComponent : function() {
		this.callParent(arguments);
	}
});
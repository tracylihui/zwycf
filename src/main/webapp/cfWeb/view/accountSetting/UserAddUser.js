Ext.define('cfWeb.view.accountSetting.UserAddUser',{
	alias : 'widget.userAddUser',
	extend : 'Ext.window.Window',
	modal : true,
	title : cfWebLang.SettingWindow.NewUser,
	height : 200,
	header:{
		height:27
	},
	cls: 'style-cfWindow',
	width : 400,
	items :[{
		xtype : 'form',
		cls: 'window-popup',
		items :[{
			margin:'15 0 0 0',
    		layout : 'column',
    		items:[{
    		xtype : 'textfield',
			fieldLabel : cfWebLang.SettingWindow.InputUserName,
			itemId : 'inputUserName',
			labelWidth:120,
			labelAlign : 'right',
			width:270,
			listeners : {
    			blur : function(me){
					var userName=me.getValue().toLowerCase();
					var errTip = me.up('userAddUser').down('displayfield[itemId=errorUserTip]');
					if(userName==''){
						errTip.setValue(cfWebLang.SettingWindow.UserNameEmp);
						errTip.setVisible(true);
					}else{
						Ext.Ajax.request({
							url:'rest/user/checkUserName',
							method:'GET',
							params:{
								userName:userName
							},
							success:function(res){
								var obj = Ext.JSON.decode(res.responseText);
								if(obj.data==false){
									errTip.setValue(cfWebLang.SettingWindow.UserNameExit);
									errTip.setVisible(true);
								}
							},
							failure : function(){
								Ext.Msg.alert(cfWebLang.Util.Error,cfWebLang.CfWindow.InputAgain);
							}
						});
					}
    			},
    			focus : function(me){
    				var tip=me.up('userAddUser').down('displayfield[itemId=errorUserTip]');
    				tip.setVisible(false);
    			}
			}
    		},{
    			xtype : 'displayfield',
    			itemId : 'errorUserTip',
    			value :cfWebLang.SettingWindow.UserNameEmp,
    			fieldStyle : {
    					'margin-left' : '5px',
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:120,
    			hidden : true
    		}]
		},{
			margin:'5 0 0 0',
			xtype : 'textfield',
			fieldLabel : cfWebLang.SettingWindow.InputPassword,
			itemId : 'inputPassword',
			labelWidth:120,
			labelAlign : 'right',
			inputType : 'password',
			width:270
		},{
			margin:'5 0 0 0',
			layout : 'column',
			items:[{
			    xtype : 'textfield',
			    fieldLabel : cfWebLang.SettingWindow.ConfirmPassword,
			    itemId : 'confirmPassword',
			    labelAlign : 'right',
			    labelWidth:120,
			    inputType : 'password',
			    width:270,
			    listeners :{
				    blur:function(me){
				    	var pw = me.up('form').down('textfield[itemId=inputPassword]').getValue();
					    var comfirmPass = me.getValue();
					    var tip = me.up('form').down('displayfield[itemId=errorPasswordTip]');
					    if(pw!= comfirmPass){
					    	tip.setVisible(true);
					    }
				    },
				    focus : function(me){
    				    var tip = me.up('form').down('displayfield[itemId=errorPasswordTip]');
    				    tip.setVisible(false);
    			    }
			    }
			},{
			    xtype : 'displayfield',
    			itemId : 'errorPasswordTip',
    			value :cfWebLang.SettingWindow.PasswordComfirm,
    			fieldStyle : {
    					'margin-left' : '5px',
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:120,
    			hidden : true
			}]
			
		},{
			xtype : 'fieldset',
			border : 0,
			margin:'20 0 0 65',
			items :[
			{
				xtype : 'button',
				text : cfWebLang.Util.Confirm,
				itemId : 'add'
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
			}]
		}]
	}
	],

	
	initComponent : function(){
		this.callParent(arguments);
	}
});
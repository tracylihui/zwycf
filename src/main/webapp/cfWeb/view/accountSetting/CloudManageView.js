Ext.define('cfWeb.view.accountSetting.CloudManageView',{
	alias : 'widget.cloudManageView',
	extend : 'Ext.panel.Panel',
	items:[{
		xtype : 'textfield',
		name : 'cloudName',
		allowBlank : false,
		emptyText : cfWebLang.SettingWindow.CloudName ,
		blankText : cfWebLang.SettingWindow.CloudEmpty,
		msgTarget : 'side'
	},{
		xtype : 'textfield',
		name : 'email',
		allowBlank : false,
		emptyText : cfWebLang.SettingWindow.EmailName,
		blankText : cfWebLang.SettingWindow.EmailEmpty ,
		msgTarget : 'side'
	},{
		xtype : 'textfield',
		name : 'website',
		allowBlank : false,
		emptyText : cfWebLang.SettingWindow.WebsiteName,
		blankText :cfWebLang.SettingWindow.WebsiteEmpty,
		msgTarget : 'side'
	},{
		xtype : 'textfield',
		name : 'password',
		allowBlank : false,
		emptyText : cfWebLang.SettingWindow.Password ,
		inputType : 'password',
		blankText :cfWebLang.SettingWindow.PasswordEmpty ,
		msgTarget : 'side'
	}],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});
Ext.define('cfWeb.view.accountSetting.OrgAddUserWindow',{
	alias : 'widget.orgAddUserWindow',
	extend : 'Ext.window.Window',
	title : cfWebLang.SettingWindow.NewOrg,
	modal : true,
	edit:false,
	height : 200,
	header:{
		height:27
	},
	cls: 'style-cfWindow',
	width : 450,
	items :[{
		xtype : 'form',
		cls:'window-popup',
		items :[{
			xtype : 'fieldset',
			layout : 'column',
			border : 0,
			items :[{
				xtype : 'textfield',
				fieldLabel : cfWebLang.SettingWindow.OrgName,
				itemId : 'inputOrgName',
				labelWidth : 150,
				labelAlign:'right',
				width : 250,
				margin:'20 0 0 0',
				listeners :{
					blur : function(me){
						var orgName = me.getValue().toLowerCase();
						var errorTip = me.up('orgAddUserWindow').down('displayfield[itemId=orgExist]');
						if(orgName == ''){
							errorTip.setValue(cfWebLang.SettingWindow.OrgEmp);
							errorTip.setVisible(true);
						}
					},
					focus : function(me){
						var errorTip = me.up('orgAddUserWindow').down('displayfield[itemId=orgExist]');
						errorTip.setVisible(false);
						me.setValue('');
					}
				}
			},{
				xtype : 'displayfield',
				value : cfWebLang.Bug.OrgNameExisted,
				itemId : 'orgExist',
				fieldStyle : {
    					'margin-left' : '5px',
    					'margin-top' : '25px',
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:130,
    			hidden : true
			}]
		},{
			xtype : 'combobox',
			labelWidth : 150,
			store : Ext.create('cfWeb.store.accountSetting.QuotaStore'),
			displayField : 'name',
			valueField : 'guid',
			queryMode:'local',
			fieldLabel : cfWebLang.SettingWindow.QuotaSetting,
			labelAlign : 'right',
			width : 250,
			margin:'20 0 0 10',
			name:'quota'
		},{
			xtype : 'fieldset',
			border : 0,
			margin:'30 0 0 70',
			items :[
			{
				xtype : 'button',
				text : cfWebLang.Util.Confirm,
				itemId : 'add'
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
			}]
		}]
	}
	],

	
	initComponent : function(){
		this.callParent(arguments);
	}
});

Ext.define('cfWeb.view.accountSetting.QuotaManageView',{
	alias : 'widget.quotaManageView',
	extend : 'Ext.panel.Panel',
	cls:"account-setting",
	overflowX:'hidden',
	overflowY:'auto',
	items :[{
		xtype : 'grid',
		itemId:'quotaGrid',
		store :Ext.create('cfWeb.store.accountSetting.QuotaStore'),
		columns :[
		{header : cfWebLang.Util.Name,dataIndex : 'name',align : 'center',flex :0.8},
		{header : cfWebLang.QuotaManage.services,dataIndex : 'services',align : 'center',flex : 1},
		{header : cfWebLang.QuotaManage.routes,dataIndex : 'routes',align : 'center',flex : 1},
		{header : cfWebLang.QuotaManage.memory,dataIndex : 'memeory',align : 'center',flex : 1,renderer:function(value){
			if(value==null||value==""){
				return value;
			}
			if(value<1024){
				return value+"M";
			}else{
				return Math.round(value/1024)+"G";
			}
			
		}},
		/*{header : cfWebLang.QuotaManage.instancesmemory,dataIndex : 'instancememeory',align : 'center',flex : 1.8,renderer:function(value){
			if(value==null||value==""){
				return value;
			}
			if(value<1024){
				return value+"M";
			}else{
				return Math.round(value/1024)+"G";
			}
		}},*/
		{
		  header : cfWebLang.Util.Operate,
		  menuText:cfWebLang.Util.Operate,
	      xtype : 'actioncolumn',
	      align : 'center',
	      renderer : function(me,classes,data,row,col){
	      	var me=this;
	        me.items[0].iconCls = 'dis_n';
	      	me.items[1].iconCls = 'dis_n';
			var windowPanel = me.up('quotaManageView');
			var button = windowPanel.down('button[itemId=addQuota]');
	        if(isAdmin==1){
				me.items[0].iconCls = '';
				me.items[1].iconCls = '';
				button.setVisible(true);
			}
	      },
	      flex : 1,
	      items : [ {
	      	xtype : 'actioncolumn',
	      	cls:'edit',
	        tooltip : cfWebLang.Util.Edit,
	        iconCls:'editQuota',
	        handler:function(grid,index){
	        	var store=grid.getStore();
	      		var record=store.getAt(index);
	      		var win=Ext.widget('quotaEditWindow');
	      		win.down('form').loadRecord(record);
	      		win.title=cfWebLang.QuotaManage.editTitle;
	      		win.down('button[action=addQuota]').action='editQuota';
	      		win.store=store;
	      		win.show();
	        }
	      },{
	    	  header :cfWebLang.Util.Delete,
	    	  xtype : 'actioncolumn',
	      	  tooltip :cfWebLang.Util.Delete,
	      	  handler:function(grid,index){
	      		  var store=grid.getStore();
	      		  var record=store.getAt(index);
	      		Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){  
	                if(btn == 'yes') {
	                	Ext.Ajax.request({
		                	url : 'rest/quota/delete',
		                	method : 'POST',
		                	params :{
		                		guid : record.get('guid')
		                	},
		                	success:function(){
		                		store.reload();
		                	}
	                	});
	                }
	      		});
	      	  }
	      	  
	   }]
	  }
	]
	},{
		xtype : 'button',
		cls:'style-addButton icon-plus22',
		itemId:'addQuota',
		buttonAlign : 'right',
		hidden:true,
		style :{
			'float':'right',
			'margin-right':'4px',
			'line-height':'10px!important',
			'padding-left':'2px',
			//'margin-top':'-28px'
		}
	}
	],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});

Ext.define('cfWeb.view.accountSetting.SpaceEditWindow',{
	alias : 'widget.spaceEditWindow',
	extend : 'Ext.window.Window',
	title : cfWebLang.SettingWindow.ChangeSpaceName,
	modal : true,
	height : 160,
	header:{
		height:27
	},
	cls: 'style-cfWindow',
	width : 450,
	items :[{
		xtype : 'form',
		cls: 'window-popup',
		items :[{
			xtype : 'fieldset',
			border : 0,
			layout : 'column',
			items :[{
				xtype : 'textfield',
				margin:'20 0 0 0',
				fieldLabel : cfWebLang.SettingWindow.SpaceName,
				itemId : 'inputSpaceName',
				name:'name',
				labelWidth : 150,
				labelAlign:'right',
				width : 250,
				listeners : {
					blur : function(me){
						var spaceName = me.getValue().toLowerCase();
						var errorTip = me.up('spaceEditWindow').down('displayfield[itemId=spaceExist]');
						if(spaceName == ''){
							errorTip.setValue(cfWebLang.SettingWindow.SpaceEmp);
							errorTip.setVisible(true);
						}
					},
					focus : function(me){
						var window = me.up('window');
						var errorTip = window.down('displayfield');
						errorTip.setVisible(false);
						me.setValue('');
					}
				}
			},{
				xtype : 'displayfield',
				value : cfWebLang.SettingWindow.SpaceEmp,
				itemId : 'spaceExist',
				fieldStyle : {
    					'margin-left' : '5px',
    					'margin-top' : '25px',
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:130,
    			hidden : true
			}]
		},{
			xtype : 'fieldset',
			border : 0,
			margin:'30 0 0 75',
			items :[
			{
				xtype : 'button',
				text : cfWebLang.Util.Confirm,
				itemId : 'confirm'
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
			}]
		}]
	}
	],

	
	initComponent : function(){
		this.callParent(arguments);
	}
});
Ext.define('cfWeb.view.accountSetting.SettingWindow',{
	alias : 'widget.SettingWindow',
	extend : 'cfWeb.cfWebComponent.CfWindow',
	title : cfWebLang.Util.Setting,
	layout : 'fit',
	height:350,
	width:650,
	items:[{
		xtype : 'cfPanelTypeII',
		items :[{
			xtitle : cfWebLang.SettingWindow.OrgManage,
			xtype : 'orgManageView'
		},{
			xtitle : cfWebLang.SettingWindow.SpaceManage,
			xtype : 'spaceManageView'
		},{
			xtitle : cfWebLang.SettingWindow.DomainManage,
			xtype : 'domainManageView'
		},{
			xtitle : cfWebLang.SettingWindow.RouteManage,
			xtype : 'routeManageView'
		},{
			xtitle : cfWebLang.SettingWindow.QuotaManage,
			xtype : 'quotaManageView'
		},/*{

			xtitle : cfWebLang.SettingWindow.BuildpackManage,
			xtype : 'BuildpackManageView'
		},*/{
			xtitle : cfWebLang.SettingWindow.UserManage,
			xtype : 'userManageView'
		}]
	}],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});

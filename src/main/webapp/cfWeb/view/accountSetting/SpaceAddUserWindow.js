Ext.define('cfWeb.view.accountSetting.SpaceAddUserWindow',{
	alias : 'widget.spaceAddUserWindow',
	extend : 'Ext.window.Window',
	title : cfWebLang.SettingWindow.NewSpace,
	modal : true,
	height : 170,
	width : 450,
	header:{
		height:27
	},
	cls: 'style-cfWindow',
	items :[{
		xtype : 'form',
		cls: 'window-popup',
		items :[{
			xtype : 'fieldset',
			border : 0,
			layout : 'column',
			items :[{
				xtype : 'textfield',
				fieldLabel : cfWebLang.SettingWindow.SpaceName,
				itemId : 'inputSpaceName',
				labelWidth : 150,
				labelAlign:'right',
				margin:'20 0 0 0',
				width : 250,
				listeners : {
					blur : function(me){
						var spaceName = me.getValue().toLowerCase();
						var errorTip = me.up('spaceAddUserWindow').down('displayfield[itemId=spaceExist]');
						if(spaceName == ''){
							errorTip.setValue(cfWebLang.SettingWindow.SpaceEmp);
							errorTip.setVisible(true);
						}
					},
					focus : function(me){
						var window = me.up('window');
						var errorTip = window.down('displayfield');
						errorTip.setVisible(false);
						me.setValue('');
					}
				}
			},{
				xtype : 'displayfield',
				value : cfWebLang.SettingWindow.SpaceEmp,
				itemId : 'spaceExist',
				fieldStyle : {
    					'margin-left' : '5px',
    					'margin-top' : '25px',
    					'color' : "red"
    			},
    			labelStyle:"font-size:'24px'",
    			width:130,
    			hidden : true
			}]
		},{
			xtype : 'fieldset',
			border : 0,
			margin:'30 0 0 80',
			items :[
			{
				xtype : 'button',
				text :cfWebLang.Util.Confirm,
				itemId : 'add'
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				itemId : 'cancel'
			}]
		}]
	}
	],

	
	initComponent : function(){
		this.callParent(arguments);
	}
});
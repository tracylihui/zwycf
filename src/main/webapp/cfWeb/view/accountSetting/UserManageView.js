Ext.define('cfWeb.view.accountSetting.UserManageView',{
	alias : 'widget.userManageView',
	extend : 'Ext.panel.Panel',
	view : ['accountSetting.AuthOrgUserWindow'],
	cls:"account-setting",
	overflowX:'hidden',
	overflowY:'auto',
	items:[
			{
			xtype : 'grid',
			itemId : 'userManagePanel',
			store : Ext.create('cfWeb.store.accountSetting.UserStore'),
			columns :[{header : cfWebLang.Util.Id,dataIndex : 'id',align : 'center',hidden:true,flex :1},
			{header :cfWebLang.Util.UserName,dataIndex : 'userName',align : 'center',flex :1,
			 field :{
			 	xtype : 'textfield'
			 }
			},
			{header : cfWebLang.Util.Password,dataIndex : 'userPass',align : 'center',flex : 1,
		     field :{
		     	xtype : 'textfield',
		     	inputType : 'password'
		     },
		     renderer : function(value){
		     	return "******";
		     }
			},
			{
			  header :cfWebLang.Util.Operate,
			  menuText:cfWebLang.Util.Operate,
		      xtype : 'actioncolumn',
		      align : 'center',
		      items : [{
	      	icon : 'resources/img/authorize.png',
	      	tooltip : cfWebLang.Util.UserAuth,
	      	iconCls:'auth_user_selector'
	        },{
//		      	icon : 'resources/images/icon_delete.png',
		      	tooltip : cfWebLang.Util.Delete,
		      	handler : function(me,rindex){
		      		Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){  
		                if(btn == 'yes') { 
		                	var store=me.getStore();
		                	console.log(store);
		                	console.log(rindex);
		                	var record=store.getAt(rindex);
		                	console.log(record);
		                	store.remove(record);
		                	var userName=record.data.userName;
		                	console.log(userName);
		                	Ext.Ajax.request({
		                		url : 'rest/user/delete',
		                		method : 'GET',
		                		params :{
		                			userName : userName
		                		},
		                		success : function(resp){
		                			console.log(resp);
		                			console.log("delete success");
		                		},
		                		failure : function(response){
		                			var error = Ext.JSON.decode(response.responseText);
				                    if(error.data!=null&&error.data.source==1){
					                    Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				                    }else{
					                    Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
				                    }	
		                		}
		                	});
		                }
		      		}); 
		       	}
		      }],
		      flex : 1
		   }
			
		]
		},
		{
			xtype : 'button',
			cls:'style-addButton icon-plus22',
			itemId : 'addUser',
			buttonAlign : 'right',
			style :{
				'float':'right',
				'margin-right':'4px',
				'line-height':'10px!important',
				'padding-left':'2px',
				//'margin-top':'-28px'
			}
		}
		
	],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});
Ext.define('cfWeb.view.accountSetting.AccountSettingWindow',{
	alias : 'widget.accountSettingWindow',
	extend : 'Ext.window.Window',
	title : cfWebLang.Util.Setting,
	layout : 'fit',
	height:350,
	width:550,
	items:[{
		xtype : 'tabpanel',
		layout : 'fit',
		items :[{
//			title : 'Organizations管理',
			title: cfWebLang.SettingWindow.OrgManage,
			xtype : 'orgManageView'
		},{
			title : 'space管理',
			xtype : 'spaceManageView'
		},{
			title : 'domain管理',
			xtype : 'domainManageView'
		},{
			title : 'route管理',
			xtype : 'routeManageView'
		}]
	}],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});
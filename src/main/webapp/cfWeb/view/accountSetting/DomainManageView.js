Ext.define('cfWeb.view.accountSetting.DomainManageView',{
	alias : 'widget.domainManageView',
	extend : 'Ext.panel.Panel',
	itemId : 'domainManageWindow',
	autoScroll : true,
	items :[{
		xtype : 'combobox',
		labelWidth:60,
		labelAlign:'right',
		fieldLabel:cfWebLang.SettingWindow.OrgList,
		editable : false,
		margin:'10 0 10 10',
		labelStyle:'color:#8d8d8d',
		itemId :'org',
		cls:'style-cfCombo',
		emptyText : cfWebLang.SettingWindow.OrgChoose,
		displayField : 'name',
		store : 'accountSetting.OrgStore'
	},{
		xtype : 'grid',
		cls:"account-setting",
		itemId : 'domainManagePanel',
		hidden: true,
		plugins: [
		  		Ext.create('Ext.grid.plugin.CellEditing', {
		  			clicksToEdit: 1
		  		})
		  	    ],
		store : Ext.create('cfWeb.store.accountSetting.DomainStore'),
		columns :[
		{header : cfWebLang.Util.Name,dataIndex : 'name',align : 'center',flex :1},
		{header :cfWebLang.Util.Status,dataIndex : 'status',align : 'center',flex : 2},
		{
		  header : cfWebLang.Util.Operate ,
		  menuText:cfWebLang.Util.Operate,
	      xtype : 'actioncolumn',
	      align : 'center',
	      renderer : function(me,classes,data,row,col){
		      	var me=this;
		      	me.items[0].iconCls = 'dis_n';
		      	var windowPanel = me.up('panel[itemId=domainManageWindow]');
		      	var grid = windowPanel.down('grid[itemId=domainManagePanel]');
		      	var orgName=windowPanel.down('combobox').getValue();
		      	console.log(orgName);
		      	var button= windowPanel.down('button[itemId=addDomain]');
		      	button.setVisible(false);
		      	var orgAuthFlag = false;
		      	var domainAuthFlag = undefined;
				if(isAdmin==1){
					me.items[0].iconCls = '';
					windowPanel.isAdmin = '1';
					button.setVisible(true);
				}
		      	if(isAdmin == 0){
		      		windowPanel.isAdmin = '0';
		      		
		      		Ext.Ajax.request({
		      			url : 'rest/user/currentuserOrgRole',
		      			method : 'GET',
		      			async : false,
		      			params :{
		      				orgName : orgName
		      			},
		      			success : function(resp){
		      				var data=Ext.decode(resp.responseText).data;
		      				var length=data.length;
		      				var record =grid.getStore().getAt(row);
		      		        var status = record.data.status;
		      				var i=0;
		      				for(;i<length;i++){
		      						if(data[i].authType == 'OrgManager'){
				      					button.setVisible(true);
				      					if(status=="owned"){
				      						me.items[0].iconCls = '';
				      					}
		      						}
		      				}
		      				console.log("orgAuthFlag = "+orgAuthFlag);
		      			},
		      			failure : function(resp){
		      				console.log(resp);
		      				console.log("resp failure");
		      			}
		      		})
		      	}
	      },
	      items : [ {
//	      	icon : 'resources/images/icon_delete.png',
	      	tooltip :cfWebLang.Util.Delete,
	      	handler : function(me,rindex){
	      		Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){  
	                if(btn == 'yes') { 
	                	var store=me.getStore();
	                	console.log(store);
	                	var record=store.getAt(rindex);
	                	var domainName = record.data.name;
	                	var status=record.data.status;
	                	var windowPanel=me.up('panel').up('panel');
	                	console.log(windowPanel);
	                	var orgName=windowPanel.down('combobox').getValue();
	                	console.log(status);
	                	store.remove(record);
	                	if(status=='shared'){
	                		Ext.Ajax.request({
	                			url : 'rest/domain/delete-shared',
	                			method : 'GET',
	                			params :{
	                				domainName : domainName
	                			},
	                			success : function(resp){
	                				console.log("success");
	                			},
	                			failure : function(resp){
	                				console.log("failure");
	                			}
	                		})
	                	}
	                	if(status=='owned'){
	                		Ext.Ajax.request({
	                			url : 'rest/domain/delete-private',
	                			method : 'GET',
	                			params :{
	                				orgName : orgName ,
	                				domainName : domainName
	                			},
	                			success : function(resp){
	                				console.log("success");
	                			},
	                			failure : function(resp){
	                				var error = Ext.JSON.decode(resp.responseText);
				                    if(error.data!=null&&error.data.source==1){
					                      Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				                    }else{
					                     Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
				                    }
	                			}
	                		});
	                	}
	                }
	      		});
	      }}],
	      flex : 1
	   }]
	},
	{
		xtype : 'button',
		hidden : true,
		cls:'style-addButton icon-plus22',
		itemId : 'addDomain',
		buttonAlign : 'right',
		style :{
			'float':'right',
			'margin-right':'4px',
			'line-height':'10px!important',
			'padding-left':'2px',
			//'margin-top':'-28px'
		}
	}
	],
	initComponent : function(){
		this.callParent(arguments);
	}
});

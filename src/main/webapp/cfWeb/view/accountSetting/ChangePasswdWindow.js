Ext.define('cfWeb.view.accountSetting.ChangePasswdWindow',{
	alias : 'widget.changePasswdWindow',
	extend : 'Ext.window.Window',
	cls: 'style-cfWindow',
	modal : true,
	title : cfWebLang.QuotaManage.changePassword,
	layout : 'fit',
	height:250,
	width:500,
	items:[{
			xtype : 'form',
			cls: 'window-popup',
			border : 0,
			items:[{
				layout:'column',
				margin:'40 0 20 0',
				items:[{
					xtype: 'textfield',
			        name: 'oldPassword',
			        itemId:'oldPassword',
			        labelWidth : 100,
					labelAlign:'right',
					width : 350,
			        fieldLabel: cfWebLang.QuotaManage.oldPassword,
			        inputType : 'password',
				},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.SettingWindow.PasswordEmpty,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		},,{
	    			xtype : 'displayfield',
	    			itemId : 'passwordError',
	    			value :cfWebLang.SettingWindow.PasswordError,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		}]
			},{
				layout:'column',
				margin:'10 0 20 0',
				items:[{
					xtype: 'textfield',
					itemId:'password',
			        name: 'password',
			        labelWidth : 100,
					labelAlign:'right',
					width : 350,
			        fieldLabel: cfWebLang.QuotaManage.newPassword,
			        inputType : 'password',
				},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.SettingWindow.PasswordEmpty,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		}]
			},{
				layout:'column',
				margin:'10 0 20 0',
				items:[{
					xtype: 'textfield',
					itemId:'verifyPassword',
			        name: 'verifyPassword',
			        fieldLabel: cfWebLang.QuotaManage.confirmPassword,
			        inputType : 'password',
			        labelWidth : 100,
					labelAlign:'right',
					msgTarget:'side',
					width : 350
				},{
	    			xtype : 'displayfield',
	    			itemId : 'nullValueTip',
	    			value :cfWebLang.SettingWindow.PasswordEmpty,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
	    		},{
				    xtype : 'displayfield',
	    			itemId : 'confirmPasswordTip',
	    			value : cfWebLang.SettingWindow.PasswordComfirm,
	    			fieldStyle : {
	    					'margin-left' : '5px',
	    					'color' : "red"
	    			},
	    			labelStyle:"font-size:'24px'",
	    			width:120,
	    			hidden : true
				}]
			},{
				xtype : 'fieldset',
				border : 0,
				margin:'20 0 0 80',
				items :[
				{
					xtype : 'button',
					text : cfWebLang.Util.Confirm,
					action : 'changepasswd'
				},{
					xtype : 'button',
					text : cfWebLang.Util.Cancel,
					action : 'close'
				}]
			}]
		}],
	initComponent : function(){
		this.callParent(arguments);
	}
});

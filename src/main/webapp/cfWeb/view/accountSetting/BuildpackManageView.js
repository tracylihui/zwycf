Ext.define('cfWeb.view.accountSetting.BuildpackManageView',{
	alias : 'widget.BuildpackManageView',
	extend : 'Ext.panel.Panel',	
	itemId : 'buildpackManageView',
	cls:"account-setting",
	autoScroll : true,
	items :[{
		xtype : 'grid',
		itemId : 'buildpackManageView',
		hidden: true,
		/*plugins: [
		  		Ext.create('Ext.grid.plugin.CellEditing', {
		  			clicksToEdit: 1
		  		})
		  	    ],*/
		store : Ext.create('cfWeb.store.accountSetting.BuildpackStore'),
		columns :[
		{header : cfWebLang.Util.Name,dataIndex : 'envName',align : 'center',flex :1},
	//	{header : '中文描述',dataIndex : 'envDescChn',align : 'center',flex : 2},
	//	{header : '英文描述',dataIndex : 'envDescEng',align : 'center',flex : 3},
		//{header : '属性',dataIndex : 'name' ,align : 'center',flex :1},
		{
		  header : cfWebLang.Util.Operate ,
	      xtype : 'actioncolumn',
	      align : 'center',
	      items : [ {
//	      	icon : 'resources/images/icon_delete.png',
	      	tooltip : cfWebLang.Util.Delete,
	      	handler : function(me,rindex){
	      		var store=me.getStore();
	      		var record=store.getAt(rindex);
	      		
	      		var msg = Ext.Msg.confirm(cfWebLang.Util.ConfirmWindow,cfWebLang.Util.DeleteConfirm,function(btn){
					if(btn=='yes'){
						Ext.Ajax.request({
			      			url : 'rest/buildpack/delete',
			      			method : 'POST',
			      			params :{
			      				name: record.get('envName')
			      			},
			      			success : function(resp){
			      				store.removeAt(rindex);
			      			},
			      			failure : function(resp){
			      				var error = Ext.JSON.decode(resp.responseText);
				                if(error.data!=null&&error.data.source==1){
					                 Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				                }else{
					                 Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
				                }
			      			}
			      		});
						/*Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.Success);
					}else
						Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.failed);*/
					}
	      		});
//	      		msg.addCls("prompt");
	       	}
	      }],
	      flex : 1
	   }
		
	]
	},
	{
		xtype : 'button',
		cls:'style-addButton icon-plus22',
		itemId : 'addBuildpack',
		name:'uploadFile',
		buttonAlign : 'right',
		style :{
			'float':'right',
			'margin-right':'4px',
			'line-height':'10px!important',
			'padding-left':'2px',
			//'margin-top':'-28px'
		}
	}
	],
	
	initComponent : function(){
		this.callParent(arguments);
	}
});
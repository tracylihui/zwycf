Ext.define('cfWeb.view.log.LogFileWindow', {
	extend : 'Ext.container.Container',
	alias : 'widget.logFileWindow',
//	title : cfWebLang.LogFileWindow.CheckLog,
	/*
	 * defaults: { border: false },
	 */
	modal:false,
	cls : 'style-logFileWindow',
	maskOnDisable :false,
	items : [{
				xtype : 'form',
				layout : 'hbox',
				style : {
					width : '100%'
				},
				//height :autoHeight,
				// cls : 'x-panel-header',
				items : [{
							labelAlign : 'right',
							labelWidth : 95,
							style : {
								clear : 'left'
							},
							margin : '20 0 5 0',
							xtype : "datefield",
							cls:'cf_datefield',
							name : 'from',
							width:'30.5%',
							format : "Y-m-d H:i:s",
							fieldLabel : cfWebLang.LogFileWindow.ChooseDateFrom,
							value : Ext.Date.format(new Date(), 'Y-m-d')
									+ " 00:00:00"
						}, /*{
							xtype : "label",
							text : cfWebLang.Util.To,
							margin : '20 5 0 5'
						},*/ {
							/*labelStyle:'text-align:center;',
							 * labelSeparator:'',
							 * */
							labelWidth : 90,
							fieldLabel : cfWebLang.Util.To,
							labelAlign : 'right',
							itemId : 'to',
							name : 'to',
							xtype : "datefield",
							cls:'cf_datefield',
							margin : '20 0 5 0',
							format : "Y-m-d H:i:s",
							width:'30.5%',
							value : Ext.Date.format(new Date(), 'Y-m-d')
									+ " 23:59:59"
						}, {
							xtype : 'textfield',
							name : 'logKey',
							cls:'cf_textfield',
							fieldLabel : cfWebLang.LogFileWindow.KeyWordSearch,
							emptyText : cfWebLang.LogFileWindow.InputKeyWord,
							labelWidth : 90,
							width:'23%',
							editable : false,
							margin : '20 0 5 0',
							labelAlign : 'right'
						}, {
							xtype : 'button',
							action : 'logfileSearch',
							width : 70,
							height : 25,
							margin : '20 0 0 15',
							text : cfWebLang.LogFileWindow.SearchButton
						},{
							xtype : 'button',
							action : 'back',
							width : 70,
							height : 25,
							margin : '20 0 0 15',
							text : cfWebLang.Util.back
						}]
			}, {
				xtype : 'grid',
				cls:'logFile-grid',
				header : false,
				hideHeaders : false,
				store : 'log.LogFileStore',
				columnLines : true,
				columns : [{
							text : cfWebLang.Util.FileName,
							dataIndex : 'fileName',
							flex : 1.5
						}, {
							text : 'error',
							dataIndex : 'errorCount',
							renderer : function(value) {
								if (value > 0) {
									return '<div class="couunt_head"><div>Error<span class="log_noti_count">'
											+ value + '</span></div></div>';
								} else {
									return '<div class="couunt_head"><div>Error</div></div>';
								}

							},
							flex : 0.3
						}, {
							text : 'exception',
							dataIndex : 'exceptionCount',
							renderer : function(value) {
								if (value > 0) {
									return '<div class="couunt_head"><div>Exception<span class="log_noti_count">'
											+ value + '</span></div></div>';
								} else {
									return '<div class="couunt_head"><div>Exception</div></div>';
								}
							},
							flex : 0.3
						}, {
							header : cfWebLang.Util.Operate,
							menuDisabled : true,
							sortable : false,
							xtype : 'actioncolumn',
							align : 'center',
							items : [{
										iconCls : 'tab-download-logfile-icon',
										tooltip : cfWebLang.Util.Download
									}],
							flex : 0.3
						}]
			}],

	initComponent : function() {
		this.callParent(arguments);
	}
});

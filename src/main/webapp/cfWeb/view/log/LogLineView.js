Ext.define('cfWeb.view.log.LogLineView', {
			extend : 'Ext.chart.Chart',
			alias : 'widget.logLineView',
			margin:'0 0 0 80',
			minHeight : 510,
			minWidth : 500,
			hidden : false,
			maximizable : true,
			autoShow : true,
			xtype : 'chart',
			animate : false,
			shadow : false,
			store : 'cfWeb.store.log.AppLogTermStore',
			height : 500,
			axes : [{
						type : 'Numeric',
						position : 'left',
						fields : ['count'],
						grid : true,
						minimum : 0
					}, {
						type : 'Category',
						position : 'bottom',
						fields : ['name'],
						label : {
							rotate : {
								degrees : 10
							}
						}
					}],
			series : [{
				type : 'line',
				axis : 'left',
				// gutter: 80,
				xField : 'name',
				yField : ['count'],
				tips : {
					trackMouse : true,
					width : 235,
					renderer : function(storeItem, item) {
						this.setTitle(storeItem.get('name')+': '
								+ storeItem.get('count'));
					}
				},
				style : {
					fill : '#38B8BF',
					stroke : '#17a2f7',
					'stroke-width' : 3
				},
				markerConfig : {
					type : 'circle',
					radius : 4,
					fill : '#82dddf',
					//stroke : '#82dddf'
				}
			}],
			initComponent : function() {
				switch(this.type){
				case 'responseTime':this.axes[1].title=this.type+'(ms)';break;
				case 'eventType':this.axes[1].title=this.type;break;
				case 'timestamp':this.axes[1].title=this.type;break;
				case 'serviceName':this.axes[1].title=this.type;break;
				default:this.axes[1].title=this.type;
				}
				this.callParent(arguments);
			}
		});

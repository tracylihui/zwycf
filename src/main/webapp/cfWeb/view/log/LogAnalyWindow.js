Ext.define('cfWeb.view.log.LogAnalyWindow', {
	extend : 'Ext.container.Container',
	store : Ext.create("cfWeb.store.log.AppLogConfigStore"),
	alias : 'widget.logAnalyWindow',
	modal:false,
	title : cfWebLang.LogFileWindow.Log,
	cls : 'style-logFileWindow account-setting',
	items : [{
				layout : {
					type : 'column'
				},
				margin : '20 0 0 0',
				items : [{
					xtype : 'panel',
					cls : 'style-CfPanelTypeI logAnaly-input',
					title : cfWebLang.LogFileWindow.ChartSetting,
					itemId : 'chartConfig',
					defaults:{
						labelAlign:'right'
					},
					layout : {
						type : 'vbox'
					},
					margin : '0 0 0 24',
					columnWidth : 0.49,
					items : [{
						layout : 'column',
						items : [{
							xtype : 'combo',
							margin : '0 0 0 10',
							labelAlign:'right',
							fieldLabel : cfWebLang.LogFileWindow.StatisticType,
							name : 'count_combo',
							itemId : 'count_combo',
							store : Ext.create('Ext.data.Store', {
								fields : ['value', 'text'],
								data : [{
											value : cfWebLang.LogFileWindow.SurveyStatic,
											text : cfWebLang.LogFileWindow.SurveyStatic
										}, {
											value : cfWebLang.LogFileWindow.KeyWordStatic,
											text : cfWebLang.LogFileWindow.KeyWordStatic
										}, {
											value : cfWebLang.LogFileWindow.ChartStatic,
											text : cfWebLang.LogFileWindow.ChartStatic
										}]
							}),
							emptyText : cfWebLang.Util.PleaseChoose,
							valueField : 'value',
							displayField : 'text',
							triggerAction : 'all',
							listeners : {
								blur : function(me) {
									var count = me.getValue();
									if(count==null){
										var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=typeErrorTip]');
									tip.setVisible(true);
									}
								},
								focus : function(me) {
									var tip = me.up('logAnalyWindow')
										  .down('displayfield[itemId=typeErrorTip]');
									tip.setVisible(false);
								}
							}
						}, {
							xtype : 'displayfield',
							itemId : 'typeErrorTip',
							margin : '0 0 0 20',
							value : cfWebLang.LogFileWindow.typeEmp,
							fieldStyle : {
								'color' : "red"
							},
							labelStyle : "font-size:'24px'",
							hidden : true
						}]

					}, {
						xtype : 'textfield',
						margin : '10 0 0 20',
						fieldLabel : cfWebLang.LogFileWindow.ChartTitle,
						itemId : 'chart_title',
						emptyText : cfWebLang.Util.PleaseInput

					}, {
						cls : 'style-cfRadio',
						xtype : 'radiogroup',
						margin : '10 0 10 20',
						fieldLabel : cfWebLang.LogFileWindow.ChartSize,
						itemId : 'chart_span',
						layout : 'hbox',
						columns : 3,
						items : [{
									boxLabel : cfWebLang.Util.Large,
									name : 'span',
									inputValue : '0.87'
								},

								{
									boxLabel : cfWebLang.Util.Medium,
									name : 'span',
									inputValue : '0.87',
									checked : true
								}, {
									boxLabel : cfWebLang.Util.Small,
									name : 'span',
									inputValue : '0.45'

								}]
					}]

				}, {
					xtype : 'panel',
					cls : 'style-CfPanelTypeI logAnaly-input',
					defaults:{
						labelAlign:'right'
					},
					title : cfWebLang.LogFileWindow.ParameterSetting,
					layout : {
						type : 'vbox'
					},
					itemId : 'paraConfig',
					margin : '0 0 0 20',
					columnWidth : 0.49,
					items : [{
						layout : 'column',
						items : [{
							margin : '0 0 0 10',
							labelAlign:'right',
							xtype : 'combo',
							fieldLabel : cfWebLang.LogFileWindow.FieldName,
							name : 'field_combo',
							labelWidth:128,
							itemId : 'field_combo',
							mode : 'local',
							store : 'log.AppLogFieldStore',
							queryMode : 'local',
							emptyText : cfWebLang.Util.PleaseChoose,
							valueField : 'patternField',
							displayField : 'patternField',
							listeners : {
								blur : function(me) {
									if(me.getValue()==null){
										var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=fieldErrorTip]');
									tip.setVisible(true);
									}		
								},
								focus : function(me) {
									var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=fieldErrorTip]');
									tip.setVisible(false);
								}
							}
						}, {
							xtype : 'displayfield',
							itemId : 'fieldErrorTip',
							margin : '0 0 0 20',
							value : cfWebLang.LogFileWindow.fieldEmp,
							fieldStyle : {
								'color' : "red"
							},
							labelStyle : "font-size:'24px'",
							hidden : true
						}]

					}, {
						layout : 'column',
						items : [{
							xtype : 'textfield',
							margin : '0 0 0 10',
							labelWidth:128,
							labelAlign:'right',
							itemId : 'intervalText',
							fieldLabel : cfWebLang.LogFileWindow.IntervalValue,
							emptyText : cfWebLang.LogFileWindow.InputInterval,
							listeners : {
								blur : function(me) {
									if(me.getValue()==''){
										var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=invalErrorTip]');
									tip.setVisible(true);
									}
								},
								focus : function(me) {
									var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=invalErrorTip]');
									tip.setVisible(false);
								}
							}
						}, {
							xtype : 'displayfield',
							itemId : 'invalErrorTip',
							margin : '0 0 0 20',
							value : cfWebLang.LogFileWindow.invalEmp,
							fieldStyle : {
								'color' : "red"
							},
							labelStyle : "font-size:'24px'",
							hidden : true
						}]

					}, {
						xtype : 'numberfield',
						margin : '10 0 10 20',
						labelWidth:128,
						itemId : 'TOP',
						fieldLabel : 'TOP',
						value : 10,
						minValue : 0,
						maxValue : 50
					}]
				}]
			}, {
				layout : {
					type : 'column'
				},
				margin : '20 0 0 0',
				items : [{
					xtype : 'panel',
					cls : 'style-CfPanelTypeI logAnaly-input',
					title : cfWebLang.LogFileWindow.StyleSetting,
					layout : 'vbox',
					margin : '0 0 0 24',
					columnWidth : 0.49,
					itemId : 'styleConfig',
					items : [{
						layout : 'column',
						items : [{
							margin : '0 0 20 10',
							xtype : 'combo',
							fieldLabel : cfWebLang.LogFileWindow.ChartType,
							name : 'type_combo',
							itemId : 'type_combo',
							store : Ext.create('Ext.data.Store', {
								fields : ['value', 'text'],
								data : [{
											value : 'pie',
											text : cfWebLang.LogFileWindow.PieChart
										}, {
											value : 'columnbar',
											text : cfWebLang.LogFileWindow.ColumnbarChart
										}, {
											value : 'line',
											text : cfWebLang.LogFileWindow.LineChart
										}],
								filterOnLoad : true
							}),
							emptyText : cfWebLang.Util.PleaseChoose,
							valueField : 'value',
							displayField : 'text',
							triggerAction : 'all',
							listeners : {
								blur : function(me) {
									if(me.getValue()==null){
										var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=chartErrorTip]');
									tip.setVisible(true);
									}
								},
								focus : function(me) {
									var tip = me
											.up('logAnalyWindow')
											.down('displayfield[itemId=chartErrorTip]');
									tip.setVisible(false);
								}
							}
						}, {
							xtype : 'displayfield',
							itemId : 'chartErrorTip',
							margin : '0 0 0 20',
							value : cfWebLang.LogFileWindow.chartEmp,
							fieldStyle : {
								'color' : "red"
							},
							labelStyle : "font-size:'24px'",
							hidden : true
						}]

					}]

				}, {
					xtype : 'panel',
					cls : 'style-CfPanelTypeI logAnaly-input',
					title : cfWebLang.LogFileWindow.DataFilter,
					layout : 'vbox',
					margin : '0 0 0 20',
					columnWidth : 0.49,
					itemId : 'dataFilter',
					items : [{
								margin : '10 0 20 20',
								xtype : 'textfield',
								labelWidth:128,
								labelAlign:'right',
								fieldLabel : cfWebLang.LogFileWindow.KeyWord,
								width:338,
								itemId : 'termtext',
								emptyText : cfWebLang.LogFileWindow.ProjectCensus
							}]
				}]
			}, {
				xtype : 'toolbar',
				itemId : 'toolButton',
				style : {
					width : '100%',
					'padding-left':'50%'
				},
				layout:'hbox',
				items : [ {
							//margin : '0 0 0 700',
							xtype : 'button',
							text : cfWebLang.LogFileWindow.GenerateChart,
							itemId : 'createChart',
						}, {
							xtype : 'button',
						    //margin:'0 0 0 50',
							text : cfWebLang.LogFileWindow.SaveChart,
						    itemId : 'saveChart',
						},{
							//margin : '0 0 0 50',
							xtype : 'button',
							text : cfWebLang.Util.Cancel,
							itemId : 'cancle',
						},{
							xtype : 'button',
							text : cfWebLang.Util.back,
							itemId : 'back',
						}]
			}, {
				xtype : 'tabpanel',
				cls : 'logAnaly-input',
				itemId : 'showChart',
				hidden : true,
				margin : '40 0 0 0',
				enableTabScroll : true,
				defaults : {
					autoScroll : true
				}
			}],

	initComponent : function() {
		this.callParent(arguments);
	}
});
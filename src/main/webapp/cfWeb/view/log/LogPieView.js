Ext.define('cfWeb.view.log.LogPieView', {
			extend : 'Ext.chart.Chart',
			alias : 'widget.logPieView',
			requires : ['Ext.chart.*', 'Ext.Window', 'Ext.fx.target.Sprite',
					'Ext.layout.container.Fit', 'Ext.window.MessageBox'],
			minHeight : 510,
			minWidth : 500,
			margin:'0 0 0 80',
			hidden : false,
			maximizable : true,
			autoShow : true,
			xtype : 'chart',
			animate : true,
			store : 'cfWeb.store.log.AppLogTermStore',
			shadow : false,
			legend : {
				position : 'right'
			},
			insetPadding : 60,
			series : [{
				type : 'pie',
				field : 'count',
				colorSet:[
				           '#a3c9ce','#bcd3db','#e6eaeb','#eef1f8','#a3d8ea','#78c1e1','#29b2d4','#1299c8','#0188c0','#035e8d','#00addc'],
				showInLegend : true,
				tips : {
					width : 235,
					trackMouse : true,
					renderer : function(me, item) {
						var total = 0;
						me.store.each(function(rec) {
									total += rec.get('count');
								});
						var storeItem = item.storeItem;
						this.setTitle(storeItem.get('name')+'('+storeItem.get('count')+')'
								+ ': '
								+ Math.round(storeItem.get('count') / total
										* 100) + '%');
					}
				},
				highlight : {
					segment : {
						margin : 20
					}
				},
				label : {
					field : 'name',
					display : 'rotate',
					contrast : true,
					font : '14px'
				}
			}],
			initComponent : function() {
				this.callParent(arguments);
			}
		});

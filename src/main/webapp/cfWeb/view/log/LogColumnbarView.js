Ext.define('cfWeb.view.log.LogColumnbarView', {
			alias : 'widget.logColumnbarView',
			extend : 'Ext.chart.Chart',
			margin:'0 0 0 80',
			minHeight : 510,
			minWidth : 500,
			hidden : false,
			maximizable : true,
			autoShow : true,
			xtype : 'chart',
			animate : true,
			shadow : true,
			store : 'cfWeb.store.log.AppLogTermStore',
			height : 500,
			axes : [{
						type : 'Numeric',
						position : 'left',
						fields : ['count'],
						grid : true,
						minimum : 0
					}, {
						type : 'Category',
						position : 'bottom',
						fields : ['name'],
					//	title : 'test'+this.type
							/*{
							renderer:function(){
							var combo=this.up('logFileWindow').down('combobox[itemId=field_combo]');
							alert(combo.getValue());
							switch(combo.getValue()){
							case 'responseTime':return 'responseTime(ms)';break;
							case 'eventType':return 'eventType';break;
							case 'timestamp':return 'timestamp';break;
							case 'serviceName':return 'serviceName';break;
							default: return '';
							}
						}
						}*/
					}],
			series : [{
				type : 'column',
				axis : 'left',
				highlight : true,
				style : {
					fill : '#17a2f7',
					//stroke : '#17a2f7',
					'stroke-width' : 3
				},
				renderer: function(sprite, record, attr, index, store){
                   return Ext.apply(attr, {
                      fill: '#54b1f6'
                   });
                },
				tips : {
					width : 235,
					trackMouse : true,
					renderer : function(storeItem, item) {
						this.setTitle(storeItem.get('name') + ': '
								+ storeItem.get('count'));
					}
				},
				label : {
					display : 'insideEnd',
					'text-anchor' : 'middle',
					field : 'count',
					renderer : Ext.util.Format.numberRenderer('0'),
					orientation : 'horizontal',
					color : '#000'
				},
				xField : 'name',
				yField : 'count'
			}],
			initComponent : function() {
				console.log(this.axes[1]);
				switch(this.type){
				case 'responseTime':this.axes[1].title=this.type+'(ms)';break;
				case 'eventType':this.axes[1].title=this.type;break;
				case 'timestamp':this.axes[1].title=this.typebreak;
				case 'serviceName':this.axes[1].title=this.type;break;
				default: this.axes[1].title=this.type;
				}
				this.callParent(arguments);
			}
		});

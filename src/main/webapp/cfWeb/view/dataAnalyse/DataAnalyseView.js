Ext.define('cfWeb.view.dataAnalyse.DataAnalyseView',{
	extend : 'Ext.container.Container',
	alias : 'widget.dataAnalyseView',
	overflowY : 'auto',
	
	initComponent : function(){
		this.items = [{
			xtype : 'collapsibleBlock',
			title : cfWebLang.DataAnalyseView.dataAnalyse,
			searchable : true,
			items : [{
				xtype : 'lbuttonIconListView',
				isAddable : false,
				buttonMenu : [[
				
				],[
				{
					ico : 'plus22',
					name : cfWebLang.Util.New,
					act : 'new'
				},{
					ico : 'filter',
					name : cfWebLang.Util.Filter,
					act : 'filter'
				}
				]],
				store : Ext.create('cfWeb.store.dataAnalyse.DataAnalyseStore')
				}]
		},
			   {
			   	xtype : 'collapsibleBlock',
			    title : cfWebLang.AppServiceView.AppServiceList,
			    searchable : true,
			    items:[{
			}]
		}];
		this.callParent(arguments);
	}
});
Ext.define('cfWeb.view.app.AppView', {
			extend : 'Ext.container.Container',
			alias : 'widget.appView',
			overflowY : 'auto',
			initComponent : function() {
				this.items = [{
			       xtype : 'collapsibleBlock',
			       searchable : true,
			       items : []
		      }];
				this.callParent(arguments);
			}
		});

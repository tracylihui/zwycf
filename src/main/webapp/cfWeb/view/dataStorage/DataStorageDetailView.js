Ext.define('cfWeb.view.dataStorage.DataStorageDetailView',{
	extend : 'cfWeb.cfWebComponent.SlideLayer',
	alias : 'widget.dataStorageDetailView',	
	resizable: false,
	overflowY:'hidden',
	overflowX:'hidden',
	initComponent : function(){
		this.items = [{
			xtype : 'headView',
			itemId : 'serviceHead',
			detailList:[{
							name:cfWebLang.Service.Hostname,
							dataIndex:'hostname'
						},{
							name:cfWebLang.Service.Port,
							dataIndex:'port'
						},{
							name:cfWebLang.Service.Username,
							dataIndex:'username'
						},{
							name:cfWebLang.Service.Password,
							dataIndex:'password'
						},{
							name:"数据库",
							dataIndex:'databasename'
						},{
							name:''
						},{
							name:cfWebLang.Service.Plan,
							dataIndex:'plan'
						},{
							name:cfWebLang.Service.Label,
							dataIndex:'label'						
						}
						],
			actionList:{'add':cfWebLang.Util.New,
						'delete':cfWebLang.Util.Delete,
						'edit':cfWebLang.Util.Edit,
						'save':cfWebLang.Util.Save,
						'start':cfWebLang.Util.Start,
						'stop':cfWebLang.Util.Stop,
						'restart':cfWebLang.Util.Restart,
						'loading':cfWebLang.Util.Loading}
		}/*,{
			xtype : 'container',
			overflowY:'auto',
			overflowX:'hidden',
			items : []
		}*/];
		this.callParent(arguments);
		this.listeners={
				'boxready' : function(){
					var hHeight = this.items.items[0].getHeight();
					var height = this.getHeight();
					var bHeight = height - hHeight;
					if(this.items.length>1){
						this.items.items[1].setHeight(bHeight);
					}
					this.on('resize',function(me, width, height, eOpts){
						if(this.items.length>1){
							var content = this.items.items[1];
							var newHeight = height - 180;
							content.setHeight(newHeight);	
						}
					},this);
				}
		};
	}
});

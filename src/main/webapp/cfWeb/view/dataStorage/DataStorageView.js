Ext.define('cfWeb.view.dataStorage.DataStorageView',{
	extend : 'Ext.container.Container',
	alias : 'widget.dataStorageView',
	overflowY : 'auto',	
	initComponent : function(){
		this.items = [{
			xtype : 'collapsibleBlock',
			title : cfWebLang.DataStorageView.dataStorage,
			searchable : true,
			items : [{
				xtype : 'lbuttonIconListView',
				isAddable : false,
				buttonMenu : [[
				
				],[
				{
					ico : 'plus22',
					name : cfWebLang.Util.New,
					act : 'new'
				},{
					ico : 'filter',
					name : cfWebLang.Util.Filter,
					act : 'filter'
				}
				]],
				store : Ext.create('cfWeb.store.dataStorage.DatabaseTmpStore')
				}]
		},
			   {
			   	xtype : 'collapsibleBlock',
			    title : cfWebLang.AppServiceView.AppServiceList,
			    searchable : true,
			    items:[{
				xtype : 'cbuttonIconListView',
				id : 'databaselist',
				readyStatus:1,
				detailList:[
				            {name:cfWebLang.AppManageView.Plan,dataIndex:'plan', unit:''},
				            {name:cfWebLang.AppManageView.ServiceType,dataIndex:'service', unit:''},
//				            {name:cfWebLang.DataStorageView.DatabaseStatus,dataIndex:'', unit:''},
//				            {name:cfWebLang.DataStorageView.TableSpace,dataIndex:'', unit:''},
//				            {name:cfWebLang.DataStorageView.TempTableSpace, dataIndex:'', unit:''},
				            {name:'主机地址', dataIndex:'host', unit:''},
				            {name:'端口', dataIndex:'port', unit:''},
				            {name:'用户名', dataIndex:'user', unit:''}
				            ],
				menuList :[{
					ico:'cross',
					name:cfWebLang.Util.Delete ,
					act:'deleteService'
				}],
				store : Ext.create('cfWeb.store.dataStorage.DataStorageStore')
			}]
		}];
		this.callParent(arguments);
	}
});

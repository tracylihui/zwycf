Ext.define('cfWeb.view.dataStorage.NewDatabaseView',{
    extend : 'cfWeb.cfWebComponent.CfWindow',
	alias : 'widget.newDatabaseView',
	store : Ext.create('cfWeb.store.dataStorage.DatabaseServiceStore'),
	title:cfWebLang.Util.CreateDB,
	langSet:{"finish":cfWebLang.Util.Finish},
	style : {
		float : 'left'
	},
	initComponent : function(){
		this.items = [{
			xtype : 'container',
			width : 350,
		    height : 80,
		    padding:'0 10',
    		defaults:{
    			labelWidth:80,
    			cls:'appPanel-input'
    		},
    		items : [{
				xtype : 'form',
				itemId : 'databaseForm',
				width : 350,
				border : true,
				bodyStyle: 'background:#fff;',
				margin : '20 0 0 0',
				defaults:{
					labelAlign:'right',
					labelWidth:120,
					width : 350
				},
				items:[{
				   xtype:'combobox',
				   name:'service',
				   itemId:'serviceName',
				   fieldLabel:'service',
				   store:'dataStorage.DatabaseServiceBrokerStore',
				   queryMode : 'local',
				   displayField:'label',
				   valueField:'label',
				   emptyText:cfWebLang.Util.ChooseService,
				   labelAlign:'right',
				   editable : false
				},{
				   xtype:'combobox',
				   name:'plan',
				   itemId:'servicePlan',
				   queryMode : 'local',
				   store:'dataStorage.DatabaseBrokerPlanStore',
				   fieldLabel:'servicePlan',
				   displayField:'name',
				   valueField:'name',
				   emptyText:cfWebLang.Util.ChoosePlan ,
				   labelAlign:'right',
				   editable : false
				}]
    		}]

		}],
		this.callParent(arguments);
	}
});
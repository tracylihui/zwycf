Ext.define('cfWeb.view.appManage.NewAppLoadTem', {
    extend: 'Ext.panel.Panel',
    itemId: 'newAppLoadTemplate',
    alias: 'widget.newAppLoadTem',
    cls: 'style-appPanel',
    style: {
        float: 'left'
    },
    items: [{
        xtype: 'form',
        name: 'LoadTem.Form',
        border: false,
        padding: '20 20 0 20',
        defaults: {
            labelWidth: 170,
            cls: 'appPanel-input',
            labelStyle: 'color:#2a3950'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'template.id'
        }, {
            xtype: 'hiddenfield',
            name: 'env.envName'
        }, {
            xtype: 'hiddenfield',
            name: 'stoList.serName'
        }, {
            xtype: 'hiddenfield',
            name: 'template.envId'
        }, {
            xtype: 'hiddenfield',
            name: 'anaList.serName'
        }, {
            xtype: 'hiddenfield',
            name: 'appList.serName'
        }, {
            xtype: 'hiddenfield',
            name: 'space'
        }, {
            xtype: 'hiddenfield',
            name: 'org'
        }, {
            xtype: 'hiddenfield',
            name: 'template.memory'
        }, {
            xtype: 'displayfield',
            fieldLabel: cfWebLang.CfWindow.newAppTemplateName,
            itemId: "temName",
            labelAlign: 'right',
            name: 'template.temName'
        }, {
            xtype: 'displayfield',
            fieldLabel: cfWebLang.CfWindow.newAppMemSize,
            itemId: 'memory',
            labelAlign: 'right',
            name: 'template.memory'
        },
            {
                xtype: 'combobox',
                store: {
                    fields: ['memoryKey', 'memoryValue'],
                    data: [
                        {'memoryKey': "512M", 'memoryValue': 512},
                        {'memoryKey': "1G", 'memoryValue': 1024},
                        {'memoryKey': "2G", 'memoryValue': 2048},
                        {'memoryKey': "4G", 'memoryValue': 4096},
                        {'memoryKey': "8G", 'memoryValue': 8192},
                        {'memoryKey': "16G", 'memoryValue': 16348}
                    ]
                },
                displayField: 'memoryKey',
                valueField: 'memoryValue',
                queryMode: 'local',
                valueField: 'memoryValue',
                fieldLabel: cfWebLang.CfWindow.newAppMemSize,
                itemId: 'memory',
                labelAlign: 'right',
                listeners: {
                    change: function (me, value) {
                        me.up('form').down('hiddenfield[name=template.memory]').setValue(value);
                    }
                }
            }, {
                xtype: 'numberfield',
                fieldLabel: cfWebLang.CfWindow.newAppinstSize,
                labelAlign: 'right',
                itemId: 'instance',
                minValue: 1,
                maxValue: 10,
                name: 'template.instance'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.CfWindow.newAppRunEnv,
                itemId: 'envName',
                labelAlign: 'right',
                name: 'env.envName'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.HomeTabView.dataStorageService,
                itemId: 'stoSerName',
                labelAlign: 'right',
//    			name : 'stoList.serName'
            }, /*{
                xtype: 'displayfield',
                fieldLabel: cfWebLang.HomeTabView.dataAnalyseService,
                itemId: 'anaSerName',
                labelAlign: 'right',
//    			name : 'anaList.serName'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.HomeTabView.appService,
                itemId: 'appSerName',
                labelAlign: 'right',
//    			name : 'appList.serName'
            },*/ {
                xtype: 'fieldset',
                layout: 'column',
                items: [{
                    xtype: 'displayfield',
                    labelStyle: 'color:#2a3950',
                    labelWidth: 170,
                    fieldLabel: cfWebLang.Util.Organization,
                    labelAlign: 'right',
                    name: 'org',
                    itemId: 'org',
                    style: {
                        'margin-left': '-11px'
                    },
                    width: 250
                }]
            }, {
                xtype: 'displayfield',
                name: 'space',
                fieldLabel: cfWebLang.Util.Space,
                labelAlign: 'right',
                itemId: 'space',
                width: 170
            },
            {
                xtype: 'fieldset',
                layout: 'column',
                padding: 0,
                items: [{
                    xtype: 'textfield',
                    labelWidth: 170,
                    itemId: 'appName',
                    labelStyle: 'color:#2a3950',
                    fieldLabel: cfWebLang.CfWindow.newAppAppName,
                    style: {
                        //'margin-left' : '10px',
                        //'margin-right':'10px'
                    },
                    width: 300,
                    emptyText: cfWebLang.CfWindow.InputUserName,
                    name: 'appName',
                    labelAlign: 'right',
                    listeners: {
                        blur: function (me) {
                            var appNameUpper = me.getValue();
                            var appName = appNameUpper.toLowerCase();
                            var form = me.up('form');
                            var org = form.down("hiddenfield[name=org]").getValue();
                            var space = form.down("hiddenfield[name=space]").getValue();
                            var tip = form.down('displayfield[itemId=errorTip]');
                            if (appName == '') {
                                tip.setValue(cfWebLang.CfWindow.UsernameEmpty);
                                tip.setVisible(true);
                            } else {
                                Ext.Ajax.request({
                                    url: 'rest/app/checkAppName',
                                    params: {
                                        appName: appName,
                                        orgName: org,
                                        spaceName: space
                                    },
                                    method: 'GET',
                                    success: function (response) {
                                        var existTag = Ext.decode(response.responseText).data;
                                        if (existTag == true) {

                                        } else {
                                            tip.setValue(cfWebLang.CfWindow.UsernameExisted);
                                            tip.setVisible(true);
                                        }
                                    },
                                    failure: function () {
                                        var obj = Ext.JSON.decode(response.responseText);
                                        if (obj.data.source == 1) {
                                            Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry, error.data.message);
                                        } else {
                                            Ext.MessageBox.alert(cfWebLang.ErrorSource.app, cfWebLang.Error.SystemError);
                                        }
                                    }
                                });
                            }
                        },
                        focus: function (me) {
                            var form = me.up('form');
                            var tip = form.down('displayfield[itemId=errorTip]');
                            tip.setVisible(false);
                        }
                    }
                }, {
                    xtype: 'combobox',
                    itemId: 'domain',
                    labelAlign: 'right',
                    margin: '0 0 0 10',
                    store: 'appManage.DomainStore',
                    displayField: 'name',
                    width: 122,
                    valueField: 'name',
                    name: 'template.domain',
                    listeners: {
                        focus: function (me) {
                            var form = me.up('form');
                            var tip = form.down('displayfield[itemId=errorTip]');
                            tip.setVisible(false);
                        }
                    }
                }, {
                    xtype: 'displayfield',
                    itemId: 'errorTip',
                    value: cfWebLang.CfWindow.UsernameEmpty,
                    fieldStyle: {
                        'color': "red"
                    },
                    labelStyle: "font-size:'24px'",
                    hidden: true
                }]
            },
            {
                xtype: 'radiogroup',
                fieldLabel: cfWebLang.CfWindow.newAppUploadFile,
                labelAlign: 'right',
                cls: 'style-cfRadio',
                name: 'uploadGroup',
                hidden: false,//提交审批时隐藏，审批通过后显示
                columns: 1,
                items: [
                    {
                        boxLabel: cfWebLang.CfWindow.newAppUploadLocalFile,
                        name: 'uploadExample',
                        itemId: 'upFile',
                        inputValue: '1',
                        checked: true
                    }, {
                        xtype: 'fieldset',
                        layout: 'hbox',
                        border: 0,
                        items: [{
                            xtype: 'filefield',
                            itemId: 'uploadFile',
                            cls: 'style-cfFilefield',
                            labelWidth: 170,
                            name: 'uploadFile',
                            /*allowBlank  : false,*/
                            emptyText: cfWebLang.CfWindow.UploadFileTip,
                            hidden: false,
                            width: 280,
                            listeners: {
                                blur: function (me) {
                                    var form = me.up('form');
                                    var fileTip = form.down('displayfield[itemId=fileTip]');
                                    if (me.getValue() == '') {
                                        fileTip.setVisible(true);
                                    }
                                },
                                focus: function (me) {
                                    var form = me.up('form');
                                    var fileTip = form.down('displayfield[itemId=fileTip]');
                                    fileTip.setVisible(false);
                                }
                            }
                        }, {
                            xtype: 'displayfield',
                            value: cfWebLang.CfWindow.InputFilename,
                            itemId: 'fileTip',
                            hidden: false,
                            fieldStyle: {
                                "margin-left": '-60px',
                                color: "red"
                            },
                            hidden: true
                        }]
                    },
//    				{ xtype : 'textfield',hidden : true,id : 'uploadFile'},
                    {
                        boxLabel: cfWebLang.CfWindow.newAppUploadAppTemp,
                        name: 'uploadExample',
                        itemId: 'upApp',
                        inputValue: 'true',
                        style: {
                            float: "left"
                        },
                        listeners: {
                            focus: function (me) {
                                var form = me.up('form');
                                var fileTip = form.down('displayfield[itemId=fileTip]');
                                fileTip.setVisible(false);
                            }
                        }
                    },
                    {
                        xtype: 'label', name: 'uploadExample',
                        html: '<a href="war/helloworld.war">' + cfWebLang.CfWindow.DownAppTemplate + '</a>',
                        style: {
                            float: "left",
                            "line-height": "28px"
                        },
                        hidden: true
                    } /*, {
                     xtype : 'displayfield',
                     hidden : true,
                     itemId : 'chooseTip',
                     value : 'If you are  <b>creating an application</b>,please choose <b>UploadLocalFile</b>'
                     +'\n, or if you want an template application, choose <b>downloadAppTemp</b>',
                     style:{
                     float:"left",
                     "line-height":"28px",
                     color : 'red'
                     },
                     }*/
                ],
                listeners: {
                    change: function () {
                        var checkboxFile = this.down('checkbox[itemId=upFile]');
                        var checkboxApp = this.down('checkbox[itemId=upApp]');
                        var upload = this.down('filefield');
                        var downApp = this.down('label');
//    					this.down('displayfield[itemId=chooseTip]').setVisible(false);
                        if (checkboxFile.checked) {
                            upload.setVisible(true);
                            downApp.setVisible(false);
                        } else if (checkboxApp.checked) {
                            upload.setVisible(false);
                            downApp.setVisible(true);
                        }
                    }
                }
            }
        ]
    }],

    initComponent: function () {
        this.callParent(arguments);
    }
});

Ext.define('cfWeb.view.appManage.NewAppCfWindow',{
	extend : 'cfWeb.cfWebComponent.CfWindow',
	alias  : 'widget.newAppCfWindow',
	title : cfWebLang.Util.NewApp ,
	initComponent : function(){
		this.callParent(arguments);
	}
})
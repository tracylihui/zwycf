Ext.define('cfWeb.view.appManage.AppTemplateView',{
	extend : 'cfWeb.cfWebComponent.CfWindow',
	alias : 'widget.appTemplateView',	
	title : cfWebLang.AppManageView.UserDefinedTemplate,
	langSet:{"next":cfWebLang.Util.Next,"prev":cfWebLang.Util.Prev,
			 "finish":cfWebLang.Util.Finish},
//	width : 1000,
	width : 1100,
	height : 600,
	initComponent : function(){
		this.items = [
		{
			xtype : 'container',
			layout : 'column',
			height:560,
			width:1100,

			items : [
			{
				xtype : 'container',
//				width : 300,
				width : 400,
				height : 560,
				border: '0 1 0 0',
				style: {
				    borderColor: '#ddd',
				    borderStyle: 'solid'
				},
				autoScroll : true,
				items : [{					
					xtype : 'cfPanelTypeI',
					title : cfWebLang.AppManageView.RunEnvs,
					items : [{
						insertType:'buildPack',
						xtype : 'iconListViewTypeII',
						itemId : 'buildpackTemplate',
						store : Ext.create('cfWeb.store.appManage.BuildpackStore')
					}]
				},{
					xtype : 'cfPanelTypeI',
					title : cfWebLang.HomeTabView.dataStorageService,
					items : [{
						insertType:'serType1',
						xtype : 'iconListViewTypeII',
						itemId : 'storageTemplate',
						store : Ext.create('cfWeb.store.appManage.DataStorageStore')
					}]
				}/*,{
					xtype : 'cfPanelTypeI',
					title : cfWebLang.HomeTabView.dataAnalyseService,
					items : [{
						insertType:'serType2',
						xtype : 'iconListViewTypeII',
						itemId : 'analyseTemplate',
						store : Ext.create('cfWeb.store.appManage.DataAnalyseStore')
					}]
				}
				,{
					xtype : 'cfPanelTypeI',
					title : cfWebLang.HomeTabView.appService,
					items : [{
						insertType:'serType3',
						xtype : 'iconListViewTypeII',
						itemId : 'appTemplate123',
//						itemId : 'appTemplate',
						store : Ext.create('cfWeb.store.appManage.ApplicationStore')
					}]
				}*/
				]
			},{
				xtype : 'container',
//				width : 550,
				width : 600,
				height : 560,
				items : [{
					xtype : 'moduleConstructView',
					
					title:'',
					subTitle:'',
					langSetting:{
									serType1:cfWebLang.HomeTabView.dataStorageService,
									serType2:cfWebLang.HomeTabView.dataAnalyseService,
									serType3:cfWebLang.HomeTabView.appService,
									buildPack:cfWebLang.AppManageView.RunEnvs
								},
					moduleData:[{
						buildPack:[],
						serType1:[],
						serType2:[],
						serType3:[]
					}]
				}]
			}]
		},{
			xtype : 'container',
			width : 550,
			height : 340,
			padding:'0 10',
    		defaults:{
    			labelWidth:120,
    			cls:'appPanel-input',
    		},
			items : [{
				xtype : 'form',
				itemId : 'appForm',
				width : 550,
//				height : 150,
				border : true,
				bodyStyle: 'background:#fff;',
//				bodyStyle: 'background:#fff; padding:10px; border-color:#000',
				margin : '20 0 0 0',
				defaults:{
					labelAlign:'right',
					labelWidth:170,
					width : 450
				},
				items : [{
					xtype : 'displayfield',
					id : 'appTextfield',
					name : 'appTextfield',
					readOnly : true,
					fieldLabel :cfWebLang.AppManageView.AppContainer
				},{
					xtype : 'displayfield',
					id : 'storageTextfield',
					name : 'storageTextfield',
					readOnly : true,
					fieldLabel : cfWebLang.HomeTabView.dataStorageService
				},{
					xtype : 'displayfield',
					id : 'analyseTextfield',
					name : 'analyseTextfield',
					readOnly : true,
					fieldLabel : cfWebLang.HomeTabView.dataAnalyseService
				},{
					xtype : 'displayfield',
					id : 'serviceTextfield',
					name : 'serviceTextfield',
					readOnly : true,
					fieldLabel :cfWebLang.HomeTabView.appService
				}]
			},{
				/*xtype : 'combobox',
				id : 'templateOrg',
//				itemId : 'templateOrg',
				name : 'orgName', 
				store : 'appManage.TemplateOrgStore',
				displayField : 'name',
    			valueField : 'name',
				emptyText : 'Organization名称',
				editable : false,
				listeners : {
					change : function(me,newValue,oldValue,e){
						var spaceCombobox=me.nextSibling();
						var store = spaceCombobox.getStore();
						store.load({
							params : {
								orgName : newValue
							},
							callback : function(){
								console.log(store);
								spaceCombobox.setValue(store.data.items[0].data.name);
								console.log(store.data.items[0].data.name);
							}
						});
					}
				}*/
				xtype : 'displayfield',
				id : 'templateOrg',
				itemId:'templateOrg',
				name : 'orgName', 
				fieldLabel :cfWebLang.AppManageView.OrganizationName,
				labelAlign : 'top',
//				readOnly : true,
				labelAlign:'right',
				labelWidth:170,
				width : 450
			},{
				/*xtype : 'combobox',
				id : 'templateSpace',
//				itemId : 'templateSpace',
				name : 'space',
				store : 'appManage.TemplateSpaceStore',
				editable : false,
				displayField : 'name',
    			valueField : 'name',
				emptyText : 'Space名称'*/
				xtype : 'displayfield',
				id : 'templateSpace',
				itemId : 'templateSpace',
				name : 'space',
				fieldLabel : cfWebLang.AppManageView.SpaceName ,
//				readOnly : true,
				labelAlign:'right',
				labelWidth:170,
				width : 450
			},{
				xtype : 'fieldset',
				layout : 'column',
				items:[{
				    xtype : 'combobox',
				    id:'templateDomain',
//				    itemId:'templateDomain',
				    name : 'domain',
				    store :'appManage.DomainStore',
    			    displayField : 'name',
    			    valueField : 'name',
				    fieldLabel : 'Domain',
				    emptyText :cfWebLang.AppManageView.ChooseDomain ,
				    editable : false,
				    labelAlign:'right',
				    labelWidth:160,
				    width : 340,
				    listeners : {
				    	focus : function(me){
    					var win=me.up('window');
    					var tip=win.down('displayfield[itemId=errorDomainEmpTip]');
    					tip.setVisible(false);
    				}
				    }
				},{
				    xtype : 'displayfield',
    			    itemId : 'errorDomainEmpTip',
    			    value :cfWebLang.SettingWindow.DomainEmp,
    			    fieldStyle : {
    					'color' : "red"
    			    },
    			    labelStyle:"font-size:'24px'",
    			    hidden : true
				}]
				
			},{
				xtype : 'fieldset',
				layout : 'column',
				items:[{
				    xtype : 'combobox',
				id : 'memory',
				name : 'memory',
				width : 300,
				editable : false,
				fieldLabel : cfWebLang.CfWindow.newAppMemSize,
				emptyText : cfWebLang.AppManageView.ChooseMemory ,
				labelAlign:'right',
				labelWidth:160,
				width : 340,
				store : new Ext.data.SimpleStore({
					fields:['key','value'],
					data:[[512,'512M'],[1024,'1G'],[2048,'2G'],[4096,'4G'],[8192,'8G'],[16348,'16G']
						]
				}),
				valueField:'key',
            	displayField:'value',
            	queryMode:'local',
            	listeners : {
				    	focus : function(me){
    					var win=me.up('window');
    					var tip=win.down('displayfield[itemId=errorMemoryEmpTip]');
    					tip.setVisible(false);
    				}
				    }
				},{
				    xtype : 'displayfield',
    			    itemId : 'errorMemoryEmpTip',
    			    value :cfWebLang.CfWindow.memoryEmp,
    			    fieldStyle : {
    					'color' : "red"
    			    },
    			    labelStyle:"font-size:'24px'",
    			    hidden : true
				}]
			},{
				xtype : 'numberfield',
				id : 'instance',
//				cls:'style-cfSlider',
				name : 'instance',
				fieldLabel :cfWebLang.CfWindow.newAppinstSize,
				value: 1,
				increment: 1,
				minValue: 1,
				maxValue: 10,
				renderTo: Ext.getBody(),
				labelAlign:'right',
				labelStyle:'color:#69738c;margin:0px;line-height:24px;',
				labelWidth:170,
				width : 350,
				margin:'0 0 10 0'
			},{
				xtype : 'fieldset',
				layout : 'column',
				items:[{
				    xtype : 'textfield',
				    id : 'temName',
				    name : 'temName',
				    fieldLabel :cfWebLang.AppManageView.TemplateName,
				    labelAlign:'right',
				    labelWidth:160,
				    width : 340,
				    listeners : {
				    	blur:function(me){
				    		var temName=me.getValue().toLowerCase();
				    		var orgName = Ext.ComponentQuery.query('#templateOrg')[0].getValue();
		                    var space = Ext.ComponentQuery.query('#templateSpace')[0].getValue();
					        var errTip = me.up('window').down('displayfield[itemId=errorTemEmpTip]');
					        if(temName!=''){
						       Ext.Ajax.request({
			                       url:'rest/template/checkTemName',
			                       method:'GET',
			                       params:{
					                   temName:temName,
					                   spaceName:space,
					                   orgName:orgName
				                   },
			                       success:function(response){
					                  var obj = Ext.JSON.decode(response.responseText);
					                  if(obj.data==false){
						                 errTip.setValue(cfWebLang.CfWindow.temNameExit);
						                 errTip.setVisible(true);
					                  }
			                      },
				                  failure : function(response){
				                	  if(response.status=="401"){
									    	Ext.Msg.show({
									    	    title: cfWebLang.Util.Tip,
									    	    id:'firstWin',
									    	    msg: cfWebLang.Main.SessionOverdue,
									    	    width: 250,
									    	    buttons: Ext.MessageBox.OK,
									    	    fn: function(){  
									                parent.location.href = '/zwycf/login.html';    
									            },
									            cls:'overtimeWin'
									    	});
									    	//Ext.WindowManager.bringToFront ('firstWin');
									       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
									            parent.location.href = '/zwycf/login.html';    
									        }); */
									       return;
									    }
				                      Ext.Msg.alert(cfWebLang.Util.Error,cfWebLang.CfWindow.InputAgain);
				                  }
		                      });
					        }
				    	},
				    	focus : function(me){
    					var win=me.up('window');
    					var tip=win.down('displayfield[itemId=errorTemEmpTip]');
    					tip.setVisible(false);
    				}
				    }
				},{
				    xtype : 'displayfield',
    			    itemId : 'errorTemEmpTip',
    			    value :cfWebLang.CfWindow.temNameEmp,
    			    fieldStyle : {
    					'color' : "red"
    			    },
    			    labelStyle:"font-size:'24px'",
    			    hidden : true
				}]
			}] 
		}];
		this.callParent(arguments);
	}
});
Ext.define('cfWeb.view.appManage.NewAppAnaLog',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.logService',
	cls:'style-appPanel',
	autoHeight : true,
	items :[{
		xtype : 'form',
		name : 'AnaLog.Form',
		defaults:{
			labelWidth:120,
			cls:'appPanel-input',
		},
		items :[{
			xtype : 'hiddenfield',
			name : 'service',
			value : 'logservice'
		},{
			xtype:'panel',
			title:cfWebLang.CfWindow.PatternSetting,
			shadow:false,
			cls:'service_param'
		},{
			xtype : 'fieldset',
			layout : 'vbox',
			padding:'0 35px',
			items :[{
			xtype : 'checkboxgroup',
			cls:'style-cfCheck',
			columns : 1,
			items :[
				{xtype : 'checkbox',name: 'error',boxLabel :'Error',inputValue: true,checked : true,readOnly:true},
				{xtype : 'checkbox',name: 'exception',boxLabel :'Exception',inputValue: true,checked : true,readOnly:true}
			]
		},{
			xtype : 'fieldset',
			border : 0,
			itemId : 'pattern',
			padding:'0 5px',
			layout : 'hbox',
			items : [{
				xtype : 'textfield',
				emptyText : cfWebLang.CfWindow.PatternName,
				itemId : 'addService1',
				width : 120,
				margin:'0 10 0 0'
			},{
				xtype : 'textfield',
				emptyText :cfWebLang.CfWindow.PatternContent,
				itemId : 'addService2',
				width : 120
			},{
				xtype : 'button',
				text: cfWebLang.CfWindow.PatternTest,
				width : 100,
				margin:'0 10 0 20',
				itemId : 'buttonTest',
			},{
				xtype : 'button', 
				text : 'add',
				width :100,
				margin:0,
				listeners: {
					click : function(me){
						var form=me.up('form');
						console.log(form);
						var boxgroup=form.down('checkboxgroup');
						console.log(boxgroup);
						var text1=form.down('textfield[itemId=addService1]');
						var text2=form.down('textfield[itemId=addService2]');
						textValue=text1.getValue()+':'+text2.getValue();
						console.log(textValue);
						if(text1.getValue()!=''&&text2.getValue()!=''){
							var box=Ext.widget('checkbox',{
								boxLabel : textValue,
								checked : true,
								name : 'patternkey',
								inputValue :text1.getValue(),		
							});
							boxgroup.add(box);
							var box2=Ext.widget('checkbox',{
								name : 'pattern',
								checked: true,
								hidden : true,
								inputValue : text2.getValue()
							});
							boxgroup.add(box2);
							box.on("change",function(checkbox,newValue,oldValue){
								box2.checked=checkbox.checked;
							});
						}
						me.up('cfWindow').indexInit();
						text1.setValue('');
						text2.setValue('');
						console.log(boxgroup);
					}
				}
			}]
		}]
		}]
	}],
	
	initComponent : function(){
		this.callParent(arguments);
	}
})

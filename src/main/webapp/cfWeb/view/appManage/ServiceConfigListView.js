/**
 * Created by Zorro on 2/24 024.
 */
Ext.define('cfWeb.view.appManage.ServiceConfigListView', {
    extend : 'Ext.panel.Panel',
    alias : 'widget.serviceConfigListView',
    selfId: undefined,
    cls:'style-ServiceConfigView',
    title:cfWebLang.HomeTabView.dataStorageService,
    header:{
        height:40
    },
//	titleCollapse : true,
    items: [{
        xtype: 'form',
        name: 'ServiceConfigList.Form',
        border: false,
        padding: '20 20 0 20',
        defaults: {
            labelWidth: 30,
            cls: 'appPanel-input',
            labelStyle: 'color:#2a3950'
        },
        items: [{
            xtype: 'fieldset',
            itemId: 'mysqlFieldSet',
            layout: 'hbox',
            hidden: true,
            items:[{
                xtype: 'label',
                text: 'MySQL',
                width: 60
            }, {
                xtype: 'combobox',
                store: {
                    fields: ['stoSizeDisplay', 'stoSizeValue'],
                    data: [
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeLarge, 'stoSizeValue': "large"},
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeMedium, 'stoSizeValue': "medium"},
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeSmall, 'stoSizeValue': "small"}
                    ]
                },
                displayField: 'stoSizeDisplay',
                valueField: 'stoSizeValue',
                queryMode: 'local',
                fieldLabel: cfWebLang.ServiceConfig.StorageSize,
                labelAlign: 'right',
                width: 130,
                labelWidth: 60,
                value: 'medium',
                name: 'mysql.size'
            }, {
                xtype: 'radiogroup',
                cls:'style-cfRadio',
                columns: 2,
                margin:'0 0 0 26',
                items: [
                    { boxLabel: cfWebLang.ServiceConfig.Create, name:'mysql', inputValue: 'create' ,checked:true},
                    { boxLabel: cfWebLang.ServiceConfig.Select , name:'mysql',inputValue: 'select'}
                ],
                listeners: {
                    change: function (me, value) {
                        if (value.mysql === 'create') {
                            me.up('form').down('#mysqlSelect').hide();
                        } else if (value.mysql === 'select') {
                            me.up('form').down('#mysqlSelect').show();
                        }
                    }
                }
            }, {
                xtype: 'combobox',
                store: Ext.create('cfWeb.store.dataStorage.DatabaseServiceSelectStore'),
                displayField: 'title',
                valueField: 'title',
                queryMode: 'local',
                itemId: 'mysqlSelect',
                fieldLabel: '实例' + cfWebLang.Util.Name,
                width: 210,
                labelWidth: 100,
                labelAlign: 'right',
                name: 'mysql.name',
                hidden: true
            }]
        }, {
            xtype: 'fieldset',
            itemId: 'oracleFieldSet',
            layout: 'hbox',
            hidden: true,
            items:[{
                xtype: 'label',
                text: 'Oracle',
                width: 60
            }, {
                xtype: 'combobox',
                store: {
                    fields: ['stoSizeDisplay', 'stoSizeValue'],
                    data: [
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeLarge, 'stoSizeValue': "large"},
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeMedium, 'stoSizeValue': "medium"},
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeSmall, 'stoSizeValue': "small"}
                    ]
                },
                displayField: 'stoSizeDisplay',
                valueField: 'stoSizeValue',
                queryMode: 'local',
                fieldLabel: cfWebLang.ServiceConfig.StorageSize,
                labelAlign: 'right',
                width: 130,
                labelWidth: 60,
                value: 'medium',
                name: 'oracle.size'
            }, {
                xtype: 'radiogroup',
                cls:'style-cfRadio',
                columns: 2,
                margin:'0 0 0 26',
                items: [
                    { boxLabel: cfWebLang.ServiceConfig.Create, name:'oracle', inputValue: 'create' ,checked:true},
                    { boxLabel: cfWebLang.ServiceConfig.Select , name:'oracle',inputValue: 'select'}
                ],
                listeners: {
                    change: function (me, value) {
                        if (value.oracle === 'create') {
                            me.up('form').down('#oracleSelect').hide();
                        } else if (value.oracle === 'select') {
                            me.up('form').down('#oracleSelect').show();
                        }
                    }
                }
            }, {
                xtype: 'combobox',
                store: Ext.create('cfWeb.store.dataStorage.DatabaseServiceSelectStore'),
                displayField: 'title',
                valueField: 'title',
                queryMode: 'local',
                itemId: 'oracleSelect',
                fieldLabel: '实例' + cfWebLang.Util.Name,
                width: 210,
                labelWidth: 100,
                labelAlign: 'right',
                hidden: true,
                name: 'oracle.name'
            }]
        }, {
            xtype: 'fieldset',
            itemId: 'redisFieldSet',
            layout: 'hbox',
            hidden: true,
            items:[{
                xtype: 'label',
                text: 'Redis',
                width: 60
            }, {
                xtype: 'combobox',
                store: {
                    fields: ['stoSizeDisplay', 'stoSizeValue'],
                    data: [
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeLarge, 'stoSizeValue': "large"},
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeMedium, 'stoSizeValue': "medium"},
                        {'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeSmall, 'stoSizeValue': "small"}
                    ]
                },
                displayField: 'stoSizeDisplay',
                valueField: 'stoSizeValue',
                queryMode: 'local',
                fieldLabel: cfWebLang.ServiceConfig.StorageSize,
                labelAlign: 'right',
                width: 130,
                labelWidth: 60,
                value: 'medium',
                name: 'redis.size'
            }, {
                xtype: 'radiogroup',
                cls:'style-cfRadio',
                columns: 2,
                margin:'0 0 0 26',
                items: [
                    { boxLabel: cfWebLang.ServiceConfig.Create, name:'redis', inputValue: 'create' ,checked:true},
                    { boxLabel: cfWebLang.ServiceConfig.Select , name:'redis',inputValue: 'select'}
                ],
                listeners: {
                    change: function (me, value) {
                        if (value.redis === 'create') {
                            me.up('form').down('#redisSelect').hide();
                        } else if (value.redis === 'select') {
                            me.up('form').down('#redisSelect').show();
                        }
                    }
                }
            }, {
                xtype: 'combobox',
                store: Ext.create('cfWeb.store.dataStorage.DatabaseServiceSelectStore'),
                displayField: 'title',
                valueField: 'title',
                queryMode: 'local',
                itemId: 'redisSelect',
                fieldLabel: '实例' + cfWebLang.Util.Name,
                width: 210,
                labelWidth: 100,
                labelAlign: 'right',
                hidden: true,
                name: 'redis.name'
            }]
        }, {
            xtype: 'fieldset',
            itemId: 'rabbitmqFieldSet',
            layout: 'hbox',
            hidden: true,
            items: [{
                xtype: 'label',
                text: 'RabbitMQ',
                width: 60
            }, {
                xtype: 'combobox',
                store: {
                    fields: ['stoSizeDisplay', 'stoSizeValue'],
                    data: [{
                        'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeLarge,
                        'stoSizeValue': "large"
                    }, {
                        'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeMedium,
                        'stoSizeValue': "medium"
                    }, {
                        'stoSizeDisplay': cfWebLang.ServiceConfig.StorageSizeSmall,
                        'stoSizeValue': "small"
                    }]
                },
                displayField: 'stoSizeDisplay',
                valueField: 'stoSizeValue',
                queryMode: 'local',
                fieldLabel: cfWebLang.ServiceConfig.StorageSize,
                labelAlign: 'right',
                width: 130,
                labelWidth: 60,
                value: 'medium',
                name: 'rabbitmq.size'
            }, {
                xtype: 'radiogroup',
                cls: 'style-cfRadio',
                columns: 2,
                margin: '0 0 0 26',
                items: [{
                    boxLabel: cfWebLang.ServiceConfig.Create,
                    name: 'rabbitmq',
                    inputValue: 'create',
                    checked: true
                }, {
                    boxLabel: cfWebLang.ServiceConfig.Select,
                    name: 'rabbitmq',
                    inputValue: 'select'
                }],
                listeners: {
                    change: function(me, value) {
                        if (value.rabbitmq === 'create') {
                            me.up('form').down('#rabbitmqSelect').hide();
                        } else if (value.rabbitmq === 'select') {
                            me.up('form').down('#rabbitmqSelect').show();
                        }
                    }
                }
            }, {
                xtype: 'combobox',
                store: Ext.create('cfWeb.store.dataStorage.DatabaseServiceSelectStore'),
                displayField: 'title',
                valueField: 'title',
                queryMode: 'local',
                itemId: 'rabbitmqSelect',
                fieldLabel: '实例' + cfWebLang.Util.Name,
                width: 210,
                labelWidth: 100,
                labelAlign: 'right',
                hidden: true,
                name: 'rabbitmq.name'
            }]
        }]
    }],
    initComponent : function() {
        this.addEvents({
        });
        this.callParent(arguments);
    },
    listeners:{
        boxready : function(){
            this.selfId = this.getEl().id;
            this.eventBind();
        }
    },
    getTarNode :function(tar,tag){
        if(tar.tagName ==undefined){
            return false;
        }
        var res = tar.getAttribute(tag);
        if(res != null && res.length > 0){
            return tar;
        }
        else{
            return this.getTarNode(tar.parentNode,tag);
        }
    },
    eventBind :function(){
//	   var header = this.header.getEl();
//	   header.on('click',this.headClick,this);
//	   console.log(header);
    }
});

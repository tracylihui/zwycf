Ext.define('cfWeb.view.appManage.AppAuditLoadTem', {
    extend: 'Ext.panel.Panel',
    itemId: 'appAuditLoadTemplate',
    alias: 'widget.appAuditLoadTem',
    cls: 'style-appPanel',
    style: {
        float: 'left'
    },
    items: [{
        xtype: 'form',
        name: 'LoadTem.Form',
        border: false,
        padding: '20 20 0 20',
        defaults: {
            labelWidth: 170,
            cls: 'appPanel-input',
            labelStyle: 'color:#2a3950'
        },
        items: [{
            xtype: 'hiddenfield',
            name: 'template.id'
        }, {
            xtype: 'hiddenfield',
            name: 'env.envName'
        }, {
            xtype: 'hiddenfield',
            name: 'stoList.serName'
        }, {
            xtype: 'hiddenfield',
            name: 'template.envId'
        }, {
            xtype: 'hiddenfield',
            name: 'anaList.serName'
        }, {
            xtype: 'hiddenfield',
            name: 'appList.serName'
        }, {
            xtype: 'hiddenfield',
            name: 'space'
        }, {
            xtype: 'hiddenfield',
            name: 'org'
        }, {
            xtype: 'hiddenfield',
            name: 'template.memory'
        }, {
            xtype: 'displayfield',
            fieldLabel: cfWebLang.CfWindow.newAppTemplateName,
            itemId: "temName",
            labelAlign: 'right',
            name: 'template.temName'
        }, {
            xtype: 'displayfield',
            fieldLabel: cfWebLang.CfWindow.newAppMemSize,
            itemId: 'memory',
            labelAlign: 'right',
            name: 'template.memory'
        },
            {
                xtype: 'combobox',
                store: {
                    fields: ['memoryKey', 'memoryValue'],
                    data: [
                        {'memoryKey': "512M", 'memoryValue': 512},
                        {'memoryKey': "1G", 'memoryValue': 1024},
                        {'memoryKey': "2G", 'memoryValue': 2048},
                        {'memoryKey': "4G", 'memoryValue': 4096},
                        {'memoryKey': "8G", 'memoryValue': 8192},
                        {'memoryKey': "16G", 'memoryValue': 16348}
                    ]
                },
                displayField: 'memoryKey',
                valueField: 'memoryValue',
                queryMode: 'local',
                valueField: 'memoryValue',
                fieldLabel: cfWebLang.CfWindow.newAppMemSize,
                itemId: 'memory',
                labelAlign: 'right',
                listeners: {
                    change: function (me, value) {
                        me.up('form').down('hiddenfield[name=template.memory]').setValue(value);
                    }
                }
            }, {
                xtype: 'numberfield',
                fieldLabel: cfWebLang.CfWindow.newAppinstSize,
                labelAlign: 'right',
                itemId: 'instance',
                minValue: 1,
                maxValue: 10,
                name: 'template.instance'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.CfWindow.newAppRunEnv,
                itemId: 'envName',
                labelAlign: 'right',
                name: 'env.envName'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.HomeTabView.dataStorageService,
                itemId: 'stoSerName',
                labelAlign: 'right',
//    			name : 'stoList.serName'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.HomeTabView.dataAnalyseService,
                itemId: 'anaSerName',
                labelAlign: 'right',
//    			name : 'anaList.serName'
            }, {
                xtype: 'displayfield',
                fieldLabel: cfWebLang.HomeTabView.appService,
                itemId: 'appSerName',
                labelAlign: 'right',
//    			name : 'appList.serName'
            }, {
                xtype: 'fieldset',
                layout: 'column',
                items: [{
                    xtype: 'displayfield',
                    labelStyle: 'color:#2a3950',
                    labelWidth: 170,
                    fieldLabel: cfWebLang.Util.Organization,
                    labelAlign: 'right',
                    name: 'org',
                    itemId: 'org',
                    style: {
                        'margin-left': '-11px'
                    },
                    width: 250
                }]
            }, {
                xtype: 'displayfield',
                name: 'space',
                fieldLabel: cfWebLang.Util.Space,
                labelAlign: 'right',
                itemId: 'space',
                width: 170
            }
        ]
    }],

    initComponent: function () {
        this.callParent(arguments);
    }
});

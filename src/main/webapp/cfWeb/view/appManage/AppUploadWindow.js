Ext.define('cfWeb.view.appManage.AppUploadWindow',{
	alias : 'widget.appUploadWindow',
	extend : 'Ext.window.Window',
	cls: 'style-cfWindow',
	title :cfWebLang.AppManageView.AppUpdate,
	layout : 'fit',
	height:170,
	width:500,
	items:[{
		xtype : 'form',
		cls: 'window-popup',
		border : 0,
		items:[{
			layout:{
				type: 'column'
			},
			margin:'30 80 10 30',
			border:0,
			items : [{
				xtype : 'filefield',
				itemId : 'uploadFile',
				name: 'uploadFile',
				/*allowBlank  : false,*/
				emptyText :cfWebLang.CfWindow.UploadFileTip,
				hidden : false,
				width : 380,
			},{
				xtype:'checkbox',
				boxLabel:cfWebLang.AppManageView.greyScaleUpdate,
				itemId:'isGrayUpdate',
				cls:'style-cfCheck',
				name:'isGrayUpdate'
			},
			{
				xtype : 'hiddenfield',
				name : 'appName'
			},{
			    xtype : 'hiddenfield',
				name : 'isGrayUpdating'
			},{
				xtype : 'displayfield',
				value : cfWebLang.CfWindow.InputFilename,
				itemId: 'fileTip',
				hidden : false,
				fieldStyle :{
					"margin-left" : '5px',
					color : "red"
				},
				hidden : true
			}]
		},{
			xtype : 'fieldset',
			border : 0,
			width : 240,
			margin:'20 0 0 100',
			items :[
			{
				xtype : 'button',
				text:cfWebLang.Util.Confirm,
				action:'updateAppWar',
				width : 70
			},{
				xtype : 'button',
				text : cfWebLang.Util.Cancel,
				action : 'close',
				width : 70
			}]
		}]
	}],
	initComponent : function(){
		this.callParent(arguments);
	}
});



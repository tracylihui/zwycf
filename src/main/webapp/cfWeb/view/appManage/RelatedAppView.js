Ext.define('cfWeb.view.appManage.RelatedAppView', {
	extend : 'Ext.container.Container',
	alias : 'widget.relatedAppView',
	cls : 'style-logFileWindow',
	border : 0,
	bodyPadding:'10 0 10 0',
	items : [{
				layout : {
					type : 'column',
					align: 'center'
				},
				margin : 0,
				header:{
					title:cfWebLang.AppManageView.greyScaleUpdateTitle,
					cls:'style-compareHead',
					style:{
						"background":"#EEF1F8",
						
					},
					height:50,
					border:0,
					titleAlign:'center',
					margin:'0 0 10 0'
				},
				items : [{
					bodyStyle:{
						borderColor:'#EEF1F8',
						borderStyle:'solid',
						border:1,
						"margin-top":"-2px"
					},

					xtype : 'panel',
					cls : 'style-CfPanelTypeI logAnaly-input style-compare',
					title : this.thisName,
					margin : '0 10 0 10',
					padding: '0 0 10 0',
					itemId : 'chartConfig',
					defaults:{
						labelAlign:'right'
					},
					layout : {
						type : 'vbox',
						align:'center'
					},
//					margin : '0 0 0 24',
					columnWidth : 0.49,
					items : [
						{
							xtype : 'cfChartTypeI',
							store :  'appManage.AppAccessStore',
							itemId : 'thisHist',
							title:cfWebLang.AppManageView.VisitTime30m,
							region:'center'
							
						},
						{
							  xtype:'thresholdControlView',
							  flex:1,
							  itemId:'thisBlock',
							  margin:'30 80 30 30',
							  padding:'40 0 30 90',
							  title:cfWebLang.AppManageView.Autoscaling,
							  hasSwitcher:false,
								 items:[{
								    	xtype:'cfSlider',
								    	itemId:'thisInstance',
								    	title:cfWebLang.AppManageView.InstanceNumber,
								    	maxValue:20
								}]
							}
					]

				}, {
					xtype : 'panel',
					cls : 'style-CfPanelTypeI logAnaly-input style-compare',
					margin:'0 0 0 10',
					bodyStyle:{
						borderColor:'#EEF1F8',
						borderStyle:'solid',
						border:1,
						"margin-top":"-2px"
					},
					defaults:{
						labelAlign:'right'
					},
					title :this.thatName,
					layout : {
						type : 'vbox',
						align: 'center'
					},
					itemId : 'paraConfig',
//					margin : '0 0 0 20',
					columnWidth : 0.49,
					items : [
							{
								xtype : 'cfChartTypeI',
								store : 'appManage.AppAccessStore',
								itemId : 'thatHist',
								title:cfWebLang.AppManageView.VisitTime30m							
							},
							{
								  xtype:'thresholdControlView',
								  flex:1,
								  itemId:'thatBlock',
								  margin:'30 80 30 30',
								  padding:'40 0 30 90',
								  title:cfWebLang.AppManageView.Autoscaling,
								  hasSwitcher:false,
									 items:[{
							            	xtype:'cfSlider',
							            	itemId:'thatInstance',
							            	title:cfWebLang.AppManageView.InstanceNumber,
							            	maxValue:20
							            }]
								}
					         
					         
				    ]
				}]
			}],
	listeners:{
		afterrender : function(me,e){
			var now = new Date(),
			year = now.getFullYear(),
			month = now.getMonth()+1,
			day = now.getDate(),
			hour = now.getHours(),
			min = now.getMinutes();
			var then = new Date(now.getTime()-1800000),
			fromYear = then.getFullYear(),
			fromMonth = then.getMonth()+1,
			fromDay = then.getDate(),
			fromHour = then.getHours(),
			fromMin = then.getMinutes();
			var store1 =  Ext.create('cfWeb.store.appManage.AppAccessStore');
			me.down('thresholdControlView#thisBlock').appName = me.thisName;
			me.down('thresholdControlView#thatBlock').appName = me.thatName;
			store1.setParams({
				appName : this.thisName,
				from : fromYear+'-'+fromMonth+'-'+fromDay+' '+fromHour+':'+fromMin+':00',
				to : year+'-'+month+'-'+day+' '+hour+':'+min+':59',
				step : '1m'
			}).load();
			me.thisStore = store1;
			me.down('cfChartTypeI#thisHist').bindStore(store1);
			var store2 =  Ext.create('cfWeb.store.appManage.AppAccessStore');
			store2.setParams({
				appName : this.thatName,
				from : fromYear+'-'+fromMonth+'-'+fromDay+' '+fromHour+':'+fromMin+':00',
				to : year+'-'+month+'-'+day+' '+hour+':'+min+':59',
				step : '1m'
			}).load();
			me.thatStore = store2;
			me.down('cfChartTypeI#thatHist').bindStore(store2);	
			me.autoRefresh();
		},
		beforedestroy:function(me,e){
			clearTimeout(me.tmo);
		}
	},
	autoRefresh : function(){
		var me = this;
		var now = new Date(),
		year = now.getFullYear(),
		month = now.getMonth()+1,
		day = now.getDate(),
		hour = now.getHours(),
		min = now.getMinutes();
		var then = new Date(now.getTime()-1800000),
		fromYear = then.getFullYear(),
		fromMonth = then.getMonth()+1,
		fromDay = then.getDate(),
		fromHour = then.getHours(),
		fromMin = then.getMinutes();
		
		me.thisStore.setParams({
			appName : this.thisName,
			from : fromYear+'-'+fromMonth+'-'+fromDay+' '+fromHour+':'+fromMin+':00',
			to : year+'-'+month+'-'+day+' '+hour+':'+min+':59',
			step : '1m'
		}).load({
			scope:me,
			callback : function(){
				me.tmo  = setTimeout(function(){
					me.autoRefresh.apply(me);
				},1000);
			}
		});
		me.thatStore.setParams({
			appName : this.thatName,
			from : fromYear+'-'+fromMonth+'-'+fromDay+' '+fromHour+':'+fromMin+':00',
			to : year+'-'+month+'-'+day+' '+hour+':'+min+':59',
			step : '1m'
		}).load();
		
		Ext.Ajax.request({
			url : 'rest/app/details',
			method : 'GET',
			params : {
				appName : me.thisName
			},
			success : function(response){
				var obj = Ext.JSON.decode(response.responseText);
				var tar = me.down('cfSlider#thisInstance');
				var threshold = me.down('thresholdControlView#thisBlock');
				if(tar && threshold && !tar.isDirty && !threshold.isUpdating){
					tar.setValue(obj.instance);
				}
			},
			failure : function(response) {
				
			}
		});
		Ext.Ajax.request({
			url : 'rest/app/details',
			method : 'GET',
			params : {
				appName : me.thatName
			},
			success : function(response){
				var obj = Ext.JSON.decode(response.responseText);
				var tar = me.down('cfSlider#thatInstance');
				var threshold = me.down('thresholdControlView#thatBlock');
				if(tar && threshold && !tar.isDirty && !threshold.isUpdating){
					tar.setValue(obj.instance);
				}
			},
			failure : function(response) {
				
			}
		});		
		
		
		
		
		
	},
	initComponent : function() {
		this.items[0].items[0].title = this.thisName;
		this.items[0].items[1].title = this.thatName;

		
		this.callParent(arguments);
	}
});
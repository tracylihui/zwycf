Ext.define('cfWeb.view.appManage.AppManageView', {
    extend: 'Ext.container.Container',
    alias: 'widget.appManageView',
    overflowY: 'auto',
    overflowX: 'hidden',
    initComponent: function () {
        this.items = [{
            xtype: 'collapsibleBlock',
            title: cfWebLang.AppManageView.AppList,
            searchable: true,
            items: [{
                xtype: 'cbuttonIconListView',
                id: 'temp',
                detailList: [{
                    name: cfWebLang.AppManageView.RunStatus,
                    dataIndex: 'status',
                    unit: ''
                }, {
                    name: cfWebLang.AppManageView.InstanceNumber,
                    dataIndex: 'instance',
                    unit: ''
                }, {
                    name: [cfWebLang.AppManageView.Mem, cfWebLang.AppManageView.Disk],
                    dataIndex: ['memory', 'space'],
                    unit: ['G', 'G']
                }, {
                    name: cfWebLang.AppManageView.RunEnv,
                    dataIndex: 'runtime',
                    unit: ''
                }, {
                    name: cfWebLang.AppManageView.Service,
                    dataIndex: 'service',
                    unit: '',
                    renderer: function (v) {
                        var items = v.split(',');
                        var lth = items.length - 1;
                        var res = '';
                        for (var i = 0;
                             (i < lth) && (i < 4); i++) {
                            var type = items[i].split('_')[0].toLowerCase();
                            res += '<span class="service_sign ' + type + '" title="' + items[i] + '"></span>';
                        }
                        if (lth > 4) {
                            res += '<span class="service_more"></span>';
                        }
                        return res;
                    }
                }],


                menuList: [{
                    ico: 'icon-stop',
                    name: cfWebLang.Util.Stop,
                    act: 'stop'
                }, {
                    ico: 'icon-rotate2',
                    name: cfWebLang.Util.Restart,
                    act: 'reboot'
                }, {
                    ico: 'icon-play3',
                    name: cfWebLang.Util.Start,
                    act: 'boot'
                }, {
                    ico: 'icon-cross',
                    name: cfWebLang.Util.Delete,
                    act: 'delete'
                }, {
                    ico: 'menu3',
                    name: cfWebLang.Util.Detail,
                    act: 'details'
                }, {
                    ico: 'icon2-update',
                    name: cfWebLang.Util.Update,
                    act: 'update'
                }],
                store: Ext.create('cfWeb.store.appManage.AppListStore')
            }]
        },
            {
                xtype: 'collapsibleBlock',
                title: '审批应用',
                searchable: true,
                items: [{
                    xtype: 'cbuttonIconListViewII',
                    id: 'tempII',
                    detailList: [{
                        name: '应用名称',
                        dataIndex: 'appName',
                        unit: ''
                    },{
                        name: '用户',
                        dataIndex: 'userName',
                        unit: ''
                    },{
                        name: '应用状态',
                        dataIndex: 'status',
                        unit: ''
                    }, {
                        name: cfWebLang.AppManageView.InstanceNumber,
                        dataIndex: 'instance',
                        unit: ''
                    }, {
                        name: cfWebLang.AppManageView.Mem,
                        dataIndex: 'memory',
                        unit: ''
                    }],

                    //暂时取消这个菜单，有用再说~
                    /*menuList: [{
                        ico: 'plus22',
                        name: cfWebLang.Util.New,
                        act: 'new'
                    }, {
                        ico: 'icon-eye',
                        name: '查看详情',
                        act: 'details'
                    }, {
                        ico: 'icon-cross',
                        name: cfWebLang.Util.Delete,
                        act: 'delete'
                    }],*/
                    store: Ext.create('cfWeb.store.appManage.AppListStoreII')
                }]
            }
            //将复制一份原来的应用模板作为"系统模板"，系统模板只能继承，不能添加
            , {
                xtype: 'collapsibleBlock',
                title: cfWebLang.AppManageView.SysAppTemplate,
                searchable: true,
                items: [{
                    xtype: 'slbuttonIconListView',
                    addText: cfWebLang.Util.New,
                    buttonMenu: [
                        [{
                            ico: 'plus22',
                            name: cfWebLang.Util.New,
                            act: 'new'
                        }, {
                            ico: 'filter',
                            name: cfWebLang.Util.Filter,
                            act: 'filter'
                        }, {
                            ico: 'eye',
                            name: cfWebLang.Util.View,
                            act: 'detail'
                        }],
                        [{
                            ico: 'plus22',
                            name: cfWebLang.Util.New,
                            act: 'new'
                        }, {
                            ico: 'filter',
                            name: cfWebLang.Util.Filter,
                            act: 'filter'
                        }, {
                            ico: 'pencil3',
                            name: cfWebLang.Util.Edit,
                            act: 'edit'
                        }, {
                            ico: 'cross',
                            name: cfWebLang.Util.Delete,
                            act: 'delete'
                        }],

                    ],
                    store: Ext.create('cfWeb.store.appManage.SysAppTemplateStore')
                }]
            }

            //将原来的应用模板作为"我的模板
            , {
                xtype: 'collapsibleBlock',
                title: cfWebLang.AppManageView.AppTemplate,
                searchable: true,
                items: [{
                    xtype: 'lbuttonIconListView',
                    addText: cfWebLang.Util.New,
                    buttonMenu: [
                        [{
                            ico: 'plus22',
                            name: cfWebLang.Util.New,
                            act: 'new'
                        }, {
                            ico: 'filter',
                            name: cfWebLang.Util.Filter,
                            act: 'filter'
                        }],
                        [{
                            ico: 'plus22',
                            name: cfWebLang.Util.New,
                            act: 'new'
                        }, {
                            ico: 'filter',
                            name: cfWebLang.Util.Filter,
                            act: 'filter'
                        }, {
                            ico: 'pencil3',
                            name: cfWebLang.Util.Edit,
                            act: 'edit'
                        }, {
                            ico: 'cross',
                            name: cfWebLang.Util.Delete,
                            act: 'delete'
                        }],

                    ],
                    store: Ext.create('cfWeb.store.appManage.AppTemplateStore')
                }]
            }
        ];
        this.callParent(arguments);
    }
});
Ext.define('cfWeb.view.appManage.AppListDetailsView',{
	extend : 'cfWeb.cfWebComponent.SlideLayer',
	alias : 'widget.appListDetailsView',	
	resizable: false,
	overflowY:'hidden',
	overflowX:'hidden',
	initComponent : function(){
		this.items = [{
			xtype : 'appHeadView',
			id : 'appListDetails',
			detailList:[cfWebLang.AppManageView.RunStatus,cfWebLang.AppManageView.InstanceNumber,
						cfWebLang.AppManageView.MemDisk,cfWebLang.AppManageView.RunEnv,
						cfWebLang.AppManageView.Service,cfWebLang.AppManageView.InstanceStatus,
						cfWebLang.AppManageView.Domain ],
			actionList:{'add':cfWebLang.Util.New,
						'delete':cfWebLang.Util.Delete,
						'edit':cfWebLang.Util.Edit,
						'save':cfWebLang.Util.Save,
						'start':cfWebLang.Util.Start,
						'stop':cfWebLang.Util.Stop,
						'restart':cfWebLang.Util.Restart,
						'loading':cfWebLang.Util.Loading,
						'cancel':cfWebLang.Util.Cancel}
		},{
			xtype : 'container',
			overflowY:'auto',
			overflowX:'hidden',
			itemId:'detailContainer',
			items : [{
				xtype : 'cfPanelTypeI',
				title : cfWebLang.AppManageView.AppDistribution,
				cls:'cfPanelAlCenter',
				items:[
					{
					    xtype : 'button' ,
					   	text : cfWebLang.AppManageView.AppLog,
					    itemId : 'openLog' ,
					    cls:'viewLog'
					},
					{
						xtype : 'button',
						text : cfWebLang.LogFileWindow.LogAnalyse,
						itemId : 'analyLog',
						style:{
							margin:'0 0 -60px 25px',
							background:'#54b1f6'
						}
					},
					{
						xtype:'buttonListView',
						menuList:[{name:cfWebLang.Util.Today,act:'today',type:'text'},
						          {name:cfWebLang.Util.Yesterday,act:"yesterday",type:'text'},
						          {name:cfWebLang.Util.ThisWeek,act:"thisWeek",type:'text'},
						          {icoCls:'calendar',act:'calendar',type:'ico',multiClick:true}],
						width:1160,
						style:{
							margin:"10px auto"
						}
					},{
						xtype : 'cfChartTypeI',
						store : 'appManage.AppHistStore',
						itemId : 'hist',
						title:cfWebLang.AppManageView.DelayHistory
						
					},{
						xtype : 'cfChartTypeI',
						store : 'appManage.AppAccessStore',
						itemId : 'access',
						title:cfWebLang.AppManageView.VisitTime
					}
						
				]
			},{
				xtype : 'cfPanelTypeI',
				title : cfWebLang.AppManageView.AppInstance ,
				itemId:'appIns',
				cls:'appIns',
				layout:{
					type: 'column'
//					align:'middle'
				},
				items:[{
					  xtype:'thresholdControlView',
					  itemId:'detailThreshold',
					  flex:1,
					  margin:'30 80 10 30',
					  title:cfWebLang.AppManageView.Autoscaling,
						 items:[{
				            	xtype:'cfSlider',
				            	itemId:'instanceSlider',
				            	title:cfWebLang.AppManageView.InstanceNumber,
				            	maxValue:20
				            },
				            {
				            	xtype:'cfSlider',
				            	itemId:'memorySlider',
				            	maxValue:16,
				            	fixedPoints:[0.5,1,2,4,8,16],
				            	increment:0.5,
				            	title:cfWebLang.Util.Memory
				            },{
				            	xtype:'cfMultiSlider',
				            	itemId:'cpuMultiSlider',
				            	title:'CPU',
				            	values:[0,100],
				            	hidden:true
				            },{
				            	xtype:'cfMultiSlider',
				            	itemId:'instanceMultiSlider',
				            	title:cfWebLang.AppManageView.InstanceNumber,
				            	values:[0,20],
				            	maxValue:20,
				            	minValue:1,
				            	hidden:true
				            }]
					},{
				  xtype:'switchContentView',
				  itemId:'switchChart',
				  flex:2,
				  items:[{
				      xtype:'instanceStatusView',
				      ChartTypeList : [cfWebLang.Util.Disk,cfWebLang.Util.Memory]
				  }]
				}]
			},{
				xtype : 'cfPanelTypeI',
				title : cfWebLang.AppManageView.ServiceBindManage,
				items : [{
					xtype : 'serviceDetail',
					store:Ext.create('cfWeb.store.appManage.AppServiceListStore'),
					itemId : 'appServiceDetail',
					detailList:[cfWebLang.AppManageView.Plan,cfWebLang.AppManageView.ServiceType,cfWebLang.AppManageView.CreateTime,cfWebLang.AppManageView.UpdateTime]
				}]
			},{
				xtype : 'cfPanelTypeI',
				title : cfWebLang.AppManageView.EnvironmentVar,
				items : [{
					xtype : 'variableListView',
					itemId : 'appVarEnv',
					langSetting:{key:cfWebLang.AppManageView.EnvVarName,value:cfWebLang.AppManageView.EnvVarValue,save:cfWebLang.Util.Save,cancel:cfWebLang.Util.Cancel},
					store :Ext.create('cfWeb.store.appManage.AppDetailsEnvVarStore')
				}]
			},/*{
				xtype : 'cfPanelTypeI',
				title : cfWebLang.AppManageView.AppUpdate,
				items : [{
					xtype : 'form',
					layout:{
						type: 'column'
					},
					margin:'30 80 10 30',
					border:0,
					items : [{
						xtype : 'filefield',
						itemId : 'uploadFile',
						labelWidth:170,
    					name: 'uploadFile',
    					allowBlank  : false,
    					emptyText :cfWebLang.CfWindow.UploadFileTip,
    					hidden : false,
    					width : 400,
					},{
		    			xtype : 'hiddenfield',
		    			name : 'appName'
		    		},{
						xtype : 'displayfield',
						value : cfWebLang.CfWindow.InputFilename,
						itemId: 'fileTip',
						hidden : false,
						fieldStyle :{
							"margin-left" : '5px',
							color : "red"
						},
						hidden : true
					},{
						xtype : 'button',
						text:cfWebLang.Util.Confirm,
						action:'updateAppWar'
					}]
				}]
			}*/]
		}];
		this.callParent(arguments);
		this.listeners={
				'boxready' : function(){
					var hHeight = this.items.items[0].getHeight();
					var height = this.getHeight();
					var bHeight = height - hHeight;
					this.items.items[1].setHeight(bHeight);
					this.on('resize',function(me, width, height, eOpts){
						var content = this.items.items[1];
						var newHeight = height - 180;
						content.setHeight(newHeight);
					},this);
				}
		};
	}
});

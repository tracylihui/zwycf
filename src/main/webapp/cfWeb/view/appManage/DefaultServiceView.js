Ext.define('cfWeb.view.appManage.DefaultServiceView',{
	extend : 'Ext.container.Container',
	alias  : 'widget.defaultService',
	title:undefined,
	store:undefined,
	layout:'hbox',
	cls:'addDB',
	initComponent : function(){
		this.callParent(arguments);
		this.addEvents({
			'onItemAdd':true
		});
		this.items =[/*{
	      	 xtype:'button',
	    	 cls:'addDB',
	    	 height:30,
	    	 width:200,
	    	 margin:'8,10,0,0',
	    	 text:'<div class="addBox icon-plus22 barTools"></div><span class="addDbText">'+cfWebLang.AppManageView.Create+this.title+'</span>',
	    	 handler:function(button,e){	
		    	var me = button.up('defaultService');
		    	me.fireEvent('onItemAdd',me);
		    }
	     },*/{
	    	 margin:'10,0,0,0',
	    	 itemId:'default_service_comb',
	    	 xtype:'combobox',
			fieldLabel: "选择实例",
			labelAlign: 'right',
	    	 height:28,
	    	 store :this.store,
             cls:'appPanel-input',
//             fieldLabel:cfWebLang.Util.ServiceInstance,
//             labelAlign:'right',
             emptyText: cfWebLang.Util.PleaseChoose,
             mode:'local',
             queryMode: 'local',
             triggerAction: 'all',
             valueField : 'title',
             displayField : 'title',
	     }];
		this.callParent(arguments);
	}
});
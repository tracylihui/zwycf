Ext.define('cfWeb.view.appManage.AppAccessView',{
	extend	: 'Ext.panel.Panel',
	alias	: 'widget.appAccessView',
	layout : 'column',
	border : '20px',

	initComponent : function() {
		this.callParent(arguments);
	},
	items:[{
			xtype : 'cfChartTypeI',
			store : 'appManage.AppInstStore'
		},{
			xtype : 'cfChartTypeI',
			store : 'appManage.AppInstStore'
	}],

    tbar : {
		cls : 'x-panel-header',
		style:{
			border:'1px solid #999',
			'margin':'30px 0px 0px 40px',
				'font-size':'20px'
		},
		items : [ {
				xtype:'button',
				disabled:true,
				text:cfWebLang.Util.Today,
				padding:10,
			},{
				xtype:'button',
				disabled:true,
				text:cfWebLang.Util.Yesterday,
				padding:10,
			} ,{
				xtype:'button',
				disabled:true,
				text:cfWebLang.Util.ThisWeek,
				padding:10,
			},{
				xtype:'button',
				disabled:true,
				text:cfWebLang.Util.SelectDate,
				padding:10,
			}]

	}

});

Ext.define('cfWeb.view.appManage.AppTemplateDetailView', {
    extend: 'cfWeb.cfWebComponent.CfWindow',
    alias: 'widget.appTemplateDetailView',
    title: cfWebLang.AppManageView.UserDefinedTemplate,
    langSet: null,
    title:'查看模板',
    showDock: false,
    width: 550,
    height: 340,
    initComponent: function() {
        this.items = [{
            xtype: 'container',
            width: 550,
            height: 340,
            padding: '0 10',
            defaults: {
                labelWidth: 120,
                cls: 'appPanel-input',
            },
            items: [{
                xtype: 'form',
                itemId: 'appForm',
                width: 550,
                //				height : 150,
                border: true,
                bodyStyle: 'background:#fff;',
                //				bodyStyle: 'background:#fff; padding:10px; border-color:#000',
                margin: '20 0 0 0',
                defaults: {
                    labelAlign: 'right',
                    labelWidth: 170,
                    width: 450
                },
                items: [
                    {
                        xtype: 'displayfield',
                        id: 'appTextfield',
                        name: 'appTextfield',
                        readOnly: true,
                        fieldLabel: cfWebLang.AppManageView.AppContainer
                    }, {
                        xtype: 'displayfield',
                        id: 'storageTextfield',
                        name: 'storageTextfield',
                        readOnly: true,
                        fieldLabel: cfWebLang.HomeTabView.dataStorageService
                    }, {
                        xtype: 'displayfield',
                        id: 'analyseTextfield',
                        name: 'analyseTextfield',
                        readOnly: true,
                        fieldLabel: cfWebLang.HomeTabView.dataAnalyseService
                    }, {
                        xtype: 'displayfield',
                        id: 'serviceTextfield',
                        name: 'serviceTextfield',
                        readOnly: true,
                        fieldLabel: cfWebLang.HomeTabView.appService
                    }, {
                        xtype: 'displayfield',
                        id: 'domainTextfield',
                        name: 'domainTextfield',
                        readOnly: true,
                        fieldLabel: '域名'
                    }, {
                        xtype: 'displayfield',
                        id: 'memoryTextfield',
                        name: 'memoryTextfield',
                        readOnly: true,
                        fieldLabel: '内存大小'
                    }, {
                        xtype: 'displayfield',
                        id: 'instanceTextfield',
                        name: 'instanceTextfield',
                        readOnly: true,
                        fieldLabel: '实例数量'
                    }, {
                        xtype: 'displayfield',
                        id: 'temNameTextfield',
                        name: 'temNameTextfield',
                        readOnly: true,
                        fieldLabel: '模板名称'
                    }
                ]
            }
            ]
        }];
        this.callParent(arguments);
    }
});

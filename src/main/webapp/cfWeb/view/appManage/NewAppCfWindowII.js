Ext.define('cfWeb.view.appManage.NewAppCfWindowII',{
    extend : 'cfWeb.cfWebComponent.CfWindow',
    alias  : 'widget.newAppCfWindowII',
    title : cfWebLang.Util.NewApp ,
    initComponent : function(){
        this.callParent(arguments);
    }
})
Ext.define('cfWeb.view.appManage.NewBindServiceWindow',{
	extend : 'cfWeb.cfWebComponent.CfWindow',
	alias  : 'widget.newBindServiceWindow',
	title: cfWebLang.Util.bindService ,
	langSet:{"next":"下一步","prev":"上一步","finish":cfWebLang.Util.Finish},
	autoHeight : true,
	width :600,
	initComponent : function(){		
		this.items=[
		            {
		            	xtype:'form',
		            	itemId : 'form',
		            	cls:'style-appPanel',
		            	items:[
								{
									xtype : 'hiddenfield',
									name : 'appName'
								},
		            	       {
		 		            	xtype:'fieldset',
		 		            	itemId : 'combo_field',
		 		            	border : 0,
		 		                title: 'logServiceConfig',
		 		                cls:'style-appPanel',
		 		                layout: 'hbox',
		 		                items :[/*{
		 		                	  xtype:'combobox',
		 		                	  itemId : 'midCombo',
		 		                	  editable:false,
		 		                	  emptyText:cfWebLang.Util.PleaseChoose,
		 		                	  cls:'appPanel-input',
		 		                	  margin:'0 10 0 0',
		 		                	  store:Ext.create('Ext.data.Store', {
		 		             			 fields:['id','midware'],
		 		             		    data : [
		 		             		            {'id':1,'midware':cfWebLang.HomeTabView.dataStorageService},
		 		             		            // {'id':2,'midware':cfWebLang.HomeTabView.dataAnalyseService},
		 		             		            // {'id':3,'midware':cfWebLang.HomeTabView.appService}
		 		             		            ]
		 		                	  }),
			 		                   displayField: 'midware',
			 		                   valueField: 'id',
	//		 		                   value : 1,
			 		                   listeners:{
			 		                	   select:function(me){
			 		                		   console.log('midware selected');
			 		                		   var win=me.up('newBindServiceWindow');
			 		                		   var serCombo=win.down('combobox[itemId=serCombo]');
			 		                		   serCombo.store.proxy.url='rest/service/listall?type='+me.getValue();
			 		                		   serCombo.setValue('');
			 		                		  var form=me.up('newBindServiceWindow').down('form[itemId=form]');
			 		                		  if(form.items.length==4)
			 		                			  form.remove(form.items.items[2]);
			 		                		   serCombo.store.load({callback:function(records){
			 		                			   if(records.length>0){
			 		                				  serCombo.setValue(records[0].get('serName'));
			 		                				  serCombo.fireEvent('select',serCombo);
			 		                			   }
			 		                			  
			 		                		   }});
			 		                	   }
			 		                   }
		 		                },*/
		 		                {
		 		                	xtype:'combobox',
		 			            	itemId : 'serCombo',
		 			            	editable:false,
									fieldLabel: "选择服务",
									labelAlign: 'right',
		 			            	cls:'appPanel-input',
		 			            	store : Ext.create('cfWeb.store.appManage.ServiceStore'),
		 			  				 displayField : 'serName_cfWebLang',
		 			  				 valueField : 'serName',
		 			  				 emptyText:cfWebLang.Util.PleaseChoose,
		 			  				 listeners:{
		 			  					 select:function(me){
			 		      		 			var form=me.up('newBindServiceWindow').down('form[itemId=form]');
			 		      		 			var oldHeight=form.getHeight();
			 		      		 			var adjustHeight=0;
			 		      		 			form.remove(form.items.items[2],true); 
		 			  						var value=me.getValue();
		 			  						 if(value=='logService'){
		 			  							if(form.items.length==4)
				 		                			  form.remove(form.items.items[2]);
		 			  							var logview=Ext.create('cfWeb.view.appManage.NewAppAnaLog',{
		 			  								 itemId : 'appAnaLog'
		 			  							 });
		 			  							 form.insert(2,logview);
		 			  						 }/*else if(value=='Oracle'||value=='MySQL'){
		 			  							 var store=me.getStore();
		 			  							 var serivceId=store.findRecord("serName",value).get("id");
		 			  							if(form.items.length==4)
				 		                			  form.remove(form.items.items[2]);
		 			  							/!* var oracleview=Ext.create('cfWeb.view.appManage.ConfigOracle',{
		 			  								 itemId : 'ConfigOracle',
		 			  								 title:value
		 			  							 });*!/
		 			  							var oracleview=Ext.widget(value,{title:value,store:Ext.create('cfWeb.store.dataStorage.DatabaseServiceSelectStore')});
		 			  							oracleview.store.load({params:{first:1,second:serivceId}});
		 			  							form.insert(2,oracleview);
		 			  							adjustHeight=60;
		 			  						 }*/else{
		 			  							 var record=me.getStore().findRecord("serName",value);
		 			  							var broker=record.get("brokers")[0];
		 			  							if(broker!=null){
		 			  								var defaultView=Ext.create('cfWeb.view.appManage.DefaultServiceView',{
			 			  								 itemId : 'defaultView',
			 			  								 title:value,
			 			  								 brokers:broker,
			 			  								 prefix:record.get("serPrefix"),
			 			  								 serviceId:record.get("id"),
			 			  								 store:Ext.create('cfWeb.store.dataStorage.DatabaseServiceSelectStore')
			 			  							 });
		 			  								defaultView.store.load({params:{second:record.get("id")}});
			 			  							form.insert(2,defaultView);
		 			  							}
		 			  						 }
	 			  							var height=me.up('newBindServiceWindow').getHeight()+(form.getHeight()-oldHeight);
	 			  							me.up('newBindServiceWindow').setHeight(height+adjustHeight);
		 			  					 }
		 			  				 }
		 		                }]
		 		            }]
		            }
		           
		            ];
		this.callParent(arguments);
	}
})

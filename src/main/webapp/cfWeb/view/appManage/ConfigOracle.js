Ext.define('cfWeb.view.appManage.ConfigOracle',{
	extend : 'Ext.container.Container',
	alias : ['widget.configOracle','widget.Oracle','widget.MySQL'],
	title:undefined,
	autoHeight : true,
	html:'<p class="errorMsg" style="color:#f00;text-align:center;font-size:16px;display:none">请添加数据源</p>',
	initComponent : function(){	
		this.addEvents({
			'grow':true,
			'shrink':true,
			'onItemAdd':true
		});
		this.items =[
		             {
		            	 xtype:'button',
		            	 cls:'addDB',
		            	 height:30,
		            	 width:140,
		            	 text:'<div class="addBox icon-plus22 barTools"></div><span class="addDbText">'+cfWebLang.AppManageView.Create+this.title+'</span>',
		            	 handler :this.onItemAdd
		             },
		             {  layout:'column',
		                items:[{
		                   xtype : 'checkboxgroup',
		     			cls:'style-cfCheck',
		     			columns : 1,
		     			margin:'0 0 0 16',
		     			items :[
		     				{xtype : 'checkbox',boxLabel :cfWebLang.AppManageView.MappingDB,checked: true,listeners:{
		     					change:function(me, newValue, oldValue, eOpts){
		     						var radio = me.up('configOracle').down('radiogroup');
		     						var grid = me.up('configOracle').down('grid');
		     						if(newValue){
		     							radio.show();
		     							grid.columns[0].show();
		     						}
		     						else{
		     							radio.hide();
		     							grid.columns[0].hide();
		     						}
		     					}
		     				}}
		     			]
		                },{
		            	 xtype:'label',
		            	 itemId:'tipAdd',
		            	 text:cfWebLang.Error.chooseDatabase,
		            	 margin:'0 0 0 10',
		            	 style:{
		            		 color:'#f00',
		            		 'text-align':'center'
		            	 },
		            	 hidden:true
		            	 
		             },
		             {
		             	 xtype:'label',
		             	 itemId :'tipText',
		            	 text:cfWebLang.Error.DatasourceName,
		            	 margin:'0 0 0 10',
		            	 style:{
		            		 color:'#f00',
		            		 'text-align':'center'
		            	 },
		            	 hidden:true
		             },{
		             	 xtype:'label',
		            	 text:cfWebLang.Error.Datasource,
		            	 itemId:'tipCombo',
		            	 margin:'0 0 0 10',
		            	 style:{
		            		 color:'#f00',
		            		 'text-align':'center'
		            	 },
		            	 hidden:true
		             },{
		             	 xtype:'label',
		            	 text:cfWebLang.Error.DuplicateDatasource ,
		            	 itemId:'tipServiceConflict',
		            	 margin:'0 0 0 10',
		            	 style:{
		            		 color:'#f00',
		            		 'text-align':'center'
		            	 },
		            	 hidden:true
		             }]
		     			
		     		},
		     		{
		     	        xtype: 'radiogroup',
		     	        cls:'style-cfRadio',
		     	        columns: 1,
		     	        vertical: true,
		     	       margin:'0 0 0 26',
		     	        items: [
		     	            { boxLabel: cfWebLang.AppManageView.Manual, name:'temp', inputValue: false ,checked:true},
		     	            { boxLabel: cfWebLang.AppManageView.Auto , name:'temp',inputValue: true, disabled: true}
		     	      
		     	        ]
		     	    },
		             {
		            	xtype:'grid',
	            		store: Ext.create('cfWeb.store.appManage.StorageStore'),
	            		cls:'account-setting',
	            	    plugins: [
	            	        Ext.create('Ext.grid.plugin.CellEditing', {
	            	            clicksToEdit: 1
	            	        })
	            	    ],
	            	    listeners:{
	            	    	'render':function(grid){
	            	    		grid.getStore().removeAll();
	            	    	}
	            	    },
	            	    columns:[
	            	             {
	            	                 header :cfWebLang.AppManageView.AppDataSource ,
	            	                 dataIndex : 'appSource',
	            	                 name:'appSource',
	            	                 flex : 1,
	            	                 editor:new Ext.form.field.Text({
	            	                     emptyText:cfWebLang.AppManageView.InputDataSourceName,
                                         listeners:{
                                             'focus':function(me){
                                             	if(me.getValue()==cfWebLang.AppManageView.InputDataSourceName){
                                             		 me.reset();
                                             	}  
                                              }
                                          }
	            	                 })
	            	             },{
	            	                 header:cfWebLang.AppManageView.CloudSource ,
	            	                 dataIndex:'cloudSource',
	            	                 sortable:false,
	            	                 editor:Ext.create('widget.combobox',{
	            	                	 store :this.store,
	            	                     cls:'appPanel-input',
	            	                     emptyText: cfWebLang.Util.PleaseChoose,
	            	                     mode:'local',
	            	                     queryMode: 'local',
	            	                     triggerAction: 'all',
	            	                     valueField : 'title',
	            	                     displayField : 'title',
	            	                     editable: false
	            	                 }),
	            	                 flex : 1
	            	             },{
	            	   			  header :cfWebLang.Util.Operate,
	            			      xtype : 'actioncolumn',
	            			      align : 'center',
	            			      items : [{
	            			    	  			icon : 'resources/images/icon_delete.png',
	            			    	  			iconCls:'size20',
					            		      	tooltip :cfWebLang.Util.Delete
	            			      }],
	            			      handler : function(grid,rowIndex,colIndex,item,e,record,row){
	            			    	  var view = this.up('configOracle');
	            			    	  view.fireEvent('shrink',view,-1);
	            			    	  var store = grid.getStore();
	            			    	  store.remove(record);
	            			      }
	            	             }
	            	         ],
	            	 dockedItems: [{
	            	             xtype: 'toolbar',
	            	             dock: 'bottom',
	            	             items: ['->',{
	         		     	     xtype: 'button',
	       		            	 cls:'addDB',
	       		            	 height:30,
	       		            	 width:90,
	       		            	 text:'<div class="addBox icon-plus22 barTools"></div><span class="addDbText">'+cfWebLang.Util.Add+'</span>',
	       		            	 handler:function(me){
	       		            		console.log(me.up('configOracle'));
	       		            		 var data = {'appSource':cfWebLang.AppManageView.InputDataSourceName ,'cloudSource' :cfWebLang.AppManageView.ChooseSource};
	       		            		 var store = this.up('configOracle').down('grid').getStore();
	       		            		 var view = me.up('configOracle');
	       		            		 view.fireEvent('grow',view,1);
	       		            		 store.add(data);
	       		            	 }
	            	             }]
	            	       }]
		             }
		             ];
//		this.columns=  [
//			                    { header: '应用数据源',  dataIndex: 'appSource',flex : 1 },
//			                    { header: '云平台数据源', dataIndex: 'cloudSource', flex: 1 }
//			                ],
		this.callParent(arguments);
	},
	getValues : function(){
		var me = this;
		var checkbox = me.items.items[1].items.items[0].items.items[0];
		console.log(checkbox);
		var radio = me.items.items[2];
		var grid = me.items.items[3];
		var isMapping  = checkbox.getValue();
		var isAuto = radio.getValue();
		var store = grid.getStore();
		var lth = store.data.items.length;
		var array = [];
//		var env=Ext.getCmp('appVarEnv');
		var appSource = [];
		var service = [];
		for(var i = 0;i<lth;i++ ){
			appSource.push(store.data.items[i].data.appSource);
			service.push(store.data.items[i].data.cloudSource);
		}
//		console.log(env);
//		env.store.add(array);
		var res = {};
		console.log(isAuto);
		res.isMapping = isMapping;
		res.isAuto = isAuto.temp;
		res.appSource = appSource;
		res.service =service;
		res.serName = this.title.toLowerCase();
		return res;
	},
	onItemAdd: function(button,e){
		var me = button.up('configOracle');
		me.fireEvent('onItemAdd',me);
	},
	validate : function(){
		var me = this;
		var grid = me.items.items[3];
		var store = grid.getStore();
		var lth = store.data.items.length;
		if(lth == 0){
			this.showMsg(cfWebLang.Error.AddDB);
			return false;
		}
		else{
			return true;
		}
	},
	validateServiceRepeat : function(){
		this.hideMsg();
		var me = this;
		var grid = me.items.items[3];
		var store = grid.getStore();
		var lth = store.data.items.length;
		var data=store.data.items;
		for(var i=0;i<lth;i++){
			for(var j=0;j<lth;j++){
				if(data[j].data.cloudSource!=cfWebLang.AppManageView.ChooseSource){
					console.log(data[j].data.cloudSource);
				  if(i!=j&&data[i].data.cloudSource==data[j].data.cloudSource){
					 var view = me.items.items[1];
					 var serviceT = view.down('label[itemId=tipServiceConflict]');
					 serviceT.setText(cfWebLang.Error.DuplicateDatasource);
					 serviceT.setVisible(true);
					 return false;
				}
			}
			}
		}
		return true;
	},
	showMsg : function(msg){
		var me = this;
		var view = me.items.items[1];
		var tipAdddb = view.down('label[itemId=tipAdd]');
		var tipT = view.down('label[itemId=tipText]');
		var tipC = view.down('label[itemId=tipCombo]');
		var serviceT = view.down('label[itemId=tipServiceConflict]');
		serviceT.setVisible(false);
		tipT.setVisible(false);
		tipC.setVisible(false);
		tipAdddb.setVisible(true);
		return false;
	},
	hideMsg : function(){
		var me = this;
		var view = me.items.items[1];
		var tipAdddb = view.down('label[itemId=tipAdd]');
		tipAdddb.setVisible(false);
		var serviceT = view.down('label[itemId=tipServiceConflict]');
		serviceT.setVisible(false);
		var tipT = view.down('label[itemId=tipText]');
		tipT.setVisible(false);
		var tipC = view.down('label[itemId=tipCombo]');
		tipC.setVisible(false);
	},
	validateText : function(){
		this.hideMsg();
		var me = this;
		var view = me.items.items[1];
		var tipT = view.down('label[itemId=tipText]');
		var tipC = view.down('label[itemId=tipCombo]');
		tipT.setText(cfWebLang.Error.DatasourceName);
		tipT.setVisible(true);
		tipC.setVisible(false);
		return false;
	},
	validateCombo : function(){
		this.hideMsg();
		var me = this;
		var view = me.items.items[1];
		var tipCom = view.down('label[itemId=tipCombo]');
		var tipText = view.down('label[itemId=tipText]');
		tipCom.setVisible(true);
		tipText.setVisible(false);
		return false;
	},
	
	validateEnvNameRepeat : function(){
		this.hideMsg();
		var me = this;
		var view = me.items.items[1];
		var tipT = view.down('label[itemId=tipText]');
		var tipC = view.down('label[itemId=tipCombo]');
		tipT.setText(cfWebLang.Error.DbEnvNameRepeat);
		tipT.setVisible(true);
		tipC.setVisible(false);
		return false;
	},
	
	validateSourceNameRepeat : function(){
		this.hideMsg();
		var me = this;
		var grid = me.items.items[3];
		var store = grid.getStore();
		var lth = store.data.items.length;
		var data=store.data.items;
		for(var i=0;i<lth;i++){
			for(var j=0;j<lth;j++){
				console.log(data[j].data);
				if(data[j].data.appSource!=cfWebLang.AppManageView.InputDataSourceName){
					console.log(data[j].data.appSource);
				  if(i!=j&&data[i].data.appSource==data[j].data.appSource){
					 var view = me.items.items[1];
					 var serviceT = view.down('label[itemId=tipServiceConflict]');
					 serviceT.setText(cfWebLang.Error.DuplicateDatasourceName);
					 serviceT.setVisible(true);
					 return false;
				}
			}
			}
		}
		return true;
	}

});

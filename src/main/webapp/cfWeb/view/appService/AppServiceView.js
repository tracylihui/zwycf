Ext.define('cfWeb.view.appService.AppServiceView',{
	extend : 'Ext.container.Container',
	alias : 'widget.appServiceView',
	overflowY : 'auto',
	
	initComponent : function(){
		this.items = [{
			xtype : 'collapsibleBlock',
			title : cfWebLang.AppServiceView.AppServiceTemplate,
			searchable : true,
			items : [{
				xtype : 'lbuttonIconListView',
				isAddable:false,
				buttonMenu : [[],
								[{
									ico : 'plus22',
									name : cfWebLang.Util.New,
									act : 'new'
								},{
									ico : 'filter',
									name : cfWebLang.Util.Filter,
									act : 'filter'
								}]
				              
			 ],
				store : Ext.create('cfWeb.store.appService.AppServiceStore')
			}]
		},{
			xtype : 'collapsibleBlock',
			title : cfWebLang.AppServiceView.AppServiceList,
			searchable : true,
			items : [{
				xtype : 'cbuttonIconListView',
				id : 'dataserverlist',
				readyStatus:1,
				detailList:[
				            {name:cfWebLang.AppManageView.Plan,dataIndex:'plan', unit:''},
				            {name:cfWebLang.AppManageView.ServiceType,dataIndex:'label', unit:''}
//				            {name:cfWebLang.DataStorageView.DatabaseStatus,dataIndex:'', unit:''},
//				            {name:cfWebLang.DataStorageView.TableSpace,dataIndex:'', unit:''},
//				            {name:cfWebLang.DataStorageView.TempTableSpace, dataIndex:'', unit:''},
//				            {name:cfWebLang.DataStorageView.CreateDate, dataIndex:'', unit:''}
				            ],
				menuList :[{
							ico:'cross',
							name:cfWebLang.Util.Delete ,
							act:'deleteService'
						}],
				store : Ext.create('cfWeb.store.appService.AppServiceListStore')
			}]
		}];
		this.callParent(arguments);
	}
});

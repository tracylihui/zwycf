Ext.define('cfWeb.view.appService.NewAppServiceView',{
    extend : 'cfWeb.cfWebComponent.CfWindow',
	alias : 'widget.newAppServiceView',
	store : Ext.create('cfWeb.store.appService.NewAppServiceStore'),
	title:'新建',
	langSet:{"finish":cfWebLang.Util.Finish},
	style : {
		float : 'left'
	},
	items : [{
		xtype : 'container',
		width : 350,
	    height : 80,
	    padding:'0 10',
		defaults:{
			labelWidth:80,
			cls:'appPanel-input'
		},
		items : [{
			xtype : 'form',
			itemId : 'databaseForm',
			width : 350,
			border : true,
			bodyStyle: 'background:#fff;',
			margin : '20 0 0 0',
			defaults:{
				labelAlign:'right',
				labelWidth:120,
				width : 350
			},
			items:[{
			   xtype:'combobox',
			   name:'service',
			   itemId:'serviceName',
			   fieldLabel:'service' ,
			   store:'appService.NewAppServiceStore',
			   queryMode : 'local',
			   displayField:'label',
			   valueField:'label',
			   emptyText:cfWebLang.Util.ChooseService,
			   labelAlign:'right',
			   editable : false
			},{
			   xtype:'combobox',
			   name:'plan',
			   itemId:'servicePlan',
			   queryMode : 'local',
			   store:'appService.NewAppBrokersPlanStore',
			   fieldLabel:'servicePlan',
			   displayField:'name',
			   valueField:'name',
			   labelAlign:'right',
			   editable : false
			}]
		}]

	}],
	initComponent : function(){
		console.log('window opened');
		
		this.callParent(arguments);
	}
});
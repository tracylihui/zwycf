Ext.define('cfWeb.view.messageManage.Messages',{
	extend: 'Ext.grid.Panel',
	alias : 'widget.messages',
	store : Ext.create("cfWeb.store.message.MessageStore"),
	cls:"account-setting",
	initComponent : function(){
		this.store.load();
		
		this.columns = [
               {header :cfWebLang.Util.appName , dataIndex:'fileName',    align : 'center',      flex:1},
               {  header :cfWebLang.Util.ErrorCount, 
                  dataIndex:'errorCount', 
                  renderer: function(value){
				if(value>0){
					return '<div class="couunt_head"><div>Error<span class="log_noti_count">'+value+'</span></div></div>';
				}else{
					return '<div class="couunt_head"><div>Error</div></div>';
				}
				 
			   },
                  align : 'center',   
                  flex:1
                },
               {  
                 header :cfWebLang.Util.exceptionCount , 
                 dataIndex:'exceptionCount',
                 renderer: function(value){
				if(value>0){
					return '<div class="couunt_head"><div>Exception<span class="log_noti_count">'+value+'</span></div></div>';
				}else{
					return '<div class="couunt_head"><div>Exception</div></div>';
				}
			   },
                 align : 'center',  
                 flex:1},
               {
                  header :cfWebLang.Util.details ,
                  xtype : 'actioncolumn',
                  icon : 'resources/images/information-balloon.png',
                  handler : function(me,rindex){
	      		    
		      		var store=me.getStore();	
		      		var record=store.getAt(rindex);
		      		var fileName=record.data.fileName;
		      		console.log(fileName);
		      		
		      		var appdetailWin=me.up("messagesWindows");
		      		
		            var appdetailWinH=appdetailWin.getHeight();
		            var win=Ext.widget("logFileWindow",{
			        blackHeight:appdetailWinH-10,
			        height:appdetailWinH-10,
			        width:appdetailWin.getWidth(),
			        adaptTo:appdetailWin,
			        header:{
				      border:false,
				      shadow:false
			        }
		            });
		               win.appName=record.data.fileName;
		               win.slideUp();
		               
		               Ext.Ajax.request({
		       			url : 'rest/log/markRead',
		       			method : 'GET',
		       			params :{
		       				appName : fileName,
		       				},
		       				success: function(){
		       					store.reload();
		       				}
		       			});
		               
	      	      },
                  align : 'center',
		          flex:1
		         
	           }
        ];
        
        this.callParent(arguments);
	}
});


		     
		    
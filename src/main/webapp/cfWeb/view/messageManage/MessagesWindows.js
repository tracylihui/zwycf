Ext.define('cfWeb.view.messageManage.MessagesWindows',{
	alias : 'widget.messagesWindows',
	extend : 'Ext.window.Window',
	autoScroll:true,
	title : cfWebLang.Util.Message,
	layout : 'fit',
	
	height:400,
	width:1000,
	cls: 'select-cls',
	items:[{
	       xtype: 'messages'
	}],
	
	initComponent : function(){
        this.callParent(arguments);
	}
});
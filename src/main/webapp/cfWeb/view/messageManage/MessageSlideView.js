Ext.define('cfWeb.view.messageManage.MessageSlideView',{
	extend: 'cfWeb.cfWebComponent.SlideLayer',
	alias : 'widget.messageSlideView',
	cls:'messageSlide',
	bodyStyle: {
	    background: '#364860'
	},
	overflowX:'hidden',
	overflowY:'hidden',
	initComponent : function(){
		this.items = [{
			xtype:'messageList',
			store: Ext.create("cfWeb.store.message.MessageStore"),
			title:cfWebLang.Util.message 
		}];
        this.callParent(arguments);
	}
});


		     
		    
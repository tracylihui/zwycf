Ext.define('cfWeb.view.messageManage.MessageList', {
			extend : 'Ext.view.View',
			alias : 'widget.messageList',
			loadingStatus:0,
			readyStatus:2,
			title:'消息中心',
			loadMask:false,
			selfId : undefined,
			tpl:[
			     '<div class="message_center">',
			     	'<h3 class="message_head">',
			     	'',
			     	'</h3>',
			     	'<ul class="message_list">',
			     		'<tpl for=".">',
			     			'<li class="message_item" data-name="{fileName}" data-err="{errorCount}" data-exp="{exceptionCount}">',
			     				'<span class="message_title">{fileName}</span>',
			     				'<span class="message_error',
			     				'<tpl if="errorCount &lt; 1">',
			     				' dis_n',
			     				'</tpl>',
			     				'">{errorCount}</span>',
			     				'<span class="message_exception',
			     				'<tpl if="exceptionCount &lt; 1">',
			     				' dis_n',
			     				'</tpl>',
			     				'">{exceptionCount}</span>',
			     			'</li>',
			     		'</tpl>',
			     	'</ul>',
			     '</div>'
			     ],
			initComponent : function() {
				this.tplInit();
				this.addEvents({
					'onItemsClick':true,
				});
				this.store.on('load',this.loadInit,this);
				this.callParent(arguments);
			},
			tplInit : function(){
				this.tpl[2] = this.title;
			},
			loadInit : function(){
				this.loadingStatus ++;
				if(this.loadingStatus >= this.readyStatus){
					this.eventBind();
					this.heightAdj();
				}
			},
			heightAdj : function(){
				var me = this;
				var height = this.up('messageSlideView').getHeight();
				var head = Ext.select("div[id='"+me.selfId+"'] h3.message_head").elements[0];
				var h = head.offsetHeight;
				var list = Ext.select("div[id='"+me.selfId+"'] ul.message_list").elements[0];
				var lHeight = height- h;
				list.setAttribute("style","height:"+lHeight +"px");
			},
			eventBind : function(){
				var me = this;
				var items = Ext.select("div[id='"+me.selfId+"'] li.message_item");
				items.on('click',me.itemClick,me);
			},
			listeners:{
				viewready : function(){
					var me = this;
					me.selfId = me.getEl().id;
					this.loadInit();
				
				}
			},
			itemClick : function(e,dom){
				console.log(e);
				console.log(dom);
				var li = this.getTarNode(dom, 'data-name');
				var appName = li.getAttribute('data-name');
				this.fireEvent('onItemsClick',this,appName);
				Ext.Ajax.request({
					url : 'rest/log/markRead',
		       		method : 'GET',
		       		params :{
		       			appName : appName,
		       			},
		       			success: function(){
		       				store.reload();
		       			}
		       		});
			},
			getTarNode :function(tar,tag){
			    if(tar.tagName ==undefined){
			      return false;
			    }
			    var res = tar.getAttribute(tag);
			      if(res != null && res.length > 0){
			        return tar;
			      }
			      else{
			        return this.getTarNode(tar.parentNode,tag);
			      }
			}
		});




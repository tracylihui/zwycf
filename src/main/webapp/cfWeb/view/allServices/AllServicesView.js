Ext.define('cfWeb.view.allServices.AllServicesView',{
	extend : 'Ext.container.Container',
	alias : 'widget.allServicesView',
	overflowY : 'auto',
	
	initComponent : function(){
		this.items = [{
			xtype : 'collapsibleBlock',
			title : '所有服务',
			searchable : true,
			items : [{
				xtype : 'lbuttonIconListView',
				isAddable : false,
				buttonMenu : [[
				
				],[
				{
					ico : 'plus22',
					name : cfWebLang.Util.New,
					act : 'new'
				},{
					ico : 'filter',
					name : cfWebLang.Util.Filter,
					act : 'filter'
				}
				]],
				store : Ext.create('cfWeb.store.allServices.AllServicesStore')
				}]
		},
			   {
			   	xtype : 'collapsibleBlock',
			    title : '服务列表',
			    searchable : true,
			    items:[{
			}]
		}];
		this.callParent(arguments);
	}
});
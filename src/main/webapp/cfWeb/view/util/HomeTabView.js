Ext.define('cfWeb.view.util.HomeTabView', {
	extend: 'Ext.container.Container',
	alias: 'widget.homeTabView',
	//title : '用户主页';
	id: 'tab_home',
	requires: [
		'Ext.toolbar.TextItem',
		'Ext.view.View',
		'Ext.ux.DataView.Animated'
	],

	overflowY: 'auto',

	initComponent: function () {
		this.items = [{
			xtype: 'collapsibleBlock',
			title: cfWebLang.HomeTabView.platformState,
			layout: 'column',
			cls: 'style-cfDashboard',
			items: [{
				xtype: 'ringChart',
				itemId: 'joinedOrg',
//                id:'joinedOrg',
				title: cfWebLang.HomeTabView.joinedOrg,
				//unit:'G',
				icoType: 'database',
				cls: 'ringChart',
				color: 'grey',
				width: 200,
				margin: '30 30 0 0',
				store: Ext.create('cfWeb.store.util.dashboard.JoinedOrgStore')
			}, {
				xtype: 'ringChart',
				itemId: 'routeUsed',
//                id:'routeUsed',
				sortOnLoad: true,
				title: cfWebLang.HomeTabView.appUsage,
				color: 'blue',
				icoType: 'rocket',
				unit: cfWebLang.HomeTabView.unit,
				cls: 'ringChart',
				width: 200,
				margin: '30 30 0 0',

				store: Ext.create('cfWeb.store.util.dashboard.RouteUsedStore')

//                store : Ext.create('Ext.data.Store', {
//                    autoLoad: true,
//                    fields: ['itemId','itemName', 'itemCount','total'],
//                    data:[{
//                        itemId:1,
//                        itemName:'运行',
//                        itemCount:14,
//                        total : 20
//                    }]
//                })

			}, {
				xtype: 'ringChart',
				itemId: 'serviceUsed',
//                id:'serviceUsed',
				title: cfWebLang.HomeTabView.serviceUsage,
				unit: cfWebLang.HomeTabView.unit,
				icoType: 'puzzle',
				color: 'purple',
				cls: 'ringChart',
				width: 200,
				margin: '30 30 0 0',
				store: Ext.create('cfWeb.store.util.dashboard.ServiceUsedStore')

//                store : Ext.create('Ext.data.Store', {
//                    autoLoad: true,
//                    sortOnLoad: true,
//                    fields: ['itemId','itemName', 'itemCount','total'],
//                    data:[{
//                        itemId:1,
//                        itemName:'运行',
//                        itemCount:11,
//                        total : 20
//                    },{
//                        itemId:2,
//                        itemName:'运行',
//                        itemCount:4,
//                        total : 20
//                    },{
//                        itemId:2,
//                        itemName:'运行',
//                        itemCount:5,
//                        total : 20
//                    }]
//                })
			},
				{
					xtype: 'ringChart',
					itemId: 'appStatus',
//                id:'appStatus',
					title: cfWebLang.HomeTabView.appState,
					unit: cfWebLang.HomeTabView.unit,
					icoType: 'dashboard',
					color: 'green',
					cls: 'ringChart',
					width: 200,
					margin: '30 30 0 0',
					store: Ext.create('cfWeb.store.util.dashboard.ServiceUsedStore')

//                store : Ext.create('Ext.data.Store', {
//                    autoLoad: true,
//                    sortOnLoad: true,
//                    fields: ['itemId','itemName', 'itemCount','total'],
//                    data:[{
//                        itemId:1,
//                        itemName:'运行',
//                        itemCount:11,
//                        total : 20
//                    },{
//                        itemId:2,
//                        itemName:'运行',
//                        itemCount:4,
//                        total : 20
//                    },{
//                        itemId:2,
//                        itemName:'运行',
//                        itemCount:5,
//                        total : 20
//                    }]
//                })
				},
				{
					xtype: 'ringChart',
					itemId: 'memoryUsed',
//                id:'memoryUsed',
					title: cfWebLang.HomeTabView.memoryUsage,
					//unit:'G',
					icoType: 'database',
					cls: 'ringChart',
					color: 'orange',
					width: 200,
					margin: '30 0 0 0',
					store: Ext.create('cfWeb.store.util.dashboard.MemoryUsedStore')

//                store : Ext.create('Ext.data.Store', {
//                    autoLoad: true,
//                    sortOnLoad: true,
//                    fields: ['itemId','itemName', 'itemCount','total'],
//                    data:[{
//                        itemId:1,
//                        itemName:'运行',
//                        itemCount:11,
//                        total : 20
//                    },{
//                        itemId:2,
//                        itemName:'运行',
//                        itemCount:4,
//                        total : 20
//                    },{
//                        itemId:2,
//                        itemName:'运行',
//                        itemCount:5,
//                        total : 20
//                    }]
//                })
				}]
		}, {
			xtype: 'collapsibleBlock',
			title: cfWebLang.HomeTabView.functionIntro,
			height: 400,
			loader: {
				url: './functionIntro.html',
				autoLoad: true
			}
		}, {
			xtype: 'collapsibleBlock',
			title: cfWebLang.HomeTabView.serviceIntro,
			height: 400,
			loader: {
				url: './serviceIntro.html',
				autoLoad: true
			}
		}
			//首页部分只显示仪表盘，运行环境和中间件注释掉了
			/*,
			 {
			 xtype : 'collapsibleBlock',
			 title : cfWebLang.HomeTabView.RuntimeEnvironment,
			 searchable : true,
			 items : [{
			 xtype : 'iconListView',
			 itemId : 'environmentHome',
			 fieldLabel : 'Start date',
			 store : Ext.create('cfWeb.store.util.environment.EnvironmentStore')
			 }]
			 }*//*,{
			 xtype : 'collapsibleBlock',
			 title : cfWebLang.HomeTabView.Middleware,
			 searchable : true,
			 items : [{
			 xtype : 'collapsibleIconListView',
			 id:'middlewareStorage',
			 title : cfWebLang.HomeTabView.dataStorageService,
			 store : Ext.create('cfWeb.store.util.middleware.MiddlewareStorageStore')
			 //                store : 'cfWeb.store.util.middleware.MiddlewareStorageStore'
			 },{
			 xtype : 'collapsibleIconListView',
			 id:'middlewareAnalyse',
			 title : cfWebLang.HomeTabView.dataAnalyseService,
			 store : Ext.create('cfWeb.store.util.middleware.MiddlewareAnalyseStore')
			 //                store : 'cfWeb.store.util.middleware.MiddlewareAnalyseStore'
			 },{
			 xtype : 'collapsibleIconListView',
			 id:'middlewareApplication',
			 title : cfWebLang.HomeTabView.appService,
			 store : Ext.create('cfWeb.store.util.middleware.MiddlewareApplicationStore')
			 //                store : 'cfWeb.store.util.middleware.MiddlewareApplicationStore'
			 }]
			 }*/
			/*,{
			 xtype : 'collapsibleBlock',
			 title : '应用模板',
			 items :[]
			 }*/];
		this.callParent(arguments);
	},
	/**
	 * Returns the array of Ext.util.Sorters defined by the current toolbar button order
	 * @return {Array} The sorters
	 */
	getSorters: function () {
		var buttons = this.query('toolbar dataview-multisort-sortbutton'),
			sorters = [];
		Ext.Array.each(buttons, function (button) {
			sorters.push({
				property: button.getDataIndex(),
				direction: button.getDirection()
			});
		});
		return sorters;
	}
	/**
	 * @private
	 * Updates the DataView's Store's sorters based on the current Toolbar button configuration
	 */
});

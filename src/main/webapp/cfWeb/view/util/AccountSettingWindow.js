Ext.define('cfWeb.view.util.AccountSettingWindow',{
	alias : 'widget.accountSettingWindow',
	extend : 'Ext.window.Window',
	title : '账号管理',
	layout:'fit',
	height:250,
	width:750,
	items:[{
		xtype:'tabpanel',
		layout : 'column',
		items:[{
			title : '云管理',
			items : [{
				xtype : 'textfield',
				fieldLabel : '云名称'
			},{
				xtype : 'textfield',
				fieldLabel : '邮箱地址'
			},{
				xtype : 'textfield',
				fieldLabel : '网址'
			},{
				xtype : 'textfield',
				fieldLabel : '密码'
			}]
		},{
			title : 'Orgnizations管理',
			xtype : 'textfield'
		},{
			title : 'Spaces管理',
			xtype : 'textfield'
		},{
			title : 'Domains管理',
			xtype : 'textfield'
		},{
			title : 'Route管理',
			xtype : 'textfield'
		},{
			title : '用户管理',
			xtype : 'textfield'
		},{
			title : '运行环境管理',
			xtype : 'textfield'
		}]
	}]
	
	
//	extend : 'Ext.panel.Panel',
//	items:[{
//		xtype:'tabpanel',
//		layout : 'fit',
//		items:[{
//			title : '云管理',
//			xtype : 'textfield'
//		},{
//			title : 'Orgnizations管理',
//			xtype : 'textfield'
//		},{
//			title : 'Spaces管理',
//			xtype : 'textfield'
//		},{
//			title : 'Domains管理',
//			xtype : 'textfield'
//		},{
//			title : 'Route管理',
//			xtype : 'textfield'
//		},{
//			title : '用户管理',
//			xtype : 'textfield'
//		},{
//			title : '运行环境管理',
//			xtype : 'textfield'
//		}]
//	}]
});
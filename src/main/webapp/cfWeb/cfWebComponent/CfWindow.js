Ext.define('cfWeb.cfWebComponent.CfWindow', {
	extend :'Ext.window.Window',
	alias :'widget.cfWindow',
	cls:'style-cfWindow',
//	height:600,
	width:600,
	padding:0,
	ghost:false,
//	bodyPadding :'20 0',
	border:false,
	modal:true,
	indexNow:0,
	indexLength:0,
	selfId:undefined,
	header:{
		height:36
	},
	showDock:true,
	title:'新建应用',
	
	langSet:null,
	noToolbar:true,
//	bodyStyle:{
//		"padding-top":"7px"
//	},
	dockedItems: [{
		langset :this.langSet,
	    xtype: 'toolbar',
	    dock: 'bottom',
	    padding:'0 5',
	    height:50,
	    items: [
	       { xtype: 'button', text:'',height:30,width:150,cls:'goPrev',act:'goPrev'},'->',
	       { xtype: 'button', text:'',height:30,width:150,cls:'goNext',act:'goNext'}
	    ]
	}],
	initComponent : function() {
	    this.addEvents({
	    	onSubmit:true,
	    	beforeNext : true,
	    	beforePrev :true
	    	//事件
	    });
	    if(!this.showDock)this.dockedItems=null;
		this.callParent(arguments);
	},
  listeners:{
	  boxready : function(){
	      this.selfId = this.getEl().id;
	      this.buttonInit();
	      this.indexInit();
	      this.eventBind();
    }
  },
  indexInit:function(){
	  this.indexLength = this.items.length;
	  this.items.each(function(me){
		  me.hide();
	  });
	  this.items.items[this.indexNow].show();
	  this.setHeight(this.items.items[this.indexNow].getHeight()+(this.showDock?100:0));
	  this.buttonAdj();
  },
  buttonInit : function(){
	  if(!this.showDock)return;
	  var lang = {};
	  if(this.langSet){
		  lang = this.langSet;
	  }
	  var prevText = lang.prev;
	  var nextText = lang.next;
	  var prev = this.down('button[act=goPrev]');
	  var next = this.down('button[act=goNext]');
	  if(prev){
		  prev.setText(prevText); 
	  }
	  if(next){
		  next.setText(nextText); 
	  }
  },
  buttonAdj : function(){
	  if(!this.showDock){
		  return;
	  }
	  var next = this.down('button[act=goNext]'); 
	  var prev = this.down('button[act=goPrev]'); 
	  if(next&& prev){
		  if(this.indexNow < this.indexLength-1){
			  
			  next.show().setText(this.langSet.next);
		  }
		  else{
			  this.indexNow = this.indexLength-1;
			  next.show().setText(this.langSet.finish);
		  }
		  if(this.indexNow == 0){
			  prev.hide();
		  }
		  else{
			  prev.show();
		  }
	  }
  },

  
  goNext : function(){
	  var items = this.items.items;
	  var index = this.indexNow;
	  console.log('wtf',items,index);
	  if(index >= this.indexLength-1){
		  this.fireEvent('onSubmit',this);
		  return;
	  }
	  
	  var res = this.fireEvent('beforeNext',this,items[index],items[index+1]);
	  if(!res){
		  return false;
	  }
	  
	  items[index].hide();
	  items[index + 1].show();
	  this.setHeight(items[index+1].getHeight() + 140);
	  this.setWidth(items[index+1].getWidth());
	  this.indexNow ++;
	  this.buttonAdj();
  },
  goPrev : function(){
	  var items = this.items.items;
	  var index = this.indexNow;
	  var res = this.fireEvent('beforePrev',this,items[index],items[index-1]);
	  if(!res){
		  return false;
	  }
	  
	  items[index].hide();
	  items[index - 1].show();
	  this.setHeight(items[index-1].getHeight() + 140);
	  this.setWidth(items[index-1].getWidth());
	  this.indexNow --;
	  this.buttonAdj();
  },
  eventBind : function(){
	  var next = Ext.select("div[id='"+this.selfId+"'] .goNext");
	  var prev = Ext.select("div[id='"+this.selfId+"'] .goPrev");
	  next.on('click',this.goNext,this);
	  prev.on('click',this.goPrev,this);
	  this.on('add',this.indexInit,this);
  },
  getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
	      if(res != null && res.length > 0){
	        return tar;
	      }
	      else{
	        return this.getTarNode(tar.parentNode,tag);
	      }
  }
});
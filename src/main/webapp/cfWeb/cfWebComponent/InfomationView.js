Ext.define('cfWeb.cfWebComponent.InfomationView', {
	extend : 'Ext.container.Container',
	alias : 'widget.infomationView',
	layout : 'border',
	height:181,
	selfId:undefined,
	title : undefined,
	htmlTemp:'<div class="app_detail_head">'+
        	 '<div class="adh_logo" data-name="itemColor" style="color:#f00">'+
        	 '<img src="resources/img/icons/app1.png" height="90" width="180" data-name="itemImg" />'+
        	 '<span class="adh-icon adh_message_count" data-name="message"></span>'+
        	 '<p class="adh_title" data-name="title"></p>'+
        	 '</div>'+
        	 '<div class="add_detail_status" style="width:80%">',
    htmlTemp2 :'</div></div>',
    detailList:[{name:'类型',dataIndex:'tags'}],
	appData:null,
	isEditing:false,
	initComponent : function() {
		this.htmlInit();
	    this.addEvents({
	    });
		this.callParent(arguments);
	},
  listeners:{
    boxready : function(){
      this.selfId = this.getEl().id;
      this.updateAppStatus(this.appData);
      this.eventBind();
    }
  },
  eventBind : function(){	  
  },
  getValues:function(){
	  return this.appData;
  },
  updateAppStatus :function(data){
	  console.log(data);
	  if(!this.appData){
		  this.appData = new Object();
	  }
	  if(!data){
		  return false;
	  }
	  else{
		  var lth = this.detailList.length;
		  for(var i = 0; i<lth; i++){
			  var dataIndex =this.detailList[i].dataIndex;
			  if(data[dataIndex] == undefined){
				  continue;
			  }
			  else{
				  var tar = Ext.select("div[id='"+this.selfId+"'] span[data-name='"+dataIndex+"']").elements[0];
				  tar.innerHTML = data[dataIndex];
				  this.appData[dataIndex] = data[dataIndex];
			  }
		  };
		  if(data.itemColor){
			  var tar = Ext.select("div[id='"+this.selfId+"'] div[data-name='itemColor']").elements[0];
			  tar.setAttribute('style','color:'+data.itemColor);
			  this.appData.itemColor = data.itemColor;
		  }
		  if(data.itemImg){
			  var tar = Ext.select("div[id='"+this.selfId+"'] img[data-name='itemImg']").elements[0];
			  tar.setAttribute('src',data.itemImg);
			  this.appData.itemImg = data.itemImg;
		  }
		  if(data.title){
			  var tar = Ext.select("div[id='"+this.selfId+"'] p[data-name='title']").elements[0];
			  tar.innerHTML = data.title;
			  //tar.style.color = data.itemColor;
			  tar.setAttribute('style','font-size:24px;color:'+data.itemColor);
			  this.appData.title = data.title; 
		  }
		  if(data.message){
			  var extTar = Ext.select("div[id='"+this.selfId+"'] span[data-name='message']");
			  extTar.show();
			  var tar = extTar.elements[0];
			  tar.innerHTML = data.message;
			  this.appData.message = data.message;
			  alert('sdsds');
		  }
		  else{
			  var tar = Ext.select("div[id='"+this.selfId+"'] span[data-name='message']");
			  tar.hide();
		  }
	  }
	  this.eventBind();
  },
   htmlInit : function(){
	  var lth = this.detailList.length;
	  console.log(lth);
	  var detail = '';
	  detail +='<span data-name="'+this.detailList[0].dataIndex+'" style="color:#818181;text-indent:2em;padding-left:10px;padding-top: 30px;display:inline-block;line-height:30px;"></span>';
	  this.html = this.htmlTemp + detail + this.htmlTemp2;
  },
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   }
});
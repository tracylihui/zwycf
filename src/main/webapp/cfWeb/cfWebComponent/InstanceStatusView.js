Ext.define('cfWeb.cfWebComponent.InstanceStatusView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.instanceStatusView',
	ChartTypeList : ['硬盘','内存'],
	items : [{
				xtype : 'instanceChartType',
				title:'内存',
				margin:'0 0 0 0',
				max:100,
				store : Ext.create('cfWeb.store.appManage.InstanceStateStore'),
				width : 500,
				height : 300,
				itemId : 'memChart'
			}, {
				xtype : 'panel',
				margin:'0 0 0 3',
				width : 500,
				layout : {
					type : 'hbox',
					aligin : 'right'
				},
				items : [{
					xtype : 'instanceChartType',
					itemId:'diskChart',
					margin: '0 0 0 0',
					max:100,
					title:'硬盘',
					width : 250,
					height : 150,
					store : Ext.create('cfWeb.store.appManage.InstanceStateStore')
				}, {
					xtype : 'cpuChartType',
					itemId:'cpuChart',
					title:'CPU',
					max:100,
					margin: '0 0 0 -6',
					width : 250,
					height : 150,
					store : Ext.create('cfWeb.store.appManage.InstanceStateStore')
				}]
			}],
	initComponent : function() {
		var me = this;
		console.log(me.items[1].items[0].title);
		me.items[1].items[0].title=this.ChartTypeList[0];
		console.log(me.items[1].items[1].title);
		me.items[0].title=this.ChartTypeList[1];
//		var diskChart = me.down('instanceChartType[itemId=diskChart]');
//		var memChart = me.down('instanceChartType[itemId=memChart]');
//		console.log("!!!!!!!!!!!!");
//		console.log(diskChart.title);
//		console.log(memChart.title);
		this.callParent(arguments);
	}
});
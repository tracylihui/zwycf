Ext.define('cfWeb.cfWebComponent.CfChartTypeII', {
	extend : 'Ext.chart.Chart',
	alias : 'widget.cfChartTypeII',
	selfId:undefined,
	cls:'style-cfChartTypeII',
	height:300,
	width:500,
	margin:'0 10 30 0',
	title:'',
    style: 'background:#000',
    shadow: false,
    border:0,
    axes: [{
        type: 'Numeric',
        dashSize:0,
        minimum: 0,
//        maximum:100,
//        majorTickSteps:9,
        position: 'left',
        fields: ['data'], 
        label: {
        	renderer:function(){
        		return '';
        	}
        },
        // title: 'Number of Hits',
        minorTickSteps: 1,
         grid: {
             odd: {
                 opacity: 0.3,
                 type:'circle',
                 stroke: '#d6dae6',
                 'stroke-width': 0.5
             },
             even:{
                     opacity: 0.3,
                     stroke: '#d6dae6',
                     'stroke-width': 0.5
             }
         }
//        grid:true
    }, {
        type: 'Category',
        position: 'bottom',
        dashSize:0,
        fields: ['name'],
        label: {
        	renderer:function(){
        		return '';
        	}
        },
        grid:true,
//        style:{
//        	color:"#f00"
//        },
        grid: {
            odd: {
                opacity: 1,
                type:'circle',
                stroke: '#d6dae6',
                'stroke-width': 0.5
            },
            even:{
                    opacity: 1,
                    stroke: '#d6dae6',
                    'stroke-width': 0.5
            }
        }
        // title: 'Month of the Year'
    }],
    series: [
    {
        type: 'line',
        highlight: {
            size: 7,
            radius: 7
        },
        style:{
        	stroke:'#17a2f7',
        	'stroke-width': 2,
        	fill:'#e1f7f7'
        },
        axis: 'left',
        smooth: true,
        fill: true,
        xField: 'name',
        yField: 'data',
        markerConfig: {
            type: 'circle',
            radius: 3,
            'fill': '#82dddf'
        }
    }],
	initComponent : function() {
	    this.addEvents({
	    });
	    this.titleInit();
		this.callParent(arguments);
	},
	titleInit : function(){
	    this.html = "<p class='cfChartTitle'>" + this.title +"</p>";
	},
	updateTitle : function(text){
		var	Id = this.getEl().id;
		var title = Ext.select("div[id='"+Id+"'] p.cfChartTitle").elements[0];
		title.innerHTML = this.title + '(' +text +')';
	},
	resetTitle : function(){
		var	Id = this.getEl().id;
		var title = Ext.select("div[id='"+Id+"'] p.cfChartTitle").elements[0];
		title.innerHTML = this.title;
	},
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   },
   eventBind :function(){
	   var header = this.header.getEl(); 
	   header.on('click',this.headClick,this);
	   console.log(header);
   }
});

Ext.define('cfWeb.cfWebComponent.CfSlider', {
			extend : 'Ext.container.Container',
			alias : 'widget.cfSlider',
			width:100,
			maxValue:100,
			height:390,
			padding:'0 0 20 0',
			title:'测试',
			fixedPoints:null,
			unit:'',
			increment :1,
			_increment :1,
			times:1,
			isDirty:false,
			cls:'style-cfSliderWrap',
			selfId : undefined,
			initComponent : function() {
				var me = this;
				this.addEvents({
//					'onItemsClick':true,
//					'oncollapse' : true,
//					'onExpand' : true
					'changecomplete': true,
					'valueSet':true
				});
				this._increment = this.increment;
				if(!this.increment||this.increment <=0){
					this.increment = 1;
					this._increment = 1;
				}
				else{
					var y = this.increment%1;
					if(y != 0){
						this.times = parseInt(1/y);
						this._increment = this.increment * this.times;
					}
					
				}
				this.htmlInit();
				this.items=[
				            {
				            	xtype:'slider',
				            	margin:'10 20 10 60',
				                height:350,
				                cls:'style-cfSlider',
				                value:0,
				                useTips:false,
				                increment: 1,
				                minValue: 0,
				                maxValue: 2048,
				                vertical:true,
				                listeners:{
				                	changecomplete:{
				                		fn:me.changeComp,
				                		scope:me
				                	},
				                	change :{
				                		fn:me.floatChange,
				                		scope:me
				                	}
				                }

				            }
				            ];
				this.argsInit();
				console.log(2);
//				this.eventBind();
//				this.store.on('load',this.loadInit,this);
				this.callParent(arguments);
			},
			fpSort : function(){
				if(!!this.fixedPoints){
					this.fixedPoints.sort(function(v1,v2){return v1-v2;});
					var l = this.fixedPoints.length;
					for(var i=0;i<l;i++){
						this.fixedPoints[i]*=this.times;
					}
					
				}
			},
			floatChange : function(slider, newValue, thumb, eOpts){
				var float = Ext.select("div[id='"+this.selfId+"'] span.fs_content").elements[0];
				var num = this.findClosePoint(newValue);
				float.innerHTML = num/this.times;
			},
			findClosePoint: function(num){
				if(!this.fixedPoints){
					return num;
				}
				else{
					var search = function(array,number){
						var start = 0;
						var end = array.length -1;
						var index = 0;
						if(number <=array[start]){
							return array[0];
						}
						else if(number >=array[end]){
							return array[end];
						}
						while(start<end-1){
							 index= Math.ceil((start+end)/2);
							 if(array[index]>number){
								 end=index;
							 }
							 else if(array[index]<number){
								 start=index;
							 }
							 else{
								 return array[index];
							 }
						}
						if(number-array[start]<array[end]-number){
							return array[start];
						}
						else{
							return array[end];
						}
					};
					var res = search(this.fixedPoints,num);
					return res;
				}
			},
			eventBind : function(){
			},
			changeComp : function(slider, newValue, thumb, eOpts){
				var point = this.findClosePoint(newValue);
				this.items.items[0].setValue(point);
				this.fireEvent('changecomplete', this,slider, point/this.times, thumb, eOpts);
			},
			listeners:{
				boxready : function(){
					var me = this;
					me.selfId = me.getEl().id;
					var thumb = Ext.select("div[id='"+this.selfId+"'] div.x-slider-thumb").elements[0];
					thumb.innerHTML = '<div class="float_sym"><span class="fs_content">0</span><div class="fs_sym"></div></div>';
//					this.loadInit();
				}
			},
			argsInit: function(){
				if(this.height){
					this.items[0].height = this.height -40;
				}
				if(this.maxValue){
					this.items[0].maxValue = this.maxValue * this.times;
				}
				if(this.value){
					this.items[0].value = this.value * this.times;
				}
				if(this.minValue){
					this.items[0].minValue = this.minValue * this.times;
				}
				if(this.increment){
					this.items[0].increment = this._increment;
				}
				this.fpSort();
			},
			htmlInit: function(){
				var html = '<p class="cf_slider_title">'+this.title+'</p>';
				var height = this.height;
				height -=40;
				html +='<div class="cf_slider_sym" style="height:'+ height +'px"><span class="cf_slider_top">'+this.maxValue+this.unit+'</span><span class="cf_slider_bottom">0</span><div class="main_sym"></div></div>';
				this.html = html;
			},
			getTargetNode : function(dom ,tag){
			  	if(dom.tagName ==undefined){
				      return false;
				}
			    if(dom.tagName == tag){
			        return dom;
			    }
			    else{
			        return this.getTargetNode(dom.parentNode,tag);
			    }
			},
			getValue : function(){
				return this.items.items[0].getValue()/this.times;
			},
			setValue : function(val){
				var point = this.findClosePoint(val*this.times);
				
				this.fireEvent('valueSet',this,point);
				return this.items.items[0].setValue(point);

			},
			setMaxValue : function(val){
				var num = parseInt(val);
				if(this.selfId){
					 var max = Ext.select("div[id='"+this.selfId+"'] span.cf_slider_top").elements[0];
					 max.innerHTML = num + this.unit;
					 this.items.items[0].setMaxValue(val*this.times);
				}
				else{
					this.maxValue = num;
				}
				return this;
			}
		});




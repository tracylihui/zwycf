Ext.define('cfWeb.cfWebComponent.ServiceConfigView', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.serviceConfigView',
	selfId:undefined,
	cls:'style-ServiceConfigView',
	serType:cfWebLang.HomeTabView.dataStorageService,
	mainTitle : cfWebLang.AppManageView.MidwareType,
	serName : 'Oracle',
	header:{
		height:40
	},
//	titleCollapse : true,
	initComponent : function() {
	    this.addEvents({
	    });
	    this.titleInit();
		this.callParent(arguments);
	},

  listeners:{
    boxready : function(){
    	this.selfId = this.getEl().id;
    	this.eventBind();
    }
  },
  titleInit : function(){
	  var title = this.mainTitle + '：' +this.serType+'-'+this.serName;
	  this.title = title;
  },
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   },
   eventBind :function(){
//	   var header = this.header.getEl(); 
//	   header.on('click',this.headClick,this);
//	   console.log(header);
   }
});

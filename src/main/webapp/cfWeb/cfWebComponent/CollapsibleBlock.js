Ext.define('cfWeb.cfWebComponent.CollapsibleBlock', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.collapsibleBlock',
	selfId:undefined,
	searchable:false,
	addIco : false,
	isHover:false,
	searchHint:cfWebLang.CollapsibleBlock.search,
	meCls:'style-CollapsibleBlock',
	initComponent : function() {
		this.addEvents({
			'onSearch':true,
			'onAdd' : true,
			'clearSearch':true
		});
		this.clsInit();
		this.callParent(arguments);
	},
	searchBlock:undefined,
	searchInput:undefined,
	searchSpan:undefined,
	searchLabel:undefined,
	listeners:{
		boxready : function(){
			this.selfId = this.getEl().id;
			this.addIcoInit();
			this.searchInit();
			var searchBox = Ext.select("div[id='"+this.selfId+"'] div.searchBox");
			var searchInput = Ext.select("div[id='"+this.selfId+"'] input.searchText");
			var searchSpan = Ext.select("div[id='"+this.selfId+"'] span.searchSign");
			var addBox = Ext.select("div[id='"+this.selfId+"'] div.addBox");
			this.searchBlock = searchBox.elements[0];
			this.searchInput = searchInput.elements[0];
			this.searchSpan = searchSpan.elements[0];
			this.searchLabel = Ext.select("div[id='"+this.selfId+"'] label").elements[0];
			searchBox.on('mouseenter',this.onSearchHover,this);
			searchBox.on('mouseleave',this.onMouseout,this);
			searchBox.on('click',this.onSearchClick,this);
			searchSpan.on('click',this.onSearch,this);
			searchInput.on('blur',this.onSearchBlur,this);
			searchInput.on('keyup',this.onSearchKeydown,this);
			addBox.on('click',this.onAddIconClicked,this);
//			this.on('resize',this.autoAdjust,this);
			var header = Ext.select("div[id='"+this.selfId+"'] div.x-panel-header");
			header.on('click',this.headClicked,this);
		}
	},
//	autoAdjust: function(me, width, height, oldWidth, oldHeight, eOpts){
//		if(me.items.items.){
//			console.log(me.items.items[0].getwidth());
//		}
//		
//	},
	searchInit : function(){
		if(typeof(this.searchable) =='boolean' && this.searchable){// 添加搜索框
			var textBlock = Ext.select("div[id='"+this.selfId+"'] div.x-header-text-container").elements[0];
			var newDiv = document.createElement("div");
			var newInput = document.createElement("input");
			var newSpan = document.createElement("span");
			var newLabel = document.createElement("label");
			newInput.className = 'searchText';
			newLabel.className = 'searchHint';
			newLabel.innerHTML = this.searchHint;
			newDiv.className = 'searchBox short barTools';
			newInput.setAttribute('type', 'text');
			newInput.setAttribute('name', 'searchWord');
			newSpan.setAttribute('class', 'icon-search3 searchSign');
			newDiv.appendChild(newInput);
			newDiv.appendChild(newSpan);
			newDiv.appendChild(newLabel);
			var lth = Ext.select("div[id='"+this.selfId+"'] div.barTools").elements.length;
			var right = lth*35 +10;
			newDiv.style.right = right + 'px';
			textBlock.appendChild(newDiv);
		}
	},
	onMouseout : function(){
		this.isHover = false;
	},
	addIcoInit : function(){
		if(this.addIco){
			var textBlock = Ext.select("div[id='"+this.selfId+"'] div.x-header-text-container").elements[0];
			console.log(textBlock);
			var div = document.createElement("div");
			div.className = "addBox icon-plus22 barTools";
			var lth = Ext.select("div[id='"+this.selfId+"'] div.barTools").elements.length;
			var right = lth*35 +10;
			div.style.right = right + 'px';
			textBlock.appendChild(div);
		}
	},
	clsInit: function(){
		if(this.cls){
			this.cls = this.cls + ' '+this.meCls;
		}
		else{
			this.cls = this.meCls;
		}
	},
	headClicked : function(e,dom){
		this.toggleCollapse();
	},
	onAddIconClicked : function(e,dom){
		this.fireEvent('onAdd',this);
		e.stopPropagation();
	},
	onSearch : function(e,dom){
		var value = this.searchInput.value;
		value = value.trim();
		if(value.length > 0){
			this.fireEvent('onSearch',value,this);
		}
	},
	onSearchKeydown : function(e,dom){
		if(e.button == 12){
			this.onSearch(e,dom);
		}
		var className = this.searchLabel.className;
		if(dom.value && dom.value.length > 0){
			if(className.indexOf(' dis_n') < 0){
				this.searchLabel.className = className + " dis_n";
			}
		}
		else{
			this.searchLabel.className = className.replace(' dis_n','');
		}
	},
	onSearchHover : function(e,dom){
		this.isHover = true;
		var className = this.searchBlock.className;
		if(className.indexOf('short') >= 0){
			className = className.replace('short','');
			this.searchBlock.className = className;
			this.searchInput.focus();
		}
	},
	onSearchBlur : function(e,dom){
		var value = dom.value;
		value = value.trim();
		if(value.length > 0){
			return;
		}	
		className = this.searchBlock.className;
		if(className.indexOf('short') < 0&&!this.isHover){
			this.searchBlock.className = className + ' short';
		}
		this.fireEvent('clearSearch',this);
	},
	onSearchClick : function(e,dom){
		e.stopPropagation();
	},
	getTarNode :function(tar,tag){
		if(tar.tagName ==undefined){
			return false;
		}
		var res = tar.getAttribute(tag);
		if(res != null && res.length > 0){
			return tar;
		}
		else{
			return this.getTarNode(tar.parentNode,tag);
		}
	},
});
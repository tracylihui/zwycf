Ext.define('cfWeb.cfWebComponent.ServiceDetail', {
	extend : 'Ext.view.View',
	alias : 'widget.serviceDetail',
	selfId:undefined,
	buttonMenu:undefined,
	tpl:undefined,
	appName:undefined,
	loadingStatus:0,
	readyStatus:2,
	store: undefined,
	detailList:['plan','服务类型','创建时间','更新时间'],
	menuList :[{
					ico:'cross',
					name:cfWebLang.Bug.unbinding ,
					act:'unbindService'						
				}
	           ],
	tempTpl1:[
			'<ul class="clearfix icon_list app_list">',
			'<tpl for=".">',
			'<li data-serviceName="{serviceName}" data-index="{[xindex]}">',
			  '<div class="app_items">',
			    '<div class="app_head">',
			      '<img src="{itemImage}" />',
			      '<div class="app_heapp_detail_info" style="color:#fff">',
			        '<p class="app_title">{serviceName}</p>',
			      '</div>',
			    '</div>'],
	tempTpl2:[
				'</div>',
				'</li>',
				'</tpl>',
				' <li>',
		           '<div class="icon-plus22 addService_module cssAnim">',
		           '</div>',
		        ' </li>',
				'</ul>'
	          ] ,
	initComponent : function() {
		this.tplInit();
	    this.addEvents({
	    	'menuClick':true,
	    	'appClick':true,
	    	'itemAdd' : true
	    	//事件
	    });
	    this.store.on('load',this.loadInit,this);
	    this.store.on('datachanged',this.loadInit,this);
		this.callParent(arguments);
	},
	tplInit : function(){
		
	   var appDetails =[];
	   appDetails[appDetails.length]= '<div class="app_details">';
	   appDetails[appDetails.length]= '<dl class="app_detail_items">';
	   appDetails[appDetails.length] = '<dt class="app_detail_title">'+this.detailList[0]+'：</dt>';
	   appDetails[appDetails.length]= '<dd class="app_detail_content">{plan}</dd></dl>';
	   appDetails[appDetails.length]= '<dl class="app_detail_items">';
	   appDetails[appDetails.length]= '<dt class="app_detail_title">'+ this.detailList[1] +'：</dt>';
	   appDetails[appDetails.length]= '<dd class="app_detail_content">{service}</dd></dl>';
	   appDetails[appDetails.length] = '<dl class="app_detail_items"><dt class="app_detail_title">'+this.detailList[2]+'：</dt>',
	   appDetails[appDetails.length]= '<dd class="app_detail_content">{createTime}</dd></dl>';
	   appDetails[appDetails.length]= '<dl class="app_detail_items"><dt class="app_detail_title">'+ this.detailList[3] +'：</dt>';
	   appDetails[appDetails.length] = 	'<dd class="app_detail_content">{updateTime}</dd></dl></div>';
	   
	   var lth = this.menuList.length;
	   var menu = '<span class="icon-plus2 app_act"></span>';
	   menu +='<ul class="app_menu_list app_menu_'+ lth +'">';
	   for(var i = 0;i<lth; i++){
		   menu +='<li class="app_menu_items icon-'+ this.menuList[i].ico +' app_menu_items_'+ i+'" data-act="'+ this.menuList[i].act +'">';
		   menu +='<span>'+this.menuList[i].name+'</span></li>';
	   }
	   menu += '</ul>';
	   
	   this.tpl = this.tempTpl1.concat(appDetails,menu,this.tempTpl2);
	},
	listeners:{
	    viewready : function(){
	      this.selfId = this.getEl().id; 
	      this.loadInit();
	    }
	},
	loadInit: function(){
		this.loadingStatus++;
		if(this.loadingStatus >= this.readyStatus){
			this.eventBind(this);
		}
//		alert(this.loadingStatus);
	},
	
	demo :function(){
		var acts = Ext.select("div[id='"+this.selfId+"'] ul.app_menu_list").elements[0];
		var className = acts.className;
		acts.className = className +' on';
	},
	eventBind : function(me){
		var actButton = Ext.select("div[id='"+this.selfId+"'] span.app_act");
		this.bindEv(actButton,'click',this.showMenu,this);
//		actButton.on('click',this.showMenu,this);
		var menuButton = Ext.select("div[id='"+this.selfId+"'] li.app_menu_items");
		this.bindEv(menuButton,'click',this.menuClick,this);
		var head = Ext.select("div[id='"+this.selfId+"'] div.app_head");
		this.bindEv(head,'click',this.appClicked,this);
		var addButton = Ext.select("div[id='"+this.selfId+"'] div.addService_module");
		this.bindEv(addButton,'click',this.itemAdd,this);
	},
	  bindEv :function(target,eventName,fn,scope){
		  target.un(eventName,fn,scope);
		  target.on(eventName,fn,scope);
	  },
	bindAddService_module : function(){
		var addButton = Ext.select("div[id='"+this.selfId+"'] div.addService_module");
		addButton.on('click',this.itemAdd,this);
	},
	itemAdd : function(e,dom){
		/*var win=Ext.create('cfWeb.view.appManage.NewBindServiceWindow',{
			itemId:'newBindServiceWindow'
		});
		win.show();	*/
		this.fireEvent('itemAdd',this);
	  },
	showMenu:function(e,dom){
		var className = dom.className;
		var actList = dom.nextSibling;
		var tmo = undefined;
		var a_className = actList.className;
		console.log(actList);
		if(className.indexOf(' on')>0){
			dom.className = className.replace(' on','');
		}
		else{
			dom.className = className + ' on';
		}
		if(a_className.indexOf(' on')>0){
			if(a_className.indexOf(' outAni') >0){
				clearTimeout(tmo);
				actList.className = actList.className.replace(' outAni','');
				
			}
			else{
				actList.className = actList.className +' outAni';
				tmo = setTimeout(function(){
					actList.className = actList.className.replace(' outAni','');
					actList.className = actList.className.replace(' on','');
				},300);
			}
			
			
		}
		else{
			
			actList.className = a_className + ' on';
		}
	},
	menuClick : function(e,dom){
		var tar = this.getTarNode(dom,'data-act');
		var act = tar.getAttribute('data-act');
		var li = this.getTarNode(dom,'data-serviceName');
		var name = li.getAttribute('data-serviceName');
		var index = parseInt(li.getAttribute('data-index')) -1;
		var i = index +1;
		var loadingButton = Ext.select("div[id='"+this.selfId+"'] li[data-index='"+i+"'] li.loading_grey");
		console.log(loadingButton);
		if(loadingButton.elements.length >0){
			return;
		}
		else{
			dom.className = dom.className + ' loading_grey';
			this.fireEvent('menuClick',this,act,name,index,e);
		}
	},
	appClicked :function(e,dom){
		var tar = this.getTarNode(dom,'data-serviceName');
		var appName = tar.getAttribute('data-serviceName');
		var index = parseInt(tar.getAttribute('data-index')) -1;
		this.fireEvent('appClick',this,appName,index,e);
	},
	actionDone : function(i){
		var me = this;
		i++;
		var loadingButton = Ext.select("div[id='"+this.selfId+"'] li[data-index='"+i+"'] li.loading_grey");
		if(loadingButton.elements.length >0){
			loadingButton.removeCls('loading_grey');
			var actButton = Ext.select("div[id='"+this.selfId+"'] li[data-index='"+i+"'] span.app_act").elements[0];
			var e = {};
			setTimeout(function(){me.showMenu(e, actButton);},1000);
		}

	},
	getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
	      if(res != null && res.length > 0){
	        return tar;
	      }
	      else{
	        return this.getTarNode(tar.parentNode,tag);
	      }
  }

});

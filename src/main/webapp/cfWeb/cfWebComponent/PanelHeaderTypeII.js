Ext.define('cfWeb.cfWebComponent.PanelHeaderTypeII', {
	extend : 'Ext.container.Container',
	alias : 'widget.panelHeaderTypeII',
	selfId : undefined,
	title:'',
	heads:[],
	cls:'style-panelHeaderTypeII',
	html :undefined,
    initComponent: function() {
    	this.addEvents({
    		'headChange' : true
    	});
    	var me = this;
    	console.log(me);
    	this.listeners={
    			boxready:function(){
    				this.selfId = this.getEl().id;
    				this.eventBind();
    	    	}	
    	};
    	this.htmlConstructor();
        this.callParent(arguments);
    },
    htmlConstructor : function(){
    	var lth = this.heads.length;
    	var html = '<ul>';
    	if(lth >0){
    		html +='<li class="on">'+this.heads[0]+'</li>';
    	}
    	for(var i = 1; i<lth;i++){
    		html +='<li>'+this.heads[i]+'</li>';
    	}
    	html +='</ul>';
    	this.html = html;
    },
    eventBind : function(){
    	 var li =  Ext.select("div[id='"+this.selfId+"'] li");
    	 li.on('click',this.liClick,this);
    },
    liClick : function(e,dom){
    	if(dom.className =="on"){
    		return;
    	}
    	else{
    		var li =  Ext.select("div[id='"+this.selfId+"'] li.on").elements[0];
    		li.className = '';
    		dom.className = 'on';
    		this.fireEvent('headChange',this,dom.innerHTML,e);
    	}
    }


});

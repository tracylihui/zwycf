Ext.define('cfWeb.cfWebComponent.CbuttonIconListViewII', {
    extend: 'Ext.view.View',
    loadMask: false,
    loadingMask: true,
    alias: 'widget.cbuttonIconListViewII',
    selfId: undefined,
    buttonMenu: undefined,
    tpl: undefined,
    loadingStatus: 0,
    readyStatus: 2,
    detailList: undefined,
    menuList: [],
    tempTpl1: [
        '<ul class="clearfix icon_list app_list">',
        '<tpl for=".">',
        '<li data-appName="{title}" data-index="{[xindex]}">',
        '<div class="app_items">',
        '<tpl if="relatedApp.length &gt; 0">',
        '<div class="upgrade_wrap" title="{relatedApp}"><div class="upgrade_sign"></div><div class="uprade_sign_inner"></div></div>',
        '</tpl>',
        '<div class="app_head">',
        '<img src="resources/img/icons/module.png"/>',//写死它吧。。。。
        '<div class="app_heapp_detail_info" style="color:#fff">',
        '<div class="app_message',
        '<tpl if="message &lt; 1">',
        ' dis_n',
        '<tpl elseif="typeof message != \'string\'">',
        ' dis_n',
        '</tpl>',
        '">',
        '<span class="app_message_count">{message}</span>',
        '</div>',
        '<p class="app_title" title="{title}">{title}</p>',
        '</div>',
        '</div>'],
    tempTpl2: [
        '</div>',
        '</li>',
        '</tpl>',
        '</ul>'
    ],
    initComponent: function () {
        this.tplInit();
        this.addEvents({
            'menuClick': true,
            'appClick': true
            //事件
        });
        this.setLoadingMask();
        this.store.on('load', this.loadInit, this);
        this.store.on('filterchange', this.loadInit, this);
        this.callParent(arguments);
    },
    setLoadingMask: function () {
        if (this.loadingMask) {
            this.html = "<div class='loading_mask'>" + cfWebLang.HomeTabView.loadingMsg + "</div>";
        }
    },
    hideMask: function () {
        if (this.loadingMask) {
            var hideMsk = Ext.select("div[id='" + this.selfId + "'] div.loading_mask");
            hideMsk.addCls('dis_n');
            var height = parseInt(this.getHeight());
            this.setHeight(height);
        }
    },
    autoAdjust: function (me, width, height, oldWidth, oldHeight, eOpts) {
        var inner = Ext.select("div[id='" + this.selfId + "'] ul.icon_list").elements[0];
        var height = inner.getAttribute('height');
        me.setHeight(height);
    },
    tplInit: function () {
        var funcs = {};//模板函数
        var appDetails = [];//模板html
        appDetails[appDetails.length] = '<div class="app_details">';
        var dLth = this.detailList.length;
        for (var i = 0; i < dLth; i++) {
            var nowName = this.detailList[i].name;
            if (typeof nowName == "string") {
                if (!this.detailList[i].unit) {
                    this.detailList[i].unit = '';
                }
                appDetails[appDetails.length] = '<dl class="app_detail_items">';
                appDetails[appDetails.length] = '<dt class="app_detail_title">' + this.detailList[i].name + '：</dt>';
                if (this.detailList[i].renderer) {// 是否用户自定义渲染
                    funcs["func" + i] = this.detailList[i].renderer;
                    var nowIndex = this.detailList[i].dataIndex;
                    appDetails[appDetails.length] = '<dd class="app_detail_content">{[this["func"+' + i + '](values.' + nowIndex + ')]}' + this.detailList[i].unit + '</dd></dl>';  //调用对应renderer方法
                }
                else {
                    appDetails[appDetails.length] = '<dd class="app_detail_content">{' + this.detailList[i].dataIndex + '}' + this.detailList[i].unit + '</dd></dl>';
                }


            }
            else {
                var aLth = this.detailList[i].name.length;
                var name = this.detailList[i].name.join('/');
                var index = '{' + this.detailList[i].dataIndex[0] + '}' + this.detailList[i].unit[0];
                for (var j = 1; j < aLth; j++) {
                    index += '/{' + this.detailList[i].dataIndex[j] + '}' + this.detailList[i].unit[j];
                }
                appDetails[appDetails.length] = '<dl class="app_detail_items">';
                appDetails[appDetails.length] = '<dt class="app_detail_title">' + name + '：</dt>';
                appDetails[appDetails.length] = '<dd class="app_detail_content">' + index + '</dd></dl>';
            }
        }
        appDetails[appDetails.length] = '</div>';

        var lth = this.menuList.length;
        var menu = '';
        if (lth > 0 && lth) {
            menu = '<span class="icon-plus2 app_act"></span>';
            menu += '<ul class="app_menu_list app_menu_' + lth + '">';
            //如果不是批准的应用，不允许创建
            menu += '<tpl if="state">';
            menu += '<li class="app_menu_items ' + 'icon-' + this.menuList[0].ico + ' app_menu_items_' + 0 + '" data-act="' + this.menuList[0].act + '">';
            menu += '<span>' + this.menuList[0].name + '</span></li>';
            menu += '</tpl>';
            for (var i = 1; i < lth; i++) {
                var ico = this.menuList[i].ico;
                if (ico.indexOf('-') < 1) {
                    ico = "icon-" + ico;
                }
                menu += '<li class="app_menu_items ' + ico + ' app_menu_items_' + i + '" data-act="' + this.menuList[i].act + '">';
                menu += '<span>' + this.menuList[i].name + '</span></li>';
            }
            menu += '</ul>';
        }

        var tTemp = this.tempTpl1.concat(appDetails, menu, this.tempTpl2);
        tTemp[tTemp.length] = funcs;
        this.tpl = tTemp;

    },
    listeners: {
        viewready: function () {
            this.selfId = this.getEl().id;
            this.loadInit();
        }
    },
    loadInit: function () {
        this.loadingStatus++;
        if (this.loadingStatus >= this.readyStatus) {
            this.eventBind(this);
            this.hideMask();
        }
    },
    eventBind: function (me) {
        var actButton = Ext.select("div[id='" + this.selfId + "'] span.app_act");
        this.bindEv(actButton, 'click', this.showMenu, this);
        var menuButton = Ext.select("div[id='" + this.selfId + "'] li.app_menu_items");
        this.bindEv(menuButton, 'click', this.menuClick, this);
        var head = Ext.select("div[id='" + this.selfId + "'] div.app_head");
        var upgrade = Ext.select("div.app_items div.upgrade_wrap");
        this.bindEv(upgrade, "mouseenter", this.upgradeHover, this);
        this.bindEv(upgrade, "mouseleave", this.upgradeOut, this);
        this.bindEv(head, 'click', this.appClicked, this);
        this.bindEv(this, 'resize', this.autoAdjust, this);
    },
    upgradeHover: function (e, dom) {
//		dom = this.getTarNode(dom,'title');
        console.log(dom);
        var appName = dom.title;
        dom.className = dom.className + ' hover';
        var related = Ext.select("li[data-appname='" + appName + "'] div.upgrade_wrap").item(0).dom;
        console.log(related);
        related.className = related.className + ' hover';
    },
    upgradeOut: function (e, dom) {
//		dom = this.getTarNode(dom,'title');
        console.log(dom);
        var appName = dom.title;
        dom.className = dom.className.replace(' hover', '');
        ;
        var related = Ext.select("li[data-appname='" + appName + "'] div.upgrade_wrap").item(0).dom;
        related.className = related.className.replace(' hover', '');
    },
    showMenu: function (e, dom) {
        var className = dom.className;
        var actList = dom.nextSibling;
        var tmo = undefined;
        var a_className = actList.className;
        console.log(actList);
        if (className.indexOf(' on') > 0) {
            dom.className = className.replace(' on', '');
        }
        else {
            dom.className = className + ' on';
        }
        if (a_className.indexOf(' on') > 0) {
            if (a_className.indexOf('.outAni') > 0) {
                clearTimeout(tmo);
                actList.className = actList.className.replace(' outAni', '');

            }
            else {
                actList.className = actList.className + ' outAni';
                tmo = setTimeout(function () {
                    actList.className = actList.className.replace(' outAni', '');
                    actList.className = actList.className.replace(' on', '');
                }, 300);
            }


        }
        else {

            actList.className = a_className + ' on';
        }
    },
    menuClick: function (e, dom) {
        var tar = this.getTarNode(dom, 'data-act');
        var act = tar.getAttribute('data-act');
        var li = this.getTarNode(dom, 'data-appName');
        var name = li.getAttribute('data-appName');
        var index = parseInt(li.getAttribute('data-index')) - 1;
        var i = index + 1;
        var loadingButton = Ext.select("div[id='" + this.selfId + "'] li[data-index='" + i + "'] li.loading_grey");
        console.log(loadingButton);
        if (loadingButton.elements.length > 0) {
            return;
        }
        else {
            dom.className = dom.className + ' loading_grey';
            this.fireEvent('menuClick', this, act, name, index, e);
        }


//		this.closeMenu(index);
    },
    appClicked: function (e, dom) {
        var tar = this.getTarNode(dom, 'data-appName');
        var appName = tar.getAttribute('data-appName');
        var index = parseInt(tar.getAttribute('data-index')) - 1;
        this.fireEvent('appClick', this, appName, index, e);
    },
    actionDone: function (i) {
        var me = this;
        i++;
        var loadingButton = Ext.select("div[id='" + this.selfId + "'] li[data-index='" + i + "'] li.loading_grey");
        if (loadingButton.elements.length > 0) {
            loadingButton.removeCls('loading_grey');
            var actButton = Ext.select("div[id='" + this.selfId + "'] li[data-index='" + i + "'] span.app_act").elements[0];
            var e = {};
            setTimeout(function () {
                me.showMenu(e, actButton);
            }, 1000);
        }

    },
    bindEv: function (target, eventName, fn, scope) {
        target.un(eventName, fn, scope);
        target.on(eventName, fn, scope);
    },
    getTarNode: function (tar, tag) {
        if (tar.tagName == undefined) {
            return false;
        }
        var res = tar.getAttribute(tag);
        if (res != null && res.length > 0) {
            return tar;
        }
        else {
            return this.getTarNode(tar.parentNode, tag);
        }
    },

    stopLoading: function (i) {
        var me = this;
        var loadingButton = Ext.select("div[id='" + this.selfId + "'] li[data-index='" + i + "'] li.loading_grey");
        loadingButton.removeCls('loading_grey');
        var actButton = Ext.select("div[id='" + this.selfId + "'] li[data-index='" + i + "'] span.app_act").elements[0];
        var e = {};
        me.showMenu(e, actButton);
    }
});

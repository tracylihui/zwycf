Ext.define('cfWeb.cfWebComponent.SlideLayer', {
	extend : 'Ext.window.Window',
	alias : 'widget.slideLayer',
	border:false,
	padding:0,
	fixed:true,
	shadow:false,
	header:false,
	height:200,
	closeAction:'destroy',
	overflowY:'auto',
	selfId:undefined,
	adaptTo:null,
	closeSign:true,
	blankHeight:200,
	style:undefined,
	verticalIn:true,
	initComponent : function() {
		console.log(this);
	    this.addEvents({
	    	'itemsClick':true
	    	//事件
	    });
	    
	    var adt = this.adaptTo;
	    if(this.verticalIn){
		    this.x = adt.getX()+10;
		    var y = adt.getY();
		    this.y = adt.getHeight() + y;
		    this.width = adt.getWidth()-20;
		    this.style = {"max-height" : adt.getHeight() - this.blankHeight};
	    }
	    else{
	    	var height = adt.getHeight();
	    	this.height = height;
	    	this.y = adt.getY();
	    	this.x = adt.getX() + adt.getWidth() +20;
	    }
	    if(this.cls){
	    	this.cls = 'slideUpLayer '+ this.cls;
	    }
	    else{
	    	this.cls = 'slideUpLayer';
	    }
	    console.log(this.style);
		this.callParent(arguments);
	},
	loadMask : function(){
		var body = Ext.select("body");
	    var newMask = document.createElement("div"); 
		newMask.className = 'mask '+this.selfId;
		console.log(newMask);
		body.elements[0].appendChild(newMask);
	},
	adjMask : function(zIndex){
		var msk =  Ext.select("div.mask."+this.selfId).elements[0];
		zIndex--;
		msk.setAttribute('style','z-index:'+zIndex);
	},
  listeners:{
	  boxready : function(){
	      this.selfId = this.getEl().id;
	      if(this.closeSign){
		      var thisBox = Ext.select("div[id='"+this.selfId+"']");
		      var closeSign = document.createElement('span');
		      closeSign.className = 'slideLayerClose icon-cross';
		      thisBox.elements[0].appendChild(closeSign);
	      }
    },
    show:function(){
    	    this.selfId = this.getEl().id;
    	     var thisBox = Ext.select("div[id='"+this.selfId+"']");
    	    var style = thisBox.elements[0].getAttribute('style');
		     console.log("zindexxxxxxxxxxxxxxxxxxxxxxxxx");
		     console.log(style);
		     console.log(style.split(';'));
		    var arrStyle = style.split(';');
		    var zIndex;
		    for(var i = 0;i<arrStyle.length-1;i++){
		    	if(arrStyle[i].indexOf('z-index')>=0){
		    		zIndex = parseInt(arrStyle[i].substring(arrStyle[i].indexOf('z-index')+8));
		    		break;
		    	}
		    }
		    this.loadMask();
		    this.adjMask(zIndex);
		    this.eventBind();

    },
    beforedestroy : function(me,eOpts){
  	  if(this.verticalIn){
		  me.adaptTo.un('resize',me.adapt,me);
	  }
	  else{
		  me.adaptTo.un('resize',me.adaptH,me);
	  }
    }
  },
  eventBind : function(){
	  var me = this;
	  if(this.verticalIn){
		  this.adaptTo.on('resize',me.adapt,me);
	  }
	  else{
		  this.adaptTo.on('resize',me.adaptH,me);
	  }
	  if(this.closeSign){
	      var searchSpan = Ext.select("div[id='"+this.selfId+"'] span.slideLayerClose");
	      searchSpan.on('click',this.slideOut,this);
	  }
      var mask = Ext.select("div.mask."+this.selfId);
      mask.on('click',this.maskClose,this);
      this.on('resize',this.contentAdj,this);
  },
  contentAdj : function(me, width, height, eOpts){
	  var items = this.items.items;
	  if(items.length >0){
		  for(var i = 0;i<items.length;i++){
			  items[i].setWidth(width);
		  }
	  }
  },
  maskClose : function(){
	 this.slideOut();
  },
  adaptH : function(me, width, height, oldWidth, oldHeight, eOpts){
	  this.setHeight(height);
	  var X = this.getX();
	  X = X +width - oldWidth;
	  this.setX(X);
  },
  adapt : function(me, width, height, oldWidth, oldHeight, eOpts){
	  this.setX(me.getX());
	  this.setWidth(width);
	  var hVector = height -oldHeight;
	  this.setHeight(this.getHeight() +hVector);
  },
  getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
	      if(res != null && res.length > 0){
	        return tar;
	      }
	      else{
	        return this.getTarNode(tar.parentNode,tag);
	      }
  },
  slideIn: function(){
	  if(this.verticalIn){
		  this.slideUp();
	  }
	  else{
		  this.slideLeft();
	  }
  },
  slideOut : function(){
	  if(this.verticalIn){
		  this.slideDown();
	  }
	  else{
		  this.slideRight();
	  }
  },
  slideUp : function(){
  	  this.show();
  	  var msk =  Ext.select("div.mask."+this.selfId).elements[0];
  	  msk.className = msk.className + ' on';
	  var height = this.adaptTo.getHeight();
	  if(this.height > height){
		  this.setHeight(height- this.blankHeight);
		  this.setY(this.adaptTo.getY() + this.blankHeight );
	  }
	  else{
		  this.setHeight(this.height);
		  this.setY(this.adaptTo.getY() + height -this.height );
	  } 
  },
  slideLeft: function(){
  	  this.show();
	  var msk =  Ext.select("div.mask."+this.selfId).elements[0];
	  msk.className = msk.className + ' on';
	  var aWidth = this.adaptTo.getWidth();
	  var aX = this.adaptTo.getX();
	  var width = this.width;
	  var X = aX + aWidth - width;
	  this.setX(X);
  },
  slideDown : function(){
	  Ext.removeNode(Ext.select("div.mask."+this.selfId).elements[0]);
	  var me = this;
	  this.setY(this.adaptTo.getHeight() + this.adaptTo.getY());
	  setTimeout(function(){me.close();},300);
  },
  slideRight : function(){
	  Ext.removeNode(Ext.select("div.mask."+this.selfId).elements[0]);
	  var me = this;
	  var aWidth = this.adaptTo.getWidth();
	  var aX = this.adaptTo.getX();
	  var X = aX + aWidth + 100;
	  this.setX(X);
	  setTimeout(function(){me.close();},300);
  }
});
Ext.define('cfWeb.cfWebComponent.store.CfDataStore',{
	extend : 'Ext.data.Store',
	autoLoad : false,
	setParams: function(args){
		if(this.reqUrl){
			var url = this.reqUrl +'?';
			for(var i in args){
				url += i +'='+args[i]+'&';
			}
			var lth = url.length;
			url = url.substring(0, lth-1);
			this.proxy.url = url;
		}
		else{
			this.reqUrl = this.proxy.url;
			var url = this.reqUrl +'?';
			for(var i in args){
				url += i +'='+args[i]+'&';
			}
			var lth = url.length;
			url = url.substring(0, lth-1);
			this.proxy.url = url;
		}
		return this;
	},
	clearParams : function(){
		if(this.reqUrl){
			this.proxy.url = this.reqUrl;
		}
	}
});
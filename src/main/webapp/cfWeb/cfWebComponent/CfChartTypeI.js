Ext.define('cfWeb.cfWebComponent.CfChartTypeI', {
	extend : 'Ext.chart.Chart',
	alias : 'widget.cfChartTypeI',
	
	selfId:undefined,
	cls:'style-cfChartTypeI',
	height:350,
	width:575,
	margin:'0 10 30 0',
	title:'',
    style: 'background:#000',
    shadow: false,
    border:0,
    axes: [{
        type: 'Numeric',
        dashSize:0,
        minimum: 0,
//        maximum:100,
//        majorTickSteps:9,
        position: 'left',
        fields: ['data'], 
        label: {
        	fill:'#aab3c7'
        },
        // title: 'Number of Hits',
        minorTickSteps: 1,
         grid: {
             odd: {
                 opacity: 0.3,
                 type:'circle',
                 stroke: '#d6dae6',
                 'stroke-width': 0.5
             },
             even:{
                     opacity: 0.3,
                     stroke: '#d6dae6',
                     'stroke-width': 0.5
             }
         }
//        grid:true
    }, {
        type: 'Category',
        position: 'bottom',
        dashSize:0,
        fields: ['name'],
        label: {
        	fill:'#aab3c7'
        },
        grid:true,
//        style:{
//        	color:"#f00"
//        },
        constrain : function() {
        	
        },
        grid: {
            odd: {
                opacity: 1,
                type:'circle',
                stroke: '#d6dae6',
                'stroke-width': 0.5
            },
            even:{
                    opacity: 1,
                    stroke: '#d6dae6',
                    'stroke-width': 0.5
            }
        }
        // title: 'Month of the Year'
    }],
    series: [
    {
        type: 'line',
        highlight: {
            size: 7,
            radius: 7
        },
        style:{
        	stroke:'#17a2f7',
        	'stroke-width': 2,
        	fill:'#e1f7f7'
        },
        axis: 'left',
        smooth: true,
        fill: true,
        xField: 'name',
        yField: 'data',
        markerConfig: {
            type: 'circle',
            radius: 3,
            'fill': '#82dddf'
        }
    }],
	initComponent : function() {
	    this.addEvents({
	    });
	    this.titleInit();
		this.callParent(arguments);
	},
	titleInit : function(){
		alert
	    this.html = "<p class='cfChartTitle' style='width:"+this.width+"px '>" + this.title +"</p>";
	},
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   },
   eventBind :function(){
	   var header = this.header.getEl(); 
	   header.on('click',this.headClick,this);
   }
});

Ext.define('cfWeb.cfWebComponent.SwitchContentView', {
	extend : 'Ext.container.Container',
	alias : 'widget.switchContentView',
	cls:'style-switchContentView',
	width:600,
	listLength:0,
	nowIndex:0,
	height:500,
	nowStatus:0,
	readyStatus:1,
	statusList:[],
	selfId : undefined,
	html :undefined,
    initComponent: function() {
    	var me = this;
    	me.addEvents({
    		'change':true,
    		'listChange':true
    	});
    	this.htmlInit();
    
        this.callParent(arguments);
    },
    listeners:{
    	boxready : function(){
    		this.selfId = this.getEl().id;
    		this.eventBind();
    		this.nowStatus ++;
    		this.setValue(this.statusList);
    	}
    },
    eventBind :function(){
    	var next =  Ext.select("div[id='"+this.selfId+"'] span.switch_forward");
    	var prev = Ext.select("div[id='"+this.selfId+"'] span.switch_backward");
    	var dots = Ext.select("div[id='"+this.selfId+"'] div.nav_dot");
    	next.un('click',this.goNext,this);
    	prev.un('click',this.goPrev,this);
    	dots.un('click',this.jump,this);
    	next.on('click',this.goNext,this);
    	prev.on('click',this.goPrev,this);
    	dots.on('click',this.jump,this);
    },
    jump : function(e,dom){
    	var tar = this.getTarNode(dom,'data-index');
    	if(tar.className.indexOf(' on')<0){
        	var index = tar.getAttribute('data-index');
        	var on =  Ext.select("div[id='"+this.selfId+"'] div.on").elements[0];
        	on.className = on.className.replace(' on','');
        	dom.className = dom.className +' on';
        	this.nowIndex = index;
        	this.fireEvent('change',this.nowIndex ,this);
    	}
    },
    goNext : function(){
    	if(this.nowIndex > -1){
    		var on = Ext.select("div[id='"+this.selfId+"'] div.on").elements[0];
    		on.className = on.className.replace(' on','');
    		if(this.nowIndex<this.listLength-1){
    			this.nowIndex ++;
    		}
    		else{
    			this.nowIndex = 0;
    		}
    		var dots = Ext.select("div[id='"+this.selfId+"'] div.nav_dot").elements[this.nowIndex];
    		dots.className = dots.className +' on';
    		this.fireEvent('change',this.nowIndex ,this);
    		console.log(this.nowIndex);
    	}
    },
    goPrev : function(){
    	if(this.nowIndex > -1){
    		var on = Ext.select("div[id='"+this.selfId+"'] div.on").elements[0];
    		on.className = on.className.replace(' on','');
    		if(this.nowIndex>0){
    			this.nowIndex --;
    		}
    		else{
    			this.nowIndex = this.listLength -1;
    		}
    		var dots = Ext.select("div[id='"+this.selfId+"'] div.nav_dot").elements[this.nowIndex];
    		dots.className = dots.className +' on';
    		this.fireEvent('change',this.nowIndex ,this);
    	}
    },
    htmlInit : function(){
    	var list='';
    	var lth = this.statusList.length;
    	this.listLength = lth;
    	this.nowIndex = 0;
    	if(lth>0){
    		this.nowIndex = 0;
			list +='<div class="nav_dot on" data-index="'+0+'">';
			list +='<div class="nav_state">';
			list +=this.statusList[0];
			list +='</div></div>';
    		for(var i =1;i<lth;i++){
    			list +='<div class="nav_dot" data-index="'+i+'">';
    			list +='<div class="nav_state">';
    			list +=this.statusList[i];
    			list +='</div></div>';
    		}
    	}
    	html = '<span class="switch_backward icon-arrow-left3"></span>';
    	html+='<span class="switch_forward icon-uniE620"></span>';
    	html+='<div class="nav_sign">';
    	html+=list;
    	html+='</div>';
    	this.html = html;
    },
    setValue: function(data){
    	if(!this.instData){
    		this.instData = [];
    		for(var i = 0;i<data.length; i++){
    			this.instData.push({cpu:[],mem:[],disk:[]});
    			for(var j = 0;j<10;j++){
    				this.instData[i].cpu.push({date:Ext.Date.format(new Date(),'Y-m-d H:m:s')+j,value:0,data:0}); 
    				this.instData[i].mem.push({date:Ext.Date.format(new Date(),'Y-m-d H:m:s')+j,value:0,data:0}); 
    				this.instData[i].disk.push({date:Ext.Date.format(new Date(),'Y-m-d H:m:s')+j,value:0,data:0});
    			}
    		}
    	}
    	else{
    		if(data.length > this.instData.length){
        		for(var i = this.instData.length;i<data.length; i++){
        			this.instData.push({cpu:[],mem:[],disk:[]});
        			for(var j = 0;j<10;j++){
        				this.instData[i].cpu.push({date:Ext.Date.format(new Date(),'Y-m-d H:m:s')+j,value:0,data:0}); 
        				this.instData[i].mem.push({date:Ext.Date.format(new Date(),'Y-m-d H:m:s')+j,value:0,data:0}); 
        				this.instData[i].disk.push({date:Ext.Date.format(new Date(),'Y-m-d H:m:s')+j,value:0,data:0}); 
        			}
        		}
    		}
    		else{
    			for(var i = data.length;i<this.instData.length;i++){
    				this.instData.pop();
    			}
    		}
    	}
    	if(this.nowStatus >= this.readyStatus){
        	var lth =  data.length;
        	var list = '';
        	if(lth >0){
        		if(this.nowIndex >lth-1){
            		this.statusList = data;
        			list +='<div class="nav_dot on" data-index="'+0+'">';
        			list +='<div class="nav_state">';
        			list +=this.statusList[0];
        			list +='</div></div>';
            		for(var i =1;i<lth;i++){
            			list +='<div class="nav_dot" data-index="'+i+'">';
            			list +='<div class="nav_state">';
            			list +=data[i];
            			list +='</div></div>';
            		}
            		this.listLength = lth;
            		this.nowIndex = 0;
        		}
        		else{
            		this.statusList = data;
            		for(var i =0;i<lth;i++){
            			var on = '';
            			if(i == this.nowIndex){
            				on = ' on';
            			}
            			list +='<div class="nav_dot'+on+'" data-index="'+i+'">';
            			list +='<div class="nav_state">';
            			list +=data[i];
            			list +='</div></div>';
            		}
            		this.listLength = lth;
        		}

        		var wrap =  Ext.select("div[id='"+this.selfId+"'] div.nav_sign").elements[0];
        		wrap.innerHTML = list;
        		this.eventBind();
        		this.fireEvent('listChange',this);
        	}
    	}
    	else{
    		this.statusList = data;
    	}
    	console.log(this.instData);
    },
    getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
	      if(res != null && res.length > 0){
	        return tar;
	      }
	      else{
	        return this.getTarNode(tar.parentNode,tag);
	      }
  }
});

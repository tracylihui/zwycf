Ext.define('cfWeb.cfWebComponent.ThresholdControlView', {
			extend : 'Ext.container.Container',
			alias : 'widget.thresholdControlView',
			width:320,
			height:480,
			autoScale:false,
			padding: '40 0 30 60',
			cls:'style-thresholdControlView',
			selfId : undefined,
			layout:'column',
			title:'',
			isDirty:false,
			isUpdating:false,
			isSwitchable:true,
			hasSwitcher:true,
			langSetting:{submit:cfWebLang.Util.Confirm ,updating:cfWebLang.HomeTabView.update,done:cfWebLang.Util.Done},
			initComponent : function() {
				this.addEvents({
					'modeChange':true,
					'changeSubmit':true
				});
				this.originValues = new Array();
				this.htmlInit();
				this.argsInit();
				this.callParent(arguments);
			},
			eventBind : function(){
				var me = this;
				if(this.isSwitchable){
					var switchButton =  Ext.select("div[id='"+this.selfId+"'] span.switch_sign");
					switchButton.on('click',this.changeContent,this);
				}
				var button = Ext.select("div[id='"+this.selfId+"'] div.tc_saveButton");
				button.on('click',this.buttonClick,this);
			},
			buttonClick:function(e,dom){
				var className = dom.className;
				if(className.indexOf(' disable')>0){
					return;
				}
				else if (className.indexOf(' loading')>0){
					return;
				}
				else{
					className = className +' loading';
					dom.className = className;
					dom.innerHTML = this.langSetting.updating;
					 var lth =  this.items.items.length;
					 var data = [];
					 for(var i = 0;i<lth;i++){
						 var val = this.items.items[i].getValue();
						 var dirty =  this.items.items[i].isDirty;
						 data.push({value:val,isDirty:dirty});
						 this.originValues[i] = val;
						 this.items.items[i].isDirty = false;
					 }
					 this.fireEvent('changeSubmit',this,data);
					 this.isUpdating = true;
				}
			},
			argsInit: function(){
				var lth = this.items.length;
				for(var i = 0 ;i < lth;i++){
					this.items[i].sequence = i;
					this.originValues.push(0);
				}
			},
			eventAdd : function(){
				var lth = this.items.items.length;
				for(var i =0; i <lth; i++){
					this.items.items[i].addListener('changecomplete',this.buttonAdj,this);
					this.items.items[i].addListener('valueSet',this.changeOri,this);
				}
			},
			changeOri : function(cfSlider,val){
				var q = cfSlider.sequence;
				this.originValues[q] = val;
			},
			buttonAdj : function(cfSlider,slider, newValue, thumb, eOpts){
				var button = Ext.select("div[id='"+this.selfId+"'] div.tc_saveButton");
				if(arguments.length >1){
					var className = button.elements[0].className;
					var q = cfSlider.sequence;
					if(className.indexOf(' loading')>0){
						var oriV = this.originValues[q]/cfSlider.times;
						cfSlider.setValue(oriV);
						return;
					}
					var ovar=this.originValues[q]/cfSlider.times;
					if(ovar != newValue){
						button.removeCls('disable'); 
						cfSlider.isDirty = true;
					}
					else{
						cfSlider.isDirty = false;
						var lth = this.items.items.length;
						var dirty = false;
						for(var i=0;i<lth;i++){
							if(this.items.items[i].isDirty){
								dirty = true;
								break;
							}
						}
						if(!dirty){
							button.addCls('disable');
						}
					}
				}
				else if(arguments.length ==1){
					if(arguments[0] == 'enable'){
						button.removeCls('disable'); 
					}
					else{
						button.addCls('disable');
					}
				}

			},
			changeContent :function(e,dom){
				if(this.isUpdating){
					reuturn;
				}
				else{
					if(this.isDirty){
						this.isDirty = false;
						this.buttonAdj('disable');
					}
					else{
						this.isDirty = true;
						this.buttonAdj('enable');
					}
				}
				var className = dom.className;
				if(className.indexOf(' on')>=0){
					this.setMode('manual');
				}
				else{
					this.setMode('auto');
				}
			},
			setMode : function(mode){
				var button = Ext.select("div[id='"+this.selfId+"'] span.switch_sign");
				if(mode =="auto"||mode== true){
					button.addCls('on');
					this.fireEvent('modeChange',this,'auto');
					this.autoScale = true;
					this.items.items[0].hide();
					this.items.items[1].hide();
					this.items.items[2].show();
					this.items.items[3].show();
				}
				else if(mode =='manual'||mode==false){
					button.removeCls('on');
					this.fireEvent('modeChange',this,'manual');
					this.autoScale = false;
					this.items.items[0].show();
					this.items.items[1].show();
					this.items.items[2].hide();
					this.items.items[3].hide();
				}
				return this;
			},
			getMode : function(){
				return this.autoScale;
			},
			listeners:{
				boxready : function(){
					var me = this;
					me.selfId = me.getEl().id;
					this.eventBind();
					this.eventAdd();
//					this.loadInit();
				}
			},
			getTargetNode : function(dom ,tag){
				   if(dom.tagName ==undefined){
					      return false;
					}
				    if(dom.tagName == tag){
				        return dom;
				    }
				    else{
				        return this.getTargetNode(dom.parentNode,tag);
				    }
			},
			htmlInit : function(){
				var isOn = '';
				var has = '';
				if(!this.isSwitchable){
					isOn = ' disabled';
				}
				if(!this.hasSwitcher){
					has =' hidden';
				}
				this.html = '<p class="thresholdControlView_title">'+this.title+':</p><div class="thresholdControlView_slide'+ has +'"><span class="icon-cross switch_sign'+isOn+'"></span></div><div class="tc_saveButton disable">'+this.langSetting.submit+'</div>';
			},
			commitChange:function(){
				this.isUpdating = false;
				this.isDirty = false;
				var me = this,
				button = Ext.select("div[id='"+this.selfId+"'] div.tc_saveButton"),
				items = this.items.items,
				lth = items.length;
//				for(var i =0; i <lth; i++){
//					var point = items[i].getValue();
//					alert(point);
//					items[i].fireEvent('valueSet',items[i],point);
//				}
				
				button.elements[0].innerHTML = this.langSetting.done;
				setTimeout(function(){
					button.removeCls('loading');
					button.addCls('disable');
					button.elements[0].innerHTML = me.langSetting.submit;
				},1000);
			}
		});




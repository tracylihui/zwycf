Ext.define('cfWeb.cfWebComponent.RingChart', {
	extend : 'Ext.container.Container',
	alias : 'widget.ringChart',
	colorList:{
		grey : ['#222222', '#eef1f8'],
		blue : ['#54b1f6','#98d0fa','#c0e0f8'],
		purple : ['#7441d2','#b6a0de','#d7caee'],
		green : ['#2ec6c9','#abe8e9','#e1f7f7'],
		orange : ['#f39851','#f5b482','#f5d3b8']
	},
	loadingCount:0,
	readyVal :2,
	store:undefined,
	selfId : undefined,
	color:undefined,
	colorNow : undefined,
	unit:undefined,
	title:undefined,
	icoType:undefined,
	loadingMsg:cfWebLang.HomeTabView.loadingMsg,
	unusedMsg : cfWebLang.HomeTabView.unused,
	html :undefined,
    initComponent: function() {
    	var me = this;
    	me.addEvents({
    		
    	});
    	this.argsInit();
    	this.store.on('datachanged',this.loadCount,this);
        this.callParent(arguments);
    },
    
    loadCount : function(){  //加载时机判断  store 和iew 同时 ready
    	this.loadingCount++;
    	console.log(this.loadingCount);
    	if(this.loadingCount >= this.readyVal){
    		this.htmlConstructor(this);
    	}
    },
    argsInit : function(){
    	this.html = '<div class="pie_ring_wrap"><p class="loading_tip">'+this.loadingMsg+'...</p></div>';
    	if(!this.color){
    		this.colorNow = this.colorList['blue'];
    	}
    	else{
    		if(typeof this.color =='string'){
    			this.colorNow = this.colorList[this.color];
    		}
    		else if(typeof this.color =='object'){
    			this.colorNow = this.color;
    		}
    	}
    	if(!this.icoType){
    		this.icoType = 'rocket';
    	}
    	if(!this.title){
    		this.title = '';
    	}
    	if(!this.unit){
    		this.unit = '';
    	}
    },
    htmlConstructor : function(me){
    	var items = me.store.data.items,
    		lth = items.length,
    		priWidth = 50*lth +12,
    		colorList;
    	if(lth ==1){
    		priWidth = 50*2 +12;
    	}
    	colorList = me.colorNow;
 
    	var pieRing = '<div class="pie_ring">';
    	var pieRingInstru = '<ul class="pie_ring_instruction" style="width:'+ priWidth +'px">';
    	var deg = 0;
    	var total = items[0].data.total;
    	var pieCount = '';
    	for(var i = 0;i < lth ; i++){
    		var itemId = items[i].data.itemId,
    		 	itemName = items[i].data.itemName,
    			itemCount = items[i].data.itemCount;
    		var nowDeg=0;
    		if(isNaN(itemCount)){
    			itemCount=0;
    		}
    		pieCount += itemCount +'/';
    		if(isNaN(total)){
    			total=0;
    		}
    		if(total!=0){
    			nowDeg = 360*(itemCount/total);
    		}
    		//构建环
    		if(nowDeg<=180){
    			pieRing += '<b style="'+ me.cssGenerator('transform','rotate('+deg+'deg)') +'">';
    			pieRing += '<i data-id='+ itemId +' style="'+ me.cssGenerator('transform','rotate('+nowDeg+'deg)') +'border-color:'+colorList[i]+'"></i>';
    			pieRing +='</b>';
    			deg += nowDeg;
    		}
    		else{
    			pieRing += '<b style="'+ me.cssGenerator('transform','rotate('+deg+'deg)') +'">';
    			pieRing += '<i data-id='+ itemId +' style="'+ me.cssGenerator('transform','rotate(180deg)') +'border-color:'+colorList[i]+'"></i>';
    			pieRing +='</b>';
    			deg +=180;
    			nowDeg -= 180;
    			pieRing += '<b style="'+ me.cssGenerator('transform','rotate('+deg+'deg)') +'">';
    			pieRing += '<i data-id='+ itemId +' style="'+ me.cssGenerator('transform','rotate(' + nowDeg + 'deg)') +'border-color:'+colorList[i]+'"></i>';
    			pieRing +='</b>';
    			deg += nowDeg;
    		}
    		//构建说明
    		var num=0;
    		if(total==0){
    			num=0;
    		}else{
    			num = (((itemCount/total).toFixed(4))*100).toFixed(2);
    		}
        	var field = num + '';
        	var len = field.length;
        	if(len == 4){
        		if(field[2] == '0' && field[3]=='0') {
            		num = field[0];
            	}
        	}
        	if(len == 5){
        		if(field[3] == '0' && field[4]=='0') {
            		num = field[0]+field[1];
            	}
        	}if(len == 6){
        		if(field[4] == '0' && field[5]=='0') {
            		num = field[0]+field[1] +field[2];
            	}
        	}
        	
    		pieRingInstru +=  '<li class="pri_items" data-id="'+ itemId +'">';
    		pieRingInstru +=  '<span class="color_cube" style="background-color:'+colorList[i]+'"></span>';
    		pieRingInstru +=  '<p class="status_instru">'+itemName+'</p>';
    		pieRingInstru +=  '<p class="instru_percentage" style="color:'+colorList[0]+'">'+num+'%</p>';
    		pieRingInstru +=  '</li>';
    	}
    	
    	if(lth ==1){
    		var itemCount=isNaN(items[0].data.itemCount)?0:items[0].data.itemCount;
    		var num=0;
    		if(total==0){
    			num=0;
    		}else{
    			num = (100- (100*(itemCount/total))).toFixed(2);
    		}  		
    		var field = num + '';
        	var len = field.length;
        	if(len == 4){
        		if(field[2] == '0' && field[3]=='0') {
            		num = field[0];
            	}
        	}
        	if(len == 5){
        		if(field[3] == '0' && field[4]=='0') {
            		num = field[0]+field[1];
            	}
        	}if(len == 6){
        		if(field[4] == '0' && field[5]=='0') {
        			num = field[0]+field[1] +field[2];
            	}
        	}
    		
    		pieRingInstru +=  '<li class="pri_items">';
    		pieRingInstru +=  '<span class="color_cube" style="background-color:'+me.colorList['grey']+'"></span>';
    		pieRingInstru +=  '<p class="status_instru">' + this.unusedMsg + '</p>';
    		pieRingInstru +=  '<p class="instru_percentage" style="color:'+colorList[0]+'">'+ num +'%</p>';
    		pieRingInstru +=  '</li>';
    		
    		pieCount += total;
    	}
    	else{
    		var pcLength = pieCount.length;
    		pieCount = pieCount.substring(0, pcLength-1);
    	}
    	
    	pieRing += '</div>';
    	pieRingInstru += '</ul>';
    	var num=0;
		if(total==0){
			num=0;
		}else{
			var itemCount=isNaN(items[0].data.itemCount)?0:items[0].data.itemCount;
			num = itemCount/total;
	    	num = ((num.toFixed(4))*100).toFixed(2);
		}
    	
    	
    	var field = num + '';
    	var len = field.length;
    	if(len == 4){
    		if(field[2] == '0' && field[3]=='0') {
        		num = field[0];
        	}
    	}
    	if(len == 5){
    		if(field[3] == '0' && field[4]=='0') {
        		num = field[0]+field[1];
        	}
    	}if(len == 6){
    		if(field[4] == '0' && field[5]=='0') {
    			num = field[0]+field[1] +field[2];
        	}
    	}
    	
    	var pieRingSign  = '<div class="pie_ring_sign" style="color:'+ colorList[0] +'">';
    		pieRingSign += '<span class="icon-' + me.icoType + ' pie_ring_ico"></span>';
    		pieRingSign += '<p class="pie_ring_text">' + num + '%</p>';
    		pieRingSign += '</div>';
    	var pieHead = '<div class="pie_ring_head">' + pieRing + pieRingSign + '</div>';
    	var pieRingTitle = '<div class="pie_ring_title">';
    		pieRingTitle += '<p class="count"> '+ pieCount + me.unit +'</p>';
    		pieRingTitle +='<p class="title">'+me.title+'</p></div>';
    	var html = '<div class="pie_ring_wrap">' + pieHead + pieRingTitle + pieRingInstru + '</div>';
    	 var ringWrap =  Ext.select("div[id='"+me.selfId+"'] div.pie_ring_wrap").elements[0];
    	 ringWrap.innerHTML = html;
    	 return true;
    },
    cssGenerator : function(key,value){
    	var css = key +':' +value+';';
    	css+= '-ms-'+key +':' + value+';';
    	css+= '-o-'+key +':' + value+';';
    	css+= '-webkit-'+key +':' + value+';';
    	css+= '-moz-'+key +':' + value+';';
    	return css;
    },
    listeners:{
    	boxready : function(){
    		this.selfId = this.getEl().id;
    		this.loadCount();
    	}
    }


});

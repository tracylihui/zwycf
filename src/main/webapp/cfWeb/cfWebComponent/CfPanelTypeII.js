Ext.define('cfWeb.cfWebComponent.CfPanelTypeII', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.cfPanelTypeII',
	selfId:undefined,
	overflowX: 'hidden',
//	titleCollapse : true,
	cls:'',
	tools:[],
	header:{
		xtype:'panelHeaderTypeII',
		width:150
	},
	headerPosition:'left',
	layout:'fit',
	initComponent : function() {
	    this.addEvents({
	    });
	    this.clsInit();
	    this.viewInit();
    	this.headerInit();
		this.callParent(arguments);
	},
  listeners:{
    boxready : function(){
    	this.selfId = this.getEl().id;
    	this.eventBind();
    }
  },
  clsInit: function(){
	  this.cls  +=' style-CfPanelTypeII';
  },
  viewInit : function(){
	  var lth = this.items.length;
	  for(var i = 1;i<lth;i++){
		  this.items[i].hidden = true;
	  }
  },
  headerInit :function(){
	  var items = this.items;
	  var lth = items.length;
	  var heads = [];
	  for(var i = 0;i<lth;i++){
		  var title = items[i].xtitle;
		  heads.push(title);
	  }
	  this.header.heads = heads;
  },
  
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   },
   eventBind :function(){
	   var header = this.header.getEl(); 
	   header.on('click',this.headClick,this);
	   this.header.on('headChange',this.headChange,this);
	   console.log(header);
   },
   headChange: function(me,tar,e){
	   console.log(this);
	  var lth = this.items.items.length;
	  for(var i= 0;i<lth;i++){
		  this.items.items[i].hide();
	  }
	  this.down('[xtitle='+tar+']').show();
   }
});

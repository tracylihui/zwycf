Ext.define('cfWeb.cfWebComponent.IconListView', {
	extend : 'Ext.view.View',
	alias : 'widget.iconListView',
	selfId:undefined,
	loadMask : false,
	loadingMask:true,
	loadStatus:0,
	readyStatus:2,
	tpl:[
	     '<div class="cw_content">',
	     '<ul class="clearfix icon_list">',
	     '<tpl for=".">',
	     '<li data-id ="{itemId}" data-img ="{itemIco}" data-name ="{itemName}">',
         '<a class="icon_wrap"><img src="{itemIco}"></a>',
         '<p class="item_title"><a>{itemName}</a></p>',
         '</li>',
	     '</tpl>',
	     '</ul>',
	     '</div>'
	     ],
	initComponent : function() {
	    this.addEvents({
	    	'itemsClick':true
	    	//事件
	    });
	    this.setLoadingMask();
	    this.store.on('load',this.loadInit,this);
	    this.store.on('filterchange',this.bindEvents,this);
		this.callParent(arguments);
	},
	listeners:{
	  	viewready : function(){
	  		this.loadInit();
    	}
  	},
	setLoadingMask : function(){
		if(this.loadingMask){
			this.html = "<div class='loading_mask'>"+cfWebLang.HomeTabView.loadingMsg+"</div>";
		}
	},
	hideMask: function(){
		if(this.loadingMask){
			var hideMsk = Ext.select("div[id='"+this.selfId+"'] div.loading_mask");
			hideMsk.addCls('dis_n');
			var height = parseInt(this.getHeight());
			this.setHeight(height);
		}
	},
	loadInit : function(){
		this.loadStatus++;
		console.log(this.loadStatus);
		if(this.loadStatus >= this.readyStatus){
			this.bindEvents();
			this.hideMask();
		}
	},
	bindEvents : function(){
		this.selfId = this.getEl().id;
		var items_a = Ext.select("div[id='"+this.selfId+"'] ul.icon_list a.icon_wrap"); 
		var items_p = Ext.select("div[id='"+this.selfId+"'] ul.icon_list p.item_title");
		console.log(items_a);
		console.log(this.itemId);
		items_a.on('click',this.onItemsClick,this);
		items_p.on('click',this.onItemsClick,this);
		this.on('resize',this.autoAdjust,this);
	},
	autoAdjust : function(me, width, height, oldWidth, oldHeight, eOpts){
		var inner = Ext.select("div[id='"+this.selfId+"'] div.cw_content").elements[0];
		var height = inner.getAttribute('height');
		me.setHeight(height);
	},
	getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
    	if(res != null && res.length > 0){
    		return tar;
    	}
    	else{
    		return this.getTarNode(tar.parentNode,tag);
    	}
	},
	onItemsClick : function(e,dom){
		var tar = this.getTarNode(dom,'data-id');
		var id = tar.getAttribute('data-id');
		var img = tar.getAttribute('data-img');
		var name = tar.getAttribute('data-name');
		var record = {itemId:id,itemIco:img,itemName:name};
		this.fireEvent('itemsClick',this,record,e);
	}
});
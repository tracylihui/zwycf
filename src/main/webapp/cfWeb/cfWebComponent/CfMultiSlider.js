Ext.define('cfWeb.cfWebComponent.CfMultiSlider', {
			extend : 'Ext.container.Container',
			alias : 'widget.cfMultiSlider',
			width:100,
			maxValue:100,
			height:390,
			padding:'0 0 20 0',
			isDirty:false,
			title:'测试',
			cls:'style-cfSliderWrap',
//			html:'<span style="color:#f00">test</span>',
			selfId : undefined,
			initComponent : function() {
				var me = this;
				this.addEvents({
//					'onItemsClick':true,
//					'oncollapse' : true,
//					'onExpand' : true
					'changecomplete': true
				});
				
				this.htmlInit();
				
				this.items=[
				            {
				            	xtype:'multislider',
				            	margin:'10 20 10 60',
				                height:350,
				                useTips:false,
				                cls:'style-cfSlider',
				                increment: 1,
				                minValue: 0,
				                maxValue: 100,
				                vertical:true,
				                listeners:{
				                	changecomplete:{
				                		fn:me.changeComp,
				                		scope:me
				                	},
				                	change :{
				                		fn:me.floatChange,
				                		scope:me
				                	},
				                	afterrender : function(me,eOpts){
				                		var innerHTML = '<div class="float_sym"><span class="fs_content">0</span><div class="fs_sym"></div></div>';
				                		me.getEl().select('div.x-slider-thumb').setHTML(innerHTML);
				                	}
				                }
//				                fieldLabel:'cpu',
//				                labelStyle:{
//				                	color:'#8d99b3'
//				                }
				            }
				            ];
				this.argsInit();
//				this.store.on('load',this.loadInit,this);
				this.callParent(arguments);
			},
			eventBind : function(){
				var me = this;
				me.items[0].on('changecomplete',this.changeComp,this);
			},
			changeComp : function(slider, newValue, thumb, eOpts){
				this.fireEvent('changecomplete', this,slider, newValue, thumb, eOpts);
			},
			floatChange : function(slider, newValue, thumb, eOpts){
				thumb.el.dom.firstChild.firstChild.innerHTML = newValue;
			},
			listeners:{
				boxready : function(){
					var me = this;
					me.selfId = me.getEl().id;
//					var thumb = Ext.select("div[id='"+this.selfId+"'] div.x-slider-thumb").elements;
//					
//					var innerHTML = '<div class="float_sym"><span class="fs_content">0</span><div class="fs_sym"></div></div>';
//					for(var i = 0;i<thumb.length;i++){
//						thumb[i].innerHTML = innerHTML;
//					}
					
//					this.loadInit();
				},
				
			},
			argsInit: function(){
				if(this.height){
					this.items[0].height = this.height -40;
				}
				if(this.maxValue){
					this.items[0].maxValue = this.maxValue;
				}
				if(this.value){
					this.items[0].value = this.value;
				}
				if(this.values){
					this.items[0].values = this.values;
				}		
				if(this.minValue){
					this.items[0].minValue = this.minValue;
				}
				if(this.increment){
					this.items[0].increment = this.increment;
				}
			},
			htmlInit: function(){
				var html = '<p class="cf_slider_title">'+this.title+'</p>';
				var height = this.height;
				height -=40;
				
				html +='<div class="cf_slider_sym" style="height:'+ height +'px"><span class="cf_slider_top">'+this.maxValue+'</span><span class="cf_slider_bottom">0</span><div class="main_sym"></div></div>';
				this.html = html;
				
			},
			getTargetNode : function(dom ,tag){
				   if(dom.tagName ==undefined){
					      return false;
					}
				    if(dom.tagName == tag){
				        return dom;
				    }
				    else{
				        return this.getTargetNode(dom.parentNode,tag);
				    }
			},
			getValue : function(index){
				return this.items.items[0].getValue(index);
			},
			getValues : function(){
				return this.items.items[0].getValues();
			},
			setValue : function(index, value, animate ){
				return this.items.items[0].setValue(index, value, animate);
			},
			setMaxMinValue:function(max,min){
				this.items.items[0].setMaxValue(max);
				this.items.items[0].setMaxValue(min);
			}
			
		});




Ext.define('cfWeb.cfWebComponent.HeadView', {
	extend : 'Ext.container.Container',
	alias : 'widget.headView',
	layout : 'border',
	height:181,
	selfId:undefined,
	title : undefined,
	htmlTemp:'<div class="app_detail_head">'+
        	 '<div class="adh_logo" data-name="itemColor" style="color:#f00">'+
        	 '<img src="resources/img/icons/app1.png" height="90" width="180" data-name="itemImg" />'+
        	 '<span class="adh-icon adh_message_count" data-name="message"></span>'+
        	 '<p class="adh_title" data-name="title"></p>'+
        	 '</div>'+
        	 '<div class="add_detail_status">',
    htmlTemp2 :'</div></div>',
    domainList:[],
	detailList:[{name:'类型',dataIndex:'tags'},{name:'label',dataIndex:'label'},{name:'plan',dataIndex:'plan'},{name:'hostname',dataIndex:'hostname'},{name:'port',dataIndex:'port'},{name:'username',dataIndex:'username'},{name:'password',dataIndex:'password'}],
	appData:null,
	actionList:{'add':'新建','delete':'删除','edit':'编辑','save':'保存','start':'启动','stop':'停止','restart':'重启','loading':'载入中'},
	isEditing:false,
	initComponent : function() {
		this.htmlInit();
	    this.addEvents({
	    	//事件
//	      'onStatusChange': true,
//	      'onDomainChange':true
	    });
		this.callParent(arguments);
	},
  listeners:{
    boxready : function(){
      this.selfId = this.getEl().id;
      this.updateAppStatus(this.appData);
      this.eventBind();
    }
  },
  eventBind : function(){	  
  },
  getValues:function(){
	  return this.appData;
  },
  updateAppStatus :function(data){
	  console.log(data);
	  if(!this.appData){
		  this.appData = new Object();
	  }
	  if(!data){
		  return false;
	  }
	  else{
		  var lth = this.detailList.length;
		  for(var i = 0; i<lth; i++){
			  var dataIndex =this.detailList[i].dataIndex;
			  if(data[dataIndex] == undefined){
				  continue;
			  }
			  else{
				  var tar = Ext.select("div[id='"+this.selfId+"'] span[data-name='"+dataIndex+"']").elements[0];
				  tar.innerHTML = data[dataIndex];
				  this.appData[dataIndex] = data[dataIndex];
			  }
		  };
		  if(data.itemColor){
			  var tar = Ext.select("div[id='"+this.selfId+"'] div[data-name='itemColor']").elements[0];
			  tar.setAttribute('style','color:'+data.itemColor);
			  this.appData.itemColor = data.itemColor;
		  }
		  if(data.itemImg){
			  var tar = Ext.select("div[id='"+this.selfId+"'] img[data-name='itemImg']").elements[0];
			  tar.setAttribute('src',data.itemImg);
			  this.appData.itemImg = data.itemImg;
		  }
		  if(data.title){
			  var tar = Ext.select("div[id='"+this.selfId+"'] p[data-name='title']").elements[0];
			  tar.innerHTML = data.title;
			  //tar.style.color = data.itemColor;
			  tar.setAttribute('style','font-size:28px;color:'+data.itemColor);
			  this.appData.title = data.title; 
		  }
		  if(data.message){
			  var extTar = Ext.select("div[id='"+this.selfId+"'] span[data-name='message']");
			  extTar.show();
			  var tar = extTar.elements[0];
			  tar.innerHTML = data.message;
			  this.appData.message = data.message;
			  alert('sdsds');
		  }
		  else{
			  var tar = Ext.select("div[id='"+this.selfId+"'] span[data-name='message']");
			  tar.hide();
		  }
	  }
	  this.eventBind();
  },
  htmlInit : function(){
	  var lth = this.detailList.length;
	  var detail = '';
	  detail += '<div class="add_detail_left">';
	  for(var i=0;(i<lth)&&(i<5);i++){
		  detail +='<dl class="add_detail_items">';
		  var mh ="：";
		  if(this.detailList[i].name ==''){
			  mh = '';
		  }
		  detail +='<dt>' + this.detailList[i].name+mh +'</dt>';
		  detail +='<dd><span data-name="'+this.detailList[i].dataIndex+'"></span></dd></dl>';
	  }
	  detail += '</div>';
	  detail += '<div class="add_detail_right">';
	  for(var i=5;i<lth;i++){
		  var mh ="：";
		  if(this.detailList[i].name ==''){
			  mh = '';
		  }
		  detail +='<dl class="add_detail_items">';
		  detail +='<dt>' + this.detailList[i].name+mh+'</dt>';
		  detail +='<dd><span data-name="'+this.detailList[i].dataIndex+'"></span></dd></dl>';
	  }
	  detail += '</div>';
	  this.html = this.htmlTemp + detail + this.htmlTemp2;
  },
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   }
});

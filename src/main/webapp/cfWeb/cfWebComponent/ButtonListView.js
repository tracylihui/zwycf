Ext.define('cfWeb.cfWebComponent.ButtonListView', {
	extend : 'Ext.view.View',
	alias : 'widget.buttonListView',
	selfId : undefined,
	menuList:[{name:'今天',act:'today',type:'text'},{name:"昨天",act:"yesterday",type:'text'},{name:"本周",act:"thisWeek",type:'text'},{icoCls:'calendar',act:'calendar',type:'ico'}],
	html :undefined,
	onButton:1,
    initComponent: function() {
    	var me = this;
    	me.addEvents({
    		"buttonClick":true
    	});
    	this.htmlConstructor();
        this.callParent(arguments);
    },
    htmlConstructor : function(){
    	var lth = this.menuList.length;
    	var width = lth*75;
    	var ul = '<ul class="picker_list" style="width:'+width+'px">';
    	
    	var menu = this.menuList;
    	for(var i =0;i < lth; i++){
    		var mc = false;
    		if(menu[i].multiClick){
    			mc = true;
    		}
    		if(menu[i].type == 'text'){
    			ul +='<li data-act="'+menu[i].act+'" data-mc="'+mc+'">'+menu[i].name+'</li>';
    		}
    		else if(menu[i].type == 'ico'){
    			ul +='<li data-act="'+menu[i].act+'" class="'+menu[i].icoCls+'" data-mc="'+mc+'"></li>';
    		}
    	}
    	ul +='</ul>';
    	var html = '<div class="date_selector" style="width:'+width+'px">' + ul +'</div>';
    	this.tpl = html;
    },
    listeners:{
        viewready : function(){
        	this.selfId = this.getEl().id;
        	this.eventBind();
        	this.statusInit();
        }
      },
     statusInit : function(){
    	 var els = Ext.select("div[id='"+this.selfId+"'] ul.picker_list li").elements;
    	 var lth = els.length;
    	 if(lth >0 &&lth>=this.onButton ){
        	 if(this.onButton ){
        		 var q = this.onButton -1;
        		 var tar = els[q];
        		 tar.className = ' on';
        	 }
        	 else{
        		 els[0].calssName = ' on';
        	 }
    	 }

     },
    eventBind : function(){
    	 var menuItem =  Ext.select("div[id='"+this.selfId+"'] ul.picker_list li");
    	 menuItem.on('click',this.buttonClick,this);
    },
    getTarNode :function(tar,tag){
        if(tar.tagName ==undefined){
          return false;
        }
        var res = tar.getAttribute(tag);
          if(res != null && res.length > 0){
            return tar;
          }
          else{
            return this.getTarNode(tar.parentNode,tag);
          }
       },
    buttonClick :function(e,dom){
    	var className = dom.className;
    	if(dom.className.indexOf(' on')<0||dom.getAttribute("data-mc")=="true"){
    		var act = dom.getAttribute('data-act');
    		var onEl = Ext.select("div[id='"+this.selfId+"'] ul.picker_list li.on").elements[0];
    		onEl.className = onEl.className.replace(' on','');
    		dom.className = className + ' on';
    		this.fireEvent('buttonClick',this,act,dom,e);
    	}
    }
});

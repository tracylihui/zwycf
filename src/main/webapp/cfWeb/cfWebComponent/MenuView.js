Ext.define('cfWeb.cfWebComponent.MenuView', {
			extend : 'Ext.view.View',
			alias : 'widget.menuView',
			title: '帮助中心',
			loadingStatus:0,
			readyStatus:1,
			expandBlock:undefined,
			selfId : undefined,
			head : undefined,
			foldIco : undefined,
			mainBlock: undefined,
			tpl:undefined,
			tplTemp: [
			      '<div class="help_center left_width" style="width:20px">',
			      	'<div class="hc_fold">',
			      		'<span class="icon-previous menuFold" id="menuFold"></span>',
			      	'</div>',
			      	'<div id="expand_content" style="display:none"><div class="hc_head level_1" id="hc_head">'],
//				      	'<span>帮助中心</span>',
			tplTemp2: ['<span class="icon-arrow-down3 hch_arrow"></span>',
				      	'</div>',
				      	'<ul class="level_2">',
			      		'<tpl for=".">',
			      			'<tpl if="this.isAvailable(children)">',
			      				'<li class="collapsible">',
			      				'<div class="title">{itemName}<span class="icon-arrow-down3 hch_arrow"></span></div>',
			      				'<ul class="level_3">',
			      				'<tpl for="children">',
			      				'<li data-action="{itemAction}"><span>{itemName}</span></li>',
			      				'</tpl>',
			      				'</ul>',
			      			'<tpl else>',
			      			'<li data-action="{itemAction}"><span>{itemName}</span></li>',
			      			'</tpl>',
			      		'</tpl>',
			      	'</ul></div>',
			      	'</div>',
		          {
		        	  isAvailable : function(children){
		        		  if(typeof(children) == 'object'){
		        			  return true;
		        		  }
		        		  else{
		        			  return false;
		        		  }
		        	  }
		          }
		        ],
			initComponent : function() {
				this.tplInit();
				this.addEvents({
					'onItemsClick':true,
					'oncollapse' : true,
					'onExpand' : true
				});
				this.store.on('load',this.loadInit,this);
				this.callParent(arguments);
			},
			tplInit : function(){
				var span = '<span>' + this.title + '</span>';
				var spanArray =[span]; 
				this.tpl = this.tplTemp.concat(spanArray,this.tplTemp2);
			},
			loadInit : function(){
				this.loadingStatus ++;
				if(this.loadingStatus >= this.readyStatus){
					this.eventBind();
				}
			},
			eventBind : function(){
				var me = this;
				me.expandBlock = Ext.select("div[id='"+me.selfId+"'] div#expand_content").elements[0];
				me.mainBlock = Ext.select("div[id='"+me.selfId+"'] div.help_center").elements[0];
				
				me.head = Ext.select("div[id='"+me.selfId+"'] div#hc_head"); 
				me.head.on('click',me.onHCheadClicked,me);
				
				var secHead = Ext.select("div[id='"+me.selfId+"'] li[class='collapsible'] div.title"); 
				secHead.on('click',me.onHCheadClicked,me);
				
				var menuItems = Ext.select("div[id='"+me.selfId+"'] ul.level_2 li"); 
				menuItems.on('click',me.onMenuItemsClicked,me);
				
				me.foldIco = Ext.select("div[id='"+me.selfId+"'] span#menuFold"); 
				me.foldIco.on('click',me.collapse,me);
			},
			listeners:{
				viewready : function(){
					var me = this;
					me.selfId = me.getEl().id;
					this.loadInit();
				
				}
			},
			getTargetNode : function(dom ,tag){
				   if(dom.tagName ==undefined){
					      return false;
					}
				    if(dom.tagName == tag){
				        return dom;
				    }
				    else{
				        return this.getTargetNode(dom.parentNode,tag);
				    }
			},
			onHCheadClicked : function(e,dom){
				dom = this.getTargetNode(dom,'DIV');
				var className = dom.className;
				if(className.indexOf("fold") >= 0){
					var newClass = className.replace(' fold','');
					dom.className = newClass;
					dom.nextSibling.style.display = 'block';
				}
				else{
					dom.className = className +' fold';
					dom.nextSibling.style.display = 'none';
				}
			},
			onMenuItemsClicked : function(e,dom){
				dom = this.getTargetNode(dom,'LI');
				var act = dom.getAttribute('data-action');
				if(act.length > 0&& act != undefined){
					this.fireEvent('onItemsClick',act);
				}
			},
			collapse : function(){
				var me = this;
				console.log(this);
				var foldIco = me.foldIco.elements[0];
				var className = foldIco.className;
				if(className.indexOf('menuFold') >= 0){
					var newClass = className.replace(' menuFold','');
					foldIco.className = newClass;
					
					me.expandBlock.style.display = 'block';
					me.mainBlock.style.width = '180px';
					me.fireEvent('onExpand',me);
				}
				else{
					foldIco.className = className + ' menuFold';
					me.expandBlock.style.display = 'none';
					me.mainBlock.style.width = '20px';
					me.fireEvent('oncollapse',me);
				}
				return me;
				
			}

		});




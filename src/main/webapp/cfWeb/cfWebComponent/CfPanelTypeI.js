Ext.define('cfWeb.cfWebComponent.CfPanelTypeI', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.cfPanelTypeI',
	selfId:undefined,
	padding:'0 0 20 0',
//	titleCollapse : true,
	cls:'',
	tools:[],
	initComponent : function() {
	    this.addEvents({
	    });
	    this.clsInit();
		this.callParent(arguments);
	},
  listeners:{
    boxready : function(){
    	this.selfId = this.getEl().id;
    	this.eventBind();
    	this.headerInit();
    }
  },
  clsInit: function(){
	  this.cls  +=' style-CfPanelTypeI';
  },
  headerInit :function(){
	  var arr = document.createElement("span");
	  arr.className = 'icon-arrow-down3 hch_arrow';
	  var title = Ext.select("div[id='"+this.selfId+"'] div.x-header-text-container").elements[0];
	  title.appendChild(arr);
	  
  },
  getTarNode :function(tar,tag){
    if(tar.tagName ==undefined){
      return false;
    }
    var res = tar.getAttribute(tag);
      if(res != null && res.length > 0){
        return tar;
      }
      else{
        return this.getTarNode(tar.parentNode,tag);
      }
   },
   eventBind :function(){
	   var header = this.header.getEl(); 
	   header.on('click',this.headClick,this);
	   console.log(header);
   },
   headClick :function(header){
	
	   var header = Ext.select("div[id='"+this.selfId+"'] div.x-panel-header-body").elements[0];
	   var className = header.className;
	   if(className.indexOf(' collapsed') >= 0){
		   header.className = className.replace(' collapsed','');
	   }
	   else{
		   header.className = className +' collapsed';
	   }

	   this.toggleCollapse();
   }
});

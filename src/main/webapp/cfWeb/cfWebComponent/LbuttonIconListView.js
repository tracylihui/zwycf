Ext.define('cfWeb.cfWebComponent.LbuttonIconListView', {
	extend : 'Ext.view.View',
	alias : 'widget.lbuttonIconListView',
	selfId:undefined,
    buttonMenu :undefined,
	tpl:undefined,
	isAddable:true,
	loadMask : false,
	loadingMask:true,
	loadingStatus:0,
	readyStatus:2,
	addText:'新建',
	tplTemp1:[
	     '<div class="cw_content">',
	     '<ul class="clearfix icon_list app_module">',
	     '<tpl for=".">',
	     '<li data-id ="{itemId}" data-img ="{itemIco}" data-name ="{itemName}">',
         '<div class="icon_wrap"><img src="{itemIco}">'],
    tplTemp2:[
         '</div>',
         '<p class="item_title"><a>{itemName}</a></p>',
         '</li>',
	     '</tpl>'
	     ],
	tplTemp3 :[
	  	     '</ul>',
		     '</div>'
	           ],
	initComponent : function() {
		this.tplInit();
	    this.addEvents({
	    	'menuClick':true,
	    	'itemAdd' : true,
	    	'appClick':true
	    	//事件
	    });
	    this.setLoadingMask();
	    this.store.on('load',this.loadInit,this);
		this.callParent(arguments);
	},
	tplInit : function(){
		var me = this;
		var lth = me.buttonMenu.length;
		//convert menu 
		var menuList = [];
		for(var i = 0 ;i<lth;i++){
			var mLth = me.buttonMenu[i].length;
			if(mLth >0){
				menuList[i] = '<ul class="app_module_action">';
				var percent =  100/mLth;
				for(var j = 0;j<mLth;j++){
					menuList[i] +='<li style="width:'+percent+'%" data-ico="'+me.buttonMenu[i][j].ico+'" data-name="'+me.buttonMenu[i][j].name+'" data-act="'+me.buttonMenu[i][j].act+'"><div class="ama_ico icon-'+me.buttonMenu[i][j].ico+'"><span>'+me.buttonMenu[i][j].name+'</span></div></li>';
				}
				menuList[i] += '</ul>';
			}
			else{
				menuList[i] = '';
			}
		}
		var menu = [];
		if(lth >0){
			menu[menu.length] = '<tpl switch="type">';
			for(var i =0;i<lth;i++){
				menu[menu.length]  ='<tpl case="'+i+'">';
				menu[menu.length]  =menuList[i];
			}
			menu[menu.length] = '</tpl>';
		}
		var add = '<li><div class="icon-plus22 add_module cssAnim"></div><p class="add_module_text">'+me.addText+'</p></li>';
		var isAdd = this.isAddable;
		console.log(isAdd);
		if(isAdd==true){
			me.tpl = me.tplTemp1.concat(menu,me.tplTemp2,add,me.tplTemp3);
		}
		else{
			me.tpl = me.tplTemp1.concat(menu,me.tplTemp2,me.tplTemp3);
		}
		console.log(me.tpl);
	},
	listeners:{
	    viewready : function(){
	      this.selfId = this.getEl().id;
	      this.loadInit();
//	      var items_a = Ext.select("div[id='"+this.selfId+"'] ul.icon_list a.icon_wrap"); 
//	      var items_p = Ext.select("div[id='"+this.selfId+"'] ul.icon_list p.item_title");
//	      items_a.on('click',this.onItemsClick,this);
//	      items_p.on('click',this.onItemsClick,this);
	    }
	},
	setLoadingMask : function(){
		if(this.loadingMask){
			this.html = "<div class='loading_mask'>"+cfWebLang.HomeTabView.loadingMsg+"</div>";
		}
	},
	hideMask: function(){
		if(this.loadingMask){
			var hideMsk = Ext.select("div[id='"+this.selfId+"'] div.loading_mask");
			hideMsk.addCls('dis_n');
			var height = parseInt(this.getHeight());
			this.setHeight(height);
		}

	},
	loadInit : function(){
		this.loadingStatus ++;
		if(this.loadingStatus >=this.readyStatus){
			this.eventBind(this);
			this.hideMask();
		}
	},
	eventBind : function(me){
		var buttons = Ext.select("div[id='"+this.selfId+"'] div.ama_ico");
		this.bindEv(buttons, 'click', this.onMenuClick, me);
		var addBlock = Ext.select("div[id='"+this.selfId+"'] div.add_module");
		this.bindEv(addBlock, 'click', this.onItemAdd, me);
		var headIcon = Ext.select("div[id='"+this.selfId+"'] div.icon_wrap");
		this.bindEv(headIcon, 'click', this.appClicked, this);
		this.on('resize',this.autoAdjust,this);
	},
	autoAdjust : function(me, width, height, oldWidth, oldHeight, eOpts){
		var inner = Ext.select("div[id='"+this.selfId+"'] div.cw_content").elements[0];
		var height = inner.getAttribute('height');
		me.setHeight(height);
	},
  getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
	      if(res != null && res.length > 0){
	        return tar;
	      }
	      else{
	        return this.getTarNode(tar.parentNode,tag);
	      }
  },
  onMenuClick : function(e,dom){
	  var tarNode = this.getTarNode(dom,'data-act');
	  var tarData = this.getTarNode(dom,'data-id');
	  var act = tarNode.getAttribute('data-act');
	  var itemId = tarData.getAttribute('data-id');
	  var itemImg = tarData.getAttribute('data-img');
	  var itemName = tarData.getAttribute('data-name');
	  var record = {itemId:itemId,itemIco:itemImg,itemName:itemName};
	  this.fireEvent('menuClick',this,record,act,tarNode,true);
  },
  onItemAdd :function(e,dom){
	  this.fireEvent('itemAdd',this);
  },
  appClicked :function(e,dom){
		var tar = this.getTarNode(dom,'data-id');
		var actName = tar.getAttribute('data-id');
		this.fireEvent('appClick',this,actName,tar,e);
  },
  bindEv :function(target,eventName,fn,scope){
	  target.un(eventName,fn,scope);
	  target.on(eventName,fn,scope);
  }
});
Ext.define('cfWeb.cfWebComponent.MainView', {
	extend : 'Ext.container.Container',
	alias : 'widget.mainView',
	title : 'mainView',
    overflowY:'auto',
    overflowX:'hidden',
    // layout: 'fit',
    padding:10,

    initComponent: function() {
    	var me = this;
    	me.addEvents({
    		'onMainviewChange':true,
    		'beforeMainviewChange':true
    	});
        
//{
//	xtype : 'collapsibleBlock',
//	title : 'Dashboard',
//	
//	items : [{
//		xtype : 'datefield',
//		fieldLabel : 'Start date'
//	},{
//		xtype : 'datefield',
//		fieldLabel : 'End date'
//	}]
//},{
//	xtype : 'collapsibleBlock',
//	title : '运行环境',
//	searchable : true,
//	items : [{
//		xtype : 'iconListView',
//		fieldLabel : 'Start date',
//		//store : 
//	}]
//},{
//	xtype : 'collapsibleBlock',
//	title : '中间件',
//	searchable : true,
//	items : [{
//		xtype : 'collapsibleIconListView',
//		title : '数据存储服务',
//		//store : 
//	},{
//		xtype : 'collapsibleIconListView',
//		title : '数据分析服务',
//		//store :
//	}]
//},{
//	xtype : 'collapsibleBlock',
//	title : '应用模板',
//	items :[]
//}
    	var urlString=window.location.href;
		var index=urlString.indexOf('#');
		var urlStringNew=urlString.substring(index+1);
		if(urlStringNew == window.location.href){
			urlStringNew='homeTabView';
		}
//		alert(urlStringNew);
    	this.items =[
            {
                xtype:urlStringNew
            }
        ];
        this.callParent(arguments);
    },
    changeMainView: function(extComponent){
    	var me = this;
    	var oldComp = me.items.items[0].xtype;
    	var newComp = extComponent.xtype;
    	me.fireEvent('beforeMainviewChange',oldComp,newComp,me);
    	me.removeAll();
    	me.add(extComponent);
    	me.fireEvent('onMainviewChange',oldComp,newComp,me);
    	return me;
    }
});
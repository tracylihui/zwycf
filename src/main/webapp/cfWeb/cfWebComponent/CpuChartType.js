Ext.define('cfWeb.cfWebComponent.CpuChartType', {
	extend : 'cfWeb.cfWebComponent.CfChartTypeII',
	alias : 'widget.cpuChartType',
	selfId:undefined,
	cls:'style-cfChartTypeII',
	//margin:'0 10 30 0',
	title:'',
	max:'',
    style: 'background:#000',
    shadow: false,
    border:0,
    axes: [{
        type: 'Numeric',
        dashSize:0,
        minimum: 0,
        majorTickSteps:9,
        position: 'left',
        fields: ['value'], 
        label: {
        	 renderer:function(v){
        	    return v+'%';
        	 },
        	 fill:'#aab3c7'
        },
       // minorTickSteps: 0.001,
         grid: {
             odd: {
                 opacity: 0.3,
                 type:'circle',
                 stroke: '#d6dae6',
                 'stroke-width': 0.5
             },
             even:{
                     opacity: 0.3,
                     stroke: '#d6dae6',
                     'stroke-width': 0.5
             }
         }

    }, {
        type: 'Category',
        position: 'bottom',
        dashSize:0,
        fields: ['date'],
        label: {
        	renderer:function(){
        		return '';
        	}
        },
        grid: {
            odd: {
                opacity: 1,
                type:'circle',
                stroke: '#d6dae6',
                'stroke-width': 0.5
            },
            even:{
                    opacity: 1,
                    stroke: '#d6dae6',
                    'stroke-width': 0.5
            }
        }
    }],
    series: [
    {
        type: 'line',
        highlight: {
            radius: 4
        },
        style:{
        	stroke:'#17a2f7',
        	'stroke-width': 2,
        	fill:'#e1f7f7'
        },
        axis: 'left',
        smooth: true,
        fill: true,
        xField: 'date',
        yField: 'value',
        tips : {
				trackMouse : true,
				width : 210,
				renderer : function(storeItem, item) {
					this.setTitle(storeItem.get('date').substring(0,20)+':'
							+ storeItem.get('value')+'%');
			}
		},
        markerConfig: {
            type: 'circle',
            radius: 2,
            'fill': '#82dddf'
        }
    }],
	initComponent : function() {
	    this.addEvents({
	    });
	    this.maximumInit();
		this.callParent(arguments);
	},
	maximumInit:function(){
		var max = this.max;
		console.log(max);
		this.axes[0].maximum = max;
		console.log(this.axes[0].maximum);
	}
});
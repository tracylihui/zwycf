Ext.define('cfWeb.cfWebComponent.ModuleConstructView', {
	extend : 'Ext.view.View',
	alias : 'widget.moduleConstructView',
	isInit:true,
	langSetting:{
		serType1:'数据存储服务',
		serType2:'数据分析服务',
		serType3:'应用服务',
		buildPack:'buildPack'
	},
	tpl:undefined,
	title:'创建容器',
	subTitle:'拖动左侧控件创建模板', 
//	moduleData:[{
//		buildPack:[{
//			name:'ruby',
//			id:12,
//			img:'img/icons/dotnet.png'
//		}],
//		serType1:[{
//			name:'ruby',
//			id:12,
//			img:'img/icons/redis.png'
//		},{
//			name:'ruby',
//			id:14,
//			img:'img/icons/redis.png'
//		},{
//			name:'ruby',
//			id:15,
//			img:'img/icons/redis.png'
//		}
//		],
//		serType2:[{
//			name:'ruby',
//			id:56,
//			img:'img/icons/redis.png'
//		},{
//			name:'ruby',
//			id:32,
//			img:'img/icons/redis.png'
//		}],
//		serType3:[{
//			name:'ruby',
//			id:56,
//			img:'img/icons/redis.png'
//		},{
//			name:'ruby',
//			id:32,
//			img:'img/icons/redis.png'
//		}]
//	}],
    initComponent: function() {
        this.tplInit();
        this.storeInit();
        this.callParent(arguments);
    },
    tplInit : function(){
    	
    	var tpl= [];
    	tpl[tpl.length] = '<tpl for=".">';
    	tpl[tpl.length] = '<div class="module_constructor">';
    	tpl[tpl.length] = '<div class="mc_head">';
    	tpl[tpl.length] = '<span class="mc_title">'+this.title+'</span>';
    	tpl[tpl.length] = '<span class="mc_tips">'+this.subTitle+'</span>';
    	tpl[tpl.length] = '</div><div class="build_pack"><div class="build_pack_content">';
    	tpl[tpl.length] = '<h3 class="bp_title">'+ this.langSetting.buildPack+'</h3>';
    	tpl[tpl.length] = '<tpl for="buildPack">';
    	tpl[tpl.length] = '<div class="bp_item" data-id="{id}" data-type="buildPack"><img src="{img}" /><p>{name}</p><span class="close_sign icon-cross3"></span></div>';
    	tpl[tpl.length] = '</tpl></div></div>';  
    	tpl[tpl.length] = '<div class="service"><div class="ser_category" data-type="serType1">';
    	tpl[tpl.length] = '<h4 class="serC_title">'+this.langSetting.serType1+'</h4>';
    	tpl[tpl.length] = '<div class="service_wrap"><ul class="service_list clearfix">';
    	tpl[tpl.length] = '<tpl for="serType1">';
    	tpl[tpl.length] = '<li class="service_item" data-id="{id}">';
    	tpl[tpl.length] = '<img src="{img}" class="service_logo" />';
    	tpl[tpl.length] = '<p class="service_name">{name}</p>';
    	tpl[tpl.length] = '<span class="close_sign icon-cross3"> </span></li>';
    	tpl[tpl.length] = '</tpl></ul></div></div>';
    	tpl[tpl.length] = '<div class="ser_category" data-type="serType2"><h4 class="serC_title">'+this.langSetting.serType2+'</h4>';
    	tpl[tpl.length] = '<div class="service_wrap"><ul class="service_list clearfix">';
    	tpl[tpl.length] = '<tpl for="serType2">';
    	tpl[tpl.length] = '<li class="service_item" data-id="{id}">';
    	tpl[tpl.length] = '<img src="{img}" class="service_logo" />';
    	tpl[tpl.length] = '<p class="service_name">{name}</p>';
    	tpl[tpl.length] = '<span class="close_sign icon-cross3"> </span></li>';
    	tpl[tpl.length] = '</tpl></ul></div></div>';
    	tpl[tpl.length] = '<div class="ser_category" data-type="serType3"><h4 class="serC_title">'+this.langSetting.serType3+'</h4>';  
    	tpl[tpl.length] = '<div class="service_wrap"><ul class="service_list clearfix">';
    	tpl[tpl.length] = '<tpl for="serType3">';
    	tpl[tpl.length] = '<li class="service_item" data-id="{id}">';
    	tpl[tpl.length] = '<img src="{img}" class="service_logo" />';
    	tpl[tpl.length] = '<p class="service_name">{name}</p>';
    	tpl[tpl.length] = '<span class="close_sign icon-cross3"> </span></li>';
    	tpl[tpl.length] = '</tpl></ul></div></div>';   	
    	tpl[tpl.length] = '</div></div></div>';  
    	tpl[tpl.length] = '</tpl>';
    	this.tpl = tpl;
//    	console.log(tpl);
    },
    listeners:{
        viewready : function(){
          this.selfId = this.getEl().id;
          this.widthAdj();
          this.eventBind();
        },
        refresh: function(){
        	this.eventBind();
        	if(!this.isInit){
        		this.widthAdj(); 
        	}
        	else{
        		this.isInit = false;
        	}
        }
      },
    getTarNode :function(tar,tag){
  	    if(tar.tagName ==undefined){
  	      return false;
  	    }
  	    var res = tar.getAttribute(tag);
  	      if(res != null && res.length > 0){
  	        return tar;
  	      }
  	      else{
  	        return this.getTarNode(tar.parentNode,tag);
  	      }
    },
    storeInit: function(){
    	if(this.moduleData){
    		var store = Ext.create('Ext.data.Store',{
    			fields:['buildPack','serType1','serType2','serType3'],
    			data: this.moduleData
    		});
    		this.store = store;
    	}
    	else{
    		var store = Ext.create('Ext.data.Store',{
    			fields:['buildPack','serType1','serType2','serType3'],
    			data: [{
    				buildPack:[],
    				serType1:[],
    				serType2:[],
    				serType3:[]
    			}]
    		});
    		this.store = store;
    	}
    },
//    setData :function(data){
//    	this.store.loadData(data);
//    },
    widthAdj : function(){
//    	alert('adj');
    	var ser1 = Ext.select("div[id='"+this.selfId+"'] div[data-type='serType1'] ul.service_list li").elements; 
    	var lth = ser1.length;
    	var ser1Ul = Ext.select("div[id='"+this.selfId+"'] div[data-type='serType1'] ul.service_list").elements[0];
    	ser1Ul.style.width = lth*128 +'px';
    	
    	var ser2 = Ext.select("div[id='"+this.selfId+"'] div[data-type='serType2'] ul.service_list li").elements; 
    	lth = ser2.length;
    	var ser2Ul = Ext.select("div[id='"+this.selfId+"'] div[data-type='serType2'] ul.service_list").elements[0];
    	ser2Ul.style.width = lth*128 +'px';
    	
    	var ser3 = Ext.select("div[id='"+this.selfId+"'] div[data-type='serType3'] ul.service_list li").elements; 
    	lth = ser3.length;
    	var ser3Ul = Ext.select("div[id='"+this.selfId+"'] div[data-type='serType3'] ul.service_list").elements[0];
    	ser3Ul.style.width = lth*128 +'px';
    },
    eventBind: function(){
    	var deleteButton = Ext.select("div[id='"+this.selfId+"'] span.close_sign");
    	deleteButton.on('click',this.itemDelete,this);
//    	this.store.on('load',function(){alert('load');},this);
    },
//    testAdd:function(){
//    	alert('add');
//    	this.addItem({serType1:{
//    		name:'测试',
//    		img:'img/icons/redis.png',
//    		id:22
//    	}});
//    },
    itemDelete : function(e,dom){
    	var li = this.getTarNode(dom, 'data-id');
    	var id = li.getAttribute('data-id');
    	var type = this.getTarNode(dom, 'data-type').getAttribute('data-type');
    	var data = this.store.getAt(0).data;
    	console.log(data);
    	var tarData = data[type];
    	console.log(tarData);
    	for(var i = 0;i<tarData.length;i++){
    		if(tarData[i].id == id){
    			tarData.splice(i,1);
    			break;
    		}
    	}
//    	var record = this.store.getAt(0);
//    	record.data = data;
//    	console.log(this.store.getAt(0));
//    	this.store.loadRecords(record);
    	var darray = [data];
    	this.store.loadData(darray);
    },
    addItem : function(adata){
    	var data = this.store.getAt(0).data;
    	
    	for(var i in adata){
    		if(i == 'buildPack'){
    			data[i][0] = adata[i];
    		}
    		else{
    			var dataSet = data[i];
    			var isRepeat = false;
    			for(var j = 0; j<dataSet.length; j++){
    				if(dataSet[j].id == adata[i].id){
    					isRepeat = true;
    					break;
    				}
    			}
    			if(!isRepeat){
    				data[i].push(adata[i]);
    			}
    			else{
    				return false;
    			}
    		}
    	}
    	var darray = [data];
    	this.store.loadData(darray);
    },
    getData : function(){
    	return this.store.data.items[0].data;
    }
});

























Ext.define('cfWeb.cfWebComponent.VariableListView', {
	extend : 'Ext.view.View',
	alias : 'widget.variableListView',
	selfId:undefined,
	loadStatus:0,
	readyStatus:2,
//	isDragable:false,
	langSetting:{key:"变量名称",value:"变量值",save:"保存",cancel:"取消"},
	tpl:[
         '<ul class="clearfix icon_list app_module var_mng">',
         '<tpl for=".">',
         '<li>',
           '<div class="icon_wrap">',
           '<div class="var_content" data-key="{key}" data-value="{value}" data-tag="true">',
             '<dl>',
               '<dt>变量名称：</dt>',
               '<dd><label class="var_disfield">{key}</label><input class="var_editfield key" type="text" value="{key}" /></dd>',
             '</dl>',
             '<dl>',
               '<dt>变量值：</dt>',
              ' <dd><label class="var_disfield">{value}</label><input class="var_editfield value" type="text" value="{value}" /></dd>',
             '</dl>',
             '<div class="act_area">',
               '<span class="icon-pencil2 var_act va_edit" data-act="edit"></span>',
               '<span class="icon-cross var_act va_delete" data-act="delete"></span>',
               '<a class="var_act_cancel">取消</a>',
                '<a class="var_act_save">保存</a>',
             '</div>',
          ' </div>',
          ' </div>',
         '</li>',
         '</tpl>',
        ' <li>',
           '<div class="icon-plus22 add_module cssAnim">',
           '</div>',
        ' </li>',     
       '</ul>'
	     ],
	  /* store:  Ext.create('Ext.data.Store', {
		    fields:['key','value'],
		     data : [
		         {key: 'Ed',    value: 'Spencer'},
		         {key: 'Tommy', value: 'Maintz'},
		         {key: 'Aaron', value: 'Conran'},
		         {key: 'Jamie', value: 'Avins'}
		     ]
		 }),*/
	initComponent : function() {
	    this.addEvents({
	    	'itemDelete':true,
	    	'itemUpdate':true,
	    	'itemAdd':true,
	    	'heightAdj':true
	    	//事件
	    });
	    this.tplInit();
	    this.store.on('load',this.loadInit,this);
	    this.store.on('datachanged',this.loadInit,this);
		this.callParent(arguments);
	},
  listeners:{
    viewready : function(){
    	//this.store.load();
      this.loadInit();
    }
  },
  tplInit : function(){
	  this.tpl[6] = '<dt>'+this.langSetting.key+'：</dt>';
	  this.tpl[10] =  '<dt>'+this.langSetting.value+'：</dt>';
	  this.tpl[16] =   '<a class="var_act_cancel">'+this.langSetting.cancel+'</a>';
	  this.tpl[17] =   '<a class="var_act_save">'+this.langSetting.save+'</a>';
  },
  loadInit : function(){
	 
	  this.loadStatus++;
	  if(this.loadStatus >= this.readyStatus){
		  this.bindEvents();
	  }
  },
  bindEvents : function(){
	 
      this.selfId = this.getEl().id;
      var edit = Ext.select("div[id='"+this.selfId+"'] span.va_edit");
      var deletebutton =  Ext.select("div[id='"+this.selfId+"'] span.va_delete");
      var saveButton  = Ext.select("div[id='"+this.selfId+"'] a.var_act_save");
      var cancelButton = Ext.select("div[id='"+this.selfId+"'] a.var_act_cancel");
      var addButton =  Ext.select("div[id='"+this.selfId+"'] div.add_module");
      var input = Ext.select("div[id='"+this.selfId+"'] input");
      console.log(addButton);
      this.bindEv(addButton, 'click', this.itemAdd, this);
      this.bindEv(input,'focus',this.inputFocus,this);
      this.bindEv(edit,'click',this.itemEdit,this);
      this.bindEv(deletebutton,'click',this.itemDelete,this);
      this.bindEv(saveButton,'click',this.itemSave,this);
      this.bindEv(cancelButton,'click',this.itemCancel,this);
  },
  bindEv :function(target,eventName,fn,scope){
	  target.un(eventName,fn,scope);
	  target.on(eventName,fn,scope);
  },
  inputFocus : function(e,dom){
	  dom.style['border-color']='#e0e3ec';
  },
  itemDelete : function(e,dom){
	  var wrap = this.getTarNode(dom, 'data-tag');
	  var key = wrap.getAttribute('data-key');
	 var res =  this.fireEvent('itemDelete',this,key,e);
	 if(res){
		 var li = this.getTarNode(dom, 'LI', 'tag');
		 var width = li.offsetWidth;
		 li.remove();
		  var eLength =  Ext.select("div[id='"+this.selfId+"'] ul.var_mng li").elements.length;
		  var count=parseInt(this.getWidth()/width);
		  
		  if(eLength%count == 0){
			  this.fireEvent('heightAdj',this,-1);
		  }		 
	 }
  },
  itemCancel: function(e,dom){
	  var wrap = this.getTarNode(dom,'data-tag');
	  var key = wrap.getAttribute('data-key');
	  var value = wrap.getAttribute('data-value');
	  if(key){
		  console.log(wrap.firstChild.nextSibling);
		  wrap.firstChild.firstChild.nextSibling.firstChild.nextSibling.value = key;
		  console.log( wrap.firstChild.nextSibling.firstChild.nextSibling.nextSibling);
		  wrap.firstChild.nextSibling.firstChild.nextSibling.nextSibling.firstChild.nextSibling.value = value;
		  wrap.className = wrap.className.replace(" edit","");
	  }
	  else{
		  var li = this.getTarNode(wrap, 'LI', 'tag');
		  var width = li.offsetWidth;
		  li.remove();
		  var eLength =  Ext.select("div[id='"+this.selfId+"'] ul.var_mng li").elements.length;
		  var count=parseInt(this.getWidth()/width);
		  console.log("length:"+eLength);
		  console.log('count:'+count);
		  if(eLength%count == 0){
			  this.fireEvent('heightAdj',this,-1);
		  }
	  }
  },
  itemAdd : function(e,dom){
	  var li = this.getTarNode(dom,'LI','tag');
	  var ul =  this.getTarNode(dom,'UL','tag');
	  var addHTML = li.innerHTML;
	  var itemHTML = 
	                    '<div class="icon_wrap">'+
	                    '<div class="var_content edit" data-key="" data-value=""  data-tag="true">'+
	                      '<dl>'+
	                        '<dt>'+this.langSetting.key+'：</dt>'+
	                        '<dd><label class="var_disfield"></label><input class="var_editfield key" type="text" value="" /></dd>'+
	                      '</dl>'+
	                      '<dl>'+
	                        '<dt>'+this.langSetting.value+'：</dt>'+
	                       ' <dd><label class="var_disfield"></label><input class="var_editfield value" type="text" value="" /></dd>'+
	                      '</dl>'+
	                      '<div class="act_area">'+
	                        '<span class="icon-pencil2 var_act va_edit" data-act="edit"></span>'+
	                        '<span class="icon-cross var_act va_delete" data-act="delete"></span>'+
	                        '<a class="var_act_cancel">'+this.langSetting.cancel+'</a>'+
	                         '<a class="var_act_save">'+this.langSetting.save+'</a>'+
	                      '</div>'+
	                   ' </div>'+
	                   ' </div>';
	  li.innerHTML = itemHTML;
	  var addLi =  document.createElement("li");
	  addLi.innerHTML = addHTML;
	  ul.appendChild(addLi);
	  var eLength =  Ext.select("div[id='"+this.selfId+"'] ul.var_mng li").elements.length -1;
	  var count=parseInt(this.getWidth()/(addLi.offsetWidth));
	  console.log('count:'+count);
	  console.log('length:'+eLength);
	  
	  if(eLength%count == 0){
		  this.fireEvent('heightAdj',this,1);
	  }
	  
	  this.bindEvents();
	  
  },

  itemEdit : function(e,dom){
	  var wrap = this.getTarNode(dom, 'data-key');
	  console.log(wrap);
	  wrap.className = wrap.className +' edit';
  },
  itemSave : function(e,dom){
	  var wrap = this.getTarNode(dom, 'data-tag');
	  var oKey = wrap.getAttribute('data-key');  
	  console.log("okey:"+oKey);
	  var  oVal = wrap.getAttribute('data-value');
	  var nKey =  Ext.select("div[id='"+this.selfId+"'] div[data-key='"+oKey+"'] input.key").elements[0];
	  var nVal =  Ext.select("div[id='"+this.selfId+"'] div[data-key='"+oKey+"'] input.value").elements[0];
	  var disKey =  Ext.select("div[id='"+this.selfId+"'] div[data-key='"+oKey+"'] label.var_disfield").elements[0];
	  var disVal =  Ext.select("div[id='"+this.selfId+"'] div[data-key='"+oKey+"'] label.var_disfield").elements[1];
	  var newValue = nVal.value;
	  var newKey = nKey.value;
	  if(newValue.trim() ==''){
		  nVal.style['border-color']='#f00';
		  return;
	  }
	  if(newKey.trim() ==''){
		  nKey.style['border-color']='#f00';
		  return;
	  }
	  if(oKey ==nKey.value && oVal ==nVal.value ){
		  wrap.className = wrap.className.replace(" edit","");
	  }
	  else{

		  var res;
		  if(oKey==''){
			  res = this.fireEvent('itemAdd',this,newKey,newValue);
			  
		  }
		  else{
			  res =  this.fireEvent('itemUpdate',this,oKey,newKey,newValue);
		  }
		  if(res){
			  disKey.innerHTML = newKey;
			  disVal.innerHTML = newValue;
			  wrap.setAttribute('data-key',newKey);
			  wrap.setAttribute('data-value',newValue);
			  wrap.className = wrap.className.replace(" edit","");
		  }
	  }
  },
  getTarNode :function(tar,tag,type){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    if(type =='attr'){
		    var res = tar.getAttribute(tag);
		      if(res != null && res.length > 0){
		        return tar;
		      }
		      else{
		        return this.getTarNode(tar.parentNode,tag,type);
		      }	    	
	    }
	    else if(type =='tag'){
	    	var res = tar.tagName;
	    	if(res == tag){
	    		return tar;
	    	}
	    	else{
	    		return this.getTarNode(tar.parentNode,tag,type);
	    	}
	    }
	    else{
		    var res = tar.getAttribute(tag);
		      if(res != null && res.length > 0){
		        return tar;
		      }
		      else{
		        return this.getTarNode(tar.parentNode,tag,type);
		      }	    		    	
	    }
  }
});
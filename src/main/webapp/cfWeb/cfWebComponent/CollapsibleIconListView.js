Ext.define('cfWeb.cfWebComponent.CollapsibleIconListView', {
	extend : 'Ext.view.View',
	alias : 'widget.collapsibleIconListView',
	selfId:undefined,
	title:undefined,
	cwContent:undefined,
	loadMask : false,
	loadingMask:true,
	loadStatus:0,
	readyStatus:2,
	tempTpl1:[
	     '<div class="cw_content">',
	     '<div class="collapsible_block">',
	     '<div class="cb_title_wrap">',
	     '<div class="cb_title"><span class="text_node">'],
	     
	tempTpl2:['</span><span class="icon-arrow-down3 hch_arrow"></span></div>',
	     '</div>',
	     '<ul class="clearfix icon_list">',
	     '<tpl for=".">',
	     '<li data-id="{itemId}" data-img="{itemIco}" data-name="{itemName}">',
         '<a class="icon_wrap"><img src="{itemIco}"></a>',
         '<p class="item_title"><a>{itemName}</a></p>',
         '</li>',
	     '</tpl>',
	     '</ul>',
	     '</div>',
	     '</div>'
	     ],
	initComponent : function() {
	    this.addEvents({
	    	'itemsClick':true,
	    	'expand':true,
	    	'collapse':true
	    	//事件
	    });
	    this.setLoadingMask();
	    this.store.on('load',this.loadInit,this);
	    /**/
	    this.store.on('filterchange',this.bindEvents,this);
	    /**/
	    this.tplInit();
		this.callParent(arguments);
	},
  listeners:{
    viewready : function(){
      this.selfId = this.getEl().id;
//      this.loadInit();

    }
  },
  tplInit : function(){
	  this.tpl = this.tempTpl1.concat(this.title,this.tempTpl2);
  },
  loadInit : function(){
	  this.loadStatus++;
	  console.log(this.loadStatus);
	  if(this.loadStatus >= this.readyStatus){
		  this.eventsInit();
		  this.hideMask();
	  }
  },
  /**/
	autoAdjust : function(me, width, height, oldWidth, oldHeight, eOpts){
		var inner = Ext.select("div[id='"+this.selfId+"'] div.cw_content").elements[0];
		var height = inner.getAttribute('height');
		me.setHeight(height);
	},
	setLoadingMask : function(){
		if(this.loadingMask){
			this.html = "<div class='loading_mask'>"+cfWebLang.HomeTabView.loadingMsg+"</div>";
		}
	},
	hideMask: function(){
		if(this.loadingMask){
			var hideMsk = Ext.select("div[id='"+this.selfId+"'] div.loading_mask");
			hideMsk.addCls('dis_n');
			var height = parseInt(this.getHeight());
			this.setHeight(height);
		}

	},
  bindEvents : function(){
      this.selfId = this.getEl().id;
      var items_a = Ext.select("div[id='"+this.selfId+"'] ul.icon_list a.icon_wrap"); 
      var items_p = Ext.select("div[id='"+this.selfId+"'] ul.icon_list p.item_title");
      items_a.on('click',this.onItemsClick,this);
      items_p.on('click',this.onItemsClick,this);
  },
  /**/
  eventsInit : function(){
	  this.cwContent = Ext.select("div[id='"+this.selfId+"'] div.cw_content").elements[0]; 
      var items_a = Ext.select("div[id='"+this.selfId+"'] ul.icon_list a.icon_wrap"); 
      var items_p = Ext.select("div[id='"+this.selfId+"'] ul.icon_list p.item_title");
      var titleBlock = Ext.select("div[id='"+this.selfId+"'] span.text_node");
      titleBlock.on('click',this.collapse,this);
      console.log(titleBlock);
      items_a.on('click',this.onItemsClick,this);
      items_p.on('click',this.onItemsClick,this);
      this.on('resize',this.autoAdjust,this);
  },
  getTarNode :function(tar,tag){
	    if(tar.tagName ==undefined){
	      return false;
	    }
	    var res = tar.getAttribute(tag);
	      if(res != null && res.length > 0){
	        return tar;
	      }
	      else{
	        return this.getTarNode(tar.parentNode,tag);
	      }
  },
  collapse : function(e ,dom){
	  var className = this.cwContent.className;
	  if(className.indexOf(" collapsed") < 0){
		  this.cwContent.className = className + ' collapsed';
		  this.fireEvent('collapse',this);
	  }
	  else{
		  className = className.replace(' collapsed','');
		  this.cwContent.className = className;
		  this.fireEvent('expand',this);
	  }
  },
  onItemsClick : function(e,dom){
	  var tar = this.getTarNode(dom,'data-id');
	  var id = tar.getAttribute('data-id');
	  var img = tar.getAttribute('data-img');
	  var name = tar.getAttribute('data-name');
	  var record = {itemId:id,itemIco:img,itemName:name};
	  this.fireEvent('itemsClick',this,record,e);
  }
});
Ext.define('cfWeb.controller.util.MessageController',{
	extend : 'Ext.app.Controller',
	views : ['cfWeb.view.messageManage.MessageSlideView',
	         'cfWeb.view.messageManage.MessageList'
	         ],
	init : function(){
		this.control({
			'messageList' :{
				viewready:this.viewInit
			},
			'messageSlideView messageList':{
				onItemsClick :this.messageClick
			}
		});
	},
	viewInit : function(me){
		me.store.load();
	},
	messageClick : function(me,appName){
		me.up('messageSlideView').slideOut();
		var mainView = Ext.ComponentQuery.query('mainView')[0];
		var width = mainView.getWidth() -10;
		var height = mainView.getHeight();
		var win = Ext.widget('appListDetailsView',{
			adaptTo:mainView,
			blankHeight:50,
//			blankHeight:height-100,
			width:width,
			height:height	
		});
		win.appName=appName;
		win.slideUp();
		Ext.Ajax.request({
			url : 'rest/app/details',
			method : 'GET',
			params : {
				appName : appName
			},
			success : function(response){
				var obj = Ext.JSON.decode(response.responseText);
				if(obj.serviceName!=null)
				win.items.items[0].updateAppStatus(obj);	
			},
			failure : function(response) {
				var obj = Ext.JSON.decode(response.responseText);
				if(obj.data!=null&&obj.data.source==1){
					Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				}else{
					Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.failed);
				}
			}
		});
		var detailContainer=win.down("container#detailContainer");
		var winLog=Ext.widget("logFileWindow");
		winLog.appName=appName;
		var lth = detailContainer.items.items.length;
		for(var i=0;i<lth;i++){
			detailContainer.items.items[i].hide();
		}
		detailContainer.add(winLog);
		
	},
});
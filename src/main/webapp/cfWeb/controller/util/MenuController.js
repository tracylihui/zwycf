Ext.define('cfWeb.controller.util.MenuController',{
	extend : 'Ext.app.Controller',
	views : ['cfWeb.cfWebComponent.MenuView'],
	init : function(){
		this.control({
			'menuView' :{
				onExpand : this.expandAction,
				oncollapse : this.collapseAction
			}
		});
	},
	
	expandAction : function(){
		var mainView = Ext.ComponentQuery.query('mainView')[0];
		console.log(mainView);
		mainView.setWidth();
		var i=0;
		for(;i<4;i++){
			mainView.items.items[0].items.items[0].items.items[i].setWidth(200);
		}
		
		console.log(mainView.getWidth());
//		console.log(mainView.items.items[0].items.items[0].items.items[0]);
		
	},
	collapseAction : function(){
		var mainView=Ext.ComponentQuery.query('mainView')[0];
		console.log(mainView);
		mainView.setWidth();
		var i=0;
		for(;i<4;i++){
			mainView.items.items[0].items.items[0].items.items[i].setWidth(258);
		}
		console.log(mainView.getWidth());
		}
});
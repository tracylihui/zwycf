/*window.onbeforeunload = function() {
	Ext.Ajax.request({
		url :'rest/login/closeWeb',
	});
};*/
var currentORG=null;
var currentSpace=null;
var currentUserName = null;
var currentUserId = null;
var currentUserIsAdmin = 0;
Ext.define('cfWeb.controller.util.NavController',{
	extend : 'Ext.app.Controller',
	views : ['cfWeb.cfWebComponent.NavView',
			'cfWeb.cfWebComponent.HomePageNavView'
	         ,'accountSetting.AccountSettingWindow'
	         //
	         ,'cfWeb.view.accountSetting.SettingWindow'
	         ,'cfWeb.cfWebComponent.CfPanelTypeII'
	         ,'cfWeb.cfWebComponent.PanelHeaderTypeII'
	         ,'cfWeb.view.messageManage.MessagesWindows'
	         ,'cfWeb.view.messageManage.Messages'
	         ,'cfWeb.view.accountSetting.AuthOrgUserWindow'
	         ,'cfWeb.cfWebComponent.VariableListView'
	         ,'accountSetting.ChangePasswdWindow'
	         ],
	         
	         
	stores:['message.MessageStore'],
	
	init : function(){
		//window.location.href =  'rest/log/download?appName=logcxy&fileName=logcxy-0-2014-08-18 16&offset=0&size=10';
	/*	Ext.Ajax.request({
			url : 'rest/log/download?appName=log2&fileName=log2&offset=0&size=10',
			success : function(resp) {
//				Ext.decode(resp.responseText);
				console.log(resp);
			},
			failure : function(resp) {
				
			}
		});*/
		
		
		this.control({
			'navView' : {
				onNavChange : this.navChange,
				onLanguageChange : this.languageChange,
				viewready : this.setNav,
				onMenuClicked : this.onMenuClicked,
				afterrender: this.getData
			},
			'homePageNavView' : {
				onNavChange : this.navChange,
				onLanguageChange : this.languageChange,
				viewready : this.setHomePageNav,
				onMenuClicked : this.onMenuClicked
			}
		});
	},
	getData : function(me){
		Ext.Ajax.request({
			url : 'rest/login/currentCloud',
			method : 'GET',
			success : function(resp){
				var data = Ext.decode(resp.responseText).data;
				me.platform = data;
			},
			failure : function(resp){
				console.log("failure");
			}
		});

	},
	setHomePageNav: function(me) {
		var	language=getCookie('language');
		me.setLanguage(language);

		var urlString=window.location.href;
		var index=urlString.indexOf('#');
		var urlStringNew=urlString.substring(index+1);
		if(urlStringNew == window.location.href){
			urlStringNew='homeTabView';
		}
		switch(urlStringNew){
			case 'homeTabView': me.setNav('index');break;
		}
	},
	setNav : function(me){
		var	language=getCookie('language');
		me.setLanguage(language);
		me.setMessageCount(0);
		Ext.Ajax.request({
			url :'rest/login/getCurrentUser',
			success : function(re) {
				var response=Ext.JSON.decode(re.responseText);
				console.log(response);
				console.log(response.username);
				me.setUser(response.username,'resources/img/avatar.jpeg');
				me.org.load({
					callback:function(){
						var orgOption='<option value="" style="display:none;"></option>';
						this.each(function(i){						
								orgOption +='<option value="'+i.get('name')+'">'+i.get('name')+'</option>';
							
						});
						Ext.fly('selectOrg').insertHtml('beforeEnd',orgOption);
						var orgOptions = Ext.get('selectOrg').dom.options; 				
						  for(var i = 0 ; i<orgOptions.length;i++){
							  if(orgOptions[i].value==response.org){
					        	orgOptions[i].selected="selected";
					        	break;
							  }
						  }
						  currentORG=response.org;
						  currentSpace=response.space;
						  Ext.get('selectOrg').set({old_value:response.org});
						  Ext.get('selectSpace').set({old_value:response.space});
						  me.orgOnChange(response.space,true);
						  currentUserName = response.username;
						  currentUserId = response.user_primaryid;
						  currentUserIsAdmin = response.userIsAdmin;
						 // me.spaceOnChange();
					}
				
				});
				
			},
			failure : function(re) {
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.UnLogginUser);
			}
		});
		
		var urlString=window.location.href;
		var index=urlString.indexOf('#');
		var urlStringNew=urlString.substring(index+1);
		if(urlStringNew == window.location.href){
			urlStringNew='homeTabView';
		};
		switch(urlStringNew){
			case 'homeTabView': me.setNav('index');break;
			case 'appManageView': me.setNav('appManage');break;

			//添加所有服务
			case 'allServicesView': me.setNav("allServices");break;

			case 'dataStorageView': me.setNav('dataStorageService');break;
			case 'dataAnalyseView': me.setNav("dataAnalyseService");break;
			case 'appServiceView': me.setNav("appService");break;
			case 'appView': me.setNav('app');break;
		}
	},
	
	topBarInit : function(view, opts){
		var list = Ext.select("div[class=''nav_list] li");
		list.on('click', this.navChange, this, list);
		console.log(list);
	},
	navChange : function(el, target, NavView){
		console.log(el);
		console.log(target);
		console.log(NavView);
		
		NavView.setMessageCount('0');
//		NavView.setUser('linc','resources/img/avatar.jpeg');
		
		var urlString=window.location.href;
		var index=urlString.indexOf('#');
		var urlStringNew=urlString.substring(0,index);
		if(urlStringNew.length == 0){
			urlStringNew=urlString;
		}
//		alert(urlStringNew);
		console.log(target);
		if(target == "index"){
			updateDash(); // update dash data
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('homeTabView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#homeTabView";
		}
		if(target == "appManage"){

			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('appManageView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#appManageView";
//			var mainView = Ext.ComponentQuery.query('mainView')[0];
//			var newView = Ext.widget('ringChart',{
//				title:'应用使用量',
//				color:'blue',
//				icoType:'rocket',
//				unit:'个',
//				store : Ext.create('Ext.data.Store', {
//					autoLoad: true,
//					fields: ['itemId','itemName', 'itemCount','total'],
//					data:[{
//						itemId:1,
//						itemName:'运行',
//						itemCount:14,
//						total : 20
//					}]
//				})
//			});
//			mainView.changeMainView(newView);
		}else if(target == "allServices"){
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('allServicesView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#allServicesView";
		}
		else if(target == "dataStorageService"){
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('dataStorageView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#dataStorageView";
		}
		else if(target == "dataAnalyseService"){
			
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('dataAnalyseView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#dataAnalyseView";
		}
		else if(target == "appService"){
			
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('appServiceView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#appServiceView";
		}
		else if(target == "app"){
			
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var newView = Ext.widget('appView');
			mainView.changeMainView(newView);
			window.location.href=urlStringNew+"#appView";
		}
	},
	
	languageChange : function(lang){
		var language=getCookie('language');
		if (language == "cn"){
			//选择英文字体
			setCookie('language','en',365);
		var	language=getCookie('language');
//			alert(language);
			window.location.reload();
		}
		else{
			//选择中文字体
			setCookie('language','cn',365);
		var	language=getCookie('language');
//			alert(language);
			window.location.reload();
		}
	},
	
	onMenuClicked : function(dataId){
		console.log(dataId);		
		if(dataId == "setting"){
//			var window = Ext.widget('accountSettingWindow');
//			window.show();
			var window = Ext.widget('SettingWindow',{
				dockedItems: [{
					xtype:'container',
					 dock: 'bottom',
					height:20,
					width:100,
					style:{
					"background-color":"#fff"}
				}
	       
	    ]
			});
			Ext.Ajax.request({
	  			url : 'rest/user/currentuserOrgSpace',
	  			method : 'GET',
	  			root : 'data',
	  			success : function(resp){
	  				console.log(resp);
	  				console.log("success");
	  				var select=Ext.JSON.decode(resp.responseText);
					var data=select.data;
					window.currentOrg=data[0];
					window.currentSpace=data[1];
	  			},
	  			failure : function(resp){
	  				console.log(resp);
	  				console.log("failure");
	  			}
	  		});
			console.log(window);
			window.show();
		}
		if(dataId == "message"){
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var slider = Ext.widget('messageSlideView',{
				width:300,
				adaptTo:mainView,
				verticalIn:false,
				closeSign:false
			});
			slider.slideIn();
			
//			var window = Ext.widget('messagesWindows');
//			window.show();
		}
		if(dataId == "accountSetting"){
			var window = Ext.widget('changePasswdWindow');
			window.show();
		}
		if(dataId == "exit"){
			console.log("helloexit");
			var username='';
			var lastOrg='';
			var lastSpace='';
			
			Ext.Ajax.request({
				url :'rest/login/getCurrentUser',
				success : function(resp){
					var response=Ext.JSON.decode(resp.responseText);
					console.log(response);
					console.log("1!!!!!!!!!!!!");
					username = response.username;
					lastOrg = response.org;
					lastSpace =response.space;
					
					console.log(response.username);
					console.log(username);
					console.log(lastOrg);
					console.log(lastSpace);
				},
				failure : function(reps){
				}
			});
			Ext.Ajax.request({
				url : 'rest/login/logout',
				method : 'GET',
				params :{
					username : username,
					lastOrg : lastOrg,
					lastSpace : lastSpace
				},
				success : function(resp){
					console.log(resp);
					parent.location.href = '/zwycf/login.html';    
				},
				failure : function(){
				}
			})
		}
			
	}
});

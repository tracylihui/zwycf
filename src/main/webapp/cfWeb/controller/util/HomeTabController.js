Ext.define('cfWeb.controller.util.HomeTabController',{
	extend : 'Ext.app.Controller',
	views : ['util.HomeTabView',
	         'Ext.ux.linc.AppList',
	         'cfWeb.cfWebComponent.CollapsibleBlock',
	         'cfWeb.cfWebComponent.IconListView',
	         'cfWeb.cfWebComponent.CollapsibleIconListView',
	         'cfWeb.cfWebComponent.SlideLayer',
	         'cfWeb.cfWebComponent.RingChart',
	         'cfWeb.cfWebComponent.InfomationView'
	         ],
	stores : ['util.dashboard.RouteUsedStore',
	          'util.dashboard.ServiceUsedStore',
	          'util.dashboard.AppStatusStore',
	          'util.dashboard.MemoryUsedStore',
		      'util.dashboard.JoinedOrgStore',
	          'util.middleware.MiddlewareStorageStore',
	          'util.middleware.MiddlewareAnalyseStore',
	          'util.middleware.MiddlewareApplicationStore'
	          ],
	init : function() {
//		setInterval(function() {
//			// connect to cf every 5 mins 
//			Ext.Ajax.request({
//				url : 'rest/keepConnect/keeping',
//				failure : function() {
//					Ext.msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.RestartBrowse);//  not English
//				}
//			});
//		},85000);
		this.control({
			'homeTabView' : {
				 afterrender : this.homeTabRendered
			},
			'AppList':{
				appClick :this.appClicked
			},
			'collapsibleBlock' : {
				onSearch : this.onsearch,
				onAdd : this.add,
				clearSearch : this.search
			},
			'iconListView[itemId=environmentHome]' : {
				itemsClick : this.itemsClick,
				afterrender : this.load
			},
			'collapsibleIconListView' :{
				viewready:this.load,				
				itemsClick : this.itemsClick
			}
		});
	},

	refs : [{
		ref : 'tabs',
		selector : 'tabView'
	}],
	load : function(me){
		me.store.load();
	},
	homeTabRendered : function(me,eOpts){
		console.log(me);
		var store_routeUsed = me.down('ringChart[itemId=routeUsed]').store;
		var store_serviceUsed = me.down('ringChart[itemId=serviceUsed]').store;
		var store_appStatus = me.down('ringChart[itemId=appStatus]').store;
		var store_memoryUsed = me.down('ringChart[itemId=memoryUsed]').store;
		var store_joinedOrg = me.down('ringChart[itemId=joinedOrg]').store;
//		
//		Ext.Ajax.request({
//			url : 'rest/info/basicInfo',
//			method : 'GET',
//			success : function(resp){
//				var data = Ext.JSON.decode(resp.responseText);
//				
			var loadData = function(){
				var total;
				var count;
				if(dashData==false){
					setTimeout(function(){loadData()},500);
					return;
				}
				var data = dashData;
				store_routeUsed.add({itemId : '1',itemName : cfWebLang.HomeTabView.used,itemCount : data.routesNum,total : data.routesAll});
				
				store_serviceUsed.add({itemId : '1',itemName : cfWebLang.HomeTabView.used,itemCount : data.serviceNum,total : data.serviceAll});
				
				store_appStatus.add({itemId : '1',itemName :cfWebLang.HomeTabView.run,itemCount : data.started, total : data.stopped+data.started+data.updating},
						{itemId : '2',itemName : cfWebLang.HomeTabView.stop,itemCount : data.stopped, total : data.stopped+data.started+data.updating},
						{itemId : '3',itemName : cfWebLang.HomeTabView.update,itemCount : data.updating, total : data.stopped+data.started+data.updating});
				
				if(data.memoryAll<1024){
					total=data.memoryAll;
					me.down('ringChart[itemId=memoryUsed]').unit='M';
				}else{
					total=data.memoryAll/1024;
					me.down('ringChart[itemId=memoryUsed]').unit='G';
				}
				
				if(data.memoryUsed<1024){
					count=data.memoryUsed;
				}else{
					count=data.memoryUsed/1024;
				}
				
				store_memoryUsed.add({itemId : '1',itemName :cfWebLang.HomeTabView.used,itemCount : count.toFixed(2),total : total});

				store_joinedOrg.add({itemId : '1',itemName :cfWebLang.HomeTabView.joined,itemCount: data.orgNum, total: data.orgAll});
			}
			loadData();
		
//			}
//		});
		
//		Ext.Ajax.request({
//			url : 'rest/service/listall',
//			method : 'GET',
//			success : function(resp){
//				var obj = Ext.JSON.decode(resp.responseText);
//				
//				console.log(obj.data);
//				console.log(obj.data[0].serType);
//				
//				var len = obj.data.length;
//				
//				for(i=0;i<len;i++){
//					if(obj.data[i].serType == 1){
//						store_middlewareStorage = Ext.getCmp('middlewareStorage').store;
//						store_middlewareStorage.add({itemId : obj.data[i].id, itemName : obj.data[i].serName, itemIco : obj.data[i].serImg});
//					}else if(obj.data[i].serType == 2){
//						store_middlewareAnalyse = Ext.getCmp('middlewareAnalyse').store;
//						store_middlewareAnalyse.add({itemId : obj.data[i].id, itemName : obj.data[i].serName, itemIco : obj.data[i].serImg});
//					}else if(obj.data[i].serType == 3){
//						store_middlewareApplication = Ext.getCmp('middlewareApplication').store;
//						store_middlewareApplication.add({itemId : obj.data[i].id, itemName : obj.data[i].serName, itemIco : obj.data[i].serImg});
//					}
//				}
//				
//			}
//		});
		
	},
	appClicked : function(a,b){
		alert(a);
	},
	add : function(me){
		//console.log(me);
	},
	itemsClick : function(me,record,e){
		alert("ok");
	},
	itemsClick : function(me,record,e){
		console.log(me);		
		console.log(record);
		console.log(record.itemId);
		console.log(e);
		console.log(me.title);
		var title = me.title;
		
		if(title == cfWebLang.HomeTabView.dataStorageService){
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var x = mainView.getX();
			var width = mainView.getWidth() -10;
			var height = mainView.getHeight();
			var win = Ext.widget('slideLayer',{
				layout:'fit',
				adaptTo:mainView,
				blankHeight:100,
				items : [{
					xtype:'infomationView',
					itemId : 'dataStorageService',
					detailList:[{
							name:'',
							dataIndex:'info'
					}]
				}]
			});
			var id = record.itemId;
			console.log(id);
			Ext.Ajax.request({
				url : 'rest/service/listall',
				method : 'GET',
				params : {
					type : 1
//					ser_type : 1,
//					id : id
				},
				success : function(resp){
					var obj = Ext.JSON.decode(resp.responseText);
					console.log(obj.data);
					var len = obj.data.length;
					var dataStor = {};
					var language = getCookie("language");
					for(i=0; i<len; i++){
						iid = obj.data[i].id;
						console.log(iid);
						if(id == iid){
							if(language == "cn"){
								nameChn = obj.data[i].serDesCn;
							    dataStor.info = nameChn;
							    dataStor.title = cfWebLang.Service[obj.data[i].serName];
							    dataStor.itemImg = obj.data[i].serLargeImg;
							}else{
								nameEng = obj.data[i].serDesEng;
								dataStor.info = nameEng;
							    dataStor.title = cfWebLang.Service[obj.data[i].serName];
							    dataStor.itemImg = obj.data[i].serLargeImg;
							}
						}
					}
					win.slideUp();
			        win.down('infomationView').updateAppStatus(dataStor);
				}
			});
			
		}else if(title == cfWebLang.HomeTabView.dataAnalyseService){
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var x = mainView.getX();
			var width = mainView.getWidth() -10;
			var height = mainView.getHeight();
			var win = Ext.widget('slideLayer',{
				layout:'fit',
				adaptTo:mainView,
				blankHeight:100,
				items : [{
					xtype:'infomationView',
					itemId : 'dataAnalyseService',
					detailList:[{
							name:'',
							dataIndex:'info'
					}]
				}]
			});
			var id = record.itemId;
			Ext.Ajax.request({
				url : 'rest/service/listall',
				method : 'GET',
				params : {
					type : 2
//					ser_type : 2,
//					id : id
				},
				success : function(resp){
					var obj = Ext.JSON.decode(resp.responseText);
					console.log(obj.data.length);
					var dataAna= {};
					var language = getCookie("language");
					var len = obj.data.length;
					for(i=0; i<len; i++){
						iid = obj.data[i].id;
						if(id == iid){
							if(language == "cn"){
								nameChn = obj.data[i].serDesCn;
								dataAna.info = nameChn;
								dataAna.title = cfWebLang.Service[obj.data[i].serName];
							    dataAna.itemImg = obj.data[i].serLargeImg;
							}else{
							    nameEng = obj.data[i].serDesEng;
							    dataAna.info = nameEng;
								dataAna.title = cfWebLang.Service[obj.data[i].serName];
							    dataAna.itemImg = obj.data[i].serLargeImg;
							}
						}
					}
					win.slideUp();
					win.down('infomationView').updateAppStatus(dataAna);
				}
			});
			
		}else if(title == cfWebLang.HomeTabView.appService){
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var x = mainView.getX();
			var width = mainView.getWidth() -10;
			var height = mainView.getHeight();
			var win = Ext.widget('slideLayer',{
				layout:'fit',
				adaptTo:mainView,
				blankHeight:100,
				items : [{
					xtype:'infomationView',
					itemId : 'appService',
					detailList:[{
							name:'',
							dataIndex:'info'
					}]
				}]
			});
			var id = record.itemId;
			Ext.Ajax.request({
				url : 'rest/service/listall',
				method : 'GET',
				params : {
					type : 3
//					ser_type : 3,
//					id : id
				},
				success : function(resp){
					var obj = Ext.JSON.decode(resp.responseText);
					var dataSer= {};
					var language = getCookie("language");
					var len = obj.data.length;
					for(i=0; i<len; i++){
						iid = obj.data[i].id;
						if(id == iid){
							if(language == "cn"){
							    nameChn = obj.data[i].serDesCn;
								dataSer.info = nameChn;
								dataSer.title =cfWebLang.Service[obj.data[i].serName];
							    dataSer.itemImg = obj.data[i].serLargeImg;
							}else{
							    nameEng = obj.data[i].serDesEng;
							    dataSer.info = nameEng;
								dataSer.title = cfWebLang.Service[obj.data[i].serName];
							    dataSer.itemImg = obj.data[i].serLargeImg;
							}
						}
					}
					win.slideUp();
					win.down('infomationView').updateAppStatus(dataSer);
				}
			});
		}else{
			var mainView = Ext.ComponentQuery.query('mainView')[0];
			var x = mainView.getX();
			var width = mainView.getWidth() -10;
			var height = mainView.getHeight();
			var win = Ext.widget('slideLayer',{
				layout:'fit',
				adaptTo:mainView,
				blankHeight:100,
				items : [{
					xtype:'infomationView',
					itemId : 'environmentName',
					detailList:[{
							name:'',
							dataIndex:'info'
						}
						]
				}]
			});
			var id = record.itemId;
			console.log(id);
			console.log(record);
			Ext.Ajax.request({
				url : 'rest/info/cfEnvDetail',
				method : 'GET',
				params : {
					id : id
				},
				success : function(resp){
					var obj = Ext.JSON.decode(resp.responseText);
					var data={};
					console.log(obj.data);
					nameChn = obj.data.envDescChn;
					nameEng = obj.data.envDescEng;
					var language = getCookie("language");
					if(language == "cn"){
						data.info = nameChn;
						data.title = obj.data.envName;
						data.itemImg = obj.data.envLargeImg;
					}else if(language == "en"){
						data.info = nameEng;
						data.title = obj.data.envName;
						data.itemImg = obj.data.envLargeImg;
					}
					win.slideUp();
			        win.down('infomationView').updateAppStatus(data);
				}
			});
		}
	},
		
		
	onsearch: function(keyword ,me){

		console.log(keyword);
		console.log(me);
		console.log(me.title);
		var title = me.title;
		
		
		if(title == cfWebLang.HomeTabView.RuntimeEnvironment){
			var store = me.down('iconListView').store;
			store.clearFilter();
			store.filter("itemName",new RegExp('/*'+keyword,"i"));
				
		}
		if(title == cfWebLang.HomeTabView.Middleware){
			
			var store_storage = me.down('collapsibleIconListView[title=' + cfWebLang.HomeTabView.dataStorageService + ']').store;
			store_storage.clearFilter();
			store_storage.filter("itemName",new RegExp('/*'+keyword,"i"));
					
			var store_analyse = me.down('collapsibleIconListView[title=' + cfWebLang.HomeTabView.dataAnalyseService + ']').store;
			store_analyse.clearFilter();
			store_analyse.filter("itemName",new RegExp('/*'+keyword,"i"));
					
			var store_application = me.down('collapsibleIconListView[title=' + cfWebLang.HomeTabView.appService + ']').store;
			store_application.clearFilter();
			store_application.filter("itemName",new RegExp('/*'+keyword,"i"));
				
		}
	},
	search : function(me){
		console.log(me);
		var title = me.title;
			
		if(title == cfWebLang.HomeTabView.RuntimeEnvironment){
			var store = me.down('iconListView').store;
			store.clearFilter();
		}
		if(title == cfWebLang.HomeTabView.Middleware){
			var store_storage = me.down('collapsibleIconListView[title=' + cfWebLang.HomeTabView.dataStorageService + ']').store;
			store_storage.clearFilter();
				
			var store_analyse = me.down('collapsibleIconListView[title=' + cfWebLang.HomeTabView.dataAnalyseService + ']').store;
			store_analyse.clearFilter();
				
			var store_application = me.down('collapsibleIconListView[title=' + cfWebLang.HomeTabView.appService + ']').store;
			store_application.clearFilter();
		}
	}
});
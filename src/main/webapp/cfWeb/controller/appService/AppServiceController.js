Ext.define('cfWeb.controller.appService.AppServiceController',{
	extend : 'Ext.app.Controller',
	views : ['appService.AppServiceView',
	         'appService.NewAppServiceView',
	          'cfWeb.cfWebComponent.LbuttonIconListView',
			  'cfWeb.cfWebComponent.CollapsibleBlock',
	          'cfWeb.cfWebComponent.CbuttonIconListView'],
	stores : ['appService.AppServiceStore',
	     	  'appService.NewAppServiceStore',
	     	  'appService.NewAppBrokersPlanStore'
	         ],
	init : function() {
		this.control({
			'appServiceView collapsibleBlock cbuttonIconListView#dataserverlist' : {
				render : this.viewRender
			},
			'appServiceView collapsibleBlock lbuttonIconListView' : {
				menuClick : this.menuClickAct,
				appClick: this.appClick
			},
			'appServiceView collapsibleBlock' : {
				onSearch : this.onsearch,	
				clearSearch : this.search
			},
			'newAppServiceView combobox[itemId=serviceName]':{
				change : this.serviceChange
			},
			'newAppServiceView':{
				onSubmit:this.createService
			}
		});
	},
	menuClickAct : function(me, act, id, record) {
		console.log(act);
		console.log(me);
		var self = this;
		var databaseBrokerStore = me.store.data.items;
		var storeService = Ext.getCmp('dataserverlist').getStore();
		var itemId = act.itemId;
		switch(id) {
		case 'new' :{
			if(currentORG==null||currentORG==''){
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.OrgUnexit);
				return;
			}
			if(currentSpace==null||currentSpace==''){
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.SpaceUnexit);
				return;
			}
			var serviceWin = Ext.widget('newAppServiceView');
			var serviceBrokerStore = serviceWin.store;
			var storeServiceName = serviceWin.down('container').down('form')
					.down('combobox[itemId=serviceName]').getStore();
			for (var i = 0; i < databaseBrokerStore.length; i++) {
				if (databaseBrokerStore[i].data.id == itemId) {
					var prefix = databaseBrokerStore[i].data.serPrefix;
					var serviceBroker = databaseBrokerStore[i].data.brokers;
					serviceWin.prefix = prefix;
				    serviceWin.serviceId = itemId;
					var brokers=null;
					for (var j = 0; j < serviceBroker.length; j++) {
						brokers = serviceBroker[j] + ',' + brokers;
					}
					console.log(brokers);
					serviceBrokerStore.load({
								params : {
									lguid : brokers
								},
								callback : function(re) {
									storeServiceName.loadData(re);
									serviceWin.down('form').down('combobox[itemId=serviceName]')
										.setValue(storeServiceName.getAt(0).get('label'))
								}
							});

				}
			}
			serviceWin.showAt(300,250,true);
			break;
			}
		case 'filter': {
			storeService.filter("itemId",itemId);
			console.log(storeService);
			}
		}
	},
	
	appClick : function(me,act){
//		var locationUrl = "http://"+csfsmpIp+":"+csfsmpPort+"/csfsmp/index.html?userId="+currentUserId+"&userName="+currentUserName+"&premission="+currentUserIsAdmin;
//		if(act==13){
//			window.open(locationUrl);
//		}
	},
	serviceChange:function(me){
		var serviceWin=me.up('newAppServiceView');
		var planCom=serviceWin.down('form').down('combobox[itemId=servicePlan]');
		var store = me.up('newAppServiceView').store;
		var service = me.getValue();
		var planStore = me.up('newAppServiceView').down('combobox[itemId=servicePlan]').getStore();
		planStore.loadData(store.findRecord('label',service).get("plans"));
		planCom.setValue(planStore.getAt(0).get('name'));
		console.log(planStore);
	},
	createService:function(me){
		var window=me;
		var serviceForm = me.down('form');
		var publicValue = serviceForm.down('combobox[itemId=servicePlan]').getStore().getAt(0).get('public');
		var guidValue = serviceForm.down('combobox[itemId=servicePlan]').getStore().getAt(0).get('guid');
		console.log(serviceForm.down('combobox[itemId=servicePlan]').getStore());
		console.log(publicValue);
		var serviceStore = Ext.getCmp('dataserverlist').getStore();
		var waitWindow=Ext.MessageBox.show({title : cfWebLang.Util.CreateApp,closable: false,cls:'style-cfLoading'});
		Ext.Ajax.request({
			url:'rest/service/createinstance',
			method:'POST',
			params:{
				service:serviceForm.getValues().service,
				plan:serviceForm.getValues().plan,
				prefix:me.prefix,
				visibility:publicValue,
				typeId:me.serviceId,
				planId:guidValue
			},
			success:function(res){
				var obj = Ext.JSON.decode(res.responseText);
				waitWindow.close();
				if(obj.success==true){
					window.close();
				    waitWindow.close();
				}else{
					waitWindow.close();
				    Ext.MessageBox.show({title:cfWebLang.Util.Tip,msg:cfWebLang.Util.CreateFailed,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}	
				serviceStore.load({
				  params:{
				  	first : 3
				  }
				});
			},
			failure:function(response){
				waitWindow.close();
				serviceStore.load({
					  params:{
					  	first : 3
					  }
					});
				
				if(response.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
			    
			    if(response.timedout){  
			        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
			        return;
			    }
			    
			    var error = Ext.JSON.decode(resp.responseText);
				if(error.data!=null&&error.data.source==1){
					 Ext.MessageBox.show({title:cfWebLang.ErrorSource.cloudfoundry,msg:error.data.message,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
			    }else{
					Ext.MessageBox.show({title:cfWebLang.ErrorSource.app,msg:cfWebLang.Error.SystemError,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}
			}
			
		});
	},
	viewRender : function(view) {
		view.getStore().load({
					params : {
						first : 3
					}
				});
	},
	onsearch : function(keyword, me){
		var title = me.title;
		
		if(title == cfWebLang.AppServiceView.AppServiceTemplate){
			var store = me.down('lbuttonIconListView').store;
			
			store.clearFilter();
			store.filter("itemName",new RegExp('/*'+keyword,"i"));
			
		}else if(title == cfWebLang.AppServiceView.AppServiceList){
			var store = me.down('cbuttonIconListView').store;
			store.clearFilter();
			store.filter("title",new RegExp('/*'+keyword,"i"));
		}
	},
	search : function(me){
		var title = me.title;
		
		if(title ==cfWebLang.AppServiceView.AppServiceTemplate){
			var store = me.down('lbuttonIconListView').store;
			store.clearFilter();
		}else if(title == cfWebLang.AppServiceView.AppServiceList){
			var store = me.down('cbuttonIconListView').store;
			store.clearFilter();
		}
	}
});

var piejson = null;
var columnjson = null;
var linejson = null;
var tabPanelId = null;
var count = 0;
var listChart = new Ext.util.MixedCollection();
Ext.define('cfWeb.controller.log.LogController', {
	extend : 'Ext.app.Controller',
	views : ['cfWeb.view.log.LogAnalyWindow',
	         'cfWeb.cfWebComponent.CfPanelTypeI',
	         'cfWeb.cfWebComponent.SlideLayer',
	         'cfWeb.view.appManage.AppListDetailsView'],
	stores : ['log.AppLogTermStore', 'log.AppLogConfigStore',
			'log.AppLogFieldStore'],
	init : function() {
		this.control({
					'appListDetailsView logAnalyWindow toolbar button[itemId=back]' : {
						click:this.goBack
					},
					'appListDetailsView logFileWindow button[action=back]':{
						click:this.goBack2
					},
					'logAnalyWindow toolbar button[itemId=createChart]' : {
						click : this.addChart
					},
					'logAnalyWindow button[itemId=saveChart]' : {
						click : this.saveConfig
					},
					'logAnalyWindow combo[itemId=field_combo]' : {
						render : this.onFieldComboRender
					},
					'logAnalyWindow combo[itemId=count_combo]' : {
						change : this.onChangeCountCombo
					},
					'logAnalyWindow button[itemId=analyLog]':{
						click:this.goAnalyLog
					},
					'logAnalyWindow tabpanel button[itemId=LoadConfig]':{
						click:this.onChartConfig
					},
					'logAnalyWindow':{
						close:this.resetCount,
						render:this.logAnalyWinRender
					},
					'logAnalyWindow toolbar button[itemId=cancle]':{
						click:this.cancleOperation
					},
					'logAnalyWindow tabpanel[itemId=showChart] panel':{
						close:this.removePanel
					},
					'logAnalyWindow tabpanel[itemId=showChart]':{
						tabchange:this.changeTab
					},
					'appListDetailsView':{
						close:this.countReset
					}
				});
	},
	countReset:function(me){
		count=0;
		listChart.removeAll();
	},
	resetCount:function(me){
		me.down('tabpanel[itemId=showChart]').removeAll();
		count=0;
		listChart.removeAll();
	},
	goBack : function (me){
		var container = me.up('container#detailContainer');
		var logView = me.up('logAnalyWindow');
		var lth = container.items.items.length;
		for(var i=0;i<lth;i++){
			container.items.items[i].show();
		}
		logView.down('tabpanel[itemId=showChart]').removeAll();
		count=0;
		listChart.removeAll();
		container.remove(logView);
	},
	goBack2: function(me){
		var container = me.up('container#detailContainer');
		var logView = me.up('logFileWindow');
		var lth = container.items.items.length;
		for(var i=0;i<lth;i++){
			container.items.items[i].show();
		}
		container.remove(logView);
	},
	changeTab:function(me){
		var win = me.up('logAnalyWindow');
		win.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').setVisible(true);
		win.down('panel[itemId=paraConfig]').down('numberfield[itemId=TOP]').setVisible(true);
		win.down('panel[itemId=chartConfig]').down('combo[itemId=count_combo]').reset();
		win.down('panel[itemId=chartConfig]').down('textfield[itemId=chart_title]').reset();
		win.down('panel[itemId=chartConfig]').down('radiogroup[itemId=chart_span]').reset();
	    win.down('panel[itemId=paraConfig]').down('combo[itemId=field_combo]').reset();
		win.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').reset();
		win.down('panel[itemId=paraConfig]').down('numberfield[itemId=TOP]').reset();
		win.down('panel[itemId=styleConfig]').down('combo[itemId=type_combo]').reset();
		win.down('panel[itemId=dataFilter]').down('textfield[itemId=termtext]').reset();
		win.down('button[itemId=createChart]').setText(cfWebLang.LogFileWindow.GenerateChart);
	},
	
	removePanel:function(me){
		var chartOfId = me.down('chart').getId();
		for (var i = 0; i < listChart.getCount(); i++) {
			var json = Ext.JSON.decode(listChart.get(i));
			if (chartOfId == json.chartId) {
				listChart.remove(listChart.get(i));
			}
		}
		console.log(listChart.getCount());
	},
	
	cancleOperation : function(me){
		var win = me.up('logAnalyWindow');
		var isHiddenCreate = win.down('button[itemId=createChart]').isHidden();
		var buttonCreateText = win.down('button[itemId=createChart]').getText();
		if(isHiddenCreate==false && buttonCreateText==cfWebLang.LogFileWindow.GenerateChart){
		    win.down('panel[itemId=chartConfig]').down('combo[itemId=count_combo]').reset();
		    win.down('panel[itemId=chartConfig]').down('textfield[itemId=chart_title]').reset();
		    win.down('panel[itemId=chartConfig]').down('radiogroup[itemId=chart_span]').reset();
		    win.down('panel[itemId=paraConfig]').down('combo[itemId=field_combo]').reset();
		    win.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').reset();
		    win.down('panel[itemId=paraConfig]').down('numberfield[itemId=TOP]').reset();
		    win.down('panel[itemId=styleConfig]').down('combo[itemId=type_combo]').reset();
		    win.down('panel[itemId=dataFilter]').down('textfield[itemId=termtext]').reset();
		}
		if(buttonCreateText==cfWebLang.LogFileWindow.ChangeChart){
			win.down('panel[itemId=chartConfig]').down('combo[itemId=count_combo]').reset();
		    win.down('panel[itemId=chartConfig]').down('textfield[itemId=chart_title]').reset();
		    win.down('panel[itemId=chartConfig]').down('radiogroup[itemId=chart_span]').reset();
		    win.down('panel[itemId=paraConfig]').down('combo[itemId=field_combo]').reset();
		    win.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').reset();
		    win.down('panel[itemId=paraConfig]').down('numberfield[itemId=TOP]').reset();
		    win.down('panel[itemId=styleConfig]').down('combo[itemId=type_combo]').reset();
		    win.down('panel[itemId=dataFilter]').down('textfield[itemId=termtext]').reset();
			win.down('button[itemId=createChart]').setText(cfWebLang.LogFileWindow.GenerateChart);
		}
	},

	logAnalyWinRender:function(win){
    	var storeconfig = win.store;
    	var name =win.appName;
    	Ext.Ajax.request({
			url:'rest/app/getAppId?appName='+name,
			method:'GET',
			success : function(response){
					var obj = Ext.JSON.decode(response.responseText);
					var Id = obj.data;
					console.log(Id);
			        storeconfig.load({
			          params : {
				        appId : Id
			        },
			callback : function(records, operation, success) {
				if(storeconfig.data.length>0){
					win.down('tabpanel[itemId=showChart]').setVisible(true);
					var panelWidth = win.down('tabpanel[itemId=showChart]').getWidth()*0.92;
					for(var i=0;i<storeconfig.data.length;i++){
						var configString = storeconfig.getAt(i).get("configJson");
				        var configToJson = Ext.JSON.decode(storeconfig.getAt(i).get("configJson"));
				        if (configToJson.chart_style.type == 'pie') {
					        Ext.Ajax.request({
						        url:'rest/log/logAnalyzer',
						        method:'GET',
						        params:{
						        	analyzerType:configToJson.chart_conf.count,
						        	field:configToJson.chart_param.field,
						        	size:configToJson.chart_param.top,
							        key:configToJson.data_filter.term,
							        appName:name,
							        config:configString
						        },
						       success:function(response){
							        var responseText = Ext.JSON
											.decode(response.responseText);
									var configPie = Ext.JSON
											.decode(responseText.data[0].config);
									var titlePie = configPie.chart_conf.title + '——'
							                + configPie.chart_param.field + '-'
							                + configPie.chart_conf.count;
					                var chartPie = Ext.create('cfWeb.view.log.LogPieView');
					                chartPie.setWidth(panelWidth* configPie.chart_conf.span);
					                piejson = '{"chartId":"' + chartPie.getId()
							         + '","chart_conf":{"count":"'
							         + configPie.chart_conf.count + '","title":"'
							         + configPie.chart_conf.title + '","span":"'
							         + configPie.chart_conf.span
							         + '"},"chart_param":{"field":"'
							         + configPie.chart_param.field + '","top":"'
							         + configPie.chart_param.top + '"},'
							         + '"chart_style":{"type":"'
							         + configPie.chart_style.type + '"},'
							         + '"data_filter":{"term":"'
							         + configPie.data_filter.term + '" }}';
					                listChart.add(piejson);
					                chartPie.store = Ext.create('cfWeb.store.log.AppLogTermStore');
					                var storePie = chartPie.getStore();
							        storePie.loadData(responseText.data);
							        win.down('tabpanel[itemId=showChart]')
							                .add({
									           closable: true,
									           title:titlePie,
						                      layout:'hbox',
						                      items:[ 
						            	       {xtype:chartPie},
						                       {
						                  xtype:'button',
						                  margin:'0 0 0 5',
						                  text:cfWebLang.Util.Config,
						                  itemId:'LoadConfig'
						                 }]
					               }).show();
					              win.down('tabpanel[itemId=showChart]').setActiveTab(count);
					              count++;
						     },
						    failure:function(response){
						    	if(response.status=="401"){
							    	Ext.Msg.show({
							    	    title: cfWebLang.Util.Tip,
							    	    id:'firstWin',
							    	    msg: cfWebLang.Main.SessionOverdue,
							    	    width: 250,
							    	    buttons: Ext.MessageBox.OK,
							    	    fn: function(){  
							                parent.location.href = '/zwycf/login.html';    
							            },
							            cls:'overtimeWin'
							    	});
							    	//Ext.WindowManager.bringToFront ('firstWin');
							       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
							            parent.location.href = '/zwycf/login.html';    
							        }); */
							       return;
							    }
							    
							    if(response.timedout){  
							        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
							        return;
							    }
					           var responseText = Ext.JSON.decode(response.responseText);
					           Ext.MessageBox.alert(cfWebLang.Util.Tip, response.status+" "+response.statusText);
				            }	
					   });
				    }
				    if (configToJson.chart_style.type =='columnbar') {
					    if(configToJson.chart_param.interval!=''){
					        Ext.Ajax.request({
						        url:'rest/log/logAnalyzer',
						        method:'GET',
						        params:{
						     	    analyzerType:configToJson.chart_conf.count,
							        field:configToJson.chart_param.field,
							        size:configToJson.chart_param.top,
							        step:configToJson.chart_param.interval,
							        key:configToJson.data_filter.term,
							        appName:name,
							        config:configString
						       },
						       success:function(response){
						         	var responseText = Ext.JSON
							  				.decode(response.responseText);
							  		var configColIn = Ext.JSON
											.decode(responseText.data[0].config);
						         	var titleCol = configColIn.chart_conf.title + '——'
							                + configColIn.chart_param.field + '-'
							                + configColIn.chart_conf.count;
					                var chartColumn = Ext.create('cfWeb.view.log.LogColumnbarView',{type:configColIn.chart_param.field});	
					                var store = Ext.create('cfWeb.store.log.AppLogTermStore');
					                chartColumn.store = store;
					                chartColumn.setWidth(panelWidth* configColIn.chart_conf.span);
						         	columnjson = '{"chartId":"' + chartColumn.getId()
							          + '","chart_conf":{"count":"'
							          + configColIn.chart_conf.count
							          + '","title":"'+ configColIn.chart_conf.title
							          + '","span":"'+ configColIn.chart_conf.span
							          + '"},"chart_param":{"field":"'
							          + configColIn.chart_param.field + '","top":"'
							          + configColIn.chart_param.top + '","interval":"' + configColIn.chart_param.interval + '"},'
							          + '"chart_style":{"type":"' + configColIn.chart_style.type + '"},'
							          + '"data_filter":{"term":"'+ configColIn.data_filter.term + '" }}';
					               listChart.add(columnjson);
							       var storeColumn = chartColumn.getStore();
							       storeColumn.loadData(responseText.data);
							       win.down('tabpanel[itemId=showChart]')
							       .add({
									closable: true,
									title:titleCol,
						            layout:'hbox',
						            items:[ 
						            	{xtype:chartColumn},
						                {
						                  xtype:'button',
						                  margin:'0 0 0 5',
						                  text:cfWebLang.Util.Config,
						                  itemId:'LoadConfig'
						                 }]
					        }).show();
					        win.down('tabpanel[itemId=showChart]').setActiveTab(count);
					        count++;
						},
						failure:function(response){
							if(response.status=="401"){
						    	Ext.Msg.show({
						    	    title: cfWebLang.Util.Tip,
						    	    id:'firstWin',
						    	    msg: cfWebLang.Main.SessionOverdue,
						    	    width: 250,
						    	    buttons: Ext.MessageBox.OK,
						    	    fn: function(){  
						                parent.location.href = '/zwycf/login.html';    
						            },
						            cls:'overtimeWin'
						    	});
						    	//Ext.WindowManager.bringToFront ('firstWin');
						       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
						            parent.location.href = '/zwycf/login.html';    
						        }); */
						       return;
						    }
						    
						    if(response.timedout){  
						        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
						        return;
						    }
					        Ext.MessageBox.alert(cfWebLang.Util.Tip, response.status+" "+response.statusText);
				        }
					});
					}
					else{
					Ext.Ajax.request({
						url:'rest/log/logAnalyzer',
						method:'GET',
						params:{
							analyzerType:configToJson.chart_conf.count,
							field:configToJson.chart_param.field,
							size:configToJson.chart_param.top,
							key:configToJson.data_filter.term,
							appName:name,
							config:configString
						},
						success:function(response){
							var responseText = Ext.JSON
											.decode(response.responseText);
							var configCol = Ext.JSON
											.decode(responseText.data[0].config);
							var titleCol = configCol.chart_conf.title + '——'
							                + configCol.chart_param.field + '-'
							                + configCol.chart_conf.count;
					        var chartColumn = Ext.create('cfWeb.view.log.LogColumnbarView',{type:configCol.chart_param.field});	
					        var store = Ext.create('cfWeb.store.log.AppLogTermStore');
					        chartColumn.store = store;
					        chartColumn.setWidth(panelWidth* configCol.chart_conf.span);
							columnjson = '{"chartId":"' + chartColumn.getId()
							+ '","chart_conf":{"count":"'
							+ configCol.chart_conf.count
							+ '","title":"'+ configCol.chart_conf.title
							+ '","span":"'+ configCol.chart_conf.span
							+ '"},"chart_param":{"field":"'
							+ configCol.chart_param.field
							+ '","top":"'+ configCol.chart_param.top + '"},'
							+ '"chart_style":{"type":"'+ configCol.chart_style.type + '"},'
							+ '"data_filter":{"term":"'+ configCol.data_filter.term + '" }}';
					        listChart.add(columnjson);
					        var storeColumn = chartColumn.getStore();
							storeColumn.loadData(responseText.data);
							win.down('tabpanel[itemId=showChart]')
							       .add({
									closable: true,
									title:titleCol,
						            layout:'hbox',
						            items:[ 
						            	{xtype:chartColumn},
						                {
						                  xtype:'button',
						                  margin:'0 0 0 5',
						                  text:cfWebLang.Util.Config,
						                  itemId:'LoadConfig'
						                 }]
					        }).show();
					        win.down('tabpanel[itemId=showChart]').setActiveTab(count);
					        count++;
						},
						failure:function(response){
							if(response.status=="401"){
						    	Ext.Msg.show({
						    	    title: cfWebLang.Util.Tip,
						    	    id:'firstWin',
						    	    msg: cfWebLang.Main.SessionOverdue,
						    	    width: 250,
						    	    buttons: Ext.MessageBox.OK,
						    	    fn: function(){  
						                parent.location.href = '/zwycf/login.html';    
						            },
						            cls:'overtimeWin'
						    	});
						    	//Ext.WindowManager.bringToFront ('firstWin');
						       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
						            parent.location.href = '/zwycf/login.html';    
						        }); */
						       return;
						    }
						    
						    if(response.timedout){  
						        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
						        return;
						    }
					        var responseText = Ext.JSON.decode(response.responseText);
					        Ext.MessageBox.alert(cfWebLang.Util.Tip, response.status+" "+response.statusText);
				        }
					});
					}
				}
				if (configToJson.chart_style.type == 'line') {
					Ext.Ajax.request({
						url:'rest/log/logAnalyzer',
						method:'GET',
						params:{
							analyzerType:configToJson.chart_conf.count,
							field:configToJson.chart_param.field,
							size:configToJson.chart_param.top,
							key:configToJson.data_filter.term,
							appName:name,
							config:configString
						},
						success:function(response){
							var responseText = Ext.JSON
											.decode(response.responseText);
							var configLine = Ext.JSON
											.decode(responseText.data[0].config);
							var titleLine = configLine.chart_conf.title + '——'
							                + configLine.chart_param.field + '-'
							                + configLine.chart_conf.count;
					        var chartLine = Ext.create('cfWeb.view.log.LogLineView',{type:configLine.chart_param.field});
					        var store = Ext.create('cfWeb.store.log.AppLogTermStore');
					        chartLine.store = store;
					        chartLine.setWidth(panelWidth* configLine.chart_conf.span);
					        linejson = '{"chartId":"' + chartLine.getId()
							+ '","chart_conf":{"count":"'
							+ configLine.chart_conf.count
							+ '","title":"'
							+ configLine.chart_conf.title + '","span":"'
							+ configLine.chart_conf.span
							+ '"},"chart_param":{"field":"'
							+ configLine.chart_param.field + '","top":"'
							+ configLine.chart_param.top + '"},'
							+ '"chart_style":{"type":"'
							+ configLine.chart_style.type + '"},'
							+ '"data_filter":{"term":"'
							+ configLine.data_filter.term + '" }}';
					        listChart.add(linejson);
					        var storeLine = chartLine.getStore();
							storeLine.loadData(responseText.data);
							win.down('tabpanel[itemId=showChart]')
							       .add({
									closable: true,
									title:titleLine,
						            layout:'hbox',
						            items:[ 
						            	{xtype:chartLine},
						                {
						                  xtype:'button',
						                  margin:'0 0 0 5',
						                  text:cfWebLang.Util.Config,
						                  itemId:'LoadConfig'
						                 }]
					        }).show();
					        win.down('tabpanel[itemId=showChart]').setActiveTab(count);
					        count++;
						},
						failure:function(response){
							if(response.status=="401"){
						    	Ext.Msg.show({
						    	    title: cfWebLang.Util.Tip,
						    	    id:'firstWin',
						    	    msg: cfWebLang.Main.SessionOverdue,
						    	    width: 250,
						    	    buttons: Ext.MessageBox.OK,
						    	    fn: function(){  
						                parent.location.href = '/zwycf/login.html';    
						            },
						            cls:'overtimeWin'
						    	});
						    	//Ext.WindowManager.bringToFront ('firstWin');
						       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
						            parent.location.href = '/zwycf/login.html';    
						        }); */
						       return;
						    }
						    
						    if(response.timedout){  
						        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
						        return;
						    }
					        var responseText = Ext.JSON.decode(response.responseText);
					        Ext.MessageBox.alert(cfWebLang.Util.Tip, response.status+" "+response.statusText);
				        }
					});
				}
			}
					}

		 }
      });
			}
    	});
   },
    
    addChart : function(button) {
		var logview = button.up('logAnalyWindow');
		logview.down('tabpanel[itemId=showChart]').setVisible(true);
		var chart_conf = logview.down('panel[itemId=chartConfig]');
		var count_combo = chart_conf.down('combo[itemId=count_combo]').getValue();
		var chart_title = chart_conf.down('textfield[itemId=chart_title]')
				.getValue();
		var chart_check = chart_conf.down('radiogroup[itemId=chart_span]')
				.getChecked();
		var chart_span = '';
		for (var i = 0; i < chart_check.length; i++) {
			chart_span = chart_check[i].inputValue;
		}
		var chart_param = logview.down('panel[itemId=paraConfig]');
		var field_combo = chart_param.down('combo[itemId=field_combo]')
				.getValue();
		var chart_interval = chart_param.down('textfield[itemId=intervalText]')
				.getValue();
		var chart_top = chart_param.down('numberfield[itemId=TOP]').getValue();
		var chart_style = logview.down('panel[itemId=styleConfig]');
		var chart_type = chart_style.down('combo[itemId=type_combo]').getValue();
		var data_filter = logview.down('panel[itemId=dataFilter]');
		var term_filter = data_filter.down('textfield[itemId=termtext]')
				.getValue();
		var buttonText = button.getText();
		var tabpanelWidth = logview.down('tabpanel[itemId=showChart]').getWidth()*0.92;
		var appName = logview.appName;
		var chartNew = null;
		var chartHeight = 0;
		if(count_combo==null){
			var tip=logview.down('displayfield[itemId=typeErrorTip]');
			tip.setVisible(true);
			return false;
		}
		if(field_combo==null){
			var tip=logview.down('displayfield[itemId=fieldErrorTip]');
			tip.setVisible(true);
			return false;
		}
		if(chart_type==null){
			var tip=logview.down('displayfield[itemId=chartErrorTip]');
			tip.setVisible(true);
			return false;
		}
		if(count_combo==cfWebLang.LogFileWindow.ChartStatic){
			if(chart_interval==''){
				var tip=logview.down('displayfield[itemId=invalErrorTip]');
			    tip.setVisible(true);
			    return false;
			}
		}
		if (buttonText == cfWebLang.LogFileWindow.GenerateChart) {
			if (chart_type == 'pie') {
				chartNew = Ext.create('cfWeb.view.log.LogPieView');
				var store = Ext.create('cfWeb.store.log.AppLogTermStore');
				chartNew.store = store;
				chartNew.setWidth(tabpanelWidth * chart_span);
				piejson = '{"chartId":"' + chartNew.getId()
						+ '","chart_conf":{"count":"' + count_combo
						+ '","title":"' + chart_title + '","span":"'
						+ chart_span + '"},"chart_param":{"field":"'
						+ field_combo + '","top":"' + chart_top
						+ '"},'+ '"chart_style":{"type":"' + chart_type + '"},'
						+ '"data_filter":{"term":"' + term_filter + '" }}';
				listChart.add(piejson);
			} else if (chart_type == 'columnbar') {
				chartNew = Ext.create('cfWeb.view.log.LogColumnbarView',{type:field_combo});
				var store = Ext.create('cfWeb.store.log.AppLogTermStore');
				chartNew.store = store;
				chartNew.setWidth(tabpanelWidth * chart_span);
				columnjson = '{"chartId":"' + chartNew.getId()
						+ '","chart_conf":{"count":"' + count_combo
						+ '","title":"' + chart_title + '","span":"'
						+ chart_span + '"},"chart_param":{"field":"'
						+ field_combo + '","top":"' + chart_top + '","interval":"' + chart_interval + '"},'
						+ '"chart_style":{"type":"' + chart_type + '"},'
						+ '"data_filter":{"term":"' + term_filter + '" }}';
				listChart.add(columnjson);
			} else if (chart_type == 'line') {
				chartNew = Ext.create('cfWeb.view.log.LogLineView',{type:field_combo});
				var store = Ext.create('cfWeb.store.log.AppLogTermStore');
				chartNew.store = store;
				chartNew.setWidth(tabpanelWidth * chart_span);
				linejson = '{"chartId":"' + chartNew.getId()
						+ '","chart_conf":{"count":"' + count_combo
						+ '","title":"' + chart_title + '","span":"'
						+ chart_span + '"},"chart_param":{"field":"'
						+ field_combo + '","top":"' + chart_top + '"},'
						+ '"chart_style":{"type":"' + chart_type + '"},'
						+ '"data_filter":{"term":"' + term_filter + '" }}';
				listChart.add(linejson);
			}
			Ext.Ajax.request({
				url:'rest/log/logAnalyzer',
				method:'GET',
				params:{
					analyzerType:count_combo,
					field:field_combo,
					size:chart_top,
					step:chart_interval,
					key:term_filter,
					appName:appName
				},
				success:function(response){
					var store = chartNew.getStore();
					var responseText = Ext.JSON
											.decode(response.responseText);
					
					if(responseText.success==true){
						if(responseText.data.length==0){
						listChart.remove(listChart.last());
						Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.noResult);
						return;
					}
					if(responseText.data[0].count=='-Infinity'&&responseText.data.length==3){
						listChart.remove(listChart.last());
						Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.noResult);
						return;
					}
						store.loadData(responseText.data);
					logview.down('tabpanel[itemId=showChart]').add({
									closable: true,
									title:chart_title + '——' + field_combo + '-'
						                    + count_combo,
						            layout:'hbox',
						            items:[ 
						            	{xtype:chartNew},
						                {
						                  xtype:'button',
						                  margin:'0 0 0 5',
						                  text:cfWebLang.Util.Config,
						                  itemId:'LoadConfig'
						                 }]
					}).show();
					logview.down('tabpanel[itemId=showChart]').setActiveTab(count);
					  count++;
					}
					else{
						Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.CountType);
					}
				},
				failure:function(response){
					if(response.status=="401"){
				    	Ext.Msg.show({
				    	    title: cfWebLang.Util.Tip,
				    	    id:'firstWin',
				    	    msg: cfWebLang.Main.SessionOverdue,
				    	    width: 250,
				    	    buttons: Ext.MessageBox.OK,
				    	    fn: function(){  
				                parent.location.href = '/zwycf/login.html';    
				            },
				            cls:'overtimeWin'
				    	});
				    	//Ext.WindowManager.bringToFront ('firstWin');
				       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
				            parent.location.href = '/zwycf/login.html';    
				        }); */
				       return;
				    }
				    
				    if(response.timedout){  
				        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
				        return;
				    }
					var responseText = Ext.JSON.decode(response.responseText);
					listChart.remove(listChart.last());
				    if(responseText.data.httpStatusCode!=null){
				    	Ext.MessageBox.alert(cfWebLang.Util.Tip, responseText.error);
				    }
				    else{
				    	Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.CountType);
				    }
				}
			});
	    }
	     if (buttonText == cfWebLang.LogFileWindow.ChangeChart) {
			if (chart_type == 'pie') {
			    chartNew = Ext.create('cfWeb.view.log.LogPieView');
			    var store = Ext.create('cfWeb.store.log.AppLogTermStore');
			    chartNew.store = store;
				chartNew.setWidth(tabpanelWidth * chart_span);
				piejson = '{"chartId":"' + chartNew.getId()
						+ '","chart_conf":{"count":"' + count_combo
						+ '","title":"' + chart_title + '","span":"'
						+ chart_span + '"},"chart_param":{"field":"'
						+ field_combo + '","top":"' + chart_top
						+ '","interval":"' + chart_interval + '"},'
						+ '"chart_style":{"type":"' + chart_type + '"},'
						+ '"data_filter":{"term":"' + term_filter + '" }}';
				listChart.add(piejson);
			} else if (chart_type == 'columnbar') {
				chartNew = Ext.create('cfWeb.view.log.LogColumnbarView',{type:field_combo});
				var store = Ext.create('cfWeb.store.log.AppLogTermStore');
				chartNew.store = store;
				chartNew.setWidth(tabpanelWidth * chart_span);
				columnjson = '{"chartId":"' + chartNew.getId()
						+ '","chart_conf":{"count":"' + count_combo
						+ '","title":"' + chart_title + '","span":"'
						+ chart_span + '"},"chart_param":{"field":"'
						+ field_combo + '","top":"' + chart_top + '","interval":"' + chart_interval + '"},'
						+ '"chart_style":{"type":"' + chart_type + '"},'
						+ '"data_filter":{"term":"' + term_filter + '" }}';
				listChart.add(columnjson);
			} else if (chart_type == 'line') {
				chartNew = Ext.create('cfWeb.view.log.LogLineView',{type:field_combo});
				var store = Ext.create('cfWeb.store.log.AppLogTermStore');
				chartNew.store = store;
				chartNew.setWidth(tabpanelWidth * chart_span);
				linejson = '{"chartId":"' + chartNew.getId()
						+ '","chart_conf":{"count":"' + count_combo
						+ '","title":"' + chart_title + '","span":"'
						+ chart_span + '"},"chart_param":{"field":"'
						+ field_combo + '","top":"' + chart_top + '"},'
						+ '"chart_style":{"type":"' + chart_type + '"},'
						+ '"data_filter":{"term":"' + term_filter + '" }}';
				listChart.add(linejson);
			}
			Ext.Ajax.request({
				url:'rest/log/logAnalyzer',
				method:'GET',
				params:{
					analyzerType:count_combo,
					field:field_combo,
					size:chart_top,
					step:chart_interval,
					key:term_filter,
					appName:appName
				},
				success:function(response){
					var storeChart = chartNew.getStore();
					var responseText = Ext.JSON
											.decode(response.responseText);
					if(responseText.success==true){
						if(responseText.data.length==0){
						listChart.remove(listChart.last());
						Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.noResult);
						return;
					    }
					   if(responseText.data[0].count=="-Infinity"){
						listChart.remove(listChart.last());
						Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.noResult);
						return;
					    }
						storeChart.loadData(responseText.data);
						 var chartNowId = logview.down('tabpanel[itemId=showChart]').getActiveTab().down('chart').getId();
		                 for (var i = 0; i < listChart.getCount(); i++) {
			               var json = Ext.JSON.decode(listChart.get(i));
			               if (chartNowId == json.chartId) {
			    	         listChart.remove(listChart.get(i));
			               }
		                }
					logview.down('tabpanel[itemId=showChart]').getActiveTab().removeAll();
					logview.down('tabpanel[itemId=showChart]').getActiveTab().setTitle(chart_title + '——' + field_combo + '-'
						                    + count_combo);
					logview.down('tabpanel[itemId=showChart]').getActiveTab()
							.add({
						            layout:'hbox',
						            items:[ 
						            	{xtype:chartNew},
						                {
						                  xtype:'button',
						                  margin:'0 0 0 5',
						                  text:cfWebLang.Util.Config,
						                  itemId:'LoadConfig'
						                 }]
					}).show();
					logview.down('tabpanel[itemId=showChart]').setActiveTab(count);
					        count++;
					Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.Util.ChangeSuccess);
					button.setText(cfWebLang.LogFileWindow.GenerateChart);
					}
				else{
						Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.CountType);
					}
				},
				failure:function(response){
					listChart.remove(listChart.last());
					if(response.status=="401"){
				    	Ext.Msg.show({
				    	    title: cfWebLang.Util.Tip,
				    	    id:'firstWin',
				    	    msg: cfWebLang.Main.SessionOverdue,
				    	    width: 250,
				    	    buttons: Ext.MessageBox.OK,
				    	    fn: function(){  
				                parent.location.href = '/zwycf/login.html';    
				            },
				            cls:'overtimeWin'
				    	});
				    	//Ext.WindowManager.bringToFront ('firstWin');
				       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
				            parent.location.href = '/zwycf/login.html';    
				        }); */
				       return;
				    }
				    
				    if(response.timedout){  
				        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
				        return;
				    }
					var responseText = Ext.JSON.decode(response.responseText);
				    if(responseText.data.httpStatusCode!=null){
				    	Ext.MessageBox.alert(cfWebLang.Util.Tip, responseText.error);
				    }
				    else{
				    	Ext.MessageBox.alert(cfWebLang.Util.Tip, cfWebLang.LogFileWindow.CountType);
				    }
				}
			});
	    }
	    logview.down('panel[itemId=chartConfig]').down('combo[itemId=count_combo]').reset();
		logview.down('panel[itemId=chartConfig]').down('textfield[itemId=chart_title]').reset();
		logview.down('panel[itemId=chartConfig]').down('radiogroup[itemId=chart_span]').reset();
		logview.down('panel[itemId=paraConfig]').down('combo[itemId=field_combo]').reset();
		logview.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').reset();
		logview.down('panel[itemId=paraConfig]').down('numberfield[itemId=TOP]').reset();
		logview.down('panel[itemId=styleConfig]').down('combo[itemId=type_combo]').reset();
		logview.down('panel[itemId=dataFilter]').down('textfield[itemId=termtext]').reset();
   },
   
   onFieldComboRender : function(me) {
		var storeField = me.getStore();
		var self = this;
		var name = me.up('logAnalyWindow').appName;
		Ext.Ajax.request({
			url:'rest/app/getAppId?appName='+name,
			method:'GET',
			success : function(response){
					var obj = Ext.JSON.decode(response.responseText);
					var Id = obj.data;
					self.appId = Id;
					console.log(Id);
		            storeField.load({
					   params : {
						   appId : Id
					   },
					   callback : function() {
					   	console.log(storeField);
					   }
				});
			}
		});
		
	},
	
	saveConfig : function(me) {
		var appid = this.appId;
		var logConfig = [];
		for(var i = 0;i<listChart.getCount();i++){
			logConfig.push(listChart.get(i));
		}
		console.log(logConfig);
		Ext.Ajax.request({
			url : 'rest/app/setconfigs',
			method : 'POST',
			params : {
				appId : appid,
				logConfig : logConfig
			},
			success : function(response) {
				Ext.MessageBox.alert(cfWebLang.Util.Tip,cfWebLang.Util.SaveSuccess);
			}
		});
		},
	
	onChangeCountCombo : function(view) {
		if((view.up('logAnalyWindow')).down('displayfield[itemId=invalErrorTip]').isHidden()==false){
				(view.up('logAnalyWindow')).down('displayfield[itemId=invalErrorTip]').setVisible(false);
			}
		var chartType = (view.up('logAnalyWindow'))
				.down('panel[itemId=styleConfig]')
				.down('combo[itemId=type_combo]');
		var typeStore = chartType.getStore();
		var countValue = (view.up('logAnalyWindow'))
				.down('panel[itemId=chartConfig]')
				.down('combo[itemId=count_combo]').getValue();
		if (countValue == cfWebLang.LogFileWindow.SurveyStatic) {
			(view.up('logAnalyWindow')).down('panel[itemId=paraConfig]')
					.down('numberfield[itemId=TOP]').setVisible(false);
			(view.up('logAnalyWindow')).down('panel[itemId=paraConfig]')
					.down('textfield[itemId=intervalText]').setVisible(false);
			var chartType = (view.up('logAnalyWindow'))
					.down('panel[itemId=styleConfig]')
					.down('combo[itemId=type_combo]');
			typeStore.clearFilter(true);
			typeStore.load();
		}
		if (countValue == cfWebLang.LogFileWindow.KeyWordStatic) {
			(view.up('logAnalyWindow')).down('panel[itemId=paraConfig]')
					.down('numberfield[itemId=TOP]').setVisible(true);
			(view.up('logAnalyWindow')).down('panel[itemId=paraConfig]')
					.down('textfield[itemId=intervalText]').setVisible(false);
			typeStore.clearFilter(true);
			typeStore.load();
		}
		if (countValue == cfWebLang.LogFileWindow.ChartStatic) {
			(view.up('logAnalyWindow')).down('panel[itemId=paraConfig]')
					.down('numberfield[itemId=TOP]').setVisible(false);
			(view.up('logAnalyWindow')).down('panel[itemId=paraConfig]')
					.down('textfield[itemId=intervalText]').setVisible(true);
			typeStore.filter("value", "columnbar");
			typeStore.load();
			chartType.reset();
		}
	},
	
	onChartConfig:function(me){
		tabPanelId = me.up('tabpanel').activeTab.getId();
		console.log(tabPanelId);
		var logview = me.up('logAnalyWindow');
		logview.down('displayfield[itemId=typeErrorTip]').setVisible(false);
		logview.down('displayfield[itemId=fieldErrorTip]').setVisible(false);
		logview.down('displayfield[itemId=invalErrorTip]').setVisible(false);
		logview.down('displayfield[itemId=chartErrorTip]').setVisible(false);
		var chartId = me.up('tabpanel').getActiveTab().down('chart').getId();
		console.log(chartId);
		var checkitems = logview.down('panel[itemId=chartConfig]')
				.down('radiogroup[itemId=chart_span]').items;
		for (var i = 0; i < listChart.getCount(); i++) {
			var json = Ext.JSON.decode(listChart.get(i));
			if (chartId == json.chartId) {
				logview.down('panel[itemId=chartConfig]')
							.down('combo[itemId=count_combo]')
							.setValue(json.chart_conf.count);
				logview.down('panel[itemId=chartConfig]')
							.down('textfield[itemId=chart_title]')
							.setValue(json.chart_conf.title);
				for (var j = 0; j < checkitems.length; j++) {
						checkitems.items[j].setValue(false);
						if (checkitems.items[j].inputValue == json.chart_conf.span) {
							checkitems.items[j].setValue(true);
						}
					}
					logview.down('panel[itemId=paraConfig]')
							.down('combo[itemId=field_combo]')
							.setValue(json.chart_param.field);
				if (json.chart_conf.count == cfWebLang.LogFileWindow.SurveyStatic) {
						logview.down('panel[itemId=paraConfig]')
								.down('numberfield[itemId=TOP]')
								.setVisible(false);
						logview.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').setVisible(false);
					} else if(json.chart_conf.count == cfWebLang.LogFileWindow.KeyWordStatic){
						logview.down('panel[itemId=paraConfig]')
								.down('numberfield[itemId=TOP]')
								.setValue(json.chart_param.top);
						logview.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').setVisible(false);
					}else{
						logview.down('panel[itemId=paraConfig]').down('textfield[itemId=intervalText]').setValue(json.chart_param.interval);
					}
					logview.down('panel[itemId=styleConfig]')
							.down('combo[itemId=type_combo]')
							.setValue(json.chart_style.type);
					logview.down('panel[itemId=dataFilter]')
							.down('textfield[itemId=termtext]')
							.setValue(json.data_filter.term);
				}
			}
			logview.down('toolbar[itemId=toolButton]')
					.down('button[itemId=createChart]').setText(cfWebLang.LogFileWindow.ChangeChart);
	}

});

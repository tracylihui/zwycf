Ext.define('cfWeb.controller.accountSetting.ChangePasswdController', {
	extend : 'Ext.app.Controller',
	views : ['accountSetting.ChangePasswdWindow'],
	init : function() {
		this.control({
			'changePasswdWindow button[action=changepasswd]' : {
				click:this.changepasswd
			},
			'changePasswdWindow button[action=close]' : {
				click:this.closeWin
			},
			'changePasswdWindow textfield' : {
				blur:this.validate
			}
		});
	},
	changepasswd:function(btn){
		var win=btn.up('window');
		var form=win.down('form');
		var oldPasswordField=form.down('textfield#oldPassword');
		var oldPassword=oldPasswordField.getValue();
		var passwordField=form.down('textfield#password');
		var password=passwordField.getValue();
		var passwordVFiled=form.down('textfield#verifyPassword');
		var passwordV=passwordVFiled.getValue();
		var returnFlag=true;
		if(oldPassword==null||oldPassword==""){
			oldPasswordField.up('panel').down('displayfield#nullValueTip').show();
			returnFlag=false;
		}else{
			oldPasswordField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(password==null||password==""){
			passwordField.up('panel').down('displayfield#nullValueTip').show();
			returnFlag=false;
		}else{
			passwordField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(passwordV==null||passwordV==""){
			passwordVFiled.up('panel').down('displayfield#nullValueTip').show();
			returnFlag=false;
		}else{
			passwordVFiled.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		
		if(!returnFlag){
			return;
		}
		
		if(password!=passwordV){
			returnFlag=false;
			form.down('displayfield#confirmPasswordTip').show();
		}else{
			form.down('displayfield#confirmPasswordTip').hide();
		}
		
		if(!returnFlag){
			return;
		}
		
		form.submit({
			url: 'rest/user/changePasswd',
		    success: function(form, action) {
		    	win.close();
		       Ext.Msg.alert('TIP', '修改成功，请重新登录！');
		    },
		    failure:function(e,resp){
		    	if(resp.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
			    
			    if(resp.timedout){  
			        Ext.Msg.show({title:cfWebLang.Util.Tip,msg:cfWebLang.global.timeout,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});  
			        return;
			    }
				var error= Ext.JSON.decode(resp.response.responseText);
				if(error.data!=null&&error.data.source==1){
					Ext.MessageBox.show({title:cfWebLang.ErrorSource.cloudfoundry,msg:error.data.message,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}else if(error.error=='passwordError'){
					form.down('displayfield#passwordError').show();
				}else {
					Ext.MessageBox.alert({title:cfWebLang.ErrorSource.app,msg:cfWebLang.Error.SystemError,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}
		    }
		});
		/**/
	},
	
	closeWin:function(btn){
		var win=btn.up('window');
		win.close();
	},
	
	validate:function(text){
		var panel=text.up('panel');
		var form=text.up('form');
		var confirm=form.down('displayfield#confirmPasswordTip');
		confirm.hide();
		form.down('displayfield#passwordError').hide();
		var error=panel.down('displayfield#nullValueTip');
		if(text.getValue()==null||text.getValue()==""){
			error.show();
			return;
		}else{
			error.hide();
		}
		if(text.itemId=='verifyPassword'){
			var passwordField=form.down('textfield#password');
			var password=passwordField.getValue();
			var passwordV=text.getValue();
			if(password!=passwordV){
				confirm.show();
			}else{
				confirm.hide();
			}
		}
		
	}
});

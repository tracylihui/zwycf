Ext.define('cfWeb.controller.accountSetting.QuotaManageController', {
	extend : 'Ext.app.Controller',
	views : ['accountSetting.QuotaManageView',
	         'accountSetting.QuotaEditWindow'],
	stores:['accountSetting.QuotaStore',
	        'util.MemoryStore'],
	init : function() {
		this.control({
			'quotaManageView' : {
				show:this.quotaViewRender
			},
			'quotaManageView button#addQuota' : {
				click:this.showAddQuota
			},
//			'quotaManageView grid actioncolumn':{
//				click:this.showEditQuota
//			},
			'quotaEditWindow':{
				beforerender:this.removeCss
			},
			'quotaEditWindow button[action=addQuota]':{
				click:this.addQuota
			},
			'quotaEditWindow button[action=editQuota]':{
				click:this.editQuota
			},
			'quotaEditWindow button[action=close]':{
				click:this.closeWin
			},
			'quotaEditWindow textfield':{
				blur:this.validateText
			},
			'quotaEditWindow numberfield':{
				blur:this.validateNumber
			}
		});
	},
	quotaViewRender:function(panel){
		var grid=panel.down("grid");
		var store=grid.getStore();
		store.load();
	},
	showAddQuota:function(btn){
		var store=btn.up('panel').down('grid').getStore();
		var win=Ext.widget('quotaEditWindow');
		win.store=store;
  		win.show();
	},
	addQuota:function(btn){
		var win=btn.up('window');
		var form=win.down('form');
		var nameField=form.down('textfield[name=name]');
		var servicesField=form.down('numberfield[name=services]');
		var routesField=form.down('numberfield[name=routes]');
		var memoryField=form.down('textfield[name=memeory]');
		var imfield=form.down('combobox[name=instancememeory]');
		
		var name=nameField.getValue();
		var service=servicesField.getValue();
		var routes=routesField.getValue();
		var memory=memoryField.getValue();
		var imemory=imfield.getValue();
		
		var flag=true;
		
		if(name==null||name==""){
			flag=false;
			nameField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			nameField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(service==null||service==0){
			flag=false;
			servicesField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			servicesField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(routes==null||routes==0){
			flag=false;
			routesField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			routesField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(memory==null||memory==0){
			flag=false;
			memoryField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			memoryField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(!flag||!form.isDirty()){
			return;
		}
		
		form.submit({
			url: 'rest/quota/create',
		    success: function(form, action) {
		    	win.close();
		    	win.store.reload();
		    },
		    failure:function(e,resp){
		    	if(resp.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
			    
			    if(resp.timedout){  
			        Ext.Msg.show({title:cfWebLang.Util.Tip,msg:cfWebLang.global.timeout,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});  
			        return;
			    }
				var error= Ext.JSON.decode(resp.response.responseText);
				if(error.data!=null&&error.data.source==1){
					Ext.MessageBox.show({title:cfWebLang.ErrorSource.cloudfoundry,msg:error.data.message,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}else if(error.error=='nameRepeat'){
					form.down('displayfield#nameRepeatError').show();
				}else {
					Ext.MessageBox.alert({title:cfWebLang.ErrorSource.app,msg:cfWebLang.Error.SystemError,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}
		    }
		});
	},
	editQuota:function(btn){
		var win=btn.up('window');
		var form=win.down('form');
		var nameField=form.down('textfield[name=name]');
		var servicesField=form.down('numberfield[name=services]');
		var routesField=form.down('numberfield[name=routes]');
		var memoryField=form.down('textfield[name=memeory]');
		var imfield=form.down('combobox[name=instancememeory]');
		
		var name=nameField.getValue();
		var service=servicesField.getValue();
		var routes=routesField.getValue();
		var memory=memoryField.getValue();
		
		var flag=true;
		
		if(name==null||name==""){
			flag=false;
			nameField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			nameField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(service==null||service==0){
			flag=false;
			servicesField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			servicesField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(routes==null||routes==0){
			flag=false;
			routesField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			routesField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(memory==null||memory==0){
			flag=false;
			memoryField.up('panel').down('displayfield#nullValueTip').show();
		}else{
			memoryField.up('panel').down('displayfield#nullValueTip').hide();
		}
		
		if(!flag||!form.isDirty()){
			return;
		}
		
		form.submit({
			url: 'rest/quota/update',
		    success: function(form, action) {
		    	win.close();
		    	win.store.reload();
		    },
		    failure:function(e,resp){
		    	if(resp.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
			    
			    if(resp.timedout){  
			        Ext.Msg.show({title:cfWebLang.Util.Tip,msg:cfWebLang.global.timeout,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});  
			        return;
			    }
				var error= Ext.JSON.decode(resp.response.responseText);
				if(error.data!=null&&error.data.source==1){
					Ext.MessageBox.show({title:cfWebLang.ErrorSource.cloudfoundry,msg:error.data.message,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}else if(error.error=='nameRepeat'){
					form.down('displayfield#nameRepeatError').show();
				}else {
					Ext.MessageBox.alert({title:cfWebLang.ErrorSource.app,msg:cfWebLang.Error.SystemError,cls:'style-cfMessText',buttons: Ext.MessageBox.OK});
				}
		    }
		});
	},
	closeWin:function(btn){
		btn.up('window').close();
	},
	removeCss:function(me){
		var field1=me.down('numberfield[name=services]');
		var field2=me.down('numberfield[name=routes]');
		field1.removeCls('window-popup');
		field2.removeCls('window-popup');
	},
	validateText:function(text){
		var panel=text.up('panel');
		var form=text.up('form');
		form.down('displayfield#nameRepeatError').hide();
		var error=panel.down('displayfield#nullValueTip');
		if(text.getValue()==null||text.getValue()==""){
			error.show();
		}else{
			error.hide();
		}
	},
	validateNumber:function(text){
		var panel=text.up('panel');
		var error=panel.down('displayfield#nullValueTip');
		if(text.getValue()==null||text.getValue()==0){
			error.show();
		}else{
			error.hide();
		}
	}
});

Ext.define('cfWeb.controller.appManage.ThresholdController',{
	extend : 'Ext.app.Controller',
	views:['cfWeb.cfWebComponent.ThresholdControlView',
	       'cfWeb.cfWebComponent.CfSlider',
	       'cfWeb.cfWebComponent.CfMultiSlider'],
	detailTimer:undefined,
	init:function(){
		this.control({
			"thresholdControlView#detailThreshold":{
				changeSubmit:this.changeSubmit,
				boxready:this.updateData,
				destroy:this.destroy
			}
		});
	},
	destroy:function(){
		clearTimeout(this.detailTimer);
	},
	changeSubmit:function(view,data){
		var me = this;
		var win=view.up('appListDetailsView');
		var appName=win.appName;
		var instanceNum=null;
		var memoryNum=null;
		var minCpu=null,maxCpu=null,minInstance=null,maxInstance=null;
		var threshold = win.down('thresholdControlView');
		var autoScale = threshold.getMode();
		if(data[0].isDirty==true){
			instanceNum=data[0].value;
		}
		
		if(data[1].isDirty==true){
			memoryNum=data[1].value;
		}
		
		if(data[2].isDirty==true){
			minCpu=data[2].value[0];
			maxCpu=data[2].value[1];
		}
		
		if(data[3].isDirty==true){
			minInstance=data[3].value[0];
			maxInstance=data[3].value[1];
		}
		isOperayion = 1;
		Ext.Ajax.request({
			url : 'rest/app/scaleApp',
			method : 'POST',
			params :{
				autoScale:autoScale,
				appName : appName,
				instanceNum:instanceNum,
				minCpu:minCpu,
				maxCpu:maxCpu,
				memoryNum:memoryNum,
				minInstance:minInstance,
				maxInstance:maxInstance,
			},
			success : function(resp){	
				Ext.Ajax.request({
					url : 'rest/app/details',
					method : 'GET',
					params : {
						appName : appName
					},
					success:function(response){
						var obj = Ext.JSON.decode(response.responseText);
						console.log(obj);
						var insStatus = obj.instStatus;
						win.down('cfPanelTypeI[itemId=appIns]').down('switchContentView').setValue(obj.instStatus);
						win.down('appHeadView').updateAppStatus(obj);
						win.down("#instanceSlider").setValue(obj.instance);
					    win.down("#memorySlider").setValue(obj.memory);
						view.commitChange();
						var hasException = false;
						var lth = insStatus.length;
						for(var i = 0;i<lth;i++){
							if(insStatus[i] !='RUNNING'){
								hasException = true;
							}
						}
						if(hasException){
							this.detailTimer=setTimeout(function(){
								me.updateStatus(appName,win);
							},3000);
						}
						else{
							this.detailTimer=setTimeout(function(){
								me.updateStatus(appName,win);
							},3000);
						}
						},
					failure:function(){
						this.detailTimer=setTimeout(function(){
							me.updateStatus(appName,win);
						},5000);
						}
					}

				);
			},
			failure : function(response) {
				if(response.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
			    
			    if(response.timedout){  
			        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
			        return;
			    }
				var obj = Ext.JSON.decode(response.responseText);
				if(obj.data!=null&&obj.data.source==1){
					Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
				}else{
					Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.AppExpandFailed );
				}	
			}
		});
	},
	updateData : function(me,w,h,eOpts){
		var scop = this;
		var win = me.up('appListDetailsView');
		console.log(win);
		var name = me.up('appListDetailsView').appName;
		this.detailTimer=setTimeout(function(){
			scop.updateStatus.call(scop,name,win);
		},3000);
	},
	updateStatus : function(appName,win,view){
		if(win.isDestroyed){
			return;
		}
		var me = this;
		Ext.Ajax.request({
			url : 'rest/app/details',
			method : 'GET',
			params : {
				appName : appName
			},
			success:function(response){
				var obj = Ext.JSON.decode(response.responseText);
//				console.log(obj);
//				console.log(win);
				if(win.down('cfPanelTypeI[itemId=appIns]')==null){
					setTimeout(function(){
						me.updateStatus.call(me,appName,win);
					},10000);
					return;
				}
				win.down('cfPanelTypeI[itemId=appIns]').down('switchContentView').setValue(obj.instStatus);
				var sData = {};
				sData.status=obj.status;
				sData.instanceStatus = obj.instanceStatus;
				sData.instance=obj.instanceStatus.CRASHED+obj.instanceStatus.STARTING+obj.instanceStatus.RUNNING+obj.instanceStatus.DOWN+obj.instanceStatus.FLAPPING+obj.instanceStatus.UNKNOWN;
				win.down('appHeadView').updateAppStatus(sData);
				//win.down("#instanceSlider").setValue(sData.instance);
			    //win.down("#memorySlider").setValue(obj.memory);
				if(view){
					view.commitChange();
				}
				var insStatus = obj.instStatus;
				var hasException = false;
				var lth = insStatus.length;
				for(var i = 0;i<lth;i++){
					if(insStatus[i] !='RUNNING'){
						hasException = true;
						break;
					}
				}
				if(hasException){
					setTimeout(function(){
						me.updateStatus.call(me,appName,win);
					},6000);
				}
				else{
					setTimeout(function(){
						me.updateStatus.call(me,appName,win);
					},10000);
				}
				
			},
			failure:function(response){
				if(response.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
				setTimeout(function(){
					me.updateStatus.call(me,appName,win);
				},6000);
			}
		 });
	},
	instanceChange:function(cfSlider){
		var win=cfSlider.up('appListDetailsView');
		var appName=win.appName;
		Ext.Ajax.request({
			url : 'rest/app/updateInstNum',
			method : 'GET',
			params :{
				appName : appName,
				instanceNum : cfSlider.getValue(),
			},
			success : function(resp){
				Ext.Ajax.request({
					url : 'rest/app/details',
					method : 'GET',
					params : {
						appName : appName
					},
					success:function(response){
						var obj = Ext.JSON.decode(response.responseText);
						win.down('cfPanelTypeI[itemId=appIns]').down('switchContentView').setValue(obj.instStatus);
						win.down('appHeadView').updateAppStatus(obj);
						},
					failure:function(){
						console.log('change failed');
						}
					}

				);
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.AppExpandSuccess);
			},
			failure : function(resp){
				if(resp.status=="401"){
			    	Ext.Msg.show({
			    	    title: cfWebLang.Util.Tip,
			    	    id:'firstWin',
			    	    msg: cfWebLang.Main.SessionOverdue,
			    	    width: 250,
			    	    buttons: Ext.MessageBox.OK,
			    	    fn: function(){  
			                parent.location.href = '/zwycf/login.html';    
			            },
			            cls:'overtimeWin'
			    	});
			    	//Ext.WindowManager.bringToFront ('firstWin');
			       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){  
			            parent.location.href = '/zwycf/login.html';    
			        }); */
			       return;
			    }
			    
			    if(resp.timedout){  
			        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);  
			        return;
			    }
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.AppExpandFailed );
			}
		});
	},
	memeoryChange:function(cfSlider){
		var win=cfSlider.up('appListDetailsView');
		var appName=win.appName;
		Ext.Ajax.request({
			url : 'rest/app/updateMemoryNum',
			method : 'GET',
			params :{
				appName : appName,
				memoryNum : cfSlider.getValue(),
			},
			success : function(resp){
				Ext.Ajax.request({
					url : 'rest/app/details',
					method : 'GET',
					params : {
						appName : appName
					},
					success:function(response){
						var obj = Ext.JSON.decode(response.responseText);
						win.down('appHeadView').updateAppStatus(obj);
						},
					failure:function(){
						console.log('change failed');
						}
					}

				);
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.MemoryExpandSuccess);
			},
			failure : function(resp){
				Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Util.MemoryExpandFailed );
			}
		});

//		);
	}
});

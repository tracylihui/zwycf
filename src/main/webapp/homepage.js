var dashData = false;
Ext.Ajax.timeout = 300000;
var updateDash = function () {
    dashData = false;
    Ext.Ajax.request({
        url: 'rest/info/allBasicInfo',
        method: 'GET',
        success: function (resp) {
            dashData = Ext.JSON.decode(resp.responseText);
        }
    });
};

updateDash();
Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'cfWeb',
    appFolder: 'cfWeb',
    launch: function () {

        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            style: 'padding:0px',
            items: [{
                xtype : 'container',
                layout : 'border',
                items : [{
                    xtype : 'homePageNavView',
                    height : 56,
                    title : cfWebLang.NavView.smartcitycloud,
                    region : 'north',
                    menu : [],
                    cls : 'navView',
                    store : Ext.create('Ext.data.Store',{
                        autoLoad : true,
                        sortOnLoad : true,
                        fields : ['navName', 'navTarget'],
                        data : [{
                            navName:cfWebLang.HomeTabView.homepage,
                            navTarget:'index'
                        }]
                    })
                },{
                    xtype : 'mainView',
                    region : 'center',
                    cls : 'mainView'
                }
                ]
            }]
        });
    },
    controllers: ['util.NavController',
        'util.MenuController',
        'util.MainController',
        'util.HomeTabController'
    ]
});

var dashData = false;
Ext.Ajax.timeout =300000;
var updateDash = function(){
	dashData = false;
	Ext.Ajax.request({
		url : 'rest/info/basicInfo',
		method : 'GET',
		success : function(resp){
			dashData = Ext.JSON.decode(resp.responseText);
		}
	});
};

updateDash();
Ext.application({
	requires : ['Ext.container.Viewport'],
	name : 'cfWeb',
	appFolder : 'cfWeb',
	launch : function() {

		Ext.create('Ext.container.Viewport', {
			layout : 'fit',
			style : 'padding:0px',
			items : [{
				xtype : 'container',
				layout : 'border',
				items : [{
					xtype : 'navView',
					height : 56,
					title : cfWebLang.NavView.smartcitycloud,
					region : 'north',
					menu : [
					        {
					        	id:'setting',
					        	name:cfWebLang.NavView.setting
					        },{
					        	id:'message',
					        	name:cfWebLang.NavView.message
					        },{
					        	id:'accountSetting',
					        	name:cfWebLang.NavView.accountSetting
					        },{
					        	id:'exit',
					        	name:cfWebLang.NavView.exit
					        }
					        ],
					cls : 'navView',
					store : Ext.create('Ext.data.Store',{
						autoLoad : true,
						sortOnLoad : true,
						fields : ['navName', 'navTarget'],
						data : [{
									navName:cfWebLang.HomeTabView.homepage,
									navTarget:'index'
								},{
									navName:cfWebLang.HomeTabView.appManage,
									navTarget:'appManage'
								},
								{
									navName:cfWebLang.HomeTabView.dataStorageService,
									navTarget:'dataStorageService'
								}
								
								/*,{
									navName:cfWebLang.HomeTabView.dataAnalyseService,
									navTarget:'dataAnalyseService'
								},{
									navName:cfWebLang.HomeTabView.appService,
									navTarget:'appService'
								}*/

								/*,{
									navName:cfWebLang.HomeTabView.app,
									navTarget:'app'
								}*/]
					})
				},/*{
					xtype : 'menuView',
					region : 'west',
					collapsible : true,
					collapsed : true,
					width : '200',
					cls : 'menuView',
					title : cfWebLang.MenuView.Help,
					store : Ext.create('Ext.data.Store',{
						autoLoad : true,
						fields : ['itemName', 'itemAction', 'children'],
						data : [{
							itemName : cfWebLang.MenuView.appManage,
							itemAction : "",
							children : [{
								itemName : cfWebLang.MenuView.groupManage,
								itemAction : ""
							},{
								itemName : cfWebLang.MenuView.spaceManage,
								itemAction : ""
							},{
								itemName : cfWebLang.MenuView.domainManage,
								itemAction : ""
							},{
								itemName : cfWebLang.MenuView.userManage,
								itemAction : ""
							}]
						},{
							itemName : cfWebLang.MenuView.cloudManage,
							itemAction : ""
						}]
					})
				},*/{
					xtype : 'mainView',
					region : 'center',
					cls : 'mainView'
				}
				]
			}]
		});
	},
	controllers : ['util.NavController',
	               'util.MenuController',
	               'util.MainController',
	               'util.HomeTabController',
	               'appManage.AppManageController',
	               
	               //新添加allServices.AllServicesController
	               'allServices.AllServicesController',
	               
	               'dataStorage.DataStorageController',
	               'dataAnalyse.DataAnalyseController',
	               'appService.AppServiceController',
	               'app.AppController',
	               'accountSetting.AccountSettingController',
	               'log.LogController',
	               'appManage.ThresholdController',
	               'util.MessageController',//用户消息
	               'accountSetting.ChangePasswdController',
	               'accountSetting.QuotaManageController'//用户设置
	]
});
Ext.Ajax.on('requestexception',function(conn,response,options) {
    if(response.status=="401"){
    	Ext.Msg.show({
    	    title: cfWebLang.Util.Tip,
    	    id:'firstWin',
    	    msg: cfWebLang.Main.SessionOverdue,
    	    width: 250,
    	    buttons: Ext.MessageBox.OK,
    	    fn: function(){
                parent.location.href = './login.html';
            },
            cls:'overtimeWin'
    	});
    	//Ext.WindowManager.bringToFront ('firstWin');
       /* Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.Main.SessionOverdue, function(){
            parent.location.href = '/zwycf/login.html';
        }); */
       return;
    }

    if(response.timedout){
        Ext.Msg.alert(cfWebLang.Util.Tip,cfWebLang.global.timeout);
        return;
    }

    var error= Ext.JSON.decode(response.responseText);
	if(error.data!=null&&error.data.source==1){
		Ext.MessageBox.alert(cfWebLang.ErrorSource.cloudfoundry,error.data.message);
	}else{
		Ext.MessageBox.alert(cfWebLang.ErrorSource.app,cfWebLang.Error.SystemError);
	}
});

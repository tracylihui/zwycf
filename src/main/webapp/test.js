var DEBUG = location.protocol === 'file:';
var BASEURL = 'http:/10.82.80.3:8090/zwycf/';

Ext.util.Observable.observe(Ext.data.Connection, {
  beforerequest: function(conn, options, eOpts) {
    if (DEBUG) {
      options.url = BASEURL + options.url;
      options.cors = true;
      options.useDefaultXhrHeader = false;
      options.headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      };
      options.disableCaching = true;
      console.log(options);
    }
  }
});

/*
This file is part of Ext JS 4.2

Copyright (c) 2011-2013 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as
published by the Free Software Foundation and appearing in the file LICENSE included in the
packaging of this file.

Please review the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department
at http://www.sencha.com/contact.

Build date: 2013-05-16 14:36:50 (f9be68accb407158ba2b1be2c226a6ce1f649314)
*/
/**
 * List compiled by mystix on the extjs.com forums.
 * Thank you Mystix!
 *
 * English Translations
 * updated to 2.2 by Condor (8 Aug 2008)
 */
Ext.onReady(function() {

    if (Ext.data && Ext.data.Types) {
        Ext.data.Types.stripRe = /[\$,%]/g;
    }

    if (Ext.Date) {
        Ext.Date.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        Ext.Date.getShortMonthName = function(month) {
            return Ext.Date.monthNames[month].substring(0, 3);
        };

        Ext.Date.monthNumbers = {
            Jan: 0,
            Feb: 1,
            Mar: 2,
            Apr: 3,
            May: 4,
            Jun: 5,
            Jul: 6,
            Aug: 7,
            Sep: 8,
            Oct: 9,
            Nov: 10,
            Dec: 11
        };

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
        };

        Ext.Date.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        Ext.Date.getShortDayName = function(day) {
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.parseCodes.S.s = "(?:st|nd|rd|th)";
    }
    
    if (Ext.util && Ext.util.Format) {
        Ext.apply(Ext.util.Format, {
            thousandSeparator: ',',
            decimalSeparator: '.',
            currencySign: '$',
            dateFormat: 'm/d/Y'
        });
    }
});

Ext.define("Ext.locale.en.view.View", {
    override: "Ext.view.View",
    emptyText: ""
});

Ext.define("Ext.locale.en.grid.plugin.DragDrop", {
    override: "Ext.grid.plugin.DragDrop",
    dragText: "{0} selected row{1}"
});

// changing the msg text below will affect the LoadMask
Ext.define("Ext.locale.en.view.AbstractView", {
    override: "Ext.view.AbstractView",
    loadingText: "Loading..."
});

Ext.define("Ext.locale.en.picker.Date", {
    override: "Ext.picker.Date",
    todayText: "Today",
    minText: "This date is before the minimum date",
    maxText: "This date is after the maximum date",
    disabledDaysText: "",
    disabledDatesText: "",
    nextText: 'Next Month (Control+Right)',
    prevText: 'Previous Month (Control+Left)',
    monthYearText: 'Choose a month (Control+Up/Down to move years)',
    todayTip: "{0} (Spacebar)",
    format: "m/d/y",
    startDay: 0
});

Ext.define("Ext.locale.en.picker.Month", {
    override: "Ext.picker.Month",
    okText: "&#160;OK&#160;",
    cancelText: "Cancel"
});

Ext.define("Ext.locale.en.toolbar.Paging", {
    override: "Ext.PagingToolbar",
    beforePageText: "Page",
    afterPageText: "of {0}",
    firstText: "First Page",
    prevText: "Previous Page",
    nextText: "Next Page",
    lastText: "Last Page",
    refreshText: "Refresh",
    displayMsg: "Displaying {0} - {1} of {2}",
    emptyMsg: 'No data to display'
});

Ext.define("Ext.locale.en.form.Basic", {
    override: "Ext.form.Basic",
    waitTitle: "Please Wait..."
});

Ext.define("Ext.locale.en.form.field.Base", {
    override: "Ext.form.field.Base",
    invalidText: "The value in this field is invalid"
});

Ext.define("Ext.locale.en.form.field.Text", {
    override: "Ext.form.field.Text",
    minLengthText: "The minimum length for this field is {0}",
    maxLengthText: "The maximum length for this field is {0}",
    blankText: "This field is required",
    regexText: "",
    emptyText: null
});

Ext.define("Ext.locale.en.form.field.Number", {
    override: "Ext.form.field.Number",
    decimalSeparator: ".",
    decimalPrecision: 2,
    minText: "The minimum value for this field is {0}",
    maxText: "The maximum value for this field is {0}",
    nanText: "{0} is not a valid number"
});

Ext.define("Ext.locale.en.form.field.Date", {
    override: "Ext.form.field.Date",
    disabledDaysText: "Disabled",
    disabledDatesText: "Disabled",
    minText: "The date in this field must be after {0}",
    maxText: "The date in this field must be before {0}",
    invalidText: "{0} is not a valid date - it must be in the format {1}",
    format: "m/d/y",
    altFormats: "m/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d"
});

Ext.define("Ext.locale.en.form.field.ComboBox", {
    override: "Ext.form.field.ComboBox",
    valueNotFoundText: undefined
}, function() {
    Ext.apply(Ext.form.field.ComboBox.prototype.defaultListConfig, {
        loadingText: "Loading..."
    });
});

Ext.define("Ext.locale.en.form.field.VTypes", {
    override: "Ext.form.field.VTypes",
    emailText: 'This field should be an e-mail address in the format "user@example.com"',
    urlText: 'This field should be a URL in the format "http:/' + '/www.example.com"',
    alphaText: 'This field should only contain letters and _',
    alphanumText: 'This field should only contain letters, numbers and _'
});

Ext.define("Ext.locale.en.form.field.HtmlEditor", {
    override: "Ext.form.field.HtmlEditor",
    createLinkText: 'Please enter the URL for the link:'
}, function() {
    Ext.apply(Ext.form.field.HtmlEditor.prototype, {
        buttonTips: {
            bold: {
                title: 'Bold (Ctrl+B)',
                text: 'Make the selected text bold.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            italic: {
                title: 'Italic (Ctrl+I)',
                text: 'Make the selected text italic.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            underline: {
                title: 'Underline (Ctrl+U)',
                text: 'Underline the selected text.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            increasefontsize: {
                title: 'Grow Text',
                text: 'Increase the font size.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            decreasefontsize: {
                title: 'Shrink Text',
                text: 'Decrease the font size.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            backcolor: {
                title: 'Text Highlight Color',
                text: 'Change the background color of the selected text.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            forecolor: {
                title: 'Font Color',
                text: 'Change the color of the selected text.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyleft: {
                title: 'Align Text Left',
                text: 'Align text to the left.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifycenter: {
                title: 'Center Text',
                text: 'Center text in the editor.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyright: {
                title: 'Align Text Right',
                text: 'Align text to the right.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertunorderedlist: {
                title: 'Bullet List',
                text: 'Start a bulleted list.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertorderedlist: {
                title: 'Numbered List',
                text: 'Start a numbered list.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            createlink: {
                title: 'Hyperlink',
                text: 'Make the selected text a hyperlink.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            sourceedit: {
                title: 'Source Edit',
                text: 'Switch to source editing mode.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            }
        }
    });
});

Ext.define("Ext.locale.en.grid.header.Container", {
    override: "Ext.grid.header.Container",
    sortAscText: "Sort Ascending",
    sortDescText: "Sort Descending",
    columnsText: "Columns"
});

Ext.define("Ext.locale.en.grid.GroupingFeature", {
    override: "Ext.grid.GroupingFeature",
    emptyGroupText: '(None)',
    groupByText: 'Group By This Field',
    showGroupsText: 'Show in Groups'
});

Ext.define("Ext.locale.en.grid.PropertyColumnModel", {
    override: "Ext.grid.PropertyColumnModel",
    nameText: "Name",
    valueText: "Value",
    dateFormat: "m/j/Y",
    trueText: "true",
    falseText: "false"
});

Ext.define("Ext.locale.en.grid.BooleanColumn", {
    override: "Ext.grid.BooleanColumn",
    trueText: "true",
    falseText: "false",
    undefinedText: '&#160;'
});

Ext.define("Ext.locale.en.grid.NumberColumn", {
    override: "Ext.grid.NumberColumn",
    format: '0,000.00'
});

Ext.define("Ext.locale.en.grid.DateColumn", {
    override: "Ext.grid.DateColumn",
    format: 'm/d/Y'
});

Ext.define("Ext.locale.en.form.field.Time", {
    override: "Ext.form.field.Time",
    minText: "The time in this field must be equal to or after {0}",
    maxText: "The time in this field must be equal to or before {0}",
    invalidText: "{0} is not a valid time",
    format: "g:i A",
    altFormats: "g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"
});

Ext.define("Ext.locale.en.form.CheckboxGroup", {
    override: "Ext.form.CheckboxGroup",
    blankText: "You must select at least one item in this group"
});

Ext.define("Ext.locale.en.form.RadioGroup", {
    override: "Ext.form.RadioGroup",
    blankText: "You must select one item in this group"
});

Ext.define("Ext.locale.en.window.MessageBox", {
    override: "Ext.window.MessageBox",
    buttonText: {
        ok: "OK",
        cancel: "Cancel",
        yes: "Yes",
        no: "No"
    }    
});

// This is needed until we can refactor all of the locales into individual files
Ext.define("Ext.locale.en.Component", {	
    override: "Ext.Component"
});


		var cfWebLang={};
		cfWebLang.NavView={};
		cfWebLang.MenuView={};
		cfWebLang.Main={};
		cfWebLang.global={};
		cfWebLang.CollapsibleBlock={};
		cfWebLang.HomeTabView={};
		cfWebLang.CfWindow={};
		cfWebLang.Util={};
		cfWebLang.AppManageView={};
		cfWebLang.AppServiceView={};
		cfWebLang.DataAnalyseView={};
		cfWebLang.DataStorageView={};
		cfWebLang.SettingWindow={};
		cfWebLang.LogFileWindow={};
		cfWebLang.Service={};
		cfWebLang.Bug = {};
		cfWebLang.ErrorSource={};
		cfWebLang.Error={};
		cfWebLang.QuotaManage={};
		cfWebLang.permission={};
		cfWebLang.buildpackWindow={};
		
		cfWebLang.permission.SpaceManager='SpaceManager';
		cfWebLang.permission.SpaceDeveloper='SpaceDeveloper';
		cfWebLang.permission.SpaceAuditor='SpaceAuditor';
		
		
		cfWebLang.permission.OrgManager='OrgManager';
		cfWebLang.permission.BillingManager='BillingManager';
		cfWebLang.permission.OrgAuditor='OrgAuditor';
		
		
		cfWebLang.global.timeout="Request Time Out";
		cfWebLang.Main.SessionOverdue="Session is timeout!Please login again!";
		
		cfWebLang.MenuView.appManage="appManage";
		cfWebLang.MenuView.groupManage="groupManage";
		cfWebLang.MenuView.spaceManage="spaceManage";
		cfWebLang.MenuView.domainManage="domainManage";
		cfWebLang.MenuView.userManage="userManage";
		cfWebLang.MenuView.cloudManage="cloudManage";
		cfWebLang.MenuView.Help="Help";
		
		cfWebLang.NavView.setting="Setting";
		cfWebLang.NavView.message="Notification";
		cfWebLang.NavView.accountSetting="accountsetting";
		cfWebLang.NavView.smartcitycloud="CCI-PaaS";
		cfWebLang.NavView.exit="Exit";
		
		cfWebLang.HomeTabView.appUsage="Domain Name Usage";
		cfWebLang.HomeTabView.serviceUsage="Service Usage";
		cfWebLang.HomeTabView.appState="App State";
		cfWebLang.HomeTabView.memoryUsage="Memory Usage";
		cfWebLang.HomeTabView.dashboard="Dashboard";
		cfWebLang.HomeTabView.run="run";
		cfWebLang.HomeTabView.stop="stop";
		cfWebLang.HomeTabView.update="update";
		cfWebLang.HomeTabView.used="use";
		cfWebLang.HomeTabView.unused="unuse";
		cfWebLang.HomeTabView.RuntimeEnvironment="App Runtime Environment";
		cfWebLang.HomeTabView.Middleware="Middleware Services";
		cfWebLang.HomeTabView.homepage="Home Page";
		cfWebLang.HomeTabView.appManage="App Manager";

        //添加服务
        cfWebLang.HomeTabView.allService="all Services";


		cfWebLang.HomeTabView.dataStorageService="Storage Services";
		cfWebLang.HomeTabView.dataAnalyseService="Analytics Services";
		cfWebLang.HomeTabView.appService="App Services";
		cfWebLang.HomeTabView.app="App Store";
		cfWebLang.HomeTabView.loadingMsg="loading...";
		cfWebLang.HomeTabView.unit="";
        cfWebLang.HomeTabView.platformState="Platform State";
        cfWebLang.HomeTabView.joinedOrg="Joined Organizations";
        cfWebLang.HomeTabView.joined="Joined";
        cfWebLang.HomeTabView.unjoined="Not Joined";
        cfWebLang.HomeTabView.functionIntro="Function Introduction";
        cfWebLang.HomeTabView.serviceIntro="Service Introduction";

		cfWebLang.CollapsibleBlock.search="Please input the keyword";
		
		cfWebLang.CfWindow.newAppTemplateName="Template Name";
		cfWebLang.CfWindow.newAppMemSize="Memory";
		cfWebLang.CfWindow.newAppinstSize="Instances"
		cfWebLang.CfWindow.newAppRunEnv="Runtime Environment";
		cfWebLang.CfWindow.newAppLoaction="Location";
		cfWebLang.CfWindow.newAppAppName="App Name";
		cfWebLang.CfWindow.newAppUploadFile="Upload File";
		cfWebLang.CfWindow.newAppUploadLocalFile="Upload Local File";
		cfWebLang.CfWindow.newAppUploadAppTemp="Upload Example";
		cfWebLang.CfWindow.PatternSetting = "Pattern Setting"
		cfWebLang.CfWindow.PatternName = "Pattern Name";
		cfWebLang.CfWindow.PatternContent = "Pattern Content";
		cfWebLang.CfWindow.PatternTest = "Pattern Test";
		cfWebLang.CfWindow.InputUserName = "Input app name";
		cfWebLang.CfWindow.UsernameEmpty = "*Illegal Input";
		cfWebLang.CfWindow.UsernameExisted = "App name exists";
		cfWebLang.CfWindow.InputAgain = "Input Again";
		cfWebLang.CfWindow.InputFilename = "Input Filename";
		cfWebLang.CfWindow.AppCreated = "App isn't created!";
		cfWebLang.CfWindow.AppUpload = "App created! Upload failed!";
		cfWebLang.CfWindow.AppBinded = "Upload successfully! Bind failed!";
		cfWebLang.CfWindow.AppStart = "Bind successfully! Start failed!";
		cfWebLang.CfWindow.CreatedSuccess = "Created App successfully!";
		cfWebLang.CfWindow.EditTemplateSuccess = "Edit successfully!"
		cfWebLang.CfWindow.EditTemplateFailed = "Edit Failed!";
		cfWebLang.CfWindow.UnCompletedContext = "Uncompleted Context";
		cfWebLang.CfWindow.CreateTemplateSuccess = "Create template successfully!";
		cfWebLang.CfWindow.CreateTemplateFailed = "Create template failed!";
		cfWebLang.CfWindow.ChoseBuildpack = "Choose buildpack";
		cfWebLang.CfWindow.StartTimeEndTime = "The Start time must smaller than the End time!"
		cfWebLang.CfWindow.UploadFileTip = "Choose a file to upload";
		cfWebLang.CfWindow.DownAppTemplate = "See/Download the app of template";
		cfWebLang.CfWindow.ChooseBindService = "Please choose the service you want bind";
		cfWebLang.CfWindow.BindServiceSuccess = 'Bind successfully!';
		cfWebLang.CfWindow.inBindService  ="Binding...";
		cfWebLang.CfWindow.patternExist = " already exists";
		cfWebLang.CfWindow.addDbSource = "Please add database!"
		cfWebLang.CfWindow.memoryEmp = "*Can't be empty";
		cfWebLang.CfWindow.temNameEmp= "*Can't be empty";
		cfWebLang.CfWindow.temNameExit = "*Already exists";
		cfWebLang.CfWindow.domainNameEmp = "*Can't be empty";
		cfWebLang.CfWindow.afterBindSuccess = "Bind successfully!Please restart app to affect";
		cfWebLang.CfWindow.afterunBindSuccess = "Unbind successfully!Please restart app to affect";
		
		cfWebLang.buildpackWindow.nameEmp="*BuildpackName cant not be empty";
		cfWebLang.buildpackWindow.fileEmp="*Please select a file";
		
		cfWebLang.Util.Next="Next";
		cfWebLang.Util.Prev="Previous";
		cfWebLang.Util.Cancel="Cancel";
		cfWebLang.Util.Add = "Add";
		cfWebLang.Util.Yes="Yes";
		cfWebLang.Util.No="No";
		cfWebLang.Util.Finish="Finish";
		cfWebLang.Util.Tip="Tip";
		cfWebLang.Util.Name="Name";
		cfWebLang.Util.Edit="Edit";
		cfWebLang.Util.New="New";
		cfWebLang.Util.Restart= "Restart";
		cfWebLang.Util.Start = "Start";
		cfWebLang.Util.Stop = "Stop";
		cfWebLang.Util.Loading = "Loading";
		cfWebLang.Util.Save = "Save";
		cfWebLang.Util.Status = "Status";
		cfWebLang.Util.Filter="Filter";
		cfWebLang.Util.Delete="Delete";
		cfWebLang.Util.CreateApp = "Creating...";
		cfWebLang.Util.uploading = "Uploading...";
		cfWebLang.Util.newAppSuccess="Create app successfully！";
		cfWebLang.Util.CreateSuccess = "Create successfully!";
		cfWebLang.Util.CreateFailed = "Create Failed!";
		cfWebLang.Util.ChooseBindService = "Please choose the service you want to bind!";
		cfWebLang.Util.UserAuth = "Authorization";
		cfWebLang.Util.Detail = "Detail";
		cfWebLang.Util.Update = "Update";
		cfWebLang.Util.Operate = "Action";
		cfWebLang.Util.Confirm = "Confirm";
		cfWebLang.Util.Done = "Done";
		cfWebLang.Util.UserName="User Name";
		cfWebLang.Util.Password="Password";
		cfWebLang.Util.PleaseChoose = "Choose Chart Type";
		cfWebLang.Util.PleaseInput = "Input Chart Title";
		cfWebLang.Util.Id="Id";
		cfWebLang.Util.Today="Today";
		cfWebLang.Util.Yesterday = "Yesterday";
		cfWebLang.Util.ThisWeek = "Week";
		cfWebLang.Util.To = "End Date";
		cfWebLang.Util.SelectDate = "Select Date";
		cfWebLang.Util.Large = "Large";
		cfWebLang.Util.Medium = "Medium";
		cfWebLang.Util.Small = "Small";
		cfWebLang.Util.Memory = "Memory";
		cfWebLang.Util.Disk = "Disk";
		cfWebLang.Util.Setting = "Setting";
		cfWebLang.Util.FieldRequired = "Field Required";
		cfWebLang.Util.Message = "Notification";
		cfWebLang.Util.Config = "Configure";
		cfWebLang.Util.SpaceUnexit = "This org has not any space.Please establish a space!";
		cfWebLang.Util.OrgUnexit = "please target org and space!";
		cfWebLang.Util.ServiceInstance = "ServiceInstance";
		
		
		cfWebLang.Util.FileName = "File Name";
		cfWebLang.Util.Download = "Download";
		cfWebLang.Util.Space = "Space";
		cfWebLang.Util.Organization = "Org";
		cfWebLang.Util.Error = "Error";
		cfWebLang.Util.Domain = "Domain";
		cfWebLang.Util.Submit = "Submit";
		cfWebLang.Util.Success = "Success";
		cfWebLang.Util.failed= "Failed";
		cfWebLang.Util.ConfirmWindow = "Confirm";
		cfWebLang.Util.DeleteConfirm = "Delete?<br>";
		cfWebLang.Util.NewApp = "Create App";
		cfWebLang.Util.Unbunding = "Unbind?<br>";
		cfWebLang.Util.RestartBrowse = "Restart browser and retry";
		cfWebLang.Util.UnLogginUser = "UnLogginUser";
		cfWebLang.Util.MemoryExpandSuccess = "Memory expand successfully!";
		cfWebLang.Util.MemoryExpandFailed = "Memory expand failed!";
		cfWebLang.Util.AppExpandSuccess = "App expand successfully!";
		cfWebLang.Util.AppExpandFailed = "App expand failed!";
		cfWebLang.Util.AddSuccess = "Add successfully!";
		cfWebLang.Util.AddFailed = "Add failed!";
		cfWebLang.Util.DeleteSuccess = "Delete successfully!";
		cfWebLang.Util.DeleteFailed = "Delete failed!";
		cfWebLang.Util.OperateSuccess = "Operate successfully!";
		cfWebLang.Util.OperateFailed = "Operate failed!";
		cfWebLang.Util.ChangeSuccess = "Change successfully!";
		cfWebLang.Util.SaveSuccess = "Save successfully!";
		cfWebLang.Util.Log = "Log";
		cfWebLang.Util.bindService = "Bind Service";
		cfWebLang.Util.appName ="App Name";
		cfWebLang.Util.exceptionCount = "Exception Count";
		cfWebLang.Util.ErrorCount = "Error Count";
		cfWebLang.Util.details = "Details";
		cfWebLang.Util.CreateDB = "Create Database";
		cfWebLang.Util.ChoosePlan = "choose a plan";
		cfWebLang.Util.ChooseService = "choose a service";
		cfWebLang.Util.ServiceName = "Service Name";
		cfWebLang.Util.ServiceBrokers = 'Service Brokers';
		cfWebLang.Util.Middleware = "Middleware Services";
		cfWebLang.Util.back = "back";
		cfWebLang.Util.message = "Message";
		cfWebLang.Util.UploadAuth = "There isn't authority to upload an app";
		
		cfWebLang.AppServiceView.AppServiceList="Services";
		cfWebLang.AppServiceView.AppServiceTemplate="App Services";
		
		cfWebLang.AppManageView.AppUpdate = "AppSorce Update";
		
        //添加系统模板
        cfWebLang.AppManageView.SysAppTemplate = "SysApp Template";

        cfWebLang.AppManageView.MyAppTemplate = "App Template";

		cfWebLang.AppManageView.AppList= "Applications";
		cfWebLang.AppManageView.EnvironmentVar = "Environment Variables";
		cfWebLang.AppManageView.EnvVarName = "VarName";
		cfWebLang.AppManageView.EnvVarValue = "VarValue";
		cfWebLang.AppManageView.ServiceBindManage = "Service Binding";
		cfWebLang.AppManageView.AppInstance = "Application Instances";
		cfWebLang.AppManageView.Version = "Version";
		cfWebLang.AppManageView.Plan = "Plan";
		cfWebLang.AppManageView.ServiceType='Service Type';
		cfWebLang.AppManageView.BindAppName = "App Name";
		cfWebLang.AppManageView.CreateTime = "Create Time";
		cfWebLang.AppManageView.UpdateTime = "Update Time";
		cfWebLang.AppManageView.DelayHistory ="Access Latency History";
		cfWebLang.AppManageView.VisitTime = "Access Frequency History";
		cfWebLang.AppManageView.VisitTime30m = 'Access Frequency History in last 30 minutes';
		cfWebLang.AppManageView.AppDistribution = "Application Access Latency";
		cfWebLang.AppManageView.RunStatus = "App Status";
		cfWebLang.AppManageView.InstanceNumber = "Instances";
		cfWebLang.AppManageView.MemDisk = "Memory/Disk";
		cfWebLang.AppManageView.Mem = "Memory";
		cfWebLang.AppManageView.Disk = "Disk";
		cfWebLang.AppManageView.RunEnv = "Runtime";
		cfWebLang.AppManageView.Service = "Services";
		cfWebLang.AppManageView.InstanceStatus = "Instance Status";
		cfWebLang.AppManageView.Domain = "Domain";
		cfWebLang.AppManageView.UserDefinedTemplate = "User Defined Template";
		cfWebLang.AppManageView.AppContainer = "Runtime Environment";
		cfWebLang.AppManageView.TemplateName = "Template Name";
		cfWebLang.AppManageView.SpaceName = "Space";
		cfWebLang.AppManageView.OrganizationName = "Org";
		cfWebLang.AppManageView.AppLog = "Log Download";
		cfWebLang.AppManageView.ChooseMemory = "Choose Memory";
		cfWebLang.AppManageView.ChooseDomain = "Choose Domain";
		cfWebLang.AppManageView.RunEnvs = "Runtime Environment";
		cfWebLang.AppServiceView.appService="AppService";
		cfWebLang.AppManageView.MappingDB = "Map DB of Cloud";
		cfWebLang.AppManageView.Auto = "Auto";
		cfWebLang.AppManageView.Manual = "Manual";
		cfWebLang.AppManageView.CreateOracle = "Create Oracle";
		cfWebLang.AppManageView.Create="Create  ";
		cfWebLang.AppManageView.AppDataSource = "App Source";
		cfWebLang.AppManageView.CloudSource = "Cloud Source";
		cfWebLang.AppManageView.InputDataSourceName = "Input App Source";
		cfWebLang.AppManageView.ChooseSource = "Choose Cloud Source";
		cfWebLang.AppManageView.Autoscaling  = "Auto-Scaling";
		cfWebLang.DataAnalyseView.dataAnalyse="Analytics Services";
		cfWebLang.AppManageView.bounding = "Binding...";
		cfWebLang.AppManageView.greyScaleUpdate = "Greyscale Update";
		cfWebLang.AppManageView.greyScaleUpdateTitle = "Greyscale Updating";
		cfWebLang.AppManageView.MidwareType = "Service Type";
		
		cfWebLang.LogFileWindow.ChartSetting = "Chart Setting";
		cfWebLang.LogFileWindow.ParameterSetting = "Parameter Setting";
		cfWebLang.LogFileWindow.StyleSetting = "Style Setting";
		cfWebLang.LogFileWindow.DataFilter = "Data Filter";
		cfWebLang.LogFileWindow.ChartExhibition ="ChartExhibition";
		cfWebLang.LogFileWindow.CheckLog = "Log Download";
		cfWebLang.LogFileWindow.ChooseDateFrom = "Date Start";
		cfWebLang.LogFileWindow.KeyWordSearch = "Keyword";
		cfWebLang.LogFileWindow.InputKeyWord = "Input the keyWord";
		cfWebLang.LogFileWindow.SearchButton = "Search";
		cfWebLang.LogFileWindow.Log = "Log Analyse";
		cfWebLang.LogFileWindow.LogAnalyse = "Log Analyse";
		cfWebLang.LogFileWindow.StatisticType = "Statis Type";
		cfWebLang.LogFileWindow.ChartTitle = "Chart Title";
		cfWebLang.LogFileWindow.ChartSize = "Chart Size";
		cfWebLang.LogFileWindow.ChangeChart = "Change chart";
		cfWebLang.LogFileWindow.FieldName = "Field Name";
		cfWebLang.LogFileWindow.IntervalValue = "Interval Value";
		cfWebLang.LogFileWindow.InputInterval = "Input Interval(eg.1.5h)";
		cfWebLang.LogFileWindow.ChartType = "Chart Type";
		cfWebLang.LogFileWindow.KeyWord = "KeyWord";
		cfWebLang.LogFileWindow.ProjectCensus = "The project will be censused";
		cfWebLang.LogFileWindow.GenerateChart = "Generate Chart";
		cfWebLang.LogFileWindow.SaveChart = "Save Chart";
		cfWebLang.LogFileWindow.SurveyStatic = "SurveyStatic";
		cfWebLang.LogFileWindow.KeyWordStatic = "KeywordStatic";
		cfWebLang.LogFileWindow.ChartStatic = "ChartStatic";
		cfWebLang.LogFileWindow.PieChart = "PieChart";
		cfWebLang.LogFileWindow.ColumnbarChart = "ColumnbarChart";
		cfWebLang.LogFileWindow.LineChart = "LineChart";
		cfWebLang.LogFileWindow.ReturnPrev = "Return LogAnalyse";
		cfWebLang.LogFileWindow.CountType = "Survey Static and Chart Static are numberic only";
		cfWebLang.LogFileWindow.typeEmp = "*Can't be empty for Statistics";
		cfWebLang.LogFileWindow.fieldEmp = "*Can't be empty for Field";
		cfWebLang.LogFileWindow.invalEmp = "*Can't be empty for Inval";
		cfWebLang.LogFileWindow.chartEmp = "*Can't be empty for ChartType";
		cfWebLang.LogFileWindow.noResult = "There's no any results";
		
		cfWebLang.QuotaManage.services="total_service";
		cfWebLang.QuotaManage.routes="total_route";
		cfWebLang.QuotaManage.memory="memory_limit";
		cfWebLang.QuotaManage.instancesmemory="instance_memeory_limit";
		cfWebLang.QuotaManage.createTitle='Create Quota';
		cfWebLang.QuotaManage.editTitle='Edit Quota';
		cfWebLang.QuotaManage.nameRepeat='*Name Repeat';
		cfWebLang.QuotaManage.changePassword='Change Password';
		cfWebLang.QuotaManage.oldPassword='Old Password';
		cfWebLang.QuotaManage.newPassword='New Password';
		cfWebLang.QuotaManage.confirmPassword='Confirm Password';
		
		cfWebLang.SettingWindow.QuotaSetting = "Bind Quota";
		cfWebLang.SettingWindow.OrgChoose = "Choose Org";
		cfWebLang.SettingWindow.SpaceChoose = "Choose Space";
		cfWebLang.SettingWindow.belongToOrg ="Organization";
		cfWebLang.SettingWindow.QuotaManage="Quota";
		cfWebLang.SettingWindow.OrgManage="Org";
		cfWebLang.SettingWindow.SpaceManage="Space";
		cfWebLang.SettingWindow.DomainManage="Domain";
		cfWebLang.SettingWindow.RouteManage="Route";
		cfWebLang.SettingWindow.BuildpackManage="Buildpack";
		cfWebLang.SettingWindow.UserManage="User";
		cfWebLang.SettingWindow.belongToCloud="Cloud";
		cfWebLang.SettingWindow.NewUser = "Create User";
		cfWebLang.SettingWindow.NewSpace = "Create Space";
		cfWebLang.SettingWindow.NewOrg = "Create Org";
		cfWebLang.SettingWindow.OrgName = "Org Name";
		cfWebLang.SettingWindow.SpaceName = "Space Name";
		cfWebLang.SettingWindow.NewDomain = "Create Domain";
		cfWebLang.SettingWindow.InputDomainName = "Domain Name";
		cfWebLang.SettingWindow.InputUserName = "User Name"
		cfWebLang.SettingWindow.InputPassword = "Password";
		cfWebLang.SettingWindow.ConfirmPassword = "Confirm";
		cfWebLang.SettingWindow.ChangeOrgName = "Edit Org";
		cfWebLang.SettingWindow.InputNewOrgName = "NewOrgName";
		cfWebLang.SettingWindow.ChangeSpaceName = "Edit Space";
		cfWebLang.SettingWindow.InputNewSpaceName = "NewSpaceName";
		cfWebLang.SettingWindow.AccountManage = "AccountManage";
		cfWebLang.SettingWindow.BindApp = "Bound App";
		cfWebLang.SettingWindow.AppList = "Applications";
		cfWebLang.SettingWindow.InputName = "Route Name";
		cfWebLang.SettingWindow.InputDomain = "Domain";
		cfWebLang.SettingWindow.NewRoute = "Create Route";
		cfWebLang.SettingWindow.CloudEmpty = "Can't be empty";
		cfWebLang.SettingWindow.CloudName = "Cloud Name";
		cfWebLang.SettingWindow.EmailName = "Email";
		cfWebLang.SettingWindow.EmailEmpty = "Can't be empty";
		cfWebLang.SettingWindow.WebsiteName = "Website";
		cfWebLang.SettingWindow.WebsiteEmpty = "Can't be empty";
		cfWebLang.SettingWindow.Password = "Password";
		cfWebLang.SettingWindow.PasswordEmpty = "Can't be empty";
		cfWebLang.SettingWindow.NewBuildpack = "Create Buildpack";
		cfWebLang.SettingWindow.InputBuildpackName = "Buildpack Name";
		cfWebLang.SettingWindow.RoleList = "Role List";
		cfWebLang.SettingWindow.ChoseRole = "Selected List";
		cfWebLang.SettingWindow.UserNameEmp = "*Illegal input";
		cfWebLang.SettingWindow.UserNameExit = "*User Name exits";
		cfWebLang.SettingWindow.DomainEmp = "*Can't be empty";
		cfWebLang.SettingWindow.DomainExit = "*Domain exits";
		cfWebLang.SettingWindow.DomainProperty = "Property";
		cfWebLang.SettingWindow.DomainInOrg = "Belong Org";
		cfWebLang.SettingWindow.selectApp = "Edit Route";
		cfWebLang.SettingWindow.RouteExit = "*Domain exits";
		cfWebLang.SettingWindow.OrgEmp = "*Can't be empty";
		cfWebLang.SettingWindow.SpaceEmp = "*Can't be empty";
		cfWebLang.SettingWindow.DomainFormat = "*Format:xx.xx";
		cfWebLang.SettingWindow.RouteEmp = "*Can't be empty";
		cfWebLang.SettingWindow.OrgList = "Org";
		cfWebLang.SettingWindow.SpaceList = "Space";
		cfWebLang.SettingWindow.PasswordComfirm = "*Do not match";
		cfWebLang.SettingWindow.PasswordError = "*Password Error";

        cfWebLang.AllServicesView={};

        //新添加所有服务下的：服务名称，服务列表
        cfWebLang.AllServicesView.ServiceName = "Service Name";
        cfWebLang.AllServicesView.ServiceList = "Service List";

		cfWebLang.DataStorageView.dataStorage="Storage Services";
		cfWebLang.DataStorageView.dataList="DataList";
		cfWebLang.DataStorageView.Version = "Version";
		cfWebLang.DataStorageView.DatabaseStatus = "DatabaseStatus";
		cfWebLang.DataStorageView.TableSpace = "TableSpace";
		cfWebLang.DataStorageView.TempTableSpace = "TempTableSpace";
		cfWebLang.DataStorageView.CreateDate = "CreateDate";
		
		cfWebLang.Service.Redis = "Redis";
		cfWebLang.Service.Memcachd = "Memcachd";
		cfWebLang.Service.GlusterFS = "GlusterFS";
		cfWebLang.Service.ESB = "ESB";
		cfWebLang.Service.mongoDB = "mongoDB";
		cfWebLang.Service.MySQL = "MySQL";
		cfWebLang.Service.MySqlCluster = "MySqlCluster";
		cfWebLang.Service.Oracle = "Oracle";
		cfWebLang.Service.postgreSQL = "postgreSQL";
		cfWebLang.Service.report = "Reporting";
		cfWebLang.Service.batchData = "Hadoop";
		cfWebLang.Service.flowData = "Spark";
		cfWebLang.Service.RabbitMQ = "RabbitMQ";
		cfWebLang.Service.logService = "Log Service";
		cfWebLang.Service.pushService = "Push Service";
		cfWebLang.Service.SSO = "SSO";
		cfWebLang.Service.CAS = "CAS";
		cfWebLang.Service.Email = "Email";
		cfWebLang.Service.dataExchange = "Data Exchange";
		cfWebLang.Service.appServiceQuery = "App Service Manage";
		cfWebLang.CfWindow.ServiceAlreadyBinded=" Already bound";
		cfWebLang.Service.Plan = "Service Plan";
		cfWebLang.Service.Label = "Service Label";
		cfWebLang.Service.Hostname = "HostName";
		cfWebLang.Service.Port = "Port";
		cfWebLang.Service.Username = "UserName";
		cfWebLang.Service.Password = "Password";

		cfWebLang.Bug.OrgNameExisted = "*Org exists";
		cfWebLang.Bug.SpaceNameExisted = "*Space cfWebLang.Error.DbEnvNameRepeat";
		cfWebLang.ErrorSource.app="System Error Tip";
		cfWebLang.ErrorSource.cloudfoundry="Cloudfoundry Information Tip";
		
		cfWebLang.Error.SystemError="System Error!";
		cfWebLang.Error.chooseDatabase="Missing DB";
		cfWebLang.Error.DatasourceName ="Missing data source name";
		cfWebLang.Error.Datasource = 'missing datasource';
		cfWebLang.Error.DuplicateDatasource  = "duplicate data source";
		cfWebLang.Error.DbEnvNameRepeat = "*Name existed";
		cfWebLang.Error.VarEnvNameRepeat = "*Environment Name existed";
		cfWebLang.Error.DuplicateDatasourceName = "*Duplicate name of data source";
		
		cfWebLang.Bug.binding = "Binding...";
		cfWebLang.Bug.unbinding = "Unbinding";

        cfWebLang.ServiceConfig = {};
        cfWebLang.ServiceConfig.StorageSize = "Size";
        cfWebLang.ServiceConfig.StorageSizeLarge = "Large";
        cfWebLang.ServiceConfig.StorageSizeMedium = "Medium";
        cfWebLang.ServiceConfig.StorageSizeSmall = "Small";
        cfWebLang.ServiceConfig.Create = "New";
        cfWebLang.ServiceConfig.Select = "Select";

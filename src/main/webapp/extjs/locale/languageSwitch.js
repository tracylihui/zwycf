function getCookie(c_language) {
  if (document.cookie.length > 0) {
    c_start = document.cookie.indexOf(c_language + "=");
    if (c_start != -1) {
      c_start = c_start + c_language.length + 1;
      c_end = document.cookie.indexOf(';', c_start);
      if (c_end == -1)
        c_end = document.cookie.length;
      return unescape(document.cookie.substring(c_start, c_end));
    }
  }
  return '';
}

function setCookie(c_language, value, expiredays) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + expiredays);
  document.cookie = c_language + "=" + escape(value) +
    ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
}

(function() {
  function loadScript(src) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    head.appendChild(script);
  }

  var language = (function () {
    var language = getCookie('language')
    if (language != null && language != "") {
      if (language == 'en') {
        //		setCookie('language','cn',365);
      } else {
        //		setCookie('language','en',365);
      }
    } else {
      language = 'cn';
      setCookie('language', language, 365);
    }
    return language;
  })();

  switch (language) {
    case 'en':
      loadScript("extjs/locale/ext-lang-en.js");
      loadScript("extjs/locale/cfWeb_lang_en.js");
      break;
    default:
      loadScript("extjs/locale/ext-lang-zh_CN.js");
      loadScript("extjs/locale/cfWeb_lang_cn.js");
  }
})();

Ext.onReady(function() {
	cfLang = {};
	cfLang.navList = [{
		navName:'首页',
		navTarget:'index'
	},{
		navName:'应用管理',
		navTarget:'appManage'
	},{
		navName:'服务',
		navTarget:'allServices'
	},{
		navName:'数据存储服务',
		navTarget:'dataStorageService'
	},{
		navName:'数据分析服务',
		navTarget:'dataAnalyseService'
	},{
		navName:'应用服务',
		navTarget:'appService'
	},{
		navName:'应用基础服务',
		navTarget:'app'
	}];
	cfLang.navTitle = '工商专有云';
});
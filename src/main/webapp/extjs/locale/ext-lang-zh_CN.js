/*
 * This file is part of Ext JS 4.2
 * 
 * Copyright (c) 2011-2013 Sencha Inc
 * 
 * Contact: http://www.sencha.com/contact
 * 
 * Commercial Usage Licensees holding valid commercial licenses may use this
 * file in accordance with the Commercial Software License Agreement provided
 * with the Software or, alternatively, in accordance with the terms contained
 * in a written agreement between you and Sencha.
 * 
 * If you are unsure which license is appropriate for your use, please contact
 * the sales department at http://www.sencha.com/contact.
 * 
 * Build date: 2013-03-11 22:33:40 (aed16176e68b5e8aa1433452b12805c0ad913836)
 */
/**
 * Simplified Chinese translation By DavidHu 09 April 2007
 * 
 * update by andy_ghg 2009-10-22 15:00:57
 */
Ext.onReady(function() {
	var cm = Ext.ClassManager, exists = Ext.Function.bind(cm.get, cm), parseCodes;

	if (Ext.Updater) {
		Ext.Updater.defaults.indicatorText = '<div class="loading-indicator">加载中...</div>';
	}

	if (Ext.Date) {
		Ext.Date.monthNames = ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月",
				"九月", "十月", "十一月", "十二月"];

		Ext.Date.dayNames = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];

		Ext.Date.formatCodes.a = "(this.getHours() < 12 ? '上午' : '下午')";
		Ext.Date.formatCodes.A = "(this.getHours() < 12 ? '上午' : '下午')";

		parseCodes = {
			g : 1,
			c : "if (/(上午)/i.test(results[{0}])) {\n"
					+ "if (!h || h == 12) { h = 0; }\n"
					+ "} else { if (!h || h < 12) { h = (h || 0) + 12; }}",
			s : "(上午|下午)",
			calcAtEnd : true
		};

		Ext.Date.parseCodes.a = Ext.Date.parseCodes.A = parseCodes;
	}

	if (Ext.MessageBox) {
		Ext.MessageBox.buttonText = {
			ok : "确定",
			cancel : "取消",
			yes : "是",
			no : "否"
		};
	}

	if (exists('Ext.util.Format')) {
		Ext.apply(Ext.util.Format, {
					thousandSeparator : ',',
					decimalSeparator : '.',
					currencySign : '\u00a5',
					// Chinese Yuan
					dateFormat : 'y年m月d日'
				});
	}

	if (exists('Ext.form.field.VTypes')) {
		Ext.apply(Ext.form.field.VTypes, {
					emailText : '该输入项必须是电子邮件地址，格式如： "user@example.com"',
					urlText : '该输入项必须是URL地址，格式如： "http:/' + '/www.example.com"',
					alphaText : '该输入项只能包含半角字母和_',
					// update
					alphanumText : '该输入项只能包含半角字母,数字和_' // update
				});
	}
});

Ext.define("Ext.locale.zh_CN.view.View", {
			override : "Ext.view.View",
			emptyText : ""
		});

Ext.define("Ext.locale.zh_CN.grid.plugin.DragDrop", {
			override : "Ext.grid.plugin.DragDrop",
			dragText : "选择了 {0} 行"
		});

Ext.define("Ext.locale.zh_CN.TabPanelItem", {
			override : "Ext.TabPanelItem",
			closeText : "关闭此标签"
		});

Ext.define("Ext.locale.zh_CN.form.field.Base", {
			override : "Ext.form.field.Base",
			invalidText : "输入值非法"
		});

// changing the msg text below will affect the LoadMask
Ext.define("Ext.locale.zh_CN.view.AbstractView", {
			override : "Ext.view.AbstractView",
			msg : "讀取中..."
		});

Ext.define("Ext.locale.zh_CN.picker.Date", {
			override : "Ext.picker.Date",
			todayText : "今天",
			minText : "日期必须大于最小允许日期",
			// update
			maxText : "日期必须小于最大允许日期",
			// update
			disabledDaysText : "",
			disabledDatesText : "",
			monthNames : Ext.Date.monthNames,
			dayNames : Ext.Date.dayNames,
			nextText : '下个月 (Ctrl+Right)',
			prevText : '上个月 (Ctrl+Left)',
			monthYearText : '选择一个月 (Control+Up/Down 来改变年份)',
			// update
			todayTip : "{0} (空格键选择)",
			format : "y年m月d日",
			ariaTitle : '{0}',
			ariaTitleDateFormat : 'Y\u5e74m\u6708d\u65e5',
			longDayFormat : 'Y\u5e74m\u6708d\u65e5',
			monthYearFormat : 'Y\u5e74m\u6708',
			getDayInitial : function(value) {
				// Grab the last character
				return value.substr(value.length - 1);
			}
		});

Ext.define("Ext.locale.zh_CN.picker.Month", {
			override : "Ext.picker.Month",
			okText : "确定",
			cancelText : "取消"
		});

Ext.define("Ext.locale.zh_CN.toolbar.Paging", {
			override : "Ext.PagingToolbar",
			beforePageText : "第",
			// update
			afterPageText : "页,共 {0} 页",
			// update
			firstText : "第一页",
			prevText : "上一页",
			// update
			nextText : "下一页",
			lastText : "最后页",
			refreshText : "刷新",
			displayMsg : "显示 {0} - {1}条，共 {2} 条",
			// update
			emptyMsg : '没有数据'
		});

Ext.define("Ext.locale.zh_CN.form.field.Text", {
			override : "Ext.form.field.Text",
			minLengthText : "该输入项的最小长度是 {0} 个字符",
			maxLengthText : "该输入项的最大长度是 {0} 个字符",
			blankText : "该输入项为必输项",
			regexText : "",
			emptyText : null
		});

Ext.define("Ext.locale.zh_CN.form.field.Number", {
			override : "Ext.form.field.Number",
			minText : "该输入项的最小值是 {0}",
			maxText : "该输入项的最大值是 {0}",
			nanText : "{0} 不是有效数值"
		});

Ext.define("Ext.locale.zh_CN.form.field.Date", {
			override : "Ext.form.field.Date",
			disabledDaysText : "禁用",
			disabledDatesText : "禁用",
			minText : "该输入项的日期必须在 {0} 之后",
			maxText : "该输入项的日期必须在 {0} 之前",
			invalidText : "{0} 是无效的日期 - 必须符合格式： {1}",
			format : "y年m月d日"
		});

Ext.define("Ext.locale.zh_CN.form.field.ComboBox", {
			override : "Ext.form.field.ComboBox",
			valueNotFoundText : undefined
		}, function() {
			Ext.apply(Ext.form.field.ComboBox.prototype.defaultListConfig, {
						loadingText : "加载中..."
					});
		});

// add HTMLEditor's tips by andy_ghg
Ext.define("Ext.locale.zh_CN.form.field.HtmlEditor", {
			override : "Ext.form.field.HtmlEditor",
			createLinkText : '添加超级链接:'
		}, function() {
			Ext.apply(Ext.form.field.HtmlEditor.prototype, {
						buttonTips : {
							bold : {
								title : '粗体 (Ctrl+B)',
								text : '将选中的文字设置为粗体',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							italic : {
								title : '斜体 (Ctrl+I)',
								text : '将选中的文字设置为斜体',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							underline : {
								title : '下划线 (Ctrl+U)',
								text : '给所选文字加下划线',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							increasefontsize : {
								title : '增大字体',
								text : '增大字号',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							decreasefontsize : {
								title : '缩小字体',
								text : '减小字号',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							backcolor : {
								title : '以不同颜色突出显示文本',
								text : '使文字看上去像是用荧光笔做了标记一样',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							forecolor : {
								title : '字体颜色',
								text : '更改字体颜色',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							justifyleft : {
								title : '左对齐',
								text : '将文字左对齐',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							justifycenter : {
								title : '居中',
								text : '将文字居中对齐',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							justifyright : {
								title : '右对齐',
								text : '将文字右对齐',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							insertunorderedlist : {
								title : '项目符号',
								text : '开始创建项目符号列表',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							insertorderedlist : {
								title : '编号',
								text : '开始创建编号列表',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							createlink : {
								title : '转成超级链接',
								text : '将所选文本转换成超级链接',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							},
							sourceedit : {
								title : '代码视图',
								text : '以代码的形式展现文本',
								cls : Ext.baseCSSPrefix + 'html-editor-tip'
							}
						}
					});
		});

Ext.define("Ext.locale.zh_CN.grid.header.Container", {
			override : "Ext.grid.header.Container",
			sortAscText : "正序",
			// update
			sortDescText : "倒序",
			// update
			lockText : "锁定列",
			// update
			unlockText : "解除锁定",
			// update
			columnsText : "列"
		});

Ext.define("Ext.locale.zh_CN.grid.PropertyColumnModel", {
			override : "Ext.grid.PropertyColumnModel",
			nameText : "名称",
			valueText : "值",
			dateFormat : "y年m月d日"
		});

// This is needed until we can refactor all of the locales into individual files
Ext.define("Ext.locale.zh_CN.Component", {
			override : "Ext.Component"
		});

		var cfWebLang={};
		cfWebLang.MenuView={};
		cfWebLang.global={};
		cfWebLang.Main={};
		cfWebLang.NavView={};
		cfWebLang.CollapsibleBlock={};
		cfWebLang.HomeTabView={};
		cfWebLang.CfWindow={};
		cfWebLang.Util={};
		cfWebLang.buildpackWindow={};
		cfWebLang.AppManageView={};
		cfWebLang.AppServiceView={};
		cfWebLang.DataAnalyseView={};
		cfWebLang.DataStorageView={};
		cfWebLang.SettingWindow={};
		cfWebLang.LogFileWindow={};
		cfWebLang.Bug = {};

		cfWebLang.Service={};
		cfWebLang.AllServicesView={};
		cfWebLang.ErrorSource={};
		cfWebLang.Error={};
		cfWebLang.QuotaManage={};
		
		cfWebLang.permission={};
		
		cfWebLang.permission.SpaceManager='空间管理员';
		cfWebLang.permission.SpaceDeveloper='空间开发者';
		cfWebLang.permission.SpaceAuditor='空间审计员';
		
		
		cfWebLang.permission.OrgManager='组织管理员';
		cfWebLang.permission.BillingManager='计费管理员';
		cfWebLang.permission.OrgAuditor='组织审计员';

		cfWebLang.Main.SessionOverdue="会话超时，请重新登录!";
		cfWebLang.global.timeout="请求超时，请重试！";
		cfWebLang.MenuView.appManage="应用管理";
		cfWebLang.MenuView.groupManage="组织管理";
		cfWebLang.MenuView.spaceManage="空间管理";
		cfWebLang.MenuView.domainManage="域名管理";
		cfWebLang.MenuView.userManage="用户管理";
		cfWebLang.MenuView.cloudManage="云管理";
		cfWebLang.MenuView.Help="帮助中心";
		
		cfWebLang.HomeTabView.appUsage="Route使用量";
		cfWebLang.HomeTabView.serviceUsage="服务使用量";
		cfWebLang.HomeTabView.appState="应用运行状态";
		cfWebLang.HomeTabView.memoryUsage="内存使用量";
		cfWebLang.HomeTabView.dashboard="仪表盘";
		cfWebLang.HomeTabView.run="运行";
		cfWebLang.HomeTabView.stop="停止";
		cfWebLang.HomeTabView.update="更新中";
		cfWebLang.HomeTabView.unused="未使用";
		cfWebLang.HomeTabView.used="使用";
		cfWebLang.HomeTabView.loadingMsg="载入中..."
		cfWebLang.HomeTabView.RuntimeEnvironment="运行环境";
		cfWebLang.HomeTabView.Middleware="中间件";
		cfWebLang.HomeTabView.homepage="首页";
		cfWebLang.HomeTabView.appManage="应用管理";
//添加新的选项卡：所有服务
		cfWebLang.HomeTabView.allServices="服务";

		cfWebLang.HomeTabView.dataStorageService="服务";
		cfWebLang.HomeTabView.dataAnalyseService="数据分析服务";
		cfWebLang.HomeTabView.appService="应用服务";
		cfWebLang.HomeTabView.app="应用基础服务";
		cfWebLang.HomeTabView.unit="个";
		cfWebLang.HomeTabView.platformState="平台使用情况";
		cfWebLang.HomeTabView.joinedOrg="已加入部门";
		cfWebLang.HomeTabView.joined="加入";
		cfWebLang.HomeTabView.unjoined="未加入";
		cfWebLang.HomeTabView.functionIntro="功能介绍";
		cfWebLang.HomeTabView.serviceIntro="服务介绍";
		
		cfWebLang.NavView.setting="设置";
		cfWebLang.NavView.message="消息";
		cfWebLang.NavView.accountSetting="帐号管理";
		cfWebLang.NavView.exit="退出";
		cfWebLang.NavView.smartcitycloud="宁波政务云计算中心";
		
		cfWebLang.CollapsibleBlock.search="请输入搜索内容";
		
		cfWebLang.CfWindow.newAppTemplateName="模板名称";
		cfWebLang.CfWindow.newAppMemSize="内存大小";
		cfWebLang.CfWindow.newAppinstSize="实例数量"
		cfWebLang.CfWindow.newAppRunEnv="运行时环境";
		cfWebLang.CfWindow.newAppLoaction="创建位置";
		cfWebLang.CfWindow.newAppAppName="应用名称";
		cfWebLang.CfWindow.newAppUploadFile="上传文件";
		cfWebLang.CfWindow.newAppUploadLocalFile="上传本地文件";
		cfWebLang.CfWindow.newAppUploadAppTemp="上传模板应用";
		cfWebLang.CfWindow.PatternSetting = "参数配置--请选择pattern类型";
		cfWebLang.CfWindow.PatternName = "Pattern名称";
		cfWebLang.CfWindow.PatternContent = "Pattern内容";
		cfWebLang.CfWindow.PatternTest = "Pattern测试";
		cfWebLang.CfWindow.InputUserName = "输入应用名称";
		cfWebLang.CfWindow.UsernameEmpty = "*应用名不能为空";
		cfWebLang.CfWindow.UsernameExisted = "*应用名已存在";
		cfWebLang.CfWindow.AppCreated = "应用未创建";
		cfWebLang.CfWindow.AppUpload = "应用已创建，上传失败!";
		cfWebLang.CfWindow.AppBinded = "上传成功，服务绑定失败！";
		cfWebLang.CfWindow.AppStart = "服务绑定成功，启动失败！";
		cfWebLang.CfWindow.CreatedSuccess = "创建应用成功!";
		cfWebLang.CfWindow.EditTemplateSuccess = "编辑模板成功!";
		cfWebLang.CfWindow.EditTemplateFailed = "编辑模板失败!";
		cfWebLang.CfWindow.UnCompletedContext = "内容不完整";
		cfWebLang.CfWindow.CreateTemplateSuccess = "创建模板成功！";
		cfWebLang.CfWindow.CreateTemplateFailed = "创建模板失败!";
		cfWebLang.CfWindow.ChoseBuildpack = "请选择buildpack";
		cfWebLang.CfWindow.StartTimeEndTime = "起始时间必须小于或等于结束时间！";
		cfWebLang.CfWindow.UploadFileTip = "选择文件上传";
		cfWebLang.CfWindow.DownAppTemplate = "查看/下载模板应用";
		cfWebLang.CfWindow.ChooseBindService = '请选择要绑定的服务 !';
		cfWebLang.CfWindow.BindServiceSuccess = '绑定服务成功!';
		cfWebLang.CfWindow.inBindService = "绑定中...";
		cfWebLang.CfWindow.patternExist = " 已存在";
		cfWebLang.CfWindow.addDbSource = "请添加对应数据库";
		cfWebLang.CfWindow.memoryEmp = "*内存不能为空"; 
		cfWebLang.CfWindow.temNameEmp = "*模板名称不能为空";
		cfWebLang.CfWindow.temNameExit = "*模板名称已存在";
		cfWebLang.CfWindow.domainNameEmp = "*domain不能为空";
		cfWebLang.CfWindow.afterBindSuccess = "绑定成功，请重启应用生效";
		cfWebLang.CfWindow.afterunBindSuccess = "解绑成功，请重启应用生效";
		
		cfWebLang.buildpackWindow.nameEmp="*名字不能为空";
		cfWebLang.buildpackWindow.fileEmp="*请输入文件";
		
		cfWebLang.Util.Next="下一步";
		cfWebLang.Util.Prev="上一步";
		cfWebLang.Util.Add = "添加";
		cfWebLang.Util.Finish="完成";
		cfWebLang.Util.Tip="提示";
		cfWebLang.Util.Cancel="取消";
		cfWebLang.Util.Yes="是";
		cfWebLang.Util.No="否";
		cfWebLang.Util.Name="名称";
		cfWebLang.Util.Edit="编辑";
		cfWebLang.Util.Save = "保存";
		cfWebLang.Util.New="新建";
		cfWebLang.Util.Start = "启动";
		cfWebLang.Util.Stop = "停止";
		cfWebLang.Util.Loading = "载入中";
		cfWebLang.Util.Status = "状态";
		cfWebLang.Util.Restart= "重启";
		cfWebLang.Util.Filter="筛选";

		//添加查看
		cfWebLang.Util.View="查看";
		cfWebLang.Util.Delete="删除";
		cfWebLang.Util.CreateApp = "创建中...";
		cfWebLang.Util.uploading = "上传中...";
		cfWebLang.Util.newAppSuccess="创建应用成功！";
		cfWebLang.Util.CreateSuccess = "创建成功!";
		cfWebLang.Util.CreateFailed = "创建失败!";
		cfWebLang.Util.ChooseBindService = '请选择要绑定的服务 !';
		cfWebLang.Util.Detail = "详情";
		cfWebLang.Util.Update = "更新";
		cfWebLang.Util.UserAuth = "用户授权";
		cfWebLang.Util.Operate = "操作";
		cfWebLang.Util.Confirm = "确认";
		cfWebLang.Util.Done = "完成";
		cfWebLang.Util.UserName="用户名";
		cfWebLang.Util.PleaseChoose = "请选择";
		cfWebLang.Util.PleaseInput = "请输入";
		cfWebLang.Util.Password="密码";
		cfWebLang.Util.Id="编号";
		cfWebLang.Util.Today="今天";
		cfWebLang.Util.Yesterday = "昨天";
		cfWebLang.Util.ThisWeek = "本周";
		cfWebLang.Util.To = "结束时间";
		cfWebLang.Util.SelectDate = "选择日期";
		cfWebLang.Util.Large = "大";
		cfWebLang.Util.Medium = "中";
		cfWebLang.Util.Small = "小";
		cfWebLang.Util.Memory = "内存";
		cfWebLang.Util.Disk = "硬盘";
		cfWebLang.Util.Setting = "设置";
		cfWebLang.Util.FieldRequired = "字段名不能为空";
		cfWebLang.Util.Message = "消息";
		cfWebLang.Util.Config = "配置";
		cfWebLang.Util.FileName = "文件名";
		cfWebLang.Util.Download = "下载";
		cfWebLang.Util.Space = "空间";
		cfWebLang.Util.Organization = "组织";
		cfWebLang.Util.Error = "错误";
		cfWebLang.CfWindow.InputAgain = "请重新输入";
		cfWebLang.CfWindow.InputFilename = "*输入文件名";
		cfWebLang.Util.Domain = "域名";
		cfWebLang.Util.Submit = "提交";
		cfWebLang.Util.Success = "成功";
		cfWebLang.Util.failed = "失败";
		cfWebLang.Util.NewApp = "新建应用";
		cfWebLang.Util.ConfirmWindow = "确认窗口";
		cfWebLang.Util.DeleteConfirm = "确定要删除吗？<br>";
		cfWebLang.Util.Unbunding = '确定要解绑吗？<br>';
		cfWebLang.Util.RestartBrowse = "请关闭浏览器，重新启动";
		cfWebLang.Util.UnLogginUser = "未获取登录用户";
		cfWebLang.Util.MemoryExpandSuccess = "应用内存扩展成功!";
		cfWebLang.Util.MemoryExpandFailed = "应用内存扩展失败!";
		cfWebLang.Util.AppExpandSuccess = "应用扩展成功";
		cfWebLang.Util.AppExpandFailed = "应用扩展失败";
		cfWebLang.Util.AddSuccess ="添加成功";
		cfWebLang.Util.AddFailed = "添加失败";
		cfWebLang.Util.DeleteSuccess = "删除成功";
		cfWebLang.Util.DeleteFailed = "删除失败";
		cfWebLang.Util.OperateSuccess = "操作成功";
		cfWebLang.Util.OperateFailed = "操作失败";
		cfWebLang.Util.ChangeSuccess = "修改成功";
		cfWebLang.Util.SaveSuccess = "保存成功";
		cfWebLang.Util.Log = "统一日志";
		cfWebLang.Util.bindService = "绑定服务";
		cfWebLang.Util.appName ="应用";
		cfWebLang.Util.exceptionCount = "异常数";
		cfWebLang.Util.ErrorCount = "错误数";
		cfWebLang.Util.details = "细节";
		cfWebLang.Util.CreateDB = "创建数据库";
		cfWebLang.Util.ChoosePlan = "选择一个plan";
		cfWebLang.Util.ChooseService = "选择一个Service";
		cfWebLang.Util.ServiceName = "服务名称";
		cfWebLang.Util.ServiceBrokers = '服务Brokers';
		cfWebLang.Util.Middleware = "中间件";
		cfWebLang.Util.back = "返回";
		cfWebLang.Util.message = "消息";
		cfWebLang.Util.SpaceUnexit = "没有所处空间,请切换空间";
		cfWebLang.Util.OrgUnexit = "没有所处组织、空间,请切换具体组织、空间";
		cfWebLang.Util.UploadAuth = "没有权限创建应用";
		cfWebLang.Util.ServiceInstance = "服务实例";
		
		cfWebLang.AppServiceView.AppServiceList = "服务列表";
		cfWebLang.AppServiceView.AppServiceTemplate = "服务模板";
		
		//新添加系统模板
		cfWebLang.AppManageView.SysAppTemplate = "系统模板";

		cfWebLang.AppManageView.AppTemplate = "我的模板";
		cfWebLang.AppManageView.AppUpdate = "重新上传";
		cfWebLang.AppManageView.AppList = "应用列表";
		cfWebLang.AppManageView.EnvironmentVar = "环境变量";
		cfWebLang.AppManageView.EnvVarName = "变量名称";
		cfWebLang.AppManageView.EnvVarValue = "变量值";
		cfWebLang.AppManageView.ServiceBindManage = "服务绑定管理";
		cfWebLang.AppManageView.AppInstance = "应用实例";
		cfWebLang.AppManageView.Version = "版      本";
		cfWebLang.AppManageView.Plan = "服务计划";
		cfWebLang.AppManageView.ServiceType='服务类型';
		cfWebLang.AppManageView.BindAppName = "绑定应用名";
		cfWebLang.AppManageView.CreateTime = "创建时间";
		cfWebLang.AppManageView.UpdateTime = "更新时间";
		cfWebLang.AppManageView.DelayHistory ="历史延时分布图";
		cfWebLang.AppManageView.VisitTime = "访问量";
		cfWebLang.AppManageView.VisitTime30m = "最近30分钟访问量";
		cfWebLang.AppManageView.AppDistribution = "应用分布";
		cfWebLang.AppManageView.RunStatus = "运行状态";
		cfWebLang.AppManageView.InstanceNumber = "实例数量";
		cfWebLang.AppManageView.MemDisk = "内存/硬盘";
		cfWebLang.AppManageView.Mem = "内存";
		cfWebLang.AppManageView.Disk = "硬盘";
		cfWebLang.AppManageView.RunEnv = "运行环境";
		cfWebLang.AppManageView.Service = "服&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;务";
		cfWebLang.AppManageView.InstanceStatus = "实例状态";
		cfWebLang.AppManageView.Domain = "域&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名";
		cfWebLang.AppManageView.UserDefinedTemplate = "自定义模板";
		cfWebLang.AppManageView.AppContainer = "运行时环境";
		cfWebLang.AppManageView.TemplateName = "模板名称";
		cfWebLang.AppManageView.OrganizationName = "组织";
		cfWebLang.AppManageView.SpaceName = "空间";
		cfWebLang.AppManageView.AppLog = "应用日志";
		cfWebLang.AppManageView.ChooseMemory = "请选择内存";
		cfWebLang.AppManageView.ChooseDomain = "请选择Domain";
	    cfWebLang.AppManageView.RunEnvs = "运行时环境";
	    cfWebLang.AppManageView.MappingDB = "映射云端数据库";
	    cfWebLang.AppManageView.Auto = "自动";
	    cfWebLang.AppManageView.Manual = "手动";
	    cfWebLang.AppManageView.CreateOracle = "创建Oracle";
	    cfWebLang.AppManageView.Create = "创建";
	    cfWebLang.AppManageView.AppDataSource = "应用数据源";
	    cfWebLang.AppManageView.CloudSource = "云平台数据源";
	    cfWebLang.AppManageView.InputDataSourceName = "请输入数据源名称";
	    cfWebLang.AppManageView.ChooseSource = "请选择云平台数据源";
		cfWebLang.AppManageView.Autoscaling = "设置";
		cfWebLang.AppServiceView.appService="应用服务";
		cfWebLang.AppManageView.bounding = "正在绑定...";
		cfWebLang.AppManageView.greyScaleUpdate = "灰度升级";
		cfWebLang.AppManageView.greyScaleUpdateTitle = "灰度升级中";
		cfWebLang.DataAnalyseView.dataAnalyse="数据分析";		
		cfWebLang.AppManageView.MidwareType = "绑定中间件类型";
		

		
		cfWebLang.LogFileWindow.ChartSetting = "图表设置";
		cfWebLang.LogFileWindow.ParameterSetting = "参数设置";
		cfWebLang.LogFileWindow.StyleSetting = "样式设置";
		cfWebLang.LogFileWindow.DataFilter = "数据过滤";
		cfWebLang.LogFileWindow.ChartExhibition = "图表展示";
		cfWebLang.LogFileWindow.CheckLog = "查看日志";
		cfWebLang.LogFileWindow.ChooseDateFrom = "开始时间";
		cfWebLang.LogFileWindow.KeyWordSearch = "关键值";
		cfWebLang.LogFileWindow.InputKeyWord = "请输入关键字";
		cfWebLang.LogFileWindow.SearchButton = "查询";
		cfWebLang.LogFileWindow.Log = "查看日志";
		cfWebLang.LogFileWindow.LogAnalyse = "日志分析";
		cfWebLang.LogFileWindow.StatisticType = "统计类型";
		cfWebLang.LogFileWindow.ChartTitle = "图表标题";
		cfWebLang.LogFileWindow.ChartSize = "图表大小";
		cfWebLang.LogFileWindow.FieldName = "字段名";
		cfWebLang.LogFileWindow.IntervalValue = "间隔值";
		cfWebLang.LogFileWindow.InputInterval = "输入时间间隔(eg.1.5h)";
		cfWebLang.LogFileWindow.ChartType = "图表类型";
		cfWebLang.LogFileWindow.KeyWord = "关键字";
		cfWebLang.LogFileWindow.ProjectCensus = "包含此关键字的项目会被统计";
		cfWebLang.LogFileWindow.GenerateChart = "生成图表";
		cfWebLang.LogFileWindow.ChangeChart = "修改图表";
		cfWebLang.LogFileWindow.SaveChart = "保存";
		cfWebLang.LogFileWindow.SurveyStatic = "概况统计";
		cfWebLang.LogFileWindow.KeyWordStatic = "关键字统计";
		cfWebLang.LogFileWindow.ChartStatic = "时间直方图统计";
		cfWebLang.LogFileWindow.PieChart = "饼图";
		cfWebLang.LogFileWindow.ColumnbarChart = "柱状图";
		cfWebLang.LogFileWindow.LineChart = "折线图";
		cfWebLang.LogFileWindow.ReturnPrev = "返回日志查看";
		cfWebLang.LogFileWindow.CountType = "概况统计和时间直方图统计请选择数字类型";
		cfWebLang.LogFileWindow.typeEmp = "*统计类型不能为空";
		cfWebLang.LogFileWindow.fieldEmp = "*字段名不能为空";
		cfWebLang.LogFileWindow.invalEmp = "*间隔值不能为空";
		cfWebLang.LogFileWindow.chartEmp = "*图表类型不能为空";
		cfWebLang.LogFileWindow.noResult = "数据为空，无统计结果";
		
		cfWebLang.QuotaManage.services="服务总数";
		cfWebLang.QuotaManage.routes="应用域名总数";
		cfWebLang.QuotaManage.memory="内存限制";
		cfWebLang.QuotaManage.instancesmemory="应用实例内存限制";
		cfWebLang.QuotaManage.winTitle='创建配额';
		cfWebLang.QuotaManage.createTitle='创建配额';
		cfWebLang.QuotaManage.editTitle='编辑配额';
		cfWebLang.QuotaManage.nameRepeat='*名称重复';
		
		cfWebLang.QuotaManage.changePassword='修改密码';
		cfWebLang.QuotaManage.oldPassword='旧密码';
		cfWebLang.QuotaManage.newPassword='新密码';
		cfWebLang.QuotaManage.confirmPassword='确认密码';
		
		cfWebLang.SettingWindow.QuotaSetting = "绑定配额";
		cfWebLang.SettingWindow.OrgChoose = "请选择组织";
		cfWebLang.SettingWindow.SpaceChoose = "请选择空间";
		cfWebLang.SettingWindow.belongToOrg ="所属组织";
		
		cfWebLang.SettingWindow.QuotaManage="配额管理";
		cfWebLang.SettingWindow.OrgManage="组织管理";
		cfWebLang.SettingWindow.SpaceManage="空间管理";
		cfWebLang.SettingWindow.DomainManage="基础域名管理";
		cfWebLang.SettingWindow.RouteManage="应用域名管理";
		cfWebLang.SettingWindow.BuildpackManage="Buildpack管理";
		cfWebLang.SettingWindow.UserManage="用户管理";
		cfWebLang.SettingWindow.belongToCloud="所属云";
		cfWebLang.SettingWindow.NewUser = "新增用户";
		cfWebLang.SettingWindow.NewSpace = "新增空间";
		cfWebLang.SettingWindow.NewOrg = "新增组织";
		cfWebLang.SettingWindow.OrgName = "请输入组织名称";
		cfWebLang.SettingWindow.SpaceName = "请输入空间名称";
		cfWebLang.SettingWindow.InputUserName = "请输入用户名";
		cfWebLang.SettingWindow.NewDomain = "新增域名";
		cfWebLang.SettingWindow.InputDomainName = "输入域名";
		cfWebLang.SettingWindow.InputPassword = "输入密码";
		cfWebLang.SettingWindow.ConfirmPassword = "确认密码";
		cfWebLang.SettingWindow.ChangeOrgName = "修改组织";
		cfWebLang.SettingWindow.InputNewOrgName = "请输入新的组织名称";
		cfWebLang.SettingWindow.ChangeSpaceName = "修改空间名称";
		cfWebLang.SettingWindow.InputNewSpaceName = "请输入新的空间名称";
		cfWebLang.SettingWindow.AccountManage = "帐号管理";
		cfWebLang.SettingWindow.BindApp = "绑定的应用";
		cfWebLang.SettingWindow.AppList = "可选的应用";
		cfWebLang.SettingWindow.InputName = "输入名称";
		cfWebLang.SettingWindow.InputDomain = "输入域名"
		cfWebLang.SettingWindow.NewRoute = "新增域名";
		cfWebLang.SettingWindow.CloudEmpty = "云名称不能为空";
		cfWebLang.SettingWindow.CloudName = "云名称";
		cfWebLang.SettingWindow.EmailName = "邮箱地址";
		cfWebLang.SettingWindow.EmailEmpty = "邮箱地址不能为空";
		cfWebLang.SettingWindow.WebsiteName = "网址";
		cfWebLang.SettingWindow.WebsiteEmpty = "网址名不能为空";
		cfWebLang.SettingWindow.Password = "密码";
		cfWebLang.SettingWindow.PasswordEmpty = "*密码不能为空";
		cfWebLang.SettingWindow.NewBuildpack = "新增buildpack";
		cfWebLang.SettingWindow.InputBuildpackName = "输入buildpack名";
		cfWebLang.SettingWindow.RoleList = "可选角色";
		cfWebLang.SettingWindow.ChoseRole = "已选角色";
		cfWebLang.SettingWindow.UserNameEmp = "*用户名不能为空";
		cfWebLang.SettingWindow.UserNameExit = "*用户名已存在";
		cfWebLang.SettingWindow.DomainEmp = "*域名不能为空";
		cfWebLang.SettingWindow.DomainExit = "*域名已存在";
		cfWebLang.SettingWindow.DomainProperty = "属性";
		cfWebLang.SettingWindow.DomainInOrg = "所属组织";
		cfWebLang.SettingWindow.selectApp = '编辑应用';
		cfWebLang.SettingWindow.RouteExit = "*域名已存在";
		cfWebLang.SettingWindow.OrgEmp = "*组织名称不能为空";
		cfWebLang.SettingWindow.SpaceEmp = "*空间名称不能为空";
		cfWebLang.SettingWindow.DomainFormat = "*格式:xx.xx";
		cfWebLang.SettingWindow.RouteEmp = "*名称不能为空";
		cfWebLang.SettingWindow.OrgList = "组织";
		cfWebLang.SettingWindow.SpaceList = "空间";
		cfWebLang.SettingWindow.PasswordComfirm = "*两次密码不一致";
		cfWebLang.SettingWindow.PasswordError = "*密码错误";
		cfWebLang.AllServicesView={};

		cfWebLang.AllServicesView.ServiceName = "服务名称";
		cfWebLang.AllServicesView.ServiceList = "服务列表";


		cfWebLang.DataStorageView.dataStorage="数据存储";
		cfWebLang.DataStorageView.dataList="数据库列表";
		cfWebLang.DataStorageView.Version = "版本";
		cfWebLang.DataStorageView.DatabaseStatus = "数据库状态";
		cfWebLang.DataStorageView.TableSpace = "表空间";
		cfWebLang.DataStorageView.TempTableSpace = "临时表空间";
		cfWebLang.DataStorageView.CreateDate = "创建日期";
		
		cfWebLang.Service.Redis = "Redis";
		cfWebLang.Service.Memcachd = "Memcachd";
		cfWebLang.Service.GlusterFS = "GlusterFS";
		cfWebLang.Service.ESB = "ESB";
		cfWebLang.Service.NewService="newervice";
		cfWebLang.Service.mongoDB = "mongoDB";
		cfWebLang.Service.MySQL = "MySQL";
		cfWebLang.Service.MySqlCluster = "MySqlCluster";
		cfWebLang.Service.Oracle = "Oracle";
		cfWebLang.Service.postgreSQL = "postgreSQL";
		cfWebLang.Service.report = "报表";
		cfWebLang.Service.batchData = "批量数据";
		cfWebLang.Service.flowData = "流数据";
		cfWebLang.Service.RabbitMQ = "RabbitMQ";
		cfWebLang.Service.logService = "日志服务";
		cfWebLang.Service.pushService = "推送服务";
		cfWebLang.Service.SSO = "单点登陆";
		cfWebLang.Service.CAS = "统一认证";
		cfWebLang.Service.Email = "电子邮件";
		cfWebLang.Service.dataExchange = "数据交换";
		cfWebLang.Service.appServiceQuery = "服务治理平台";
		cfWebLang.Service.Plan="服务计划";
		cfWebLang.Service.Label="服务类型";
		cfWebLang.Service.Hostname="主机/域名";
		cfWebLang.Service.Port="端口";
		cfWebLang.Service.Username="用户名";
		cfWebLang.Service.Password="密码";
		
		cfWebLang.CfWindow.ServiceAlreadyBinded=" 已绑定";
		
		cfWebLang.ErrorSource.app="系统出错提示";
		cfWebLang.ErrorSource.cloudfoundry="云平台提示";
		
		cfWebLang.Error.SystemError="系统出错了！";
		cfWebLang.Error.chooseDatabase="请添加对应数据库";
		cfWebLang.Error.DatasourceName ="请输入应用数据源名称";
		cfWebLang.Error.Datasource = '请选择云平台数据源';
		cfWebLang.Error.DuplicateDatasource  ="应用数据源重复";
		cfWebLang.Error.DbEnvNameRepeat = "应用数据源名称已存在";
		cfWebLang.Error.VarEnvNameRepeat = "该环境变量名称已存在";
		cfWebLang.Error.DuplicateDatasourceName  ="应用数据源名称重复";
		
		cfWebLang.Bug.OrgNameExisted = "*组织已存在";
		cfWebLang.Bug.SpaceNameExisted = "*空间已存在";
		cfWebLang.Bug.binding = "正在绑定...";
		cfWebLang.Bug.unbinding = "解绑";

		cfWebLang.ServiceConfig = {};
		cfWebLang.ServiceConfig.StorageSize = "大小";
		cfWebLang.ServiceConfig.StorageSizeLarge = "大";
		cfWebLang.ServiceConfig.StorageSizeMedium = "中";
		cfWebLang.ServiceConfig.StorageSizeSmall = "小";
		cfWebLang.ServiceConfig.Create = "创建";
		cfWebLang.ServiceConfig.Select = "选择";

		
		
/*		cfWebLang.Error.e1000 = "Invalid Auth Token";
		cfWebLang.Error.e1001= "Request invalid due to parse error";
		cfWebLang.Error.e1002="Invalid relation";
		cfWebLang.Error.e10000="Unknown request";
		cfWebLang.Error.e10001= "云平台内部错误";
		cfWebLang.Error.e10002="认证错误";
		cfWebLang.Error.e10003="没有权限";
		cfWebLang.Error.e10004="The request is invalid";
		cfWebLang.Error.e10005="The query parameter is invalid";
		cfWebLang.Error.e10006="Please delete the associations for your.";
		cfWebLang.Error.e10007="Your token lacks the necessary scopes to access this resource.";
		cfWebLang.Error.e10008="The request is semantically invalid";
		cfWebLang.Error.e10009="The request could not be completed due to a conflict";
		cfWebLang.Error.e10010="ResourceNotFound";
		cfWebLang.Error.e20001="The user info is invalid";
		cfWebLang.Error.e20002="The UAA ID is taken";
		cfWebLang.Error.e20003="The user could not be found";
		cfWebLang.Error.e30001="The organization info is invalid";
		cfWebLang.Error.e30002="The organization name is taken";
		cfWebLang.Error.e30003="The organization could not be found";
		cfWebLang.Error.e40001="The app space info is invalid";
		cfWebLang.Error.e40002="The app space name is taken";
		cfWebLang.Error.e40003="The app space and the user are not in the same org";
		cfWebLang.Error.e40004="The app space could not be found";
		cfWebLang.Error.e50001="The service auth token is invalid";
		cfWebLang.Error.e50002= "The service auth token label is taken";
		cfWebLang.Error.e50003="The service auth token could not be found";
		cfWebLang.Error.e50004="Missing service auth token";
		cfWebLang.Error.e60001= "Service instance name is required.";
		cfWebLang.Error.e60002="The service instance name is taken";
		cfWebLang.Error.e60003= "The service instance and the service binding are in different app spaces";
		cfWebLang.Error.e60003="The service instance is invalid";
		cfWebLang.Error.e60004="The service instance could not be found";
		cfWebLang.Error.e60005="You have exceeded your organization's services limit.";
		cfWebLang.Error.e60006="You have exceeded your organization's services limit.";
		cfWebLang.Error.e60007="The service instance cannot be created because paid service plans are not allowed.";
		cfWebLang.Error.e60008="An instance of this service is already present in this space. Some services only support one instance per space.";
		cfWebLang.Error.e60009="You have requested an invalid service instance name. Names are limited to 50 characters.";
		cfWebLang.Error.e60010="A service instance for the selected plan cannot be created in this organization. The plan is visible because another organization you belong to has access to it.";
		cfWebLang.Error.e60011="The service broker reported an error during deprovisioning";
		cfWebLang.Error.e60012="You have exceeded your space's services limit.";
		cfWebLang.Error.e60013="The service instance cannot be created because paid service plans are not allowed for your space.";
		cfWebLang.Error.e70001="The runtime is invalid";
		cfWebLang.Error.e70002= "The runtime name is taken";
		cfWebLang.Error.e70003="The runtime could not be found";
		cfWebLang.Error.e80001="The framework is invalid";
		cfWebLang.Error.e80002= "The framework name is taken";
		cfWebLang.Error.e80003= "The framework could not be found";
		cfWebLang.Error.e90001="The service binding is invalid";
		cfWebLang.Error.e90002="The app and the service are not in the same app space";
		cfWebLang.Error.e90003="The app space binding to service is taken";
		cfWebLang.Error.e90004="The service binding could not be found";
		cfWebLang.Error.e90005= "The service doesn't support binding.";
		cfWebLang.Error.e90006="The service is attempting to stream logs from your application, but is not registered as a logging service. Please contact the service provider.";
		cfWebLang.Error.e100001="The app is invalid";
		cfWebLang.Error.e100002="The app name is taken";
		cfWebLang.Error.e100004="The app name could not be found";
		cfWebLang.Error.e100005="You have exceeded your organization's memory limit.";
		cfWebLang.Error.e100006="You have specified an invalid amount of memory for your application.";
		cfWebLang.Error.e100007="You have exceeded the instance memory limit for your organization's quota.";
		cfWebLang.Error.e110001="The service plan is invalid";
		cfWebLang.Error.e110002="The service plan name is taken";
		cfWebLang.Error.e110003="The service plan could not be found";
		cfWebLang.Error.e110004="The service does not support changing plans.";
		cfWebLang.Error.e120001="The service is invalid";
		cfWebLang.Error.e120002="The service label is taken";
		cfWebLang.Error.e120003="The service could not be found";
		cfWebLang.Error.e130001="The domain is invalid";
		cfWebLang.Error.e130002="The domain could not be found";
		cfWebLang.Error.e130003="The domain name is taken";
		cfWebLang.Error.e140001="A legacy api call requring a default app space was called, but no default app space is set for the user.";
		cfWebLang.Error.e150001="The app package is invalid";
		cfWebLang.Error.e150002="The app package could not be found";
		cfWebLang.Error.e150003="One or more instances could not be started because of insufficient running resources.";
		cfWebLang.Error.e160001="The app upload is invalid";
		cfWebLang.Error.e160002="The app copy is invalid";
		cfWebLang.Error.e170001="Staging 错误";
		cfWebLang.Error.e170002="app还没有stage完成";
		cfWebLang.Error.e170003="An app was not successfully detected by any available buildpack";
		cfWebLang.Error.e170004="App staging failed in the buildpack compile phase";
		cfWebLang.Error.e170005="App staging failed in the buildpack release phase";
		cfWebLang.Error.e170006="There are no buildpacks available";
		cfWebLang.Error.e170007="Staging time expired";
		cfWebLang.Error.e180001="Snapshot could not be found";
		cfWebLang.Error.e180002="Service gateway internal error";
		cfWebLang.Error.e180003="Operation not supported for service";
		cfWebLang.Error.e180004="No serialization service backends available";
		cfWebLang.Error.e190001="File error";
		cfWebLang.Error.e200001="Stats error";
		cfWebLang.Error.e210001= "The route is invalid";
		cfWebLang.Error.e210002="The route could not be found";
		cfWebLang.Error.e210003="The host is taken";
		cfWebLang.Error.e220001="Instances error";
		cfWebLang.Error.e220002="Instances information unavailable";
		cfWebLang.Error.e230001="Billing event query start_date and/or end_date are missing or invalid";
		cfWebLang.Error.e230002="Event could not be found";
		cfWebLang.Error.e240001="Quota Definition could not be found";
		cfWebLang.Error.e240002="Quota Definition is taken";
		cfWebLang.Error.e240003="Quota Definition is invalid";
		cfWebLang.Error.e240004="Quota Definition memory limit cannot be negative";
		cfWebLang.Error.e250001="The stack is invalid";
		cfWebLang.Error.e250002="The stack name is taken";
		cfWebLang.Error.e250003="The stack could not be found";
		cfWebLang.Error.e260001="Service Plan Visibility is invalid";
		cfWebLang.Error.e260002="This combination of ServicePlan and Organization is already taken";
		cfWebLang.Error.e260003="The service plan visibility could not be found";
		cfWebLang.Error.e270001= "Service broker is invalid";
		cfWebLang.Error.e270002="The service broker name is taken";
		cfWebLang.Error.e270003= "The service broker url is taken";
		cfWebLang.Error.e270004="The service broker was not found";
		cfWebLang.Error.e270010="Can not remove brokers that have associated service instances";
		cfWebLang.Error.e270011= "is not a valid URL";
		cfWebLang.Error.e270012="Service broker catalog is invalid";
		cfWebLang.Error.e270013="Service broker dashboard clients could not be modified";
		cfWebLang.Error.e290001= "The buildpack name is already in use";
		cfWebLang.Error.e290002="The buildpack upload is invalid";
		cfWebLang.Error.e29000= "Buildpack is invalid";
		cfWebLang.Error.e290004= "Custom buildpacks are disabled";
		cfWebLang.Error.e290005="The buildpack is locked";
		cfWebLang.Error.e290006= "The job execution has timed out.";
		cfWebLang.Error.e300001="The security group is invalid";
		cfWebLang.Error.e300002="The security group could not be found";
		cfWebLang.Error.e300003="The security group could not be found";
		cfWebLang.Error.e300004="The security group could not be found";
		cfWebLang.Error.e300005="The security group name is taken";
		cfWebLang.Error.e310001="Space Quota Definition is invalid";
		cfWebLang.Error.e310002= "The space quota definition name is taken";
		cfWebLang.Error.e310003="You have exceeded your space's memory limit.";
		cfWebLang.Error.e310004="You have exceeded the instance memory limit for your space's quota.";
		cfWebLang.Error.e310005="You have exceeded the total routes for your space's quota.";
		cfWebLang.Error.e310006="You have exceeded the total routes for your organization's quota.";
		cfWebLang.Error.e320001="Diego has not been enabled.";
		cfWebLang.Error.e320002="You cannot specify a custom buildpack and a docker image at the same time.";
		cfWebLang.Error.e320003=" Docker support has not been enabled.";
		cfWebLang.Error.e320004="The request staging completion endpoint only handles apps desired to stage on the Diego backend.";
		cfWebLang.Error.e330000="The feature flag could not be found.";
		cfWebLang.Error.e330001="The feature flag is invalid.";
		cfWebLang.Error.e330002="Feature Disabled.";
		cfWebLang.Error.e340001="The service instance could not be found.";*/
		

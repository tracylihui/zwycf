Ext.define('Ext.ux.linc.ModuleList', {
			extend : 'Ext.view.View',
			alias : 'widget.moduleList',
			tag_list:{
				
			},
			title : null,
			compId:null,
			temp : [
	           '<div class="app_template">',
	            '<h4 class="app_template_title head_1"></h4>',
	            '<div class="app_template_list">',
	              '<ul class="atl_wrap clearfix">',
	              	'<tpl for=".">',
	                '<li class="temp_items" data-type="{type}" data-id="{id}">',
	                   '<span class="template_tag {type}_template"></span>',
	                   '<p>{name}</p>',
	                   '<span class="add_tag">+</span>',
	                '</li>',
	                '</tpl>',
	                '<li class="temp_items add_temp">+</li>',
	              '</ul>',
	            '</div>',
	          '</div>'
            ],
			initComponent : function() {
				this.addEvents({
					"onAddTemplate" : true,
					"onTemplateClicked":true,
					"onAddApp":true
				});
				// var chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

				// function generateMixed(n) {
				//      var res = "";
				//      for(var i = 0; i < n ; i ++) {
				//          var id = Math.ceil(Math.random()*35);
				//          res += chars[id];
				//      }
				//      return res;
				// }
				// this.classname = "appList-"+generateMixed(6);
				// console.log(this);
				// this.addCls(this.classname);
				// this.tag = generateMixed(6);
	

				this.callParent(arguments);
			},
			listeners: {
				beforerender : function(){
					if(typeof this.title =='string' && this.title.length > 0){
						this.temp[1] = '<h4 class="app_template_title head_1">'+ this.title+'</h4>';
					}
					else{
						this.tpl[1] = '';
					}
					this.tpl = this.temp;
				},
	            viewready: function() {
	            	this.compId = this.getEl().id;
	                var tempAdd =  Ext.select("div[id='"+this.compId+"'] li[class='temp_items add_temp']");
	                console.log(tempAdd);
					tempAdd.on('click',this.onAddTemplate,this);
					var temp =  Ext.select("div[id='"+this.compId+"'] li[class='temp_items']");
					temp.on('click',this.onTemplateClicked,this);
					var addApp =  Ext.select("div[id='"+this.compId+"'] li[class='temp_items'] span[class='add_tag']");
					addApp.on('click',this.onAddApp,this);


					var title = Ext.select("div[id='"+this.compId+"'] h4[class='app_template_title head_1']");
					if(this.title){
						title.elements[0].innerHTML = this.title;
						console.log(title);
					}
					else{
						title.elements[0].style.display = 'none';
					}
	            }
     	   },
     	   getTarNode :function(tar,tag){
     	   	var res = tar.getAttribute(tag);
     	   		if(res != null && res.length > 0){
     	   			return tar;
     	   		}
     	   		else{
					return this.getTarNode(tar.parentNode,tag);
     	   		}
     	   },
			onAddTemplate:function(a,b){
				// var tar = this.getTarNode(b,"data-name");
				// console.log(tar);
				// var data = tar.getAttribute("data-name");
				this.fireEvent('onAddTemplate');
			},
			onTemplateClicked : function(a,b){
				if(b.className == 'add_tag'){
					return;
				}
				console.log(a);
				console.log(b);
				this.fireEvent('onTemplateClicked',a);
			},
			onAddApp : function(a,b){;
				console.log(this.store);
				this.fireEvent('onAddApp');
			}
		});
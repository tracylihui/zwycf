Ext.define('Ext.ux.linc.InstanceList', {
			extend : 'Ext.view.View',
			alias : 'widget.instanceList',
			baseCls :'linc_test',
			title:false,
			columns:null,
//			overItemCls: 'instanceItems', 
			itemSelector: 'tr.instanceItem',
			 multiSelect: true,  
			initComponent : function() {
				this.addEvents({
//					"appClick" : true
				});
				//生成tpl
				if(this.columns && this.columns.length >0){
					var me = this;
					var tpl = [];
					var tpl2 = [];
					var functions = {};
					var lth = this.columns.length;
					var totalWidth = 0;
					for(var i = 0; i < lth; i++){
						var flex = this.columns[i].flex;
						if(!flex && typeof flex != 'number'){
							flex = 1;
						}
						totalWidth += flex;
					}
					
					tpl.push('<div class="instance_list">');
					if(me.title && me.title.length > 0){
						tpl.push('<h4 class="instance_list_title head_1">'+ me.title +'</h4>');
					}
					tpl.push('<ul class="instance_list_head">');
					tpl2.push('<table class="instance_list_content">');
					tpl2.push('<tpl for="."><tr class="instanceItem">');

					
					
					
					for(var i = 0; i < lth; i++){
						var item = me.columns[i];
						var header = item.header;
						var dataIndex = item.dataIndex;
						var flex = item.flex;
						var renderer = item.renderer;
						if(!flex && typeof flex != 'number'){
							flex = 1;
						}
						var width = (flex/totalWidth)*100;
						width = width.toFixed(2);
						width +="%";
						tpl.push('<li style="width:'+ width + '">'+ header +'</li>');
						if(renderer && typeof renderer =='function'){
							functions[dataIndex+"_renderer"] = renderer;
							tpl2.push('<td width="' + width + '">{[this.'+dataIndex+'_renderer(values.' + dataIndex + ')]}</td>');
						}
						else{
							tpl2.push('<td width="' + width + '">{' + dataIndex + '}</td>');
						}	
					}
					
					tpl.push('</ul>');
					tpl2.push('</tr></tpl></table>');
					tpl = tpl.concat(tpl2);
					tpl.push(functions);
					me.tpl = tpl;
					
					console.log(tpl);
					
				}
				// var chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

				// function generateMixed(n) {
				//      var res = "";
				//      for(var i = 0; i < n ; i ++) {
				//          var id = Math.ceil(Math.random()*35);
				//          res += chars[id];
				//      }
				//      return res;
				// }
				// this.classname = "appList-"+generateMixed(6);
				// console.log(this);
				// this.addCls(this.classname);
				// this.tag = generateMixed(6);
	

				this.callParent(arguments);
			},
			listeners: {
				beforerender : function(){
					if(typeof this.title =='string' && this.title.length > 0){
						this.tpl[1] = '<h4 class="head_1">'+ this.title+'</h4>';
					}
					else{
						this.tpl[1] = '';
					}
				},
	            viewready: function() {
	            	var id = this.getEl().id;
	                var tar =  Ext.select("div[id='"+id+"'] div[class='rt_wrap']");
	                console.log(tar);
					tar.on('click',this.appClick,this);
					var title = Ext.select("div[id='"+id+"'] h4[class='head_1']");

	            }
     	   },
     	   getTarNode :function(tar,tag){
     	   	var res = tar.getAttribute(tag);
     	   		if(res != null && res.length > 0){
     	   			return tar;
     	   		}
     	   		else{
					return this.getTarNode(tar.parentNode,tag);
     	   		}
     	   }
//			appClick:function(a,b){
//				var tar = this.getTarNode(b,"data-name");
//				console.log(tar);
//				var data = tar.getAttribute("data-name");
//				this.fireEvent('appClick', data);
//			}
		});
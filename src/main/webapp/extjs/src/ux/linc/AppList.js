Ext.define('Ext.ux.linc.AppList', {
			extend : 'Ext.view.View',
			alias : 'widget.appList',
			title:false,
			temp :[
            '<div class="content_block runtime">',
            '<h4 class="head_1"></h4>',
           	'<div class="content_list clearfix">',   	 	
                '<tpl for=".">',
                   '<div class="content_items">',
	                '<div class="rt_wrap" data-name="{thumb}">',
	                  '<div class="rt_icon">',
                        '<img src="resources/img/content/{thumb}" />',
                       '</div>',
                     '<div class="rt_text">{name}</div>',
                     '</div>',
                    '</div>',
                '</tpl>',
                '</div>',
             '</div>'
            ],
			// tpl:[
   //          '<div class="content_block runtime">',
   //          '<h4 class="head_1"></h4>',
   //         	'<div class="content_list clearfix">',   	 	
   //              '<tpl for=".">',
   //                 '<div class="content_items">',
	  //               '<div class="rt_wrap" data-name="{thumb}">',
	  //                 '<div class="rt_icon">',
   //                      '<img src="resources/img/content/{thumb}" />',
   //                     '</div>',
   //                   '<div class="rt_text">{name}</div>',
   //                   '</div>',
   //                  '</div>',
   //              '</tpl>',
   //              '</div>',
   //           '</div>'
   //          ],
			initComponent : function() {
				this.addEvents({
					"appClick" : true
				});
				if(typeof this.title =='string' && this.title.length > 0){
					this.temp[1] = '<h4 class="head_1">'+ this.title+'</h4>';
				}
				else{
					this.tpl[1] = '';
				}
				this.tpl = this.temp;
				// var chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

				// function generateMixed(n) {
				//      var res = "";
				//      for(var i = 0; i < n ; i ++) {
				//          var id = Math.ceil(Math.random()*35);
				//          res += chars[id];
				//      }
				//      return res;
				// }
				// this.classname = "appList-"+generateMixed(6);
				// console.log(this);
				// this.addCls(this.classname);
				// this.tag = generateMixed(6);
	

				this.callParent(arguments);
			},
			listeners: {
	            viewready: function() {
	            	var id = this.getEl().id;
	                var tar =  Ext.select("div[id='"+id+"'] div[class='rt_wrap']");
	                console.log(tar);
					tar.on('click',this.appClick,this);
					var title = Ext.select("div[id='"+id+"'] h4[class='head_1']");
	            }
     	   },
     	   getTarNode :function(tar,tag){
     	   	var res = tar.getAttribute(tag);
     	   		if(res != null && res.length > 0){
     	   			return tar;
     	   		}
     	   		else{
					return this.getTarNode(tar.parentNode,tag);
     	   		}
     	   },
			appClick:function(a,b){
				var tar = this.getTarNode(b,"data-name");
				console.log(tar);
				var data = tar.getAttribute("data-name");
				this.fireEvent('appClick', data);
			}
		});
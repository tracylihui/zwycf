package com.cfweb.form;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.cfweb.domain.CfAuditServiceTemp;
import com.cfweb.util.FormUtil;
import com.cfweb.util.PropertiesUtil;

public class UploadAppForm {

	static Logger logger = Logger.getLogger(UploadAppForm.class);

	private File file; // file of application
	private String appName; // name of application
	private String domains; // domains
	private Integer temId; // name of template
	private String envId; // DB part, table:cf_environment, field:id
	private String orgName; // org
	private String spaceName; // space
	private String instances; // instance number

	private String memory; // memory of application
	private String buildPack; // buildpack

	private boolean uploadExample;
	private String serviceName;
	private List<CfAuditServiceTemp> serviceList = new ArrayList<CfAuditServiceTemp>();

	/**
	 * get the form of upload-application
	 * 
	 * @param dataInput
	 * @throws Exception
	 */
	public UploadAppForm(MultipartFormDataInput dataInput) throws Exception {
		Map<String, List<InputPart>> uploadForm = dataInput.getFormDataMap();

		temId = Integer.valueOf(FormUtil.getFormString(uploadForm.get("template.id")));
		buildPack = FormUtil.getFormString(uploadForm.get("env.envName"));
		serviceName = FormUtil.getFormString(uploadForm.get("stoList.serName"));
		envId = FormUtil.getFormString(uploadForm.get("template.envId"));
		spaceName = FormUtil.getFormString(uploadForm.get("space"));
		orgName = FormUtil.getFormString(uploadForm.get("org"));
		memory = FormUtil.getFormString(uploadForm.get("template.memory"));
		instances = FormUtil.getFormString(uploadForm.get("template.instance"));
		appName = FormUtil.getFormString(uploadForm.get("appName"));
		domains = FormUtil.getFormString(uploadForm.get("template.domain"));
		uploadExample = FormUtil.getFormBoolean(uploadForm.get("uploadExample"));

		// 对文件进行处理
		List<InputPart> fileParts = uploadForm.get("uploadFile");
		String file_path = PropertiesUtil.getProperty("filepath");
		File dir = new File(file_path + orgName);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		if (fileParts != null && !fileParts.isEmpty()) {
			InputPart inputPart = fileParts.get(0);
			InputStream inputStream = inputPart.getBody(InputStream.class, null);
			file = new File(dir, appName + "." + domains);
			FormUtil.inputstreamtofile(inputStream, file);
		}
		if (serviceName != null && !serviceName.equals("")) {
			String[] serviceNames = serviceName.split(" ");
			for (String service : serviceNames) {
				CfAuditServiceTemp cfAuditServiceTemp = new CfAuditServiceTemp();
				cfAuditServiceTemp.setServiceName(service);// 设置服务的名字
				// 获取type的类型，create or select
				cfAuditServiceTemp.setType(FormUtil.getFormString(uploadForm.get(service.toLowerCase())));
				cfAuditServiceTemp
						.setServiceSize(FormUtil.getFormString(uploadForm.get(service.toLowerCase() + ".size")));
				cfAuditServiceTemp.setServiceInstanceName(
						FormUtil.getFormString(uploadForm.get(service.toLowerCase() + ".name")));
				serviceList.add(cfAuditServiceTemp);
			}
		}
	}

	public List<CfAuditServiceTemp> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<CfAuditServiceTemp> serviceList) {
		this.serviceList = serviceList;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getAppName() {
		return appName;
	}

	public Integer getTemId() {
		return temId;
	}

	public File getFile() {
		return file;
	}

	public String getDomains() {
		return domains;
	}

	public String getEnvId() {
		return envId;
	}

	public String getOrgName() {
		return orgName;
	}

	public String getSpaceName() {
		return spaceName;
	}

	public String getInstances() {
		return instances;
	}

	public void setInstances(String instances) {
		this.instances = instances;
	}

	public String getMemory() {
		return memory;
	}

	public String getBuildPack() {
		return buildPack;
	}

	public boolean isUploadExample() {
		return uploadExample;
	}

	public void setUploadExample(boolean uploadExample) {
		this.uploadExample = uploadExample;
	}

}
package com.cfweb.exception;

/**
 * 返回给前台的错误结构
 * 
 * @author (yufangjiang) Dec 8, 2014
 */
public class ErrorBean {
	/**
	 * 
	 */
	private Integer errorCode;
	/**
	 * 
	 */
	private String message;
	/**
	 * 错误来源，1 云平台 2应用
	 */
	private int source;

	public ErrorBean(Integer errorCode, String message, int source) {
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.source = source;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}
}

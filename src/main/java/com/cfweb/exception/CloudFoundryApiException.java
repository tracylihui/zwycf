package com.cfweb.exception;

public class CloudFoundryApiException extends RuntimeException {

	/**
	 * 错误代码
	 */
	private Integer errorCode;

	/**
	 * 描述
	 */
	private String description;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CloudFoundryApiException(Integer errorCode, String description) {
		super();
		this.errorCode = errorCode;
		if(description==null){
			this.description = "status error";
		}else{
			this.description = description;
		}	
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

package com.cfweb.domain;

import java.util.ArrayList;
import java.util.List;

public class CfAppExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CfAppExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNull() {
            addCriterion("app_name is null");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNotNull() {
            addCriterion("app_name is not null");
            return (Criteria) this;
        }

        public Criteria andAppNameEqualTo(String value) {
            addCriterion("app_name =", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotEqualTo(String value) {
            addCriterion("app_name <>", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThan(String value) {
            addCriterion("app_name >", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThanOrEqualTo(String value) {
            addCriterion("app_name >=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThan(String value) {
            addCriterion("app_name <", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThanOrEqualTo(String value) {
            addCriterion("app_name <=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLike(String value) {
            addCriterion("app_name like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotLike(String value) {
            addCriterion("app_name not like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameIn(List<String> values) {
            addCriterion("app_name in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotIn(List<String> values) {
            addCriterion("app_name not in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameBetween(String value1, String value2) {
            addCriterion("app_name between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotBetween(String value1, String value2) {
            addCriterion("app_name not between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andEnvIdIsNull() {
            addCriterion("env_id is null");
            return (Criteria) this;
        }

        public Criteria andEnvIdIsNotNull() {
            addCriterion("env_id is not null");
            return (Criteria) this;
        }

        public Criteria andEnvIdEqualTo(Integer value) {
            addCriterion("env_id =", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotEqualTo(Integer value) {
            addCriterion("env_id <>", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdGreaterThan(Integer value) {
            addCriterion("env_id >", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("env_id >=", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdLessThan(Integer value) {
            addCriterion("env_id <", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdLessThanOrEqualTo(Integer value) {
            addCriterion("env_id <=", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdIn(List<Integer> values) {
            addCriterion("env_id in", values, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotIn(List<Integer> values) {
            addCriterion("env_id not in", values, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdBetween(Integer value1, Integer value2) {
            addCriterion("env_id between", value1, value2, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("env_id not between", value1, value2, "envId");
            return (Criteria) this;
        }

        public Criteria andOrgIsNull() {
            addCriterion("org is null");
            return (Criteria) this;
        }

        public Criteria andOrgIsNotNull() {
            addCriterion("org is not null");
            return (Criteria) this;
        }

        public Criteria andOrgEqualTo(String value) {
            addCriterion("org =", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotEqualTo(String value) {
            addCriterion("org <>", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgGreaterThan(String value) {
            addCriterion("org >", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgGreaterThanOrEqualTo(String value) {
            addCriterion("org >=", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgLessThan(String value) {
            addCriterion("org <", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgLessThanOrEqualTo(String value) {
            addCriterion("org <=", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgLike(String value) {
            addCriterion("org like", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotLike(String value) {
            addCriterion("org not like", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgIn(List<String> values) {
            addCriterion("org in", values, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotIn(List<String> values) {
            addCriterion("org not in", values, "org");
            return (Criteria) this;
        }

        public Criteria andOrgBetween(String value1, String value2) {
            addCriterion("org between", value1, value2, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotBetween(String value1, String value2) {
            addCriterion("org not between", value1, value2, "org");
            return (Criteria) this;
        }

        public Criteria andSpaceIsNull() {
            addCriterion("space is null");
            return (Criteria) this;
        }

        public Criteria andSpaceIsNotNull() {
            addCriterion("space is not null");
            return (Criteria) this;
        }

        public Criteria andSpaceEqualTo(String value) {
            addCriterion("space =", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotEqualTo(String value) {
            addCriterion("space <>", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceGreaterThan(String value) {
            addCriterion("space >", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceGreaterThanOrEqualTo(String value) {
            addCriterion("space >=", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceLessThan(String value) {
            addCriterion("space <", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceLessThanOrEqualTo(String value) {
            addCriterion("space <=", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceLike(String value) {
            addCriterion("space like", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotLike(String value) {
            addCriterion("space not like", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceIn(List<String> values) {
            addCriterion("space in", values, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotIn(List<String> values) {
            addCriterion("space not in", values, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceBetween(String value1, String value2) {
            addCriterion("space between", value1, value2, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotBetween(String value1, String value2) {
            addCriterion("space not between", value1, value2, "space");
            return (Criteria) this;
        }

        public Criteria andTemplateIdIsNull() {
            addCriterion("template_id is null");
            return (Criteria) this;
        }

        public Criteria andTemplateIdIsNotNull() {
            addCriterion("template_id is not null");
            return (Criteria) this;
        }

        public Criteria andTemplateIdEqualTo(Integer value) {
            addCriterion("template_id =", value, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdNotEqualTo(Integer value) {
            addCriterion("template_id <>", value, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdGreaterThan(Integer value) {
            addCriterion("template_id >", value, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("template_id >=", value, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdLessThan(Integer value) {
            addCriterion("template_id <", value, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdLessThanOrEqualTo(Integer value) {
            addCriterion("template_id <=", value, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdIn(List<Integer> values) {
            addCriterion("template_id in", values, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdNotIn(List<Integer> values) {
            addCriterion("template_id not in", values, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdBetween(Integer value1, Integer value2) {
            addCriterion("template_id between", value1, value2, "templateId");
            return (Criteria) this;
        }

        public Criteria andTemplateIdNotBetween(Integer value1, Integer value2) {
            addCriterion("template_id not between", value1, value2, "templateId");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andMaxCpuIsNull() {
            addCriterion("max_cpu is null");
            return (Criteria) this;
        }

        public Criteria andMaxCpuIsNotNull() {
            addCriterion("max_cpu is not null");
            return (Criteria) this;
        }

        public Criteria andMaxCpuEqualTo(Double value) {
            addCriterion("max_cpu =", value, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuNotEqualTo(Double value) {
            addCriterion("max_cpu <>", value, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuGreaterThan(Double value) {
            addCriterion("max_cpu >", value, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuGreaterThanOrEqualTo(Double value) {
            addCriterion("max_cpu >=", value, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuLessThan(Double value) {
            addCriterion("max_cpu <", value, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuLessThanOrEqualTo(Double value) {
            addCriterion("max_cpu <=", value, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuIn(List<Double> values) {
            addCriterion("max_cpu in", values, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuNotIn(List<Double> values) {
            addCriterion("max_cpu not in", values, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuBetween(Double value1, Double value2) {
            addCriterion("max_cpu between", value1, value2, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMaxCpuNotBetween(Double value1, Double value2) {
            addCriterion("max_cpu not between", value1, value2, "maxCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuIsNull() {
            addCriterion("min_cpu is null");
            return (Criteria) this;
        }

        public Criteria andMinCpuIsNotNull() {
            addCriterion("min_cpu is not null");
            return (Criteria) this;
        }

        public Criteria andMinCpuEqualTo(Double value) {
            addCriterion("min_cpu =", value, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuNotEqualTo(Double value) {
            addCriterion("min_cpu <>", value, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuGreaterThan(Double value) {
            addCriterion("min_cpu >", value, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuGreaterThanOrEqualTo(Double value) {
            addCriterion("min_cpu >=", value, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuLessThan(Double value) {
            addCriterion("min_cpu <", value, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuLessThanOrEqualTo(Double value) {
            addCriterion("min_cpu <=", value, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuIn(List<Double> values) {
            addCriterion("min_cpu in", values, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuNotIn(List<Double> values) {
            addCriterion("min_cpu not in", values, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuBetween(Double value1, Double value2) {
            addCriterion("min_cpu between", value1, value2, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMinCpuNotBetween(Double value1, Double value2) {
            addCriterion("min_cpu not between", value1, value2, "minCpu");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceIsNull() {
            addCriterion("max_instance is null");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceIsNotNull() {
            addCriterion("max_instance is not null");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceEqualTo(Integer value) {
            addCriterion("max_instance =", value, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceNotEqualTo(Integer value) {
            addCriterion("max_instance <>", value, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceGreaterThan(Integer value) {
            addCriterion("max_instance >", value, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceGreaterThanOrEqualTo(Integer value) {
            addCriterion("max_instance >=", value, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceLessThan(Integer value) {
            addCriterion("max_instance <", value, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceLessThanOrEqualTo(Integer value) {
            addCriterion("max_instance <=", value, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceIn(List<Integer> values) {
            addCriterion("max_instance in", values, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceNotIn(List<Integer> values) {
            addCriterion("max_instance not in", values, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceBetween(Integer value1, Integer value2) {
            addCriterion("max_instance between", value1, value2, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMaxInstanceNotBetween(Integer value1, Integer value2) {
            addCriterion("max_instance not between", value1, value2, "maxInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceIsNull() {
            addCriterion("min_instance is null");
            return (Criteria) this;
        }

        public Criteria andMinInstanceIsNotNull() {
            addCriterion("min_instance is not null");
            return (Criteria) this;
        }

        public Criteria andMinInstanceEqualTo(Integer value) {
            addCriterion("min_instance =", value, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceNotEqualTo(Integer value) {
            addCriterion("min_instance <>", value, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceGreaterThan(Integer value) {
            addCriterion("min_instance >", value, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceGreaterThanOrEqualTo(Integer value) {
            addCriterion("min_instance >=", value, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceLessThan(Integer value) {
            addCriterion("min_instance <", value, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceLessThanOrEqualTo(Integer value) {
            addCriterion("min_instance <=", value, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceIn(List<Integer> values) {
            addCriterion("min_instance in", values, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceNotIn(List<Integer> values) {
            addCriterion("min_instance not in", values, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceBetween(Integer value1, Integer value2) {
            addCriterion("min_instance between", value1, value2, "minInstance");
            return (Criteria) this;
        }

        public Criteria andMinInstanceNotBetween(Integer value1, Integer value2) {
            addCriterion("min_instance not between", value1, value2, "minInstance");
            return (Criteria) this;
        }

        public Criteria andAutoscaleIsNull() {
            addCriterion("autoscale is null");
            return (Criteria) this;
        }

        public Criteria andAutoscaleIsNotNull() {
            addCriterion("autoscale is not null");
            return (Criteria) this;
        }

        public Criteria andAutoscaleEqualTo(Short value) {
            addCriterion("autoscale =", value, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleNotEqualTo(Short value) {
            addCriterion("autoscale <>", value, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleGreaterThan(Short value) {
            addCriterion("autoscale >", value, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleGreaterThanOrEqualTo(Short value) {
            addCriterion("autoscale >=", value, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleLessThan(Short value) {
            addCriterion("autoscale <", value, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleLessThanOrEqualTo(Short value) {
            addCriterion("autoscale <=", value, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleIn(List<Short> values) {
            addCriterion("autoscale in", values, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleNotIn(List<Short> values) {
            addCriterion("autoscale not in", values, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleBetween(Short value1, Short value2) {
            addCriterion("autoscale between", value1, value2, "autoscale");
            return (Criteria) this;
        }

        public Criteria andAutoscaleNotBetween(Short value1, Short value2) {
            addCriterion("autoscale not between", value1, value2, "autoscale");
            return (Criteria) this;
        }

        public Criteria andRelatedAppIsNull() {
            addCriterion("related_app is null");
            return (Criteria) this;
        }

        public Criteria andRelatedAppIsNotNull() {
            addCriterion("related_app is not null");
            return (Criteria) this;
        }

        public Criteria andRelatedAppEqualTo(String value) {
            addCriterion("related_app =", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppNotEqualTo(String value) {
            addCriterion("related_app <>", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppGreaterThan(String value) {
            addCriterion("related_app >", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppGreaterThanOrEqualTo(String value) {
            addCriterion("related_app >=", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppLessThan(String value) {
            addCriterion("related_app <", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppLessThanOrEqualTo(String value) {
            addCriterion("related_app <=", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppLike(String value) {
            addCriterion("related_app like", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppNotLike(String value) {
            addCriterion("related_app not like", value, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppIn(List<String> values) {
            addCriterion("related_app in", values, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppNotIn(List<String> values) {
            addCriterion("related_app not in", values, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppBetween(String value1, String value2) {
            addCriterion("related_app between", value1, value2, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andRelatedAppNotBetween(String value1, String value2) {
            addCriterion("related_app not between", value1, value2, "relatedApp");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andAppNameLikeInsensitive(String value) {
            addCriterion("upper(app_name) like", value.toUpperCase(), "appName");
            return (Criteria) this;
        }

        public Criteria andOrgLikeInsensitive(String value) {
            addCriterion("upper(org) like", value.toUpperCase(), "org");
            return (Criteria) this;
        }

        public Criteria andSpaceLikeInsensitive(String value) {
            addCriterion("upper(space) like", value.toUpperCase(), "space");
            return (Criteria) this;
        }

        public Criteria andUuidLikeInsensitive(String value) {
            addCriterion("upper(uuid) like", value.toUpperCase(), "uuid");
            return (Criteria) this;
        }

        public Criteria andRelatedAppLikeInsensitive(String value) {
            addCriterion("upper(related_app) like", value.toUpperCase(), "relatedApp");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
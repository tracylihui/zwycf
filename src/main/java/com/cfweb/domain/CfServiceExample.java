package com.cfweb.domain;

import java.util.ArrayList;
import java.util.List;

public class CfServiceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CfServiceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSerNameIsNull() {
            addCriterion("ser_name is null");
            return (Criteria) this;
        }

        public Criteria andSerNameIsNotNull() {
            addCriterion("ser_name is not null");
            return (Criteria) this;
        }

        public Criteria andSerNameEqualTo(String value) {
            addCriterion("ser_name =", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameNotEqualTo(String value) {
            addCriterion("ser_name <>", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameGreaterThan(String value) {
            addCriterion("ser_name >", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameGreaterThanOrEqualTo(String value) {
            addCriterion("ser_name >=", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameLessThan(String value) {
            addCriterion("ser_name <", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameLessThanOrEqualTo(String value) {
            addCriterion("ser_name <=", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameLike(String value) {
            addCriterion("ser_name like", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameNotLike(String value) {
            addCriterion("ser_name not like", value, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameIn(List<String> values) {
            addCriterion("ser_name in", values, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameNotIn(List<String> values) {
            addCriterion("ser_name not in", values, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameBetween(String value1, String value2) {
            addCriterion("ser_name between", value1, value2, "serName");
            return (Criteria) this;
        }

        public Criteria andSerNameNotBetween(String value1, String value2) {
            addCriterion("ser_name not between", value1, value2, "serName");
            return (Criteria) this;
        }

        public Criteria andSerTypeIsNull() {
            addCriterion("ser_type is null");
            return (Criteria) this;
        }

        public Criteria andSerTypeIsNotNull() {
            addCriterion("ser_type is not null");
            return (Criteria) this;
        }

        public Criteria andSerTypeEqualTo(Integer value) {
            addCriterion("ser_type =", value, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeNotEqualTo(Integer value) {
            addCriterion("ser_type <>", value, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeGreaterThan(Integer value) {
            addCriterion("ser_type >", value, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("ser_type >=", value, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeLessThan(Integer value) {
            addCriterion("ser_type <", value, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeLessThanOrEqualTo(Integer value) {
            addCriterion("ser_type <=", value, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeIn(List<Integer> values) {
            addCriterion("ser_type in", values, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeNotIn(List<Integer> values) {
            addCriterion("ser_type not in", values, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeBetween(Integer value1, Integer value2) {
            addCriterion("ser_type between", value1, value2, "serType");
            return (Criteria) this;
        }

        public Criteria andSerTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("ser_type not between", value1, value2, "serType");
            return (Criteria) this;
        }

        public Criteria andSerImgIsNull() {
            addCriterion("ser_img is null");
            return (Criteria) this;
        }

        public Criteria andSerImgIsNotNull() {
            addCriterion("ser_img is not null");
            return (Criteria) this;
        }

        public Criteria andSerImgEqualTo(String value) {
            addCriterion("ser_img =", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgNotEqualTo(String value) {
            addCriterion("ser_img <>", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgGreaterThan(String value) {
            addCriterion("ser_img >", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgGreaterThanOrEqualTo(String value) {
            addCriterion("ser_img >=", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgLessThan(String value) {
            addCriterion("ser_img <", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgLessThanOrEqualTo(String value) {
            addCriterion("ser_img <=", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgLike(String value) {
            addCriterion("ser_img like", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgNotLike(String value) {
            addCriterion("ser_img not like", value, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgIn(List<String> values) {
            addCriterion("ser_img in", values, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgNotIn(List<String> values) {
            addCriterion("ser_img not in", values, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgBetween(String value1, String value2) {
            addCriterion("ser_img between", value1, value2, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerImgNotBetween(String value1, String value2) {
            addCriterion("ser_img not between", value1, value2, "serImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgIsNull() {
            addCriterion("ser_large_img is null");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgIsNotNull() {
            addCriterion("ser_large_img is not null");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgEqualTo(String value) {
            addCriterion("ser_large_img =", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgNotEqualTo(String value) {
            addCriterion("ser_large_img <>", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgGreaterThan(String value) {
            addCriterion("ser_large_img >", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgGreaterThanOrEqualTo(String value) {
            addCriterion("ser_large_img >=", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgLessThan(String value) {
            addCriterion("ser_large_img <", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgLessThanOrEqualTo(String value) {
            addCriterion("ser_large_img <=", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgLike(String value) {
            addCriterion("ser_large_img like", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgNotLike(String value) {
            addCriterion("ser_large_img not like", value, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgIn(List<String> values) {
            addCriterion("ser_large_img in", values, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgNotIn(List<String> values) {
            addCriterion("ser_large_img not in", values, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgBetween(String value1, String value2) {
            addCriterion("ser_large_img between", value1, value2, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerLargeImgNotBetween(String value1, String value2) {
            addCriterion("ser_large_img not between", value1, value2, "serLargeImg");
            return (Criteria) this;
        }

        public Criteria andSerDescChnIsNull() {
            addCriterion("ser_desc_chn is null");
            return (Criteria) this;
        }

        public Criteria andSerDescChnIsNotNull() {
            addCriterion("ser_desc_chn is not null");
            return (Criteria) this;
        }

        public Criteria andSerDescChnEqualTo(String value) {
            addCriterion("ser_desc_chn =", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnNotEqualTo(String value) {
            addCriterion("ser_desc_chn <>", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnGreaterThan(String value) {
            addCriterion("ser_desc_chn >", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnGreaterThanOrEqualTo(String value) {
            addCriterion("ser_desc_chn >=", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnLessThan(String value) {
            addCriterion("ser_desc_chn <", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnLessThanOrEqualTo(String value) {
            addCriterion("ser_desc_chn <=", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnLike(String value) {
            addCriterion("ser_desc_chn like", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnNotLike(String value) {
            addCriterion("ser_desc_chn not like", value, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnIn(List<String> values) {
            addCriterion("ser_desc_chn in", values, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnNotIn(List<String> values) {
            addCriterion("ser_desc_chn not in", values, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnBetween(String value1, String value2) {
            addCriterion("ser_desc_chn between", value1, value2, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescChnNotBetween(String value1, String value2) {
            addCriterion("ser_desc_chn not between", value1, value2, "serDescChn");
            return (Criteria) this;
        }

        public Criteria andSerDescEngIsNull() {
            addCriterion("ser_desc_eng is null");
            return (Criteria) this;
        }

        public Criteria andSerDescEngIsNotNull() {
            addCriterion("ser_desc_eng is not null");
            return (Criteria) this;
        }

        public Criteria andSerDescEngEqualTo(String value) {
            addCriterion("ser_desc_eng =", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngNotEqualTo(String value) {
            addCriterion("ser_desc_eng <>", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngGreaterThan(String value) {
            addCriterion("ser_desc_eng >", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngGreaterThanOrEqualTo(String value) {
            addCriterion("ser_desc_eng >=", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngLessThan(String value) {
            addCriterion("ser_desc_eng <", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngLessThanOrEqualTo(String value) {
            addCriterion("ser_desc_eng <=", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngLike(String value) {
            addCriterion("ser_desc_eng like", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngNotLike(String value) {
            addCriterion("ser_desc_eng not like", value, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngIn(List<String> values) {
            addCriterion("ser_desc_eng in", values, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngNotIn(List<String> values) {
            addCriterion("ser_desc_eng not in", values, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngBetween(String value1, String value2) {
            addCriterion("ser_desc_eng between", value1, value2, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerDescEngNotBetween(String value1, String value2) {
            addCriterion("ser_desc_eng not between", value1, value2, "serDescEng");
            return (Criteria) this;
        }

        public Criteria andSerPrefixIsNull() {
            addCriterion("ser_prefix is null");
            return (Criteria) this;
        }

        public Criteria andSerPrefixIsNotNull() {
            addCriterion("ser_prefix is not null");
            return (Criteria) this;
        }

        public Criteria andSerPrefixEqualTo(String value) {
            addCriterion("ser_prefix =", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixNotEqualTo(String value) {
            addCriterion("ser_prefix <>", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixGreaterThan(String value) {
            addCriterion("ser_prefix >", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixGreaterThanOrEqualTo(String value) {
            addCriterion("ser_prefix >=", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixLessThan(String value) {
            addCriterion("ser_prefix <", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixLessThanOrEqualTo(String value) {
            addCriterion("ser_prefix <=", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixLike(String value) {
            addCriterion("ser_prefix like", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixNotLike(String value) {
            addCriterion("ser_prefix not like", value, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixIn(List<String> values) {
            addCriterion("ser_prefix in", values, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixNotIn(List<String> values) {
            addCriterion("ser_prefix not in", values, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixBetween(String value1, String value2) {
            addCriterion("ser_prefix between", value1, value2, "serPrefix");
            return (Criteria) this;
        }

        public Criteria andSerPrefixNotBetween(String value1, String value2) {
            addCriterion("ser_prefix not between", value1, value2, "serPrefix");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andSerNameLikeInsensitive(String value) {
            addCriterion("upper(ser_name) like", value.toUpperCase(), "serName");
            return this;
        }

        public Criteria andSerImgLikeInsensitive(String value) {
            addCriterion("upper(ser_img) like", value.toUpperCase(), "serImg");
            return this;
        }

        public Criteria andSerLargeImgLikeInsensitive(String value) {
            addCriterion("upper(ser_large_img) like", value.toUpperCase(), "serLargeImg");
            return this;
        }

        public Criteria andSerDescChnLikeInsensitive(String value) {
            addCriterion("upper(ser_desc_chn) like", value.toUpperCase(), "serDescChn");
            return this;
        }

        public Criteria andSerDescEngLikeInsensitive(String value) {
            addCriterion("upper(ser_desc_eng) like", value.toUpperCase(), "serDescEng");
            return this;
        }

        public Criteria andSerPrefixLikeInsensitive(String value) {
            addCriterion("upper(ser_prefix) like", value.toUpperCase(), "serPrefix");
            return this;
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
package com.cfweb.domain;

import java.io.Serializable;

public class PatternField implements Serializable {
    private Integer patternId;

    private String patternField;

    private static final long serialVersionUID = 1L;

    public Integer getPatternId() {
        return patternId;
    }

    public void setPatternId(Integer patternId) {
        this.patternId = patternId;
    }

    public String getPatternField() {
        return patternField;
    }

    public void setPatternField(String patternField) {
        this.patternField = patternField == null ? null : patternField.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PatternField other = (PatternField) that;
        return (this.getPatternId() == null ? other.getPatternId() == null : this.getPatternId().equals(other.getPatternId()))
            && (this.getPatternField() == null ? other.getPatternField() == null : this.getPatternField().equals(other.getPatternField()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPatternId() == null) ? 0 : getPatternId().hashCode());
        result = prime * result + ((getPatternField() == null) ? 0 : getPatternField().hashCode());
        return result;
    }
}
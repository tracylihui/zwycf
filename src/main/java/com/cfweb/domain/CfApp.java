package com.cfweb.domain;

import java.io.Serializable;

public class CfApp implements Serializable {
	private Integer id;

	private String appName;

	private Integer envId;

	private String org;

	private String space;

	private Integer templateId;

	private String uuid;

	private Double maxCpu;

	private Double minCpu;

	private Integer maxInstance;

	private Integer minInstance;

	private Short autoscale;

	private String relatedApp;

	private Integer state;

	private static final long serialVersionUID = 1L;

	public CfApp() {
	}

	public CfApp(String appName, Integer envId, String orgName, String spaceName, Integer templateId, String uuid) {
		this.appName = appName;
		this.envId = envId;
		this.org = orgName;
		this.space = spaceName;
		this.templateId = templateId;
		this.uuid = uuid;
	}
	public CfApp(Integer envId, String orgName, String spaceName, Integer templateId, Integer state) {
		this.envId = envId;
		this.org = orgName;
		this.space = spaceName;
		this.templateId = templateId;
		this.state = state;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName == null ? null : appName.trim();
	}

	public Integer getEnvId() {
		return envId;
	}

	public void setEnvId(Integer envId) {
		this.envId = envId;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org == null ? null : org.trim();
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space == null ? null : space.trim();
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid == null ? null : uuid.trim();
	}

	public Double getMaxCpu() {
		return maxCpu;
	}

	public void setMaxCpu(Double maxCpu) {
		this.maxCpu = maxCpu;
	}

	public Double getMinCpu() {
		return minCpu;
	}

	public void setMinCpu(Double minCpu) {
		this.minCpu = minCpu;
	}

	public Integer getMaxInstance() {
		return maxInstance;
	}

	public void setMaxInstance(Integer maxInstance) {
		this.maxInstance = maxInstance;
	}

	public Integer getMinInstance() {
		return minInstance;
	}

	public void setMinInstance(Integer minInstance) {
		this.minInstance = minInstance;
	}

	public Short getAutoscale() {
		return autoscale;
	}

	public void setAutoscale(Short autoscale) {
		this.autoscale = autoscale;
	}

	public String getRelatedApp() {
		return relatedApp;
	}

	public void setRelatedApp(String relatedApp) {
		this.relatedApp = relatedApp == null ? null : relatedApp.trim();
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		CfApp other = (CfApp) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getAppName() == null ? other.getAppName() == null
						: this.getAppName().equals(other.getAppName()))
				&& (this.getEnvId() == null ? other.getEnvId() == null : this.getEnvId().equals(other.getEnvId()))
				&& (this.getOrg() == null ? other.getOrg() == null : this.getOrg().equals(other.getOrg()))
				&& (this.getSpace() == null ? other.getSpace() == null : this.getSpace().equals(other.getSpace()))
				&& (this.getTemplateId() == null ? other.getTemplateId() == null
						: this.getTemplateId().equals(other.getTemplateId()))
				&& (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
				&& (this.getMaxCpu() == null ? other.getMaxCpu() == null : this.getMaxCpu().equals(other.getMaxCpu()))
				&& (this.getMinCpu() == null ? other.getMinCpu() == null : this.getMinCpu().equals(other.getMinCpu()))
				&& (this.getMaxInstance() == null ? other.getMaxInstance() == null
						: this.getMaxInstance().equals(other.getMaxInstance()))
				&& (this.getMinInstance() == null ? other.getMinInstance() == null
						: this.getMinInstance().equals(other.getMinInstance()))
				&& (this.getAutoscale() == null ? other.getAutoscale() == null
						: this.getAutoscale().equals(other.getAutoscale()))
				&& (this.getRelatedApp() == null ? other.getRelatedApp() == null
						: this.getRelatedApp().equals(other.getRelatedApp()))
				&& (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getAppName() == null) ? 0 : getAppName().hashCode());
		result = prime * result + ((getEnvId() == null) ? 0 : getEnvId().hashCode());
		result = prime * result + ((getOrg() == null) ? 0 : getOrg().hashCode());
		result = prime * result + ((getSpace() == null) ? 0 : getSpace().hashCode());
		result = prime * result + ((getTemplateId() == null) ? 0 : getTemplateId().hashCode());
		result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
		result = prime * result + ((getMaxCpu() == null) ? 0 : getMaxCpu().hashCode());
		result = prime * result + ((getMinCpu() == null) ? 0 : getMinCpu().hashCode());
		result = prime * result + ((getMaxInstance() == null) ? 0 : getMaxInstance().hashCode());
		result = prime * result + ((getMinInstance() == null) ? 0 : getMinInstance().hashCode());
		result = prime * result + ((getAutoscale() == null) ? 0 : getAutoscale().hashCode());
		result = prime * result + ((getRelatedApp() == null) ? 0 : getRelatedApp().hashCode());
		result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
		return result;
	}
}
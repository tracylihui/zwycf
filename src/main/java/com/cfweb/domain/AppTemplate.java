package com.cfweb.domain;

import java.io.Serializable;

public class AppTemplate implements Serializable {
    private Integer id;

    private String temName;

    private Integer envId;

    private String org;

    private String space;

    private String domain;

    private Integer memory;

    private Integer disk;

    private Integer instance;

    private String temImg;

    private String temColor;

    private Integer type;
    
    private String uuid; //add by 龙立强  为了可以识别出应用模板为具体哪个用户所创建

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    
    public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public void setId(Integer id) {
        this.id = id;
    }

    public String getTemName() {
        return temName;
    }

    public void setTemName(String temName) {
        this.temName = temName == null ? null : temName.trim();
    }

    public Integer getEnvId() {
        return envId;
    }

    public void setEnvId(Integer envId) {
        this.envId = envId;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org == null ? null : org.trim();
    }

    public String getSpace() {
        return space;
    }

    public void setSpace(String space) {
        this.space = space == null ? null : space.trim();
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain == null ? null : domain.trim();
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public Integer getDisk() {
        return disk;
    }

    public void setDisk(Integer disk) {
        this.disk = disk;
    }

    public Integer getInstance() {
        return instance;
    }

    public void setInstance(Integer instance) {
        this.instance = instance;
    }

    public String getTemImg() {
        return temImg;
    }

    public void setTemImg(String temImg) {
        this.temImg = temImg == null ? null : temImg.trim();
    }

    public String getTemColor() {
        return temColor;
    }

    public void setTemColor(String temColor) {
        this.temColor = temColor == null ? null : temColor.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AppTemplate other = (AppTemplate) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTemName() == null ? other.getTemName() == null : this.getTemName().equals(other.getTemName()))
            && (this.getEnvId() == null ? other.getEnvId() == null : this.getEnvId().equals(other.getEnvId()))
            && (this.getOrg() == null ? other.getOrg() == null : this.getOrg().equals(other.getOrg()))
            && (this.getSpace() == null ? other.getSpace() == null : this.getSpace().equals(other.getSpace()))
            && (this.getDomain() == null ? other.getDomain() == null : this.getDomain().equals(other.getDomain()))
            && (this.getMemory() == null ? other.getMemory() == null : this.getMemory().equals(other.getMemory()))
            && (this.getDisk() == null ? other.getDisk() == null : this.getDisk().equals(other.getDisk()))
            && (this.getInstance() == null ? other.getInstance() == null : this.getInstance().equals(other.getInstance()))
            && (this.getTemImg() == null ? other.getTemImg() == null : this.getTemImg().equals(other.getTemImg()))
            && (this.getTemColor() == null ? other.getTemColor() == null : this.getTemColor().equals(other.getTemColor()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTemName() == null) ? 0 : getTemName().hashCode());
        result = prime * result + ((getEnvId() == null) ? 0 : getEnvId().hashCode());
        result = prime * result + ((getOrg() == null) ? 0 : getOrg().hashCode());
        result = prime * result + ((getSpace() == null) ? 0 : getSpace().hashCode());
        result = prime * result + ((getDomain() == null) ? 0 : getDomain().hashCode());
        result = prime * result + ((getMemory() == null) ? 0 : getMemory().hashCode());
        result = prime * result + ((getDisk() == null) ? 0 : getDisk().hashCode());
        result = prime * result + ((getInstance() == null) ? 0 : getInstance().hashCode());
        result = prime * result + ((getTemImg() == null) ? 0 : getTemImg().hashCode());
        result = prime * result + ((getTemColor() == null) ? 0 : getTemColor().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        return result;
    }


	@Override
	public String toString() {
		return "AppTemplate [id=" + id + ", temName=" + temName + ", envId=" + envId + ", org=" + org + ", space="
				+ space + ", domain=" + domain + ", memory=" + memory + ", disk=" + disk + ", instance=" + instance
				+ ", temImg=" + temImg + ", temColor=" + temColor + ", type=" + type + ", uuid=" + uuid + "]";
	}
    
}
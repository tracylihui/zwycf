package com.cfweb.domain;

import java.io.Serializable;

public class TplDetail implements Serializable{
    private Integer id;

    private String temName;

    private Integer envId;

    private String org;

    private String space;

    private String domain;

    private Integer memory;

    private Integer disk;

    private Integer instance;

    private String temImg;

    private String temColor;

    private Integer type;
    
    private String serviceName;
    
    private String envName;

    private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTemName() {
		return temName;
	}

	public void setTemName(String temName) {
		this.temName = temName;
	}

	public Integer getEnvId() {
		return envId;
	}

	public void setEnvId(Integer envId) {
		this.envId = envId;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Integer getMemory() {
		return memory;
	}

	public void setMemory(Integer memory) {
		this.memory = memory;
	}

	public Integer getDisk() {
		return disk;
	}

	public void setDisk(Integer disk) {
		this.disk = disk;
	}

	public Integer getInstance() {
		return instance;
	}

	public void setInstance(Integer instance) {
		this.instance = instance;
	}

	public String getTemImg() {
		return temImg;
	}

	public void setTemImg(String temImg) {
		this.temImg = temImg;
	}

	public String getTemColor() {
		return temColor;
	}

	public void setTemColor(String temColor) {
		this.temColor = temColor;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getService() {
		return serviceName;
	}

	public void setService(String service) {
		this.serviceName = service;
	}

	public String getEnvName() {
		return envName;
	}

	public void setEnvName(String envName) {
		this.envName = envName;
	}
    
}

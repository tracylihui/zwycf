package com.cfweb.domain;

import java.io.Serializable;

public class CfServiceBroker implements Serializable {
    private Integer id;

    private String appUrl;

    private String serviceBrokerGuid;

    private Integer serviceOwner;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl == null ? null : appUrl.trim();
    }

    public String getServiceBrokerGuid() {
        return serviceBrokerGuid;
    }

    public void setServiceBrokerGuid(String serviceBrokerGuid) {
        this.serviceBrokerGuid = serviceBrokerGuid == null ? null : serviceBrokerGuid.trim();
    }

    public Integer getServiceOwner() {
        return serviceOwner;
    }

    public void setServiceOwner(Integer serviceOwner) {
        this.serviceOwner = serviceOwner;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CfServiceBroker other = (CfServiceBroker) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAppUrl() == null ? other.getAppUrl() == null : this.getAppUrl().equals(other.getAppUrl()))
            && (this.getServiceBrokerGuid() == null ? other.getServiceBrokerGuid() == null : this.getServiceBrokerGuid().equals(other.getServiceBrokerGuid()))
            && (this.getServiceOwner() == null ? other.getServiceOwner() == null : this.getServiceOwner().equals(other.getServiceOwner()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAppUrl() == null) ? 0 : getAppUrl().hashCode());
        result = prime * result + ((getServiceBrokerGuid() == null) ? 0 : getServiceBrokerGuid().hashCode());
        result = prime * result + ((getServiceOwner() == null) ? 0 : getServiceOwner().hashCode());
        return result;
    }
}
package com.cfweb.domain;

import java.io.Serializable;

public class CfAppExternal implements Serializable {
	private Integer id;

	private String appName;

	private Integer envId;

	private String org;

	private String space;

	private Integer templateId;

	private String uuid;

	private String temImg;

	private String temColor;

	private String temName;

	private Integer maxCpu;

	private Integer minCpu;

	private Integer maxInstance;

	private Integer minInstance;

	private String relatedApp;

	private Integer state;

	public String getRelatedApp() {
		return relatedApp;
	}

	public void setRelatedApp(String relatedApp) {
		this.relatedApp = relatedApp;
	}

	public String getTemName() {
		return temName;
	}

	public void setTemName(String temName) {
		this.temName = temName;
	}

	public String getTemImg() {
		return temImg;
	}

	public void setTemImg(String temImg) {
		this.temImg = temImg;
	}

	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public Integer getEnvId() {
		return envId;
	}

	public void setEnvId(Integer envId) {
		this.envId = envId;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getMaxCpu() {
		return maxCpu;
	}

	public void setMaxCpu(Integer maxCpu) {
		this.maxCpu = maxCpu;
	}

	public Integer getMinCpu() {
		return minCpu;
	}

	public void setMinCpu(Integer minCpu) {
		this.minCpu = minCpu;
	}

	public Integer getMaxInstance() {
		return maxInstance;
	}

	public void setMaxInstance(Integer maxInstance) {
		this.maxInstance = maxInstance;
	}

	public Integer getMinInstance() {
		return minInstance;
	}

	public void setMinInstance(Integer minInstance) {
		this.minInstance = minInstance;
	}

	public String getTemColor() {
		return temColor;
	}

	public void setTemColor(String temColor) {
		this.temColor = temColor;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CfAppExternal [id=" + id + ", appName=" + appName + ", envId=" + envId + ", org=" + org + ", space="
				+ space + ", templateId=" + templateId + ", uuid=" + uuid + ", temImg=" + temImg + ", temColor="
				+ temColor + ", temName=" + temName + ", maxCpu=" + maxCpu + ", minCpu=" + minCpu + ", maxInstance="
				+ maxInstance + ", minInstance=" + minInstance + ", relatedApp=" + relatedApp + ", state=" + state
				+ "]";
	}
	
}
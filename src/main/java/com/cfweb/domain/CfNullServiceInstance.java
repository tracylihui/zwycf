package com.cfweb.domain;

import java.io.Serializable;

public class CfNullServiceInstance implements Serializable {
    private Integer id;

    private String serviceInstanceName;

    private String nullServiceName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceInstanceName() {
        return serviceInstanceName;
    }

    public void setServiceInstanceName(String serviceInstanceName) {
        this.serviceInstanceName = serviceInstanceName == null ? null : serviceInstanceName.trim();
    }

    public String getNullServiceName() {
        return nullServiceName;
    }

    public void setNullServiceName(String nullServiceName) {
        this.nullServiceName = nullServiceName == null ? null : nullServiceName.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CfNullServiceInstance other = (CfNullServiceInstance) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getServiceInstanceName() == null ? other.getServiceInstanceName() == null : this.getServiceInstanceName().equals(other.getServiceInstanceName()))
            && (this.getNullServiceName() == null ? other.getNullServiceName() == null : this.getNullServiceName().equals(other.getNullServiceName()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getServiceInstanceName() == null) ? 0 : getServiceInstanceName().hashCode());
        result = prime * result + ((getNullServiceName() == null) ? 0 : getNullServiceName().hashCode());
        return result;
    }
}
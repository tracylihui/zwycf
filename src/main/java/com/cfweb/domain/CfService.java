package com.cfweb.domain;

import java.io.Serializable;

public class CfService implements Serializable {
    private Integer id;

    private String serName;

    private Integer serType;

    private String serImg;

    private String serLargeImg;

    private String serDescChn;

    private String serDescEng;

    private String serPrefix;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerName() {
        return serName;
    }

    public void setSerName(String serName) {
        this.serName = serName == null ? null : serName.trim();
    }

    public Integer getSerType() {
        return serType;
    }

    public void setSerType(Integer serType) {
        this.serType = serType;
    }

    public String getSerImg() {
        return serImg;
    }

    public void setSerImg(String serImg) {
        this.serImg = serImg == null ? null : serImg.trim();
    }

    public String getSerLargeImg() {
        return serLargeImg;
    }

    public void setSerLargeImg(String serLargeImg) {
        this.serLargeImg = serLargeImg == null ? null : serLargeImg.trim();
    }

    public String getSerDescChn() {
        return serDescChn;
    }

    public void setSerDescChn(String serDescChn) {
        this.serDescChn = serDescChn == null ? null : serDescChn.trim();
    }

    public String getSerDescEng() {
        return serDescEng;
    }

    public void setSerDescEng(String serDescEng) {
        this.serDescEng = serDescEng == null ? null : serDescEng.trim();
    }

    public String getSerPrefix() {
        return serPrefix;
    }

    public void setSerPrefix(String serPrefix) {
        this.serPrefix = serPrefix == null ? null : serPrefix.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CfService other = (CfService) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSerName() == null ? other.getSerName() == null : this.getSerName().equals(other.getSerName()))
            && (this.getSerType() == null ? other.getSerType() == null : this.getSerType().equals(other.getSerType()))
            && (this.getSerImg() == null ? other.getSerImg() == null : this.getSerImg().equals(other.getSerImg()))
            && (this.getSerLargeImg() == null ? other.getSerLargeImg() == null : this.getSerLargeImg().equals(other.getSerLargeImg()))
            && (this.getSerDescChn() == null ? other.getSerDescChn() == null : this.getSerDescChn().equals(other.getSerDescChn()))
            && (this.getSerDescEng() == null ? other.getSerDescEng() == null : this.getSerDescEng().equals(other.getSerDescEng()))
            && (this.getSerPrefix() == null ? other.getSerPrefix() == null : this.getSerPrefix().equals(other.getSerPrefix()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSerName() == null) ? 0 : getSerName().hashCode());
        result = prime * result + ((getSerType() == null) ? 0 : getSerType().hashCode());
        result = prime * result + ((getSerImg() == null) ? 0 : getSerImg().hashCode());
        result = prime * result + ((getSerLargeImg() == null) ? 0 : getSerLargeImg().hashCode());
        result = prime * result + ((getSerDescChn() == null) ? 0 : getSerDescChn().hashCode());
        result = prime * result + ((getSerDescEng() == null) ? 0 : getSerDescEng().hashCode());
        result = prime * result + ((getSerPrefix() == null) ? 0 : getSerPrefix().hashCode());
        return result;
    }
}
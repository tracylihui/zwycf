package com.cfweb.domain;

import java.util.ArrayList;
import java.util.List;

public class CfUserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CfUserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserPassIsNull() {
            addCriterion("user_pass is null");
            return (Criteria) this;
        }

        public Criteria andUserPassIsNotNull() {
            addCriterion("user_pass is not null");
            return (Criteria) this;
        }

        public Criteria andUserPassEqualTo(String value) {
            addCriterion("user_pass =", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotEqualTo(String value) {
            addCriterion("user_pass <>", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassGreaterThan(String value) {
            addCriterion("user_pass >", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassGreaterThanOrEqualTo(String value) {
            addCriterion("user_pass >=", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassLessThan(String value) {
            addCriterion("user_pass <", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassLessThanOrEqualTo(String value) {
            addCriterion("user_pass <=", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassLike(String value) {
            addCriterion("user_pass like", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotLike(String value) {
            addCriterion("user_pass not like", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassIn(List<String> values) {
            addCriterion("user_pass in", values, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotIn(List<String> values) {
            addCriterion("user_pass not in", values, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassBetween(String value1, String value2) {
            addCriterion("user_pass between", value1, value2, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotBetween(String value1, String value2) {
            addCriterion("user_pass not between", value1, value2, "userPass");
            return (Criteria) this;
        }

        public Criteria andLastOrgIsNull() {
            addCriterion("last_org is null");
            return (Criteria) this;
        }

        public Criteria andLastOrgIsNotNull() {
            addCriterion("last_org is not null");
            return (Criteria) this;
        }

        public Criteria andLastOrgEqualTo(String value) {
            addCriterion("last_org =", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgNotEqualTo(String value) {
            addCriterion("last_org <>", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgGreaterThan(String value) {
            addCriterion("last_org >", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgGreaterThanOrEqualTo(String value) {
            addCriterion("last_org >=", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgLessThan(String value) {
            addCriterion("last_org <", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgLessThanOrEqualTo(String value) {
            addCriterion("last_org <=", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgLike(String value) {
            addCriterion("last_org like", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgNotLike(String value) {
            addCriterion("last_org not like", value, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgIn(List<String> values) {
            addCriterion("last_org in", values, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgNotIn(List<String> values) {
            addCriterion("last_org not in", values, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgBetween(String value1, String value2) {
            addCriterion("last_org between", value1, value2, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastOrgNotBetween(String value1, String value2) {
            addCriterion("last_org not between", value1, value2, "lastOrg");
            return (Criteria) this;
        }

        public Criteria andLastSpaceIsNull() {
            addCriterion("last_space is null");
            return (Criteria) this;
        }

        public Criteria andLastSpaceIsNotNull() {
            addCriterion("last_space is not null");
            return (Criteria) this;
        }

        public Criteria andLastSpaceEqualTo(String value) {
            addCriterion("last_space =", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceNotEqualTo(String value) {
            addCriterion("last_space <>", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceGreaterThan(String value) {
            addCriterion("last_space >", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceGreaterThanOrEqualTo(String value) {
            addCriterion("last_space >=", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceLessThan(String value) {
            addCriterion("last_space <", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceLessThanOrEqualTo(String value) {
            addCriterion("last_space <=", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceLike(String value) {
            addCriterion("last_space like", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceNotLike(String value) {
            addCriterion("last_space not like", value, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceIn(List<String> values) {
            addCriterion("last_space in", values, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceNotIn(List<String> values) {
            addCriterion("last_space not in", values, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceBetween(String value1, String value2) {
            addCriterion("last_space between", value1, value2, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andLastSpaceNotBetween(String value1, String value2) {
            addCriterion("last_space not between", value1, value2, "lastSpace");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumIsNull() {
            addCriterion("app_max_num is null");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumIsNotNull() {
            addCriterion("app_max_num is not null");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumEqualTo(Integer value) {
            addCriterion("app_max_num =", value, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumNotEqualTo(Integer value) {
            addCriterion("app_max_num <>", value, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumGreaterThan(Integer value) {
            addCriterion("app_max_num >", value, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("app_max_num >=", value, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumLessThan(Integer value) {
            addCriterion("app_max_num <", value, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumLessThanOrEqualTo(Integer value) {
            addCriterion("app_max_num <=", value, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumIn(List<Integer> values) {
            addCriterion("app_max_num in", values, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumNotIn(List<Integer> values) {
            addCriterion("app_max_num not in", values, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumBetween(Integer value1, Integer value2) {
            addCriterion("app_max_num between", value1, value2, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andAppMaxNumNotBetween(Integer value1, Integer value2) {
            addCriterion("app_max_num not between", value1, value2, "appMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumIsNull() {
            addCriterion("ser_max_num is null");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumIsNotNull() {
            addCriterion("ser_max_num is not null");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumEqualTo(Integer value) {
            addCriterion("ser_max_num =", value, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumNotEqualTo(Integer value) {
            addCriterion("ser_max_num <>", value, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumGreaterThan(Integer value) {
            addCriterion("ser_max_num >", value, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("ser_max_num >=", value, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumLessThan(Integer value) {
            addCriterion("ser_max_num <", value, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumLessThanOrEqualTo(Integer value) {
            addCriterion("ser_max_num <=", value, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumIn(List<Integer> values) {
            addCriterion("ser_max_num in", values, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumNotIn(List<Integer> values) {
            addCriterion("ser_max_num not in", values, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumBetween(Integer value1, Integer value2) {
            addCriterion("ser_max_num between", value1, value2, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andSerMaxNumNotBetween(Integer value1, Integer value2) {
            addCriterion("ser_max_num not between", value1, value2, "serMaxNum");
            return (Criteria) this;
        }

        public Criteria andUserIsadminIsNull() {
            addCriterion("user_isadmin is null");
            return (Criteria) this;
        }

        public Criteria andUserIsadminIsNotNull() {
            addCriterion("user_isadmin is not null");
            return (Criteria) this;
        }

        public Criteria andUserIsadminEqualTo(Integer value) {
            addCriterion("user_isadmin =", value, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminNotEqualTo(Integer value) {
            addCriterion("user_isadmin <>", value, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminGreaterThan(Integer value) {
            addCriterion("user_isadmin >", value, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_isadmin >=", value, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminLessThan(Integer value) {
            addCriterion("user_isadmin <", value, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminLessThanOrEqualTo(Integer value) {
            addCriterion("user_isadmin <=", value, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminIn(List<Integer> values) {
            addCriterion("user_isadmin in", values, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminNotIn(List<Integer> values) {
            addCriterion("user_isadmin not in", values, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminBetween(Integer value1, Integer value2) {
            addCriterion("user_isadmin between", value1, value2, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUserIsadminNotBetween(Integer value1, Integer value2) {
            addCriterion("user_isadmin not between", value1, value2, "userIsadmin");
            return (Criteria) this;
        }

        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andUserNameLikeInsensitive(String value) {
            addCriterion("upper(user_name) like", value.toUpperCase(), "userName");
            return this;
        }

        public Criteria andUserPassLikeInsensitive(String value) {
            addCriterion("upper(user_pass) like", value.toUpperCase(), "userPass");
            return this;
        }

        public Criteria andLastOrgLikeInsensitive(String value) {
            addCriterion("upper(last_org) like", value.toUpperCase(), "lastOrg");
            return this;
        }

        public Criteria andLastSpaceLikeInsensitive(String value) {
            addCriterion("upper(last_space) like", value.toUpperCase(), "lastSpace");
            return this;
        }

        public Criteria andUuidLikeInsensitive(String value) {
            addCriterion("upper(uuid) like", value.toUpperCase(), "uuid");
            return this;
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
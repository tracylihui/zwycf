package com.cfweb.domain;

import java.io.Serializable;

public class AppLogPatterns implements Serializable {
    private Integer appId;

    private Integer appPattern;

    private static final long serialVersionUID = 1L;

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getAppPattern() {
        return appPattern;
    }

    public void setAppPattern(Integer appPattern) {
        this.appPattern = appPattern;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AppLogPatterns other = (AppLogPatterns) that;
        return (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
            && (this.getAppPattern() == null ? other.getAppPattern() == null : this.getAppPattern().equals(other.getAppPattern()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
        result = prime * result + ((getAppPattern() == null) ? 0 : getAppPattern().hashCode());
        return result;
    }
}
package com.cfweb.domain;

public class CfAuditServiceTemp {
	private String serviceName;
	private String serviceSize;
	private String type;
	private String serviceInstanceName;

	private Integer serviceId;
	
	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceSize() {
		return serviceSize;
	}

	public void setServiceSize(String serviceSize) {
		this.serviceSize = serviceSize;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getServiceInstanceName() {
		return serviceInstanceName;
	}

	public void setServiceInstanceName(String serviceInstanceName) {
		this.serviceInstanceName = serviceInstanceName;
	}

	@Override
	public String toString() {
		return "CfAuditServiceTemp [serviceName=" + serviceName + ", serviceSize=" + serviceSize + ", type=" + type
				+ ", serviceInstanceName=" + serviceInstanceName + "]";
	}
	

}
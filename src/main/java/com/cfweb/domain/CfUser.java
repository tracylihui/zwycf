package com.cfweb.domain;

import java.io.Serializable;

public class CfUser implements Serializable {
    private Integer id;

    private String userName;

    private String userPass;

    private String lastOrg;

    private String lastSpace;

    private Integer appMaxNum;

    private Integer serMaxNum;

    private Integer userIsadmin;

    private String uuid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass == null ? null : userPass.trim();
    }

    public String getLastOrg() {
        return lastOrg;
    }

    public void setLastOrg(String lastOrg) {
        this.lastOrg = lastOrg == null ? null : lastOrg.trim();
    }

    public String getLastSpace() {
        return lastSpace;
    }

    public void setLastSpace(String lastSpace) {
        this.lastSpace = lastSpace == null ? null : lastSpace.trim();
    }

    public Integer getAppMaxNum() {
        return appMaxNum;
    }

    public void setAppMaxNum(Integer appMaxNum) {
        this.appMaxNum = appMaxNum;
    }

    public Integer getSerMaxNum() {
        return serMaxNum;
    }

    public void setSerMaxNum(Integer serMaxNum) {
        this.serMaxNum = serMaxNum;
    }

    public Integer getUserIsadmin() {
        return userIsadmin;
    }

    public void setUserIsadmin(Integer userIsadmin) {
        this.userIsadmin = userIsadmin;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CfUser other = (CfUser) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getUserPass() == null ? other.getUserPass() == null : this.getUserPass().equals(other.getUserPass()))
            && (this.getLastOrg() == null ? other.getLastOrg() == null : this.getLastOrg().equals(other.getLastOrg()))
            && (this.getLastSpace() == null ? other.getLastSpace() == null : this.getLastSpace().equals(other.getLastSpace()))
            && (this.getAppMaxNum() == null ? other.getAppMaxNum() == null : this.getAppMaxNum().equals(other.getAppMaxNum()))
            && (this.getSerMaxNum() == null ? other.getSerMaxNum() == null : this.getSerMaxNum().equals(other.getSerMaxNum()))
            && (this.getUserIsadmin() == null ? other.getUserIsadmin() == null : this.getUserIsadmin().equals(other.getUserIsadmin()))
            && (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getUserPass() == null) ? 0 : getUserPass().hashCode());
        result = prime * result + ((getLastOrg() == null) ? 0 : getLastOrg().hashCode());
        result = prime * result + ((getLastSpace() == null) ? 0 : getLastSpace().hashCode());
        result = prime * result + ((getAppMaxNum() == null) ? 0 : getAppMaxNum().hashCode());
        result = prime * result + ((getSerMaxNum() == null) ? 0 : getSerMaxNum().hashCode());
        result = prime * result + ((getUserIsadmin() == null) ? 0 : getUserIsadmin().hashCode());
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        return result;
    }
}
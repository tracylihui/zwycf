package com.cfweb.domain;

import java.util.ArrayList;
import java.util.List;

public class CfServiceBrokerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CfServiceBrokerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAppUrlIsNull() {
            addCriterion("app_url is null");
            return (Criteria) this;
        }

        public Criteria andAppUrlIsNotNull() {
            addCriterion("app_url is not null");
            return (Criteria) this;
        }

        public Criteria andAppUrlEqualTo(String value) {
            addCriterion("app_url =", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlNotEqualTo(String value) {
            addCriterion("app_url <>", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlGreaterThan(String value) {
            addCriterion("app_url >", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlGreaterThanOrEqualTo(String value) {
            addCriterion("app_url >=", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlLessThan(String value) {
            addCriterion("app_url <", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlLessThanOrEqualTo(String value) {
            addCriterion("app_url <=", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlLike(String value) {
            addCriterion("app_url like", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlNotLike(String value) {
            addCriterion("app_url not like", value, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlIn(List<String> values) {
            addCriterion("app_url in", values, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlNotIn(List<String> values) {
            addCriterion("app_url not in", values, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlBetween(String value1, String value2) {
            addCriterion("app_url between", value1, value2, "appUrl");
            return (Criteria) this;
        }

        public Criteria andAppUrlNotBetween(String value1, String value2) {
            addCriterion("app_url not between", value1, value2, "appUrl");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidIsNull() {
            addCriterion("service_broker_guid is null");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidIsNotNull() {
            addCriterion("service_broker_guid is not null");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidEqualTo(String value) {
            addCriterion("service_broker_guid =", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidNotEqualTo(String value) {
            addCriterion("service_broker_guid <>", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidGreaterThan(String value) {
            addCriterion("service_broker_guid >", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidGreaterThanOrEqualTo(String value) {
            addCriterion("service_broker_guid >=", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidLessThan(String value) {
            addCriterion("service_broker_guid <", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidLessThanOrEqualTo(String value) {
            addCriterion("service_broker_guid <=", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidLike(String value) {
            addCriterion("service_broker_guid like", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidNotLike(String value) {
            addCriterion("service_broker_guid not like", value, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidIn(List<String> values) {
            addCriterion("service_broker_guid in", values, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidNotIn(List<String> values) {
            addCriterion("service_broker_guid not in", values, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidBetween(String value1, String value2) {
            addCriterion("service_broker_guid between", value1, value2, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceBrokerGuidNotBetween(String value1, String value2) {
            addCriterion("service_broker_guid not between", value1, value2, "serviceBrokerGuid");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerIsNull() {
            addCriterion("service_owner is null");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerIsNotNull() {
            addCriterion("service_owner is not null");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerEqualTo(Integer value) {
            addCriterion("service_owner =", value, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerNotEqualTo(Integer value) {
            addCriterion("service_owner <>", value, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerGreaterThan(Integer value) {
            addCriterion("service_owner >", value, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerGreaterThanOrEqualTo(Integer value) {
            addCriterion("service_owner >=", value, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerLessThan(Integer value) {
            addCriterion("service_owner <", value, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerLessThanOrEqualTo(Integer value) {
            addCriterion("service_owner <=", value, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerIn(List<Integer> values) {
            addCriterion("service_owner in", values, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerNotIn(List<Integer> values) {
            addCriterion("service_owner not in", values, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerBetween(Integer value1, Integer value2) {
            addCriterion("service_owner between", value1, value2, "serviceOwner");
            return (Criteria) this;
        }

        public Criteria andServiceOwnerNotBetween(Integer value1, Integer value2) {
            addCriterion("service_owner not between", value1, value2, "serviceOwner");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andAppUrlLikeInsensitive(String value) {
            addCriterion("upper(app_url) like", value.toUpperCase(), "appUrl");
            return this;
        }

        public Criteria andServiceBrokerGuidLikeInsensitive(String value) {
            addCriterion("upper(service_broker_guid) like", value.toUpperCase(), "serviceBrokerGuid");
            return this;
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
package com.cfweb.domain;

import java.io.Serializable;
/**
 * 
 * @author (yufangjiang)
 * Dec 2, 2014
 */
public class AppLogConfig implements Serializable {
    private Integer appId;

    private String configJson;

    private static final long serialVersionUID = 1L;

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getConfigJson() {
        return configJson;
    }

    public void setConfigJson(String configJson) {
        this.configJson = configJson == null ? null : configJson.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AppLogConfig other = (AppLogConfig) that;
        return (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
            && (this.getConfigJson() == null ? other.getConfigJson() == null : this.getConfigJson().equals(other.getConfigJson()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
        result = prime * result + ((getConfigJson() == null) ? 0 : getConfigJson().hashCode());
        return result;
    }
}
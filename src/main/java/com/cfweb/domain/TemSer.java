package com.cfweb.domain;

import java.io.Serializable;

public class TemSer implements Serializable {
    private Integer id;

    private Integer temId;

    private Integer serId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTemId() {
        return temId;
    }

    public void setTemId(Integer temId) {
        this.temId = temId;
    }

    public Integer getSerId() {
        return serId;
    }

    public void setSerId(Integer serId) {
        this.serId = serId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TemSer other = (TemSer) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTemId() == null ? other.getTemId() == null : this.getTemId().equals(other.getTemId()))
            && (this.getSerId() == null ? other.getSerId() == null : this.getSerId().equals(other.getSerId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTemId() == null) ? 0 : getTemId().hashCode());
        result = prime * result + ((getSerId() == null) ? 0 : getSerId().hashCode());
        return result;
    }
}
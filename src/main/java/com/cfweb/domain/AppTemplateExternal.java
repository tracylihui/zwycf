package com.cfweb.domain;

public class AppTemplateExternal extends AppTemplate {
	private static final long serialVersionUID = 1L;

	public AppTemplateExternal(String temName, Integer envId, String orgName, String spaceName, String domain,
			Integer memory, Integer disk, Integer instance,Integer type, String userid) {
		super();
		setTemName(temName);
		setEnvId(envId);
		setOrg(orgName);
		setSpace(spaceName);
		setDomain(domain);
		setMemory(memory);
		setDisk(disk);
		setInstance(instance);
		setType(type);
		setUuid(userid);
	}

	public AppTemplateExternal(Integer id, String temName, Integer envId, String orgName, String spaceName,
			String domain, Integer memory, Integer disk, Integer instance,Integer type, String userid) {
		super();
		setId(id);
		setTemName(temName);
		setEnvId(envId);
		setOrg(orgName);
		setSpace(spaceName);
		setDomain(domain);
		setMemory(memory);
		setDisk(disk);
		setInstance(instance);
		setType(type);
		setUuid(userid);
	}

}

package com.cfweb.domain;

import java.util.Map;

/**
 * this is for store application with template informations
 * @author lmx
 *
 */
public class AppWithTpl {
	private CfAppExternal cfapp;
	private Map<String, Object> cloudApp;
	
	public AppWithTpl(CfAppExternal cfApp, Map<String, Object> cloudApplication) {
		cfapp = cfApp;
		cloudApp = cloudApplication;
	}
	
	public CfAppExternal getCfapp() {
		return cfapp;
	}
	public void setCfapp(CfAppExternal cfapp) {
		this.cfapp = cfapp;
	}
	public Map<String, Object> getCloudApp() {
		return cloudApp;
	}
	public void setCloudApp(Map<String, Object> cloudApp) {
		this.cloudApp = cloudApp;
	}
	
	

}
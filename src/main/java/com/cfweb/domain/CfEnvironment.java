package com.cfweb.domain;

import java.io.Serializable;

public class CfEnvironment implements Serializable {
    private Integer id;

    private String envName;

    private String envDescChn;

    private String envDescEng;

    private String envImg;

    private String envLargeImg;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnvName() {
        return envName;
    }

    public void setEnvName(String envName) {
        this.envName = envName == null ? null : envName.trim();
    }

    public String getEnvDescChn() {
        return envDescChn;
    }

    public void setEnvDescChn(String envDescChn) {
        this.envDescChn = envDescChn == null ? null : envDescChn.trim();
    }

    public String getEnvDescEng() {
        return envDescEng;
    }

    public void setEnvDescEng(String envDescEng) {
        this.envDescEng = envDescEng == null ? null : envDescEng.trim();
    }

    public String getEnvImg() {
        return envImg;
    }

    public void setEnvImg(String envImg) {
        this.envImg = envImg == null ? null : envImg.trim();
    }

    public String getEnvLargeImg() {
        return envLargeImg;
    }

    public void setEnvLargeImg(String envLargeImg) {
        this.envLargeImg = envLargeImg == null ? null : envLargeImg.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CfEnvironment other = (CfEnvironment) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getEnvName() == null ? other.getEnvName() == null : this.getEnvName().equals(other.getEnvName()))
            && (this.getEnvDescChn() == null ? other.getEnvDescChn() == null : this.getEnvDescChn().equals(other.getEnvDescChn()))
            && (this.getEnvDescEng() == null ? other.getEnvDescEng() == null : this.getEnvDescEng().equals(other.getEnvDescEng()))
            && (this.getEnvImg() == null ? other.getEnvImg() == null : this.getEnvImg().equals(other.getEnvImg()))
            && (this.getEnvLargeImg() == null ? other.getEnvLargeImg() == null : this.getEnvLargeImg().equals(other.getEnvLargeImg()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getEnvName() == null) ? 0 : getEnvName().hashCode());
        result = prime * result + ((getEnvDescChn() == null) ? 0 : getEnvDescChn().hashCode());
        result = prime * result + ((getEnvDescEng() == null) ? 0 : getEnvDescEng().hashCode());
        result = prime * result + ((getEnvImg() == null) ? 0 : getEnvImg().hashCode());
        result = prime * result + ((getEnvLargeImg() == null) ? 0 : getEnvLargeImg().hashCode());
        return result;
    }
}
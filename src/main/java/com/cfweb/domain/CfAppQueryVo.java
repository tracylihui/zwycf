package com.cfweb.domain;

import java.util.List;

public class CfAppQueryVo {
	private String org;
	private String space;
	private List<Integer> states;

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getSpace() {
		return space;
	}

	public void setSpace(String space) {
		this.space = space;
	}

	public List<Integer> getStates() {
		return states;
	}

	public void setStates(List<Integer> states) {
		this.states = states;
	}

	public CfAppQueryVo(String org, String space, List<Integer> states) {
		super();
		this.org = org;
		this.space = space;
		this.states = states;
	}

	public CfAppQueryVo() {
	}
}
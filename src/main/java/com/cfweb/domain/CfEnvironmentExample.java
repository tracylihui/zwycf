package com.cfweb.domain;

import java.util.ArrayList;
import java.util.List;

public class CfEnvironmentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CfEnvironmentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEnvNameIsNull() {
            addCriterion("env_name is null");
            return (Criteria) this;
        }

        public Criteria andEnvNameIsNotNull() {
            addCriterion("env_name is not null");
            return (Criteria) this;
        }

        public Criteria andEnvNameEqualTo(String value) {
            addCriterion("env_name =", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameNotEqualTo(String value) {
            addCriterion("env_name <>", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameGreaterThan(String value) {
            addCriterion("env_name >", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameGreaterThanOrEqualTo(String value) {
            addCriterion("env_name >=", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameLessThan(String value) {
            addCriterion("env_name <", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameLessThanOrEqualTo(String value) {
            addCriterion("env_name <=", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameLike(String value) {
            addCriterion("env_name like", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameNotLike(String value) {
            addCriterion("env_name not like", value, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameIn(List<String> values) {
            addCriterion("env_name in", values, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameNotIn(List<String> values) {
            addCriterion("env_name not in", values, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameBetween(String value1, String value2) {
            addCriterion("env_name between", value1, value2, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvNameNotBetween(String value1, String value2) {
            addCriterion("env_name not between", value1, value2, "envName");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnIsNull() {
            addCriterion("env_desc_chn is null");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnIsNotNull() {
            addCriterion("env_desc_chn is not null");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnEqualTo(String value) {
            addCriterion("env_desc_chn =", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnNotEqualTo(String value) {
            addCriterion("env_desc_chn <>", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnGreaterThan(String value) {
            addCriterion("env_desc_chn >", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnGreaterThanOrEqualTo(String value) {
            addCriterion("env_desc_chn >=", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnLessThan(String value) {
            addCriterion("env_desc_chn <", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnLessThanOrEqualTo(String value) {
            addCriterion("env_desc_chn <=", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnLike(String value) {
            addCriterion("env_desc_chn like", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnNotLike(String value) {
            addCriterion("env_desc_chn not like", value, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnIn(List<String> values) {
            addCriterion("env_desc_chn in", values, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnNotIn(List<String> values) {
            addCriterion("env_desc_chn not in", values, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnBetween(String value1, String value2) {
            addCriterion("env_desc_chn between", value1, value2, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescChnNotBetween(String value1, String value2) {
            addCriterion("env_desc_chn not between", value1, value2, "envDescChn");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngIsNull() {
            addCriterion("env_desc_eng is null");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngIsNotNull() {
            addCriterion("env_desc_eng is not null");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngEqualTo(String value) {
            addCriterion("env_desc_eng =", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngNotEqualTo(String value) {
            addCriterion("env_desc_eng <>", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngGreaterThan(String value) {
            addCriterion("env_desc_eng >", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngGreaterThanOrEqualTo(String value) {
            addCriterion("env_desc_eng >=", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngLessThan(String value) {
            addCriterion("env_desc_eng <", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngLessThanOrEqualTo(String value) {
            addCriterion("env_desc_eng <=", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngLike(String value) {
            addCriterion("env_desc_eng like", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngNotLike(String value) {
            addCriterion("env_desc_eng not like", value, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngIn(List<String> values) {
            addCriterion("env_desc_eng in", values, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngNotIn(List<String> values) {
            addCriterion("env_desc_eng not in", values, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngBetween(String value1, String value2) {
            addCriterion("env_desc_eng between", value1, value2, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvDescEngNotBetween(String value1, String value2) {
            addCriterion("env_desc_eng not between", value1, value2, "envDescEng");
            return (Criteria) this;
        }

        public Criteria andEnvImgIsNull() {
            addCriterion("env_img is null");
            return (Criteria) this;
        }

        public Criteria andEnvImgIsNotNull() {
            addCriterion("env_img is not null");
            return (Criteria) this;
        }

        public Criteria andEnvImgEqualTo(String value) {
            addCriterion("env_img =", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgNotEqualTo(String value) {
            addCriterion("env_img <>", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgGreaterThan(String value) {
            addCriterion("env_img >", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgGreaterThanOrEqualTo(String value) {
            addCriterion("env_img >=", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgLessThan(String value) {
            addCriterion("env_img <", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgLessThanOrEqualTo(String value) {
            addCriterion("env_img <=", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgLike(String value) {
            addCriterion("env_img like", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgNotLike(String value) {
            addCriterion("env_img not like", value, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgIn(List<String> values) {
            addCriterion("env_img in", values, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgNotIn(List<String> values) {
            addCriterion("env_img not in", values, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgBetween(String value1, String value2) {
            addCriterion("env_img between", value1, value2, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvImgNotBetween(String value1, String value2) {
            addCriterion("env_img not between", value1, value2, "envImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgIsNull() {
            addCriterion("env_large_img is null");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgIsNotNull() {
            addCriterion("env_large_img is not null");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgEqualTo(String value) {
            addCriterion("env_large_img =", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgNotEqualTo(String value) {
            addCriterion("env_large_img <>", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgGreaterThan(String value) {
            addCriterion("env_large_img >", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgGreaterThanOrEqualTo(String value) {
            addCriterion("env_large_img >=", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgLessThan(String value) {
            addCriterion("env_large_img <", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgLessThanOrEqualTo(String value) {
            addCriterion("env_large_img <=", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgLike(String value) {
            addCriterion("env_large_img like", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgNotLike(String value) {
            addCriterion("env_large_img not like", value, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgIn(List<String> values) {
            addCriterion("env_large_img in", values, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgNotIn(List<String> values) {
            addCriterion("env_large_img not in", values, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgBetween(String value1, String value2) {
            addCriterion("env_large_img between", value1, value2, "envLargeImg");
            return (Criteria) this;
        }

        public Criteria andEnvLargeImgNotBetween(String value1, String value2) {
            addCriterion("env_large_img not between", value1, value2, "envLargeImg");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andEnvNameLikeInsensitive(String value) {
            addCriterion("upper(env_name) like", value.toUpperCase(), "envName");
            return this;
        }

        public Criteria andEnvDescChnLikeInsensitive(String value) {
            addCriterion("upper(env_desc_chn) like", value.toUpperCase(), "envDescChn");
            return this;
        }

        public Criteria andEnvDescEngLikeInsensitive(String value) {
            addCriterion("upper(env_desc_eng) like", value.toUpperCase(), "envDescEng");
            return this;
        }

        public Criteria andEnvImgLikeInsensitive(String value) {
            addCriterion("upper(env_img) like", value.toUpperCase(), "envImg");
            return this;
        }

        public Criteria andEnvLargeImgLikeInsensitive(String value) {
            addCriterion("upper(env_large_img) like", value.toUpperCase(), "envLargeImg");
            return this;
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
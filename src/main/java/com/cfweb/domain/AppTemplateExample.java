package com.cfweb.domain;

import java.util.ArrayList;
import java.util.List;

import com.cfweb.domain.CfUserExample.Criteria;

public class AppTemplateExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AppTemplateExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTemNameIsNull() {
            addCriterion("tem_name is null");
            return (Criteria) this;
        }

        public Criteria andTemNameIsNotNull() {
            addCriterion("tem_name is not null");
            return (Criteria) this;
        }

        public Criteria andTemNameEqualTo(String value) {
            addCriterion("tem_name =", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameNotEqualTo(String value) {
            addCriterion("tem_name <>", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameGreaterThan(String value) {
            addCriterion("tem_name >", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameGreaterThanOrEqualTo(String value) {
            addCriterion("tem_name >=", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameLessThan(String value) {
            addCriterion("tem_name <", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameLessThanOrEqualTo(String value) {
            addCriterion("tem_name <=", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameLike(String value) {
            addCriterion("tem_name like", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameNotLike(String value) {
            addCriterion("tem_name not like", value, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameIn(List<String> values) {
            addCriterion("tem_name in", values, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameNotIn(List<String> values) {
            addCriterion("tem_name not in", values, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameBetween(String value1, String value2) {
            addCriterion("tem_name between", value1, value2, "temName");
            return (Criteria) this;
        }

        public Criteria andTemNameNotBetween(String value1, String value2) {
            addCriterion("tem_name not between", value1, value2, "temName");
            return (Criteria) this;
        }

        public Criteria andEnvIdIsNull() {
            addCriterion("env_id is null");
            return (Criteria) this;
        }

        public Criteria andEnvIdIsNotNull() {
            addCriterion("env_id is not null");
            return (Criteria) this;
        }

        public Criteria andEnvIdEqualTo(Integer value) {
            addCriterion("env_id =", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotEqualTo(Integer value) {
            addCriterion("env_id <>", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdGreaterThan(Integer value) {
            addCriterion("env_id >", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("env_id >=", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdLessThan(Integer value) {
            addCriterion("env_id <", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdLessThanOrEqualTo(Integer value) {
            addCriterion("env_id <=", value, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdIn(List<Integer> values) {
            addCriterion("env_id in", values, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotIn(List<Integer> values) {
            addCriterion("env_id not in", values, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdBetween(Integer value1, Integer value2) {
            addCriterion("env_id between", value1, value2, "envId");
            return (Criteria) this;
        }

        public Criteria andEnvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("env_id not between", value1, value2, "envId");
            return (Criteria) this;
        }

        public Criteria andOrgIsNull() {
            addCriterion("org is null");
            return (Criteria) this;
        }

        public Criteria andOrgIsNotNull() {
            addCriterion("org is not null");
            return (Criteria) this;
        }

        public Criteria andOrgEqualTo(String value) {
            addCriterion("org =", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotEqualTo(String value) {
            addCriterion("org <>", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgGreaterThan(String value) {
            addCriterion("org >", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgGreaterThanOrEqualTo(String value) {
            addCriterion("org >=", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgLessThan(String value) {
            addCriterion("org <", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgLessThanOrEqualTo(String value) {
            addCriterion("org <=", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgLike(String value) {
            addCriterion("org like", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotLike(String value) {
            addCriterion("org not like", value, "org");
            return (Criteria) this;
        }

        public Criteria andOrgIn(List<String> values) {
            addCriterion("org in", values, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotIn(List<String> values) {
            addCriterion("org not in", values, "org");
            return (Criteria) this;
        }

        public Criteria andOrgBetween(String value1, String value2) {
            addCriterion("org between", value1, value2, "org");
            return (Criteria) this;
        }

        public Criteria andOrgNotBetween(String value1, String value2) {
            addCriterion("org not between", value1, value2, "org");
            return (Criteria) this;
        }

        public Criteria andSpaceIsNull() {
            addCriterion("space is null");
            return (Criteria) this;
        }

        public Criteria andSpaceIsNotNull() {
            addCriterion("space is not null");
            return (Criteria) this;
        }

        public Criteria andSpaceEqualTo(String value) {
            addCriterion("space =", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotEqualTo(String value) {
            addCriterion("space <>", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceGreaterThan(String value) {
            addCriterion("space >", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceGreaterThanOrEqualTo(String value) {
            addCriterion("space >=", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceLessThan(String value) {
            addCriterion("space <", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceLessThanOrEqualTo(String value) {
            addCriterion("space <=", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceLike(String value) {
            addCriterion("space like", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotLike(String value) {
            addCriterion("space not like", value, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceIn(List<String> values) {
            addCriterion("space in", values, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotIn(List<String> values) {
            addCriterion("space not in", values, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceBetween(String value1, String value2) {
            addCriterion("space between", value1, value2, "space");
            return (Criteria) this;
        }

        public Criteria andSpaceNotBetween(String value1, String value2) {
            addCriterion("space not between", value1, value2, "space");
            return (Criteria) this;
        }

        public Criteria andDomainIsNull() {
            addCriterion("domain is null");
            return (Criteria) this;
        }

        public Criteria andDomainIsNotNull() {
            addCriterion("domain is not null");
            return (Criteria) this;
        }

        public Criteria andDomainEqualTo(String value) {
            addCriterion("domain =", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainNotEqualTo(String value) {
            addCriterion("domain <>", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainGreaterThan(String value) {
            addCriterion("domain >", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainGreaterThanOrEqualTo(String value) {
            addCriterion("domain >=", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainLessThan(String value) {
            addCriterion("domain <", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainLessThanOrEqualTo(String value) {
            addCriterion("domain <=", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainLike(String value) {
            addCriterion("domain like", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainNotLike(String value) {
            addCriterion("domain not like", value, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainIn(List<String> values) {
            addCriterion("domain in", values, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainNotIn(List<String> values) {
            addCriterion("domain not in", values, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainBetween(String value1, String value2) {
            addCriterion("domain between", value1, value2, "domain");
            return (Criteria) this;
        }

        public Criteria andDomainNotBetween(String value1, String value2) {
            addCriterion("domain not between", value1, value2, "domain");
            return (Criteria) this;
        }

        public Criteria andMemoryIsNull() {
            addCriterion("memory is null");
            return (Criteria) this;
        }

        public Criteria andMemoryIsNotNull() {
            addCriterion("memory is not null");
            return (Criteria) this;
        }

        public Criteria andMemoryEqualTo(Integer value) {
            addCriterion("memory =", value, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryNotEqualTo(Integer value) {
            addCriterion("memory <>", value, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryGreaterThan(Integer value) {
            addCriterion("memory >", value, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryGreaterThanOrEqualTo(Integer value) {
            addCriterion("memory >=", value, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryLessThan(Integer value) {
            addCriterion("memory <", value, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryLessThanOrEqualTo(Integer value) {
            addCriterion("memory <=", value, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryIn(List<Integer> values) {
            addCriterion("memory in", values, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryNotIn(List<Integer> values) {
            addCriterion("memory not in", values, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryBetween(Integer value1, Integer value2) {
            addCriterion("memory between", value1, value2, "memory");
            return (Criteria) this;
        }

        public Criteria andMemoryNotBetween(Integer value1, Integer value2) {
            addCriterion("memory not between", value1, value2, "memory");
            return (Criteria) this;
        }

        public Criteria andDiskIsNull() {
            addCriterion("disk is null");
            return (Criteria) this;
        }

        public Criteria andDiskIsNotNull() {
            addCriterion("disk is not null");
            return (Criteria) this;
        }

        public Criteria andDiskEqualTo(Integer value) {
            addCriterion("disk =", value, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskNotEqualTo(Integer value) {
            addCriterion("disk <>", value, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskGreaterThan(Integer value) {
            addCriterion("disk >", value, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskGreaterThanOrEqualTo(Integer value) {
            addCriterion("disk >=", value, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskLessThan(Integer value) {
            addCriterion("disk <", value, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskLessThanOrEqualTo(Integer value) {
            addCriterion("disk <=", value, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskIn(List<Integer> values) {
            addCriterion("disk in", values, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskNotIn(List<Integer> values) {
            addCriterion("disk not in", values, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskBetween(Integer value1, Integer value2) {
            addCriterion("disk between", value1, value2, "disk");
            return (Criteria) this;
        }

        public Criteria andDiskNotBetween(Integer value1, Integer value2) {
            addCriterion("disk not between", value1, value2, "disk");
            return (Criteria) this;
        }

        public Criteria andInstanceIsNull() {
            addCriterion("instance is null");
            return (Criteria) this;
        }

        public Criteria andInstanceIsNotNull() {
            addCriterion("instance is not null");
            return (Criteria) this;
        }

        public Criteria andInstanceEqualTo(Integer value) {
            addCriterion("instance =", value, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceNotEqualTo(Integer value) {
            addCriterion("instance <>", value, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceGreaterThan(Integer value) {
            addCriterion("instance >", value, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceGreaterThanOrEqualTo(Integer value) {
            addCriterion("instance >=", value, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceLessThan(Integer value) {
            addCriterion("instance <", value, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceLessThanOrEqualTo(Integer value) {
            addCriterion("instance <=", value, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceIn(List<Integer> values) {
            addCriterion("instance in", values, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceNotIn(List<Integer> values) {
            addCriterion("instance not in", values, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceBetween(Integer value1, Integer value2) {
            addCriterion("instance between", value1, value2, "instance");
            return (Criteria) this;
        }

        public Criteria andInstanceNotBetween(Integer value1, Integer value2) {
            addCriterion("instance not between", value1, value2, "instance");
            return (Criteria) this;
        }

        public Criteria andTemImgIsNull() {
            addCriterion("tem_img is null");
            return (Criteria) this;
        }

        public Criteria andTemImgIsNotNull() {
            addCriterion("tem_img is not null");
            return (Criteria) this;
        }

        public Criteria andTemImgEqualTo(String value) {
            addCriterion("tem_img =", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgNotEqualTo(String value) {
            addCriterion("tem_img <>", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgGreaterThan(String value) {
            addCriterion("tem_img >", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgGreaterThanOrEqualTo(String value) {
            addCriterion("tem_img >=", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgLessThan(String value) {
            addCriterion("tem_img <", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgLessThanOrEqualTo(String value) {
            addCriterion("tem_img <=", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgLike(String value) {
            addCriterion("tem_img like", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgNotLike(String value) {
            addCriterion("tem_img not like", value, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgIn(List<String> values) {
            addCriterion("tem_img in", values, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgNotIn(List<String> values) {
            addCriterion("tem_img not in", values, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgBetween(String value1, String value2) {
            addCriterion("tem_img between", value1, value2, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemImgNotBetween(String value1, String value2) {
            addCriterion("tem_img not between", value1, value2, "temImg");
            return (Criteria) this;
        }

        public Criteria andTemColorIsNull() {
            addCriterion("tem_color is null");
            return (Criteria) this;
        }

        public Criteria andTemColorIsNotNull() {
            addCriterion("tem_color is not null");
            return (Criteria) this;
        }

        public Criteria andTemColorEqualTo(String value) {
            addCriterion("tem_color =", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorNotEqualTo(String value) {
            addCriterion("tem_color <>", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorGreaterThan(String value) {
            addCriterion("tem_color >", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorGreaterThanOrEqualTo(String value) {
            addCriterion("tem_color >=", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorLessThan(String value) {
            addCriterion("tem_color <", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorLessThanOrEqualTo(String value) {
            addCriterion("tem_color <=", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorLike(String value) {
            addCriterion("tem_color like", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorNotLike(String value) {
            addCriterion("tem_color not like", value, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorIn(List<String> values) {
            addCriterion("tem_color in", values, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorNotIn(List<String> values) {
            addCriterion("tem_color not in", values, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorBetween(String value1, String value2) {
            addCriterion("tem_color between", value1, value2, "temColor");
            return (Criteria) this;
        }

        public Criteria andTemColorNotBetween(String value1, String value2) {
            addCriterion("tem_color not between", value1, value2, "temColor");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andTemNameLikeInsensitive(String value) {
            addCriterion("upper(tem_name) like", value.toUpperCase(), "temName");
            return this;
        }

        public Criteria andOrgLikeInsensitive(String value) {
            addCriterion("upper(org) like", value.toUpperCase(), "org");
            return this;
        }

        public Criteria andSpaceLikeInsensitive(String value) {
            addCriterion("upper(space) like", value.toUpperCase(), "space");
            return this;
        }

        public Criteria andDomainLikeInsensitive(String value) {
            addCriterion("upper(domain) like", value.toUpperCase(), "domain");
            return this;
        }

        public Criteria andTemImgLikeInsensitive(String value) {
            addCriterion("upper(tem_img) like", value.toUpperCase(), "temImg");
            return this;
        }

        public Criteria andTemColorLikeInsensitive(String value) {
            addCriterion("upper(tem_color) like", value.toUpperCase(), "temColor");
            return this;
        }
        //*************add by 龙立强*********************
        public Criteria andUuidIsNull() {
            addCriterion("uuid is null");
            return (Criteria) this;
        }

        public Criteria andUuidIsNotNull() {
            addCriterion("uuid is not null");
            return (Criteria) this;
        }

        public Criteria andUuidEqualTo(String value) {
            addCriterion("uuid =", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotEqualTo(String value) {
            addCriterion("uuid <>", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThan(String value) {
            addCriterion("uuid >", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidGreaterThanOrEqualTo(String value) {
            addCriterion("uuid >=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThan(String value) {
            addCriterion("uuid <", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLessThanOrEqualTo(String value) {
            addCriterion("uuid <=", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidLike(String value) {
            addCriterion("uuid like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotLike(String value) {
            addCriterion("uuid not like", value, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidIn(List<String> values) {
            addCriterion("uuid in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotIn(List<String> values) {
            addCriterion("uuid not in", values, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidBetween(String value1, String value2) {
            addCriterion("uuid between", value1, value2, "uuid");
            return (Criteria) this;
        }

        public Criteria andUuidNotBetween(String value1, String value2) {
            addCriterion("uuid not between", value1, value2, "uuid");
            return (Criteria) this;
        }
        
        //*************************************************
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
        
       
    }
}
package com.cfweb.util;

public class LogfileWithEE {
	private String fileName;
	private long errorCount;
	private long exceptionCount;
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public long getErrorCount() {
		return errorCount;
	}
	public void setErrorCount(long l) {
		this.errorCount = l;
	}
	public long getExceptionCount() {
		return exceptionCount;
	}
	public void setExceptionCount(long l) {
		this.exceptionCount = l;
	}
	
	
}

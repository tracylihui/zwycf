package com.cfweb.util;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.api.OauthApi;
import com.cfweb.controller.LoginHandler;
import com.cfweb.pojo.OauthToken;

@Service
@Path("/keepConnect")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KeepCennection {
	
	@Autowired  
	private HttpSession session; 

	@Path("/keeping")
	@GET
	public ExtJSResponse keepconnection() {
		try {
			OauthToken token=OauthApi.refreshToken((String)session.getAttribute(LoginHandler.REFRESH_TOKEN),session);
			session.setAttribute(LoginHandler.TOKEN, token.getTokenType() + " " + token.getAccessToken());
			session.setAttribute(LoginHandler.REFRESH_TOKEN, token.getRefreshToken());
		} catch(Exception e) {
			return ExtJSResponse.errorRes();
		}
		return ExtJSResponse.successRes();
	}
	
}

package com.cfweb.util;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class ParamUtil {
	/**
	 * return the body string send by httpclient
	 * @param stringParams
	 * @param intParams
	 * @return
	 */
	public static String getBody(Map<String,String> stringParams,
			Map<String,Integer> intParams) {
		
		int scount = (stringParams != null ? stringParams.size():0);
		int icount = (intParams != null ? intParams.size():0);
		StringBuffer body = new StringBuffer("{");
		if(stringParams != null) {
			Set<Entry<String,String>> stringEntries = stringParams.entrySet();
			for(Map.Entry<String, String> entry : stringEntries) {
				body.append("\"").append(entry.getKey()).append("\":\"")
					.append(entry.getValue()).append("\"");
				if(scount > 1) {
					body.append(",");
					scount--;
				}
			}
		}
		if(scount == 1 && icount >=1) {
			body.append(",");
		}
		if(intParams != null) {
			Set<Entry<String, Integer>> intEntries = intParams.entrySet();
			for (Map.Entry<String, Integer> entry : intEntries) {
				body.append("\"").append(entry.getKey()).append("\":").append(entry.getValue());		
				if(icount > 1) {
					body.append(",");
					icount--;
				}
			}		
		}
		body.append("}");
		return body.toString();
	}

}

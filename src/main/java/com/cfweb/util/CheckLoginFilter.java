package com.cfweb.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CheckLoginFilter implements Filter{
	private static final String USERNAME = "username";
	private static final String CLIENT = "client";
	FilterConfig filterConfig = null;
	private String redirectURL = null;
	private static HttpSession session = null;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		this.redirectURL = filterConfig.getInitParameter("redirectURL");
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, 
			FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		session = request.getSession();
	    String requestType = request.getHeader("X-Requested-With");  
		if(session.getAttribute(CheckLoginFilter.USERNAME) == null
				&& (request.getRequestURI().startsWith("/zwycf/index.html")||request.getRequestURI().equals(request.getContextPath()))) {
			response.sendRedirect(request.getContextPath() + redirectURL);
			return;
		}
		if (checkResource(request)) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}
		
		if(request.getRequestURI().startsWith("/zwycf/rest/login/checkLogin")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;			
		}
		
		if(request.getRequestURI().startsWith("/zwycf/rest/login/getAllCloud")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;			
		}
		if(request.getRequestURI().startsWith("/zwycf/rest/info/allBasicInfo")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;			
		}
		if(request.getRequestURI().startsWith("/zwycf/rest/app/patterns")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;			
		}

		if(session.getAttribute(CheckLoginFilter.USERNAME) == null
				&& requestType != null && requestType.equals("XMLHttpRequest")) {
			response.sendError(401);
			return;
		}
		
		if(session.getAttribute(CheckLoginFilter.USERNAME) == null) {
			response.sendRedirect(request.getContextPath() + redirectURL);
			return;
		}
		
		filterChain.doFilter(servletRequest, servletResponse);
		
	}
	
	private boolean checkResource(HttpServletRequest request) {
		if (request.getRequestURI().toLowerCase().endsWith(".js"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".css"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".png"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".jpg"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".html"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".ttf"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".svg"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".eot"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".woff"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".jpeg"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".gif"))
			return true;
		else if (request.getRequestURI().toLowerCase().endsWith(".ico"))
			return true;
		else if (request.getRequestURI().startsWith("../fonts"))
			return true;
		else 
			return false;
	}

	@Override
	public void destroy() {
		this.filterConfig = null;
		session.removeAttribute(USERNAME);
		session.removeAttribute(CLIENT);
	}

}
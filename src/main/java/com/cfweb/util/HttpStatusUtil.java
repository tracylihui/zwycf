package com.cfweb.util;
/**
 * 
 * @author yufangjiang
 *
 */
public class HttpStatusUtil {
	/**
	 * 2XX表示http请求返回成功
	 * @param statusCode
	 * @return
	 */
	public static boolean isSuccessStatus(int statusCode){
		return String.valueOf(statusCode).startsWith("2");
	}
}

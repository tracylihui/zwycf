//package com.cfweb.util;
//
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpSession;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//import javax.ws.rs.core.MediaType;
//
//import org.apache.log4j.Logger;
//import org.cloudfoundry.client.lib.CloudCredentials;
//import org.cloudfoundry.client.lib.CloudFoundryClient;
//import org.cloudfoundry.client.lib.domain.CloudDomain;
//import org.cloudfoundry.client.lib.domain.CloudInfo;
//import org.cloudfoundry.client.lib.domain.CloudService;
//import org.cloudfoundry.client.lib.domain.CloudSpace;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//@Path("/cloudutil")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class CloudUtil {
//	
//	Logger logger = Logger.getLogger(CloudUtil.class);
//	
////	CloudFoundryClient client = ConnectCF.getClient();
//	@Autowired  
//	private HttpSession session; 
//	
//	/**
//	 * 获取系统domains的全部信息
//	 * @return
//	 */
//	@Path("/domains")
//	@GET
//	public ExtJSResponse getDomains() {
//		List<CloudDomain> domains = null;
//		try {
//			CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
//			domains = client.getDomainsForOrg();
//		} catch(Exception e) {
//			logger.error(e);
//			return ExtJSResponse.errorRes();
//		}
//		return ExtJSResponse.successResWithData(domains);
//	}
//	
//	/**
//	 * 获取系统所有domain的名称
//	 * @return
//	 */
//	@Path("/domainName")
//	@GET
//	public ExtJSResponse getAllDomains() {
//		Map<String,Object>mapRes = new HashMap<String, Object>();
//		try {
//			List<String> alldomains = new ArrayList<String>();
//			CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
//			List<CloudDomain> domains = client.getDomainsForOrg();
//			if(domains != null && domains.size() > 0) {
//				for(CloudDomain d : domains) {
//					alldomains.add(d.getName());
//				}
//			}
//			mapRes.put("domainList", alldomains);
//		} catch(Exception e) {
//			logger.error(e);
//			return ExtJSResponse.errorRes();
//		}
//		return ExtJSResponse.successResWithData(mapRes);
//	}
//	
//	/**
//	 * return domains
//	 * <b>if orgName is not null</b>, return domains of the org
//	 * <b>else</b> return domains of the current org in session
//	 * @param orgName
//	 * @param spaceName
//	 * @return
//	 */
//	@Path("/domain")
//	@GET
//	public ExtJSResponse getDomainsOfOrg(@QueryParam("orgName") final String orgName,
//			@QueryParam("spaceName") String spaceName) {
//		List<CloudDomain> list = null;
//		try {
//			if(orgName == null) {
//				CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
//				list = client.getDomainsForOrg();  
//				return ExtJSResponse.successResWithData(list);
//			}
//			
//			String user = (String)session.getAttribute(LoginHandler.USERNAME);
//			String password = (String)session.getAttribute(LoginHandler.PASSWORD);
//			CloudCredentials credentials = new CloudCredentials(user, password);
//			URL target = (URL)session.getAttribute(LoginHandler.TARGET);
//			if(spaceName == null) {
//				CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
//				for (CloudSpace s : client.getSpaces()){
//					if (s.getOrganization().getName().equals(orgName)) {
//						spaceName = s.getName();
//						break;
//					}
//				}
//			}
//			CloudFoundryClient client = new CloudFoundryClient(credentials,
//					target,orgName,spaceName);
//			list = client.getDomainsForOrg();
//		} catch(Exception e) {
//			logger.error(e);
//			return ExtJSResponse.errorRes();
//		}
//		return ExtJSResponse.successResWithData(list);
//	}
//	
//	@Path("/services")
//	@GET
//	public ExtJSResponse getServices() {
//		List<CloudService> services = null;
//		try {
//			CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
//			services = client.getServices();
//		} catch(Exception e) {
//			logger.error(e);
//			return ExtJSResponse.errorRes();
//		}
//		return ExtJSResponse.successResWithData(services);
//	}
//	
//	@Path("/limits")
//	@GET
//	public ExtJSResponse getLimits(@QueryParam("orgName") final String orgName) {
//		CloudInfo cloudInfo = null;
//		try {
//			CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
//			cloudInfo = client.getCloudInfo();
//		} catch(Exception e) {
//			logger.error(e);
//			return ExtJSResponse.errorRes();
//		}
//		return ExtJSResponse.successResWithData(cloudInfo);
//	}
//	
//
//}

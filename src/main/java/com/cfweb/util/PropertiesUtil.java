package com.cfweb.util;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * <p>
 * 读取公共配置的工具类
 * </p>
 * 
 * @author ShaoYu
 *
 */
public final class PropertiesUtil {

    private static PropertiesConfiguration configuration;

    // 迁移至AWS时注意核查
    static {
        // loadProperties();
        try {
            configuration = new PropertiesConfiguration("nullsb.properties");
            configuration.setAutoSave(true);
        } catch (ConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private PropertiesUtil() {

    }
    /**
     * <p>
     * 获取 <code>key</code> 对应的值
     * </p>
     * 
     * @param key
     *            配置文件的键
     * @return 配置文件的值
     */
    public static String getProperty(String key) {
        return configuration.getString(key);
    }

    /**
     * <p>
     * 获取 <code>key</code> 对应的值,如果值不存在，则返回默认值
     * </p>
     * 
     * @param key
     *            配置文件对应的键
     * @param defaultValue
     *            默认值
     * @return
     */
    public static String getProperty(String key, String defaultValue) {
        return configuration.getString(key, defaultValue);
    }
}

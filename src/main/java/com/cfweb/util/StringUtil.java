package com.cfweb.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class StringUtil {
	public static boolean isNotNullString(String str){
		return !isNullString(str);
	}
	
	public static boolean isNullString(String str) {
		return null == str || str.length() == 0 || ("null").equals(str);
	}
	
	/**
	 * 空值检测
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return null == str || str.length() == 0;
	}
	
	/**
	 * 非空检测
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}
	
	/**
	 * 空格检测
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str) {
        if(StringUtil.isEmpty(str)) {
            return true;
        }
        for(int i = 0; i < str.length(); i++ ) {
            if(!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
	}
	
	/**
	 * 无空格检测
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNotBlank(String str) {
		return !isNotBlank(str);
	}
	
	/**
	 * 将整数组成的字符串转换为整数列表
	 * 
	 * 3,4,5 -> {3, 4, 5}
	 * 
	 * @param str 字符串
	 * @return
	 */
	public static List<Integer> str2IntList(String str) throws Exception{
		String[] strArrays = str.split(",");
		List<Integer> intList = new ArrayList<Integer>();
		for(String strArray:strArrays){
			intList.add(Integer.valueOf(strArray));
		}
		return intList;
	}
	
	public static String toDateString(String timeString) {
		SimpleDateFormat df = new SimpleDateFormat("MM-dd HH:mm:ss");//定义格式，不显示毫秒
		long timeLong = Long.parseLong(timeString);
		timeLong-=28800000;
		Timestamp timestamp = new Timestamp(timeLong);//获取系统当前时间
		String dateString = df.format(timestamp);
		return dateString;
	}
	
}
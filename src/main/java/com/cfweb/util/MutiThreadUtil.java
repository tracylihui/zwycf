package com.cfweb.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MutiThreadUtil {


    private static BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(128);
    private static ThreadPoolExecutor executorService = 
			new ThreadPoolExecutor(10, 50, 2L, TimeUnit.SECONDS, workQueue );
    
	public static BlockingQueue<Runnable> getWorkQueue() {
		return workQueue;
	}

	public static void setWorkQueue(BlockingQueue<Runnable> workQueue) {
		MutiThreadUtil.workQueue = workQueue;
	}

	public static ThreadPoolExecutor getExecutorService() {
		return executorService;
	}

	public static void setExecutorService(ThreadPoolExecutor executorService) {
		MutiThreadUtil.executorService = executorService;
	}

}
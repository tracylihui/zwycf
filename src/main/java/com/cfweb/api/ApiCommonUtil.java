package com.cfweb.api;

import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.cfweb.controller.LoginHandler;
import com.cfweb.util.CloudFoundryClientUtil;
import com.cfweb.util.JsonUtil;

/**
 * 
 * @author (yufangjiang) Dec 2, 2014
 */
public class ApiCommonUtil {
  /**
   * 获取一页数据
   * 
   * @param lr
   * @param nextUrl
   * @param session
   * @return 下一页数据的url
   * @throws Exception
   */
  @SuppressWarnings("unchecked")
  public static String retriveAllPageResource(List<Map<String, Object>> lr, String nextUrl,
      HttpSession session) throws Exception {
    String host = ((URL) session.getAttribute(LoginHandler.TARGET)).toString();
    nextUrl = host + nextUrl;
    String nextReturnUrl = null;
    String content = CloudFoundryClientUtil.doCCGet(nextUrl, null, session);
    Map<String, Object> data = JsonUtil.convertJsonToMap(content);
    lr.addAll((List<Map<String, Object>>) data.get("resources"));
    nextReturnUrl = (String) data.get("next_url");
    return nextReturnUrl;
  }
}

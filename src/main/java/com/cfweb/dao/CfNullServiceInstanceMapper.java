package com.cfweb.dao;

import com.cfweb.domain.CfNullServiceInstance;
import com.cfweb.domain.CfNullServiceInstanceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfNullServiceInstanceMapper {
    int countByExample(CfNullServiceInstanceExample example);

    int deleteByExample(CfNullServiceInstanceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfNullServiceInstance record);

    int insertSelective(CfNullServiceInstance record);

    List<CfNullServiceInstance> selectByExample(CfNullServiceInstanceExample example);

    CfNullServiceInstance selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfNullServiceInstance record, @Param("example") CfNullServiceInstanceExample example);

    int updateByExample(@Param("record") CfNullServiceInstance record, @Param("example") CfNullServiceInstanceExample example);

    int updateByPrimaryKeySelective(CfNullServiceInstance record);

    int updateByPrimaryKey(CfNullServiceInstance record);
}
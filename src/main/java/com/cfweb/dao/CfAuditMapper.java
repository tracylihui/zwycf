package com.cfweb.dao;

import com.cfweb.domain.CfAudit;
import com.cfweb.domain.CfAuditExample;
import com.cfweb.domain.CfAuditExternal;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfAuditMapper {
	int countByExample(CfAuditExample example);

	int deleteByExample(CfAuditExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(CfAudit record);

	int insertSelective(CfAudit record);

	List<CfAudit> selectByExampleWithBLOBs(CfAuditExample example);

	List<CfAudit> selectByExample(CfAuditExample example);

	CfAudit selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") CfAudit record, @Param("example") CfAuditExample example);

	int updateByExampleWithBLOBs(@Param("record") CfAudit record, @Param("example") CfAuditExample example);

	int updateByExample(@Param("record") CfAudit record, @Param("example") CfAuditExample example);

	int updateByPrimaryKeySelective(CfAudit record);

	int updateByPrimaryKeyWithBLOBs(CfAudit record);

	int updateByPrimaryKey(CfAudit record);

	List<CfAuditExternal> getAll();

	List<CfAuditExternal> getByOrgAndSpace(@Param("org") String org, @Param("space") String space);
}
package com.cfweb.dao;

import com.cfweb.domain.AppLogPatterns;
import com.cfweb.domain.AppLogPatternsExample;
import com.cfweb.domain.LogPattern;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface AppLogPatternsMapper {
    int countByExample(AppLogPatternsExample example);

    int deleteByExample(AppLogPatternsExample example);

    int insert(AppLogPatterns record);

    int insertSelective(AppLogPatterns record);

    List<AppLogPatterns> selectByExample(AppLogPatternsExample example);

    int updateByExampleSelective(@Param("record") AppLogPatterns record, @Param("example") AppLogPatternsExample example);

    int updateByExample(@Param("record") AppLogPatterns record, @Param("example") AppLogPatternsExample example);
    
    List<LogPattern> getAppPatters(@Param("appId") String appId);
}
package com.cfweb.dao;

import com.cfweb.domain.CfUser;
import com.cfweb.domain.CfUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfUserMapper {
    int countByExample(CfUserExample example);

    int deleteByExample(CfUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfUser record);

    int insertSelective(CfUser record);

    List<CfUser> selectByExample(CfUserExample example);

    CfUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfUser record, @Param("example") CfUserExample example);

    int updateByExample(@Param("record") CfUser record, @Param("example") CfUserExample example);

    int updateByPrimaryKeySelective(CfUser record);

    int updateByPrimaryKey(CfUser record);
}
package com.cfweb.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cfweb.domain.CfApp;
import com.cfweb.domain.CfAppExample;
import com.cfweb.domain.CfAppExternal;
import com.cfweb.domain.CfAppQueryVo;

public interface CfAppMapper {
	int countByExample(CfAppExample example);

	int deleteByExample(CfAppExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(CfApp record);

	int insertSelective(CfApp record);

	List<CfApp> selectByExample(CfAppExample example);

	CfApp selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") CfApp record, @Param("example") CfAppExample example);

	int updateByExample(@Param("record") CfApp record, @Param("example") CfAppExample example);

	int updateByPrimaryKeySelective(CfApp record);

	int updateByPrimaryKey(CfApp record);

	CfApp getAppIdByAppName(@Param("appName") String appName);

	List<CfAppExternal> getCfAppsByParams(@Param("org") String org, @Param("space") String space);

	List<CfAppExternal> getCfAppsByParamsWithState(CfAppQueryVo cfAppQueryVo);
}
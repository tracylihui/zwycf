package com.cfweb.dao;

import com.cfweb.domain.AppLogConfig;
import com.cfweb.domain.AppLogConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AppLogConfigMapper {
    int countByExample(AppLogConfigExample example);

    int deleteByExample(AppLogConfigExample example);

    int deleteByPrimaryKey(Integer appId);

    int insert(AppLogConfig record);

    int insertSelective(AppLogConfig record);

    List<AppLogConfig> selectByExampleWithBLOBs(AppLogConfigExample example);

    List<AppLogConfig> selectByExample(AppLogConfigExample example);

    AppLogConfig selectByPrimaryKey(Integer appId);

    int updateByExampleSelective(@Param("record") AppLogConfig record, @Param("example") AppLogConfigExample example);

    int updateByExampleWithBLOBs(@Param("record") AppLogConfig record, @Param("example") AppLogConfigExample example);

    int updateByExample(@Param("record") AppLogConfig record, @Param("example") AppLogConfigExample example);

    int updateByPrimaryKeySelective(AppLogConfig record);

    int updateByPrimaryKeyWithBLOBs(AppLogConfig record);
}
package com.cfweb.dao;

import com.cfweb.domain.CfServiceInstance;
import com.cfweb.domain.CfServiceInstanceExample;
import com.cfweb.pojo.CfServiceInstanceWithType;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CfServiceInstanceMapper {
    int countByExample(CfServiceInstanceExample example);

    int deleteByExample(CfServiceInstanceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfServiceInstance record);

    int insertSelective(CfServiceInstance record);

    List<CfServiceInstance> selectByExample(CfServiceInstanceExample example);

    CfServiceInstance selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfServiceInstance record, @Param("example") CfServiceInstanceExample example);

    int updateByExample(@Param("record") CfServiceInstance record, @Param("example") CfServiceInstanceExample example);

    int updateByPrimaryKeySelective(CfServiceInstance record);

    int updateByPrimaryKey(CfServiceInstance record);

	List<CfServiceInstanceWithType> selectServiceByParams(@Param("category")Integer cat,@Param("subCategoryId")Integer subcat,@Param("org")String org,@Param("space")String space);
	List<CfServiceInstanceWithType> selectAppService(@Param("serviceNames")List<String> serviceNames);
	
	int selectIdByName(@Param("service_instance_name")String service_instance_name,@Param("service_type_id")Integer service_type_id);
}
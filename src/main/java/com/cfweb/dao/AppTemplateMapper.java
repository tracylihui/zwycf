package com.cfweb.dao;

import com.cfweb.domain.AppTemplate;
import com.cfweb.domain.AppTemplateExample;
import com.cfweb.domain.TplDetail;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AppTemplateMapper {
    int countByExample(AppTemplateExample example);

    int deleteByExample(AppTemplateExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AppTemplate record);

    int insertSelective(AppTemplate record);

    List<AppTemplate> selectByExample(AppTemplateExample example);

    AppTemplate selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AppTemplate record, @Param("example") AppTemplateExample example);

    int updateByExample(@Param("record") AppTemplate record, @Param("example") AppTemplateExample example);

    int updateByPrimaryKeySelective(AppTemplate record);

    int updateByPrimaryKey(AppTemplate record);

	List<TplDetail> selectDetail();
}
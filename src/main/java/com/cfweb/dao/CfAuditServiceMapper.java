package com.cfweb.dao;

import com.cfweb.domain.CfAuditService;
import com.cfweb.domain.CfAuditServiceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfAuditServiceMapper {
    int countByExample(CfAuditServiceExample example);

    int deleteByExample(CfAuditServiceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfAuditService record);

    int insertSelective(CfAuditService record);

    List<CfAuditService> selectByExample(CfAuditServiceExample example);

    CfAuditService selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfAuditService record, @Param("example") CfAuditServiceExample example);

    int updateByExample(@Param("record") CfAuditService record, @Param("example") CfAuditServiceExample example);

    int updateByPrimaryKeySelective(CfAuditService record);

    int updateByPrimaryKey(CfAuditService record);
}
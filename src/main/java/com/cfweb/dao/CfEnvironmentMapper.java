package com.cfweb.dao;

import com.cfweb.domain.CfEnvironment;
import com.cfweb.domain.CfEnvironmentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfEnvironmentMapper {
    int countByExample(CfEnvironmentExample example);

    int deleteByExample(CfEnvironmentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfEnvironment record);

    int insertSelective(CfEnvironment record);

    List<CfEnvironment> selectByExample(CfEnvironmentExample example);

    CfEnvironment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfEnvironment record, @Param("example") CfEnvironmentExample example);

    int updateByExample(@Param("record") CfEnvironment record, @Param("example") CfEnvironmentExample example);

    int updateByPrimaryKeySelective(CfEnvironment record);

    int updateByPrimaryKey(CfEnvironment record);
}
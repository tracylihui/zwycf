package com.cfweb.dao;

import com.cfweb.domain.LogPattern;
import com.cfweb.domain.LogPatternExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LogPatternMapper {
    int countByExample(LogPatternExample example);

    int deleteByExample(LogPatternExample example);

    int deleteByPrimaryKey(Integer patternId);

    int insert(LogPattern record);

    int insertSelective(LogPattern record);

    List<LogPattern> selectByExample(LogPatternExample example);

    LogPattern selectByPrimaryKey(Integer patternId);

    int updateByExampleSelective(@Param("record") LogPattern record, @Param("example") LogPatternExample example);

    int updateByExample(@Param("record") LogPattern record, @Param("example") LogPatternExample example);

    int updateByPrimaryKeySelective(LogPattern record);

    int updateByPrimaryKey(LogPattern record);
}
package com.cfweb.dao;

import com.cfweb.domain.CfServiceBroker;
import com.cfweb.domain.CfServiceBrokerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfServiceBrokerMapper {
    int countByExample(CfServiceBrokerExample example);

    int deleteByExample(CfServiceBrokerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfServiceBroker record);

    int insertSelective(CfServiceBroker record);

    List<CfServiceBroker> selectByExample(CfServiceBrokerExample example);

    CfServiceBroker selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfServiceBroker record, @Param("example") CfServiceBrokerExample example);

    int updateByExample(@Param("record") CfServiceBroker record, @Param("example") CfServiceBrokerExample example);

    int updateByPrimaryKeySelective(CfServiceBroker record);

    int updateByPrimaryKey(CfServiceBroker record);
    
    List<String> selectByServiceId(Integer id);
}
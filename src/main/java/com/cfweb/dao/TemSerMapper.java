package com.cfweb.dao;

import com.cfweb.domain.TemSer;
import com.cfweb.domain.TemSerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TemSerMapper {
    int countByExample(TemSerExample example);

    int deleteByExample(TemSerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TemSer record);

    int insertSelective(TemSer record);

    List<TemSer> selectByExample(TemSerExample example);

    TemSer selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TemSer record, @Param("example") TemSerExample example);

    int updateByExample(@Param("record") TemSer record, @Param("example") TemSerExample example);

    int updateByPrimaryKeySelective(TemSer record);

    int updateByPrimaryKey(TemSer record);
}
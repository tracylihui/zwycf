package com.cfweb.dao;

import com.cfweb.domain.CfTarget;
import com.cfweb.domain.CfTargetExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfTargetMapper {
    int countByExample(CfTargetExample example);

    int deleteByExample(CfTargetExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfTarget record);

    int insertSelective(CfTarget record);

    List<CfTarget> selectByExample(CfTargetExample example);

    CfTarget selectByPrimaryKey(Integer id);
    CfTarget selectByTarget(String target);

    int updateByExampleSelective(@Param("record") CfTarget record, @Param("example") CfTargetExample example);

    int updateByExample(@Param("record") CfTarget record, @Param("example") CfTargetExample example);

    int updateByPrimaryKeySelective(CfTarget record);

    int updateByPrimaryKey(CfTarget record);
}
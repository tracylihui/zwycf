package com.cfweb.dao;

import com.cfweb.domain.PatternField;
import com.cfweb.domain.PatternFieldExample;

import java.util.List;

import javax.ws.rs.QueryParam;

import org.apache.ibatis.annotations.Param;

public interface PatternFieldMapper {
    int countByExample(PatternFieldExample example);

    int deleteByExample(PatternFieldExample example);

    int insert(PatternField record);

    int insertSelective(PatternField record);

    List<PatternField> selectByExample(PatternFieldExample example);

    int updateByExampleSelective(@Param("record") PatternField record, @Param("example") PatternFieldExample example);

    int updateByExample(@Param("record") PatternField record, @Param("example") PatternFieldExample example);
    
    List<PatternField> getPatternFields(@QueryParam("appId") int appId);
}
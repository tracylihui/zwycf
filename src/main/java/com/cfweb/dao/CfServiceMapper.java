package com.cfweb.dao;

import com.cfweb.domain.CfService;
import com.cfweb.domain.CfServiceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CfServiceMapper {
    int countByExample(CfServiceExample example);

    int deleteByExample(CfServiceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CfService record);

    int insertSelective(CfService record);

    List<CfService> selectByExample(CfServiceExample example);

    CfService selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CfService record, @Param("example") CfServiceExample example);

    int updateByExample(@Param("record") CfService record, @Param("example") CfServiceExample example);

    int updateByPrimaryKeySelective(CfService record);

    int updateByPrimaryKey(CfService record);
    
    int selectIdByName(String name);

}
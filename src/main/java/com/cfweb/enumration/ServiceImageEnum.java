package com.cfweb.enumration;

public enum ServiceImageEnum {
	LOG_SERVICE("Log","resources/img/icons/service/large/icon_logService.png"),
	ORACLE_SERVICE("Oracle","resources/img/icons/service/large/icon_oracle.png"),
	OTHER_SERVICE("","resources/img/icons/service/large/icon_unknow.png");
	private String serviceName;
	private String serviceImage;
	
	
	private ServiceImageEnum(String serviceName, String serviceImage) {
		this.serviceName = serviceName;
		this.serviceImage = serviceImage;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceImage() {
		return serviceImage;
	}
	public void setServiceImage(String serviceImage) {
		this.serviceImage = serviceImage;
	}
	
	public static String getImage(String serviceName) {
		for(ServiceImageEnum serviceEnum:ServiceImageEnum.values()){
			if(serviceName.toLowerCase().startsWith(serviceEnum.getServiceName().toLowerCase())){
				return serviceEnum.getServiceImage();
			}
		}
		return OTHER_SERVICE.getServiceImage();
	}
}

package com.cfweb.controller.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.dao.AppLogConfigMapper;
import com.cfweb.dao.CfAppMapper;
import com.cfweb.dao.PatternFieldMapper;
import com.cfweb.domain.AppLogConfig;
import com.cfweb.domain.AppLogConfigExample;
import com.cfweb.domain.CfApp;
import com.cfweb.domain.PatternField;
import com.cfweb.util.ExtJSResponse;

@Service
@Path("/app")
@Produces(MediaType.APPLICATION_JSON)

public class LogConfigService {
	@Autowired
	private AppLogConfigMapper appConfigMapper;
	
	@Autowired
	private PatternFieldMapper pfMapper;
 
	@Autowired
	private CfAppMapper cfAppMapper;
	
	@POST
	@Path("/setconfigs")
	
	public ExtJSResponse setAppLogConfigs(@FormParam("appId") Integer appid,
			@FormParam("logConfig") String[] logconfig){
		try {
		    AppLogConfig appLogConfig = new AppLogConfig();
		    List<AppLogConfig> lLogC = new ArrayList<AppLogConfig>();
			AppLogConfigExample logConfEx = new AppLogConfigExample();
			if(appid !=null){
				logConfEx.createCriteria().andAppIdEqualTo(appid);
			}
			lLogC = appConfigMapper.selectByExample(logConfEx);
			if(lLogC.size()==0){
				for(int i = 0;i<logconfig.length;i++){
			    	appLogConfig.setAppId(appid);
				    appLogConfig.setConfigJson(logconfig[i]);
				    appConfigMapper.insert(appLogConfig);
			    }
			}else{
				appConfigMapper.deleteByExample(logConfEx);
				for(int i = 0;i<logconfig.length;i++){
			    	appLogConfig.setAppId(appid);
			    	System.out.println(logconfig[i]);
				    appLogConfig.setConfigJson(logconfig[i]);
				    appConfigMapper.insert(appLogConfig);
			    }
			}
		    return ExtJSResponse.successRes();	
		   }catch(Exception e){
			   return ExtJSResponse.errorRes();
		}
	}
	

	@GET
	@Path("/getconfigs")
	public ExtJSResponse getAppLogConfigs(@QueryParam("appId") Integer appid){
		try{
			List<AppLogConfig> lLogC = new ArrayList<AppLogConfig>();
			AppLogConfigExample logConfEx = new AppLogConfigExample();
			if(appid !=null){
				logConfEx.createCriteria().andAppIdEqualTo(appid);
			}
			lLogC = appConfigMapper.selectByExampleWithBLOBs(logConfEx);
			return ExtJSResponse.successResWithData(lLogC);
		}catch (Exception e) {
			return ExtJSResponse.errorRes();
		}
		
	}
	
	@GET
	@Path("/getFields")
	public ExtJSResponse getAppLogFields(@QueryParam("appId") int appId){
		List<PatternField> patternfields=pfMapper.getPatternFields(appId);
		return ExtJSResponse.successResWithData(patternfields);
	}
	
	
	@Path("/getAppId")
	@GET
	public ExtJSResponse getAppId(@QueryParam("appName") String appName){
		CfApp cfApp = new CfApp();
		cfApp = cfAppMapper.getAppIdByAppName(appName);
		int appId = cfApp.getId();
		return ExtJSResponse.successResWithData(appId);
	}
	
	

}

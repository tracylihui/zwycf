package com.cfweb.controller.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.dao.AppLogPatternsMapper;
import com.cfweb.domain.LogPattern;

@Service
@Path("/app")
@Produces(MediaType.APPLICATION_JSON)
public class PatterService {
	@Autowired
	private AppLogPatternsMapper appPatternMapper;
	
	@GET
	@Path("/patterns")
	public Response getAppPatterns(@QueryParam("appId") String appId){
		List<LogPattern> response=appPatternMapper.getAppPatters(appId);
		if (response.size()==1&&response.get(0)==null) {
			response.clear();
		}
		return Response.status(Response.Status.OK).entity(response).build();
	}
}

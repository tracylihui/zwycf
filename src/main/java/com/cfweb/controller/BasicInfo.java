package com.cfweb.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.api.BasicInfoApi;
import com.cfweb.dao.CfEnvironmentMapper;
import com.cfweb.dao.CfUserMapper;
import com.cfweb.domain.CfEnvironment;
import com.cfweb.domain.CfEnvironmentExample;
import com.cfweb.util.ExtJSResponse;
/**
 * 
 * @author (mianxianliu)
 * Dec 2, 2014
 */
@Service
@Path("/info")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BasicInfo {
//	CloudFoundryClient client = ConnectCF.getClient();
	Logger logger = Logger.getLogger(BasicInfo.class);

	@Autowired  
	private HttpSession session; 
	
	@Autowired
	CfUserMapper cfuserMapper;
	@Autowired
	CfEnvironmentMapper cfenvMapper;
	
	@Path("/basicInfo")
	@GET
	public ExtJSResponse getInfos() {
		Map<String, Object> resMap = null;
		try{
	 		resMap = BasicInfoApi.getBasicInfo(session, cfuserMapper);
		}
		catch(Exception e){
			logger.error(e);
			System.err.println(e);
		}
		return ExtJSResponse.successResWithMap(resMap);
		
	}
	@Path("/allBasicInfo")
	@GET
	public ExtJSResponse getAllBasicInfo() {
		Map<String, Object> resMap = null;
		try{
			//TODO
	 		resMap = BasicInfoApi.getAllBasicInfo(session);
		}
		catch(Exception e){
			logger.error(e);
			System.err.println(e);
		}
		return ExtJSResponse.successResWithMap(resMap);
		
	}
	
	@Path("/cfEnv")
	@GET
	public ExtJSResponse getCfEnv(){
		ExtJSResponse resp = new ExtJSResponse(true);
		CfEnvironmentExample example = new CfEnvironmentExample();
		example.createCriteria().andIdIsNotNull();
		List<CfEnvironment> lce = cfenvMapper.selectByExample(example);
		resp.put("data", lce);
		return resp;
	}
	
	@Path("/cfEnvDetail")
	@GET
	public ExtJSResponse getCfEnvDetail(@QueryParam("id") int id){
		ExtJSResponse resp = new ExtJSResponse(true);
		CfEnvironmentExample example = new CfEnvironmentExample();
		example.createCriteria().andIdEqualTo(id);
		List<CfEnvironment> lce = cfenvMapper.selectByExample(example);
		resp.put("data", lce.get(0));
		return resp;
	}
	
	
}

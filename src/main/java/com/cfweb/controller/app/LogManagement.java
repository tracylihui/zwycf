package com.cfweb.controller.app;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.deletebyquery.DeleteByQueryRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.QuerySourceBuilder;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.ClusterState;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogram;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogram.Interval;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Order;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgBuilder;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.aggregations.metrics.max.MaxBuilder;
import org.elasticsearch.search.aggregations.metrics.min.Min;
import org.elasticsearch.search.aggregations.metrics.min.MinBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.controller.LoginHandler;
import com.cfweb.dao.AppLogPatternsMapper;
import com.cfweb.dao.CfAppMapper;
import com.cfweb.dao.LogPatternMapper;
import com.cfweb.domain.CfAppExternal;
import com.cfweb.pojo.AppAnalayseData;
import com.cfweb.util.ConnectES;
import com.cfweb.util.ExtJSResponse;
import com.cfweb.util.LogfileWithEE;
import com.cfweb.util.LogsForCount;
import com.cfweb.util.LogsWithMMA;
import com.cfweb.util.StringUtil;

/**
 * 应用日志
 * 
 * @author (yufangjiang) Dec 2, 2014
 */
@Service
@Path("/log")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LogManagement {
  /**
   * LOGGER
   */
  private static Logger logger = LoggerFactory.getLogger(LogManagement.class);
  /**
   * session
   */
  @Autowired
  private HttpSession session;
  /**
   * app
   */
  @Autowired
  private CfAppMapper cfAppMapper;
  /**
   * app log pattern
   */
  @Autowired
  private AppLogPatternsMapper alpMapper;
  /**
   * log pattern
   */
  @Autowired
  private LogPatternMapper lpMapper;
  
  @Autowired
  private CfAppMapper caMapper;
  
  @Autowired
  private ConnectES esUtil;

  /**
   * 获取应用日志文件
   * 
   * @param page
   * @param start
   * @param limit
   * @param appName
   * @param from
   * @param to
   * @param key
   * @return
   */
  @Path("/list")
  @GET
  public ExtJSResponse list(final @QueryParam("page") String page,
      final @QueryParam("start") String start, final @QueryParam("limit") String limit,
      final @QueryParam("appName") String appName, final @QueryParam("from") String from,
      final @QueryParam("to") String to, @QueryParam("key") String key) {

    Client client = esUtil.getClient();

    List<String> appfilesList = new ArrayList<String>();
    List<Object> filesList = new ArrayList<Object>();
    /* 获取appfiles */
    QueryBuilder appQuery = QueryBuilders.matchQuery("app", appName);
    BoolQueryBuilder bQuery = QueryBuilders.boolQuery();
    bQuery.must(appQuery);
    if (StringUtil.isNotEmpty(from) && StringUtil.isNotEmpty(to)) {
      bQuery.must(QueryBuilders.rangeQuery("timestamp").from(from).to(to));
    }

    if (StringUtil.isNotEmpty(key)) {
      bQuery.must(QueryBuilders.wildcardQuery("message", "*" + key.toLowerCase() + "*"));
    }
    Order order = Order.term(true);
    AggregationBuilder<?> aggregation = AggregationBuilders.terms("appfiles").field("fileName")
        .size(0).order(order);
    SearchResponse response = client.prepareSearch("logfiles").setQuery(bQuery)
        .addAggregation(aggregation).addSort("line", SortOrder.ASC)
        .addSort("timestamp", SortOrder.ASC).setSize(0).execute().actionGet();

    Terms terms = response.getAggregations().get("appfiles");
    for (Bucket b : terms.getBuckets()) {
      appfilesList.add(b.getKey());
    }
    System.out.println("getfiles");
    for (String file : appfilesList) {
      System.out.println(file);
      LogfileWithEE fileWithEE = new LogfileWithEE();
      fileWithEE.setFileName(file);
      QueryBuilder eeQueryBuilder = QueryBuilders.termQuery("fileName", file);
      AggregationBuilder<?> errorAggregation = AggregationBuilders.terms("errors").field("message")
          .include(".*error.*");
      AggregationBuilder<?> exceptionAggregation = AggregationBuilders.terms("exceptions")
          .field("message").include(".*exception.*");
      SearchResponse searchResponse = client.prepareSearch("logfiles").setQuery(eeQueryBuilder)
          .addAggregation(errorAggregation).addAggregation(exceptionAggregation).execute()
          .actionGet();
      System.out.println("geterrorcount");
      Terms errorTerm = searchResponse.getAggregations().get("errors");
      long errors = 0;
      for (Bucket b : errorTerm.getBuckets()) {
        errors = errors + b.getDocCount();
      }
      fileWithEE.setErrorCount(errors);
      Terms exceptionTerm = searchResponse.getAggregations().get("exceptions");
      long exception = 0;
      for (Bucket b : exceptionTerm.getBuckets()) {
        exception = exception + b.getDocCount();
      }
      fileWithEE.setExceptionCount(exception);
      filesList.add(fileWithEE);
    }
    return ExtJSResponse.successResWithData(filesList);
  }

  /**
   * 统计应用的error exception
   * 
   * @param app
   * @return
   */
  public int listApp(String app) {

	  Client client = esUtil.getClient();

    QueryBuilder eeQueryBuilder = QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery("serviceName", app))
        .must(QueryBuilders.termQuery("unread", 1));
    AggregationBuilder<?> errorAggregation = AggregationBuilders.terms("errors").field("eventType")
        .include("ERROR");
    AggregationBuilder<?> exceptionAggregation = AggregationBuilders.terms("exceptions")
        .field("eventType").include("EXCEPTION");
    SearchResponse searchResponse = client.prepareSearch("logs").setQuery(eeQueryBuilder)
        .addAggregation(errorAggregation).addAggregation(exceptionAggregation).execute()
        .actionGet();

    Terms errorTerm = searchResponse.getAggregations().get("errors");
    int errorCount = 0;
    for (Bucket b : errorTerm.getBuckets()) {
      errorCount += b.getDocCount();
    }
    Terms exceptionTerm = searchResponse.getAggregations().get("exceptions");
    int exceptionCount = 0;
    for (Bucket b : exceptionTerm.getBuckets()) {
      exceptionCount += b.getDocCount();
    }

    return errorCount + exceptionCount;
  }

  /**
   * 把已读消息进行标志
   * 
   * @param app
   * @return
   */
  @Path("/markRead")
  @GET
  public ExtJSResponse markRead(final @QueryParam("appName") String app) {
	  Client client = esUtil.getClient();

    BulkRequest request = new BulkRequest();

    LogfileWithEE fileWithEE = new LogfileWithEE();
    fileWithEE.setFileName(app);
    QueryBuilder EEQueryBuilder = QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery("serviceName", app))
        .must(QueryBuilders.termQuery("unread", 1));
    AggregationBuilder<?> errorAggregation = AggregationBuilders.terms("errors").field("eventType")
        .include("ERROR");
    AggregationBuilder<?> exceptionAggregation = AggregationBuilders.terms("exceptions")
        .field("eventType").include("EXCEPTION");
    SearchResponse searchResponse = client.prepareSearch("logs").setQuery(EEQueryBuilder)
        .addAggregation(errorAggregation).addAggregation(exceptionAggregation).execute()
        .actionGet();
    SearchHits sh = searchResponse.getHits();
    if (sh.getTotalHits() != 0) {
      searchResponse = client.prepareSearch("logs").setQuery(EEQueryBuilder)
          .setSize((int) sh.getTotalHits()).execute().actionGet();
      sh = searchResponse.getHits();
      for (int i = 0; i < sh.getTotalHits(); i++) {
        try {
          UpdateRequest update = new UpdateRequest().index("logs").type("traditional")
              .id(sh.getAt(i).getId()).doc("unread", "0");
          request.add(update);
        } catch (ElasticsearchException e) {
          e.printStackTrace();
        }
      }
      client.bulk(request).actionGet();
    }
    return ExtJSResponse.successRes();
  }

  /**
   * 删除日志
   * 
   * @param app
   */
  public void deleteLog(String app) {
	  Client client = esUtil.getClient();

    /* delete docs in logfiles */
    QueryBuilder queryFiles = QueryBuilders.matchQuery("app", app);
    QuerySourceBuilder sourceBuilderFiles = new QuerySourceBuilder().setQuery(queryFiles);
    DeleteByQueryRequest deleteQueryFiles = new DeleteByQueryRequest("logfiles")
        .source(sourceBuilderFiles);
    client.deleteByQuery(deleteQueryFiles).actionGet();

    /* delete docs in logs */
    QueryBuilder queryLogs = QueryBuilders.termQuery("serviceName", app);
    QuerySourceBuilder sourceBuilderLogs = new QuerySourceBuilder().setQuery(queryLogs);
    DeleteByQueryRequest deleteQueryLogs = new DeleteByQueryRequest("logs")
        .source(sourceBuilderLogs);
    client.deleteByQuery(deleteQueryLogs).actionGet();
  }

  /**
   * 获取应用日志文件,错误统计方式不一样
   * 
   * @return
   */
  @Path("/listEE")
  @GET
  public ExtJSResponse listEE() {

	  Client client = esUtil.getClient();

    List<String> appsList = new ArrayList<String>();
    List<Object> filesList = new ArrayList<Object>();
    /* 获取appfiles */
   /* QueryBuilder bQuery = QueryBuilders.matchAllQuery();

    Order order = Order.term(true);
    AggregationBuilder<?> aggregation = AggregationBuilders.terms("apps").field("serviceName")
        .size(Integer.MAX_VALUE).order(order);
    SearchResponse response = client.prepareSearch("logs").setQuery(bQuery)
        .addAggregation(aggregation).execute().actionGet();

    Terms terms = response.getAggregations().get("apps");
    for (Bucket b : terms.getBuckets()) {
      if (!appsList.contains(b.getKey())) {
        appsList.add(b.getKey());
      }
    }*/
    List<CfAppExternal> cfAppList = caMapper.getCfAppsByParams(
			(String) session.getAttribute(LoginHandler.ORG),
			(String) session.getAttribute(LoginHandler.SPACE));

    for (CfAppExternal app : cfAppList) {
      String appName=app.getAppName();
      LogfileWithEE fileWithEE = new LogfileWithEE();
      long errorSum = 0;
      long exceptionSum = 0;
      fileWithEE.setFileName(appName);
      QueryBuilder eEQueryBuilder = QueryBuilders.boolQuery()
          .must(QueryBuilders.termQuery("serviceName", appName))
          .must(QueryBuilders.termQuery("unread", 1));
      AggregationBuilder<?> errorAggregation = AggregationBuilders.terms("errors")
          .field("eventType").include("ERROR");
      AggregationBuilder<?> exceptionAggregation = AggregationBuilders.terms("exceptions")
          .field("eventType").include("EXCEPTION");
      SearchResponse searchResponse = client.prepareSearch("logs").setQuery(eEQueryBuilder)
          .addAggregation(errorAggregation).addAggregation(exceptionAggregation).execute()
          .actionGet();

      Terms errorTerm = searchResponse.getAggregations().get("errors");
      for (Bucket b : errorTerm.getBuckets()) {
        errorSum += b.getDocCount();
      }
      fileWithEE.setErrorCount(errorSum);
      Terms exceptionTerm = searchResponse.getAggregations().get("exceptions");
      for (Bucket b : exceptionTerm.getBuckets()) {
        exceptionSum += b.getDocCount();
      }
      fileWithEE.setExceptionCount(exceptionSum);
      filesList.add(fileWithEE);
    }
    return ExtJSResponse.successResWithData(filesList);
  }

  /**
   * 下载日志文件
   * 
   * @param fileName
   * @param response
   */
  @Path("/download")
  @GET
  public void download(final @QueryParam("fileName") String fileName,
      @Context HttpServletResponse response) {
	  Client client = esUtil.getClient();
    /* match query */
    QueryBuilder qb = QueryBuilders.matchQuery("fileName", fileName);

    SearchResponse searchResponse = client.prepareSearch("logfiles").setQuery(qb)
        .addSort("line", SortOrder.ASC).setSize(100000).execute().actionGet();
    SearchHit[] hits = searchResponse.getHits().getHits();
    BufferedWriter outputStream = null;
    response.setContentType("text/plain");
    response.setCharacterEncoding("UTF-8");
    response.setHeader("Location", fileName);
    response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
    try {
      outputStream = new BufferedWriter(new OutputStreamWriter(response.getOutputStream(),"UTF-8"));
      for (int i = 0; i < hits.length; i++) {
        String message = (String) hits[i].getSource().get("message");
        if (StringUtil.isEmpty(message)) {
          continue;
        }
        outputStream.append(message);
        outputStream.append("\n");
      }
      outputStream.flush();
      outputStream.close();
    } catch (IOException e) {
      logger.error("download log file error:", e);
    }
  }

  /**
   * 应用访问量
   * 
   * @param appName
   * @param from
   * @param to
   * @param step
   * @return
   */
  @Path("/pageview")
  @GET
  public ExtJSResponse pageview(final @QueryParam("appName") String appName,
      final @QueryParam("from") String from, final @QueryParam("to") String to,
      final @QueryParam("step") String step) {

    List<AppAnalayseData> results = new ArrayList<AppAnalayseData>();
    Client client = esUtil.getClient();
    char stepUnit = step.charAt(step.length() - 1);

    QueryBuilder qb = QueryBuilders.rangeQuery("timestamp").from(from).to(to);

    BoolQueryBuilder bQuery = QueryBuilders.boolQuery();
    bQuery.must(qb);
    bQuery.must(QueryBuilders.matchQuery("serviceName", appName));
    bQuery.must(QueryBuilders.termQuery("eventType", "access"));

    Interval interval = new Interval(step);

    DateHistogramBuilder agg = AggregationBuilders.dateHistogram("latency").field("timestamp")
        .interval(interval).extendedBounds(from, to).minDocCount(0);

    SearchResponse searchResponse = client.prepareSearch("logs").setQuery(bQuery)
        .addAggregation(agg).setSize(100).execute().actionGet();

    DateHistogram histograms = searchResponse.getAggregations().get("latency");
    for (org.elasticsearch.search.aggregations.bucket.histogram.DateHistogram.Bucket b : histograms
        .getBuckets()) {
      String datetime = b.getKeyAsText().toString();
      String date;
      if (stepUnit == 'h')
        date = datetime.substring(11, 13) + "h";
      else if (stepUnit == 'd')
        date = datetime.substring(5, 10);
      else if (stepUnit == 'M') {
          date = datetime.substring(5, 7) + 'M';
     } else if (stepUnit == 'm') {
            date = datetime.substring(14,16) + 'm';
     }
      else
        date = datetime;
      results.add(new AppAnalayseData(date, b.getDocCount()));
    }

    return ExtJSResponse.successResWithData(results);

  }

  @Path("/serviceLogMistakes")
  @GET
  public ExtJSResponse serviceLogMistakes(final @QueryParam("serviceName") String serviceName,
      final @QueryParam("eventType") String eventType) {

	  Client client = esUtil.getClient();

    BoolQueryBuilder bQuery = QueryBuilders.boolQuery();
    bQuery.must(QueryBuilders.matchQuery("serviceName", serviceName));
    bQuery.must(QueryBuilders.termQuery("eventType", eventType));

    SearchResponse searchResponse = client.prepareSearch("logs").setQuery(bQuery).execute()
        .actionGet();

    return ExtJSResponse.successResWithData(searchResponse.getHits().getTotalHits());
  }

  /**
   * 应用访问延时
   * 
   * @param appName
   * @param from
   * @param to
   * @param step
   * @return
   */
  @Path("/latency")
  @GET
  public ExtJSResponse getLatencyTime(final @QueryParam("appName") String appName,
      final @QueryParam("from") String from, final @QueryParam("to") String to,
      final @QueryParam("step") String step) {

    List<AppAnalayseData> results = new ArrayList<AppAnalayseData>();
    Client client = esUtil.getClient();
    char stepUnit = step.charAt(step.length() - 1);

    QueryBuilder qb = QueryBuilders.rangeQuery("timestamp").from(from).to(to);
    BoolQueryBuilder bQuery = QueryBuilders.boolQuery();
    bQuery.must(qb);
    bQuery.must(QueryBuilders.matchQuery("serviceName", appName));
    bQuery.must(QueryBuilders.termQuery("eventType", "access"));

    AvgBuilder sub = AggregationBuilders.avg("avg").field("responseTime");
    Interval interval = new Interval(step);
    DateHistogramBuilder agg = AggregationBuilders.dateHistogram("latency").field("timestamp")
        .interval(interval).extendedBounds(from, to).minDocCount(0);
    agg.subAggregation(sub);

    SearchResponse searchResponse = client.prepareSearch("logs").setQuery(bQuery)
        .addAggregation(agg).setSize(100).execute().actionGet();

    DateHistogram histograms = searchResponse.getAggregations().get("latency");
    for (org.elasticsearch.search.aggregations.bucket.histogram.DateHistogram.Bucket b : histograms
        .getBuckets()) {
      Avg avg = b.getAggregations().get("avg");
      String datetime = b.getKeyAsText().toString();
      String date;
      if (stepUnit == 'h') {
        date = datetime.substring(11, 13) + "h";
      } else if (stepUnit == 'd') {
        date = datetime.substring(5, 10);
      } else if (stepUnit == 'M') {
        date = datetime.substring(5, 7) + 'M';
      } else if (stepUnit == 'm') {
          date = datetime.substring(14,16) + 'm';
      } else {
        date = datetime;
      }
      results.add(new AppAnalayseData(date, avg.getValue()));
    }
    return ExtJSResponse.successResWithData(results);
  }

  /**
   * 日志分析
   * 
   * @param analyzerType
   * @param field
   * @param size
   * @param from
   * @param to
   * @param appName
   * @param step
   * @param key
   * @param config
   * @return
   */
  @Path("/logAnalyzer")
  @GET
  public ExtJSResponse logAnalyzer(@QueryParam("analyzerType") String analyzerType,
      @QueryParam("field") String field, @QueryParam("size") String size,
      @QueryParam("from") String from, @QueryParam("to") String to,
      @QueryParam("appName") String appName, @QueryParam("step") String step,
      @QueryParam("key") String key, @QueryParam("config") String config) {

	  Client client = esUtil.getClient();

    // 设定搜索条件
    QueryBuilder qb = null;
    if (StringUtil.isNotEmpty(appName)) {
      qb = QueryBuilders.matchQuery("serviceName", appName);
    } else {
      qb = QueryBuilders.matchAllQuery();
    }
    BoolQueryBuilder bQuery = QueryBuilders.boolQuery();
    bQuery.must(qb);
    if (StringUtil.isNotEmpty(key) /* && !key.equals("") && key !=null */) {
      // bQuery.must(QueryBuilders.queryString("*"+key+"*").field("_all"));
      bQuery.must(QueryBuilders.wildcardQuery("_all", "*" + key + "*"));
    }
    if (StringUtil.isNotEmpty(from) && StringUtil.isNotEmpty(to)) {
      bQuery.must(QueryBuilders.rangeQuery("timestamp").from(from).to(to));
    }

    /*
     * 关键字统计
     */
    if (analyzerType.equals("关键字统计") || analyzerType.equals("KeywordStatic")) {
      List<LogsForCount> results = new ArrayList<LogsForCount>();

      AggregationBuilder<?> agg = AggregationBuilders.terms("logAnalyzer").field(field)
          .size(Integer.parseInt(size));
      SearchResponse searchResponse = client.prepareSearch("logs").setQuery(bQuery)
          .addAggregation(agg).setSize(10000).execute().actionGet();
      Terms terms = searchResponse.getAggregations().get("logAnalyzer");
      if (field.equals("timestamp")) {
        for (Bucket b : terms.getBuckets()) {
          String dateString = StringUtil.toDateString(b.getKeyAsNumber().toString());
          LogsForCount logsForCount = new LogsForCount();
          logsForCount.setName(dateString);
          logsForCount.setCount(b.getDocCount());
          logsForCount.setConfig(config);
          logger.info(dateString);
          results.add(logsForCount);
        }
      } else {
        for (Bucket b : terms.getBuckets()) {
          LogsForCount logsForCount = new LogsForCount();
          logsForCount.setName(b.getKeyAsText().toString());
          logsForCount.setCount(b.getDocCount());
          logsForCount.setConfig(config);
          logger.info(b.getKeyAsText().toString());
          results.add(logsForCount);
        }
      }
      return ExtJSResponse.successResWithData(results);
    } else if ("概况统计".equals(analyzerType) || "SurveyStatic".equals(analyzerType)) {
      try {
        List<LogsWithMMA> results = new ArrayList<LogsWithMMA>();
        LogsWithMMA logsWithMax = new LogsWithMMA();
        LogsWithMMA logsWithMin = new LogsWithMMA();
        LogsWithMMA logsWithAvg = new LogsWithMMA();

        /*
         * if(field.equals("timestamp")) return ExtJSResponse.errorResWithMsg("请选择responseTime字段");
         */

        MinBuilder minBuilder = AggregationBuilders.min("min").field(field);
        MaxBuilder maxBuilder = AggregationBuilders.max("max").field(field);
        AvgBuilder avgBuilder = AggregationBuilders.avg("avg").field(field);
        SearchResponse searchResponse = client.prepareSearch("logs").setQuery(bQuery)
            .addAggregation(minBuilder).addAggregation(maxBuilder).addAggregation(avgBuilder)
            .setSize(10000).execute().actionGet();
        Max maxCount = searchResponse.getAggregations().get("max");
        Min minCount = searchResponse.getAggregations().get("min");
        Avg avgCount = searchResponse.getAggregations().get("avg");

        logsWithMax.setName("max");
        logsWithMax.setCount(maxCount.getValue());
        logsWithMax.setConfig(config);
        results.add(logsWithMax);
        logsWithMin.setName("min");
        logsWithMin.setCount(minCount.getValue());
        logsWithMin.setConfig(config);
        results.add(logsWithMin);
        logsWithAvg.setName("avg");
        logsWithAvg.setCount(avgCount.getValue());
        logsWithAvg.setConfig(config);
        results.add(logsWithAvg);

        return ExtJSResponse.successResWithData(results);
      } catch (Exception e) {
        return ExtJSResponse.errorRes();
      }

    } else if ("时间直方图统计".equals(analyzerType) || "ChartStatic".equals(analyzerType)) {
      List<LogsForCount> results = new ArrayList<LogsForCount>();

      Interval interval = new Interval(step);
      DateHistogramBuilder agg = AggregationBuilders.dateHistogram("responses").field(field)
          .interval(interval).minDocCount(0);
      SearchResponse searchResponse = client.prepareSearch("logs").setQuery(bQuery)
          .addAggregation(agg).setSize(10000).execute().actionGet();

      DateHistogram histograms = searchResponse.getAggregations().get("responses");
      for (org.elasticsearch.search.aggregations.bucket.histogram.DateHistogram.Bucket b : histograms
          .getBuckets()) {
        if (b.getDocCount() != 0) {
          LogsForCount logsForCount = new LogsForCount();
          if ("timestamp".equals(field)) {
            logsForCount.setName(b.getKeyAsText().toString().substring(5, 16));
          } else {
            logsForCount.setName(b.getKeyAsText().toString());
          }
          logsForCount.setCount(b.getDocCount());
          logsForCount.setConfig(config);
          results.add(logsForCount);
        }
      }
      return ExtJSResponse.successResWithData(results);
    } else {
      return ExtJSResponse.errorRes();
    }
  }

  /**
   * 
   * @param mappingType
   * @return
   */
  @Path("/fields")
  @GET
  public ExtJSResponse getFields(@QueryParam("mappingType") String mappingType) {
	 Client client = esUtil.getClient();
    ClusterState cs = client.admin().cluster().prepareState().setIndices("logs").execute()
        .actionGet().getState();

    IndexMetaData imd = cs.getMetaData().index("logs");
    MappingMetaData mdd = imd.mapping("traditional");
    return ExtJSResponse.successResWithData(mdd.source());
  }
}

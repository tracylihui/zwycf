/*
 * FILE     :  QuotaManagement.java
 *
 * CLASS    :  QuotaManagement
 *
 * COPYRIGHT:
 *
 *   The computer systems, procedures, data bases and programs
 *   created and maintained by Qware Technology Group Co Ltd, are proprietary
 *   in nature and as such are confidential.  Any unauthorized
 *   use or disclosure of such information may result in civil
 *   liabilities.
 *
 *   Copyright Mar 4, 2015 by Qware Technology Group Co Ltd.
 *   All Rights Reserved.
 */
package com.cfweb.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.api.QuotaApi;
import com.cfweb.util.ExtJSResponse;
import com.cfweb.util.StringUtil;

@Service
@Path("/quota")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class QuotaManagement {
	
	@Autowired
	private HttpSession session;

	@GET
	@Path("/listall")
	public ExtJSResponse listAll() throws Exception {
		return ExtJSResponse.successResWithData(QuotaApi.getQuotas(session, null));
	}

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse createQuota(@FormParam("name") String name, @FormParam("services") Integer totalServices,
			@FormParam("routes") Integer totalRoute, @FormParam("memeory") String memoryLimit,
			@FormParam("instancememeory") String instanceMemoryLimit) throws Exception {
		if(QuotaApi.getQuotas(session, name).size()>0){
			return ExtJSResponse.errorResWithMsg("nameRepeat");
		}
		HashMap<String, Object> params=new HashMap<>();
		params.put("name", name);
		params.put("non_basic_services_allowed", true);
		params.put("total_services", totalServices);
		params.put("total_routes", totalRoute);
		if(StringUtil.isEmpty(instanceMemoryLimit)){
			params.put("instance_memory_limit", -1);
		}else{
			params.put("instance_memory_limit", Integer.valueOf(instanceMemoryLimit));
		}
		
		if(memoryLimit.indexOf("M")!=-1){
			params.put("memory_limit", Integer.valueOf(memoryLimit.substring(0, memoryLimit.length()-1)));
		}else if(memoryLimit.indexOf("G")!=-1){
			params.put("memory_limit", Integer.valueOf(memoryLimit.substring(0, memoryLimit.length()-1))*1024);
		}else{
			params.put("memory_limit", Integer.valueOf(memoryLimit));
		}

		QuotaApi.createQuota(session, params);
		return ExtJSResponse.successRes();
	}
	
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse updateQuota(@FormParam("guid") String guid,@FormParam("name") String name, @FormParam("services") Integer totalServices,
			@FormParam("routes") Integer totalRoute, @FormParam("memeory") String memoryLimit,
			@FormParam("instancememeory") String instanceMemoryLimit) throws Exception {
		List<Map<String,Object>> quotas=QuotaApi.getQuotas(session, name);
		
		for(Map<String,Object> quota:quotas){
			@SuppressWarnings("unchecked")
			Map<String,Object> metadata=(Map<String, Object>) quota.get("metadata");
			if(!guid.equals((String) metadata.get("guid"))){
				return ExtJSResponse.errorResWithMsg("nameRepeat");
			}
		}
		HashMap<String, Object> params=new HashMap<>();
		params.put("name", name);
		params.put("non_basic_services_allowed", true);
		params.put("total_services", totalServices);
		params.put("total_routes", totalRoute);
//		params.put("memory_limit", memoryLimit);
		if(StringUtil.isEmpty(instanceMemoryLimit)){
			params.put("instance_memory_limit", -1);
		}else{
			params.put("instance_memory_limit", Integer.valueOf(instanceMemoryLimit));
		}
		
		if(memoryLimit.indexOf("M")!=-1){
			params.put("memory_limit", Integer.valueOf(memoryLimit.substring(0, memoryLimit.length()-1)));
		}else if(memoryLimit.indexOf("G")!=-1){
			params.put("memory_limit", Integer.valueOf(memoryLimit.substring(0, memoryLimit.length()-1))*1024);
		}else{
			params.put("memory_limit", Integer.valueOf(memoryLimit));
		}
		params.put("instance_memory_limit", instanceMemoryLimit);
		QuotaApi.updateQuota(session, guid, params);
		return ExtJSResponse.successRes();
	}
	
	@POST
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse updateQuota(@FormParam("guid") String guid) throws Exception {
		QuotaApi.deleteQuota(session, guid);
		return ExtJSResponse.successRes();
	}
}

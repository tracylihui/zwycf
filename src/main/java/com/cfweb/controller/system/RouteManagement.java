package com.cfweb.controller.system;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.api.RouteApi;
import com.cfweb.controller.LoginHandler;
import com.cfweb.util.ExtJSResponse;

/**
 * 
 * @author dt
 *
 */
@Service
@Path("/route")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RouteManagement {
	
	Logger logger = Logger.getLogger(OrgManagement.class);
	
	@Autowired  
	private HttpSession session; 
	
	/**
     * list all of routes
     * @param 无
     * @return list
     */
	@Path("/listall")
	@GET
	public ExtJSResponse listall() throws Exception {
		List<Map<String, Object>> routes = null;
		routes = RouteApi.get_routes(session);
		return ExtJSResponse.successResWithData(routes);
	}
	
	/**
     * list routes by org and space
     * @param orgName
     * @param spaceName
     * @return list
     */
	@Path("/list")
	@GET
	public ExtJSResponse list(@QueryParam("orgName") String orgName,
			@QueryParam("spaceName") String spaceName) throws Exception {
		System.out.println("------------------orgName:" + orgName + "-----spaceName:" + spaceName);
		List<Map<String, Object>> routes = null;
		//CloudFoundryClient client = (CloudFoundryClient) session.getAttribute(LoginHandler.CLIENT);
		routes = RouteApi.get_routes_by_space(session, orgName, spaceName);
		return ExtJSResponse.successResWithData(routes);
	}
	
	/**
     * add a route
     * @param host
     * @param orgName
     * @param spaceName
     * @param domainName
     * @return success or error
     */
	@Path("/add")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse add(@FormParam("host") String host, @FormParam("orgName") String orgName,
			@FormParam("spaceName") String spaceName, @FormParam("domainName") String domainName) throws Exception {
		RouteApi.create_route_for_space(session, orgName, spaceName, domainName, host);
		return ExtJSResponse.successRes();
	}
	
	/**
     * delete a route
     * @param host
     * @param orgName
     * @param spaceName
     * @param domainName
     * @return success or error
     */
	@Path("/delete")
	@GET
	public ExtJSResponse delete(@QueryParam("host") String host, @QueryParam("orgName") String orgName,
			@QueryParam("spaceName") String spaceName, @QueryParam("domainName") String domainName) throws Exception {
		RouteApi.delete_route_for_space(session, orgName, spaceName, domainName, host);
		return ExtJSResponse.successRes();
	}
	
	/**
     * map a route
     * @param host
     * @param orgName
     * @param spaceName
     * @param domainName
     * @param appName
     * @return success or error
     */
	@Path("/map")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse map(@FormParam("host") String host, @FormParam("orgName") String orgName,
			@FormParam("spaceName") String spaceName, @FormParam("domainName") String domainName,
			@FormParam("appName") String appName) throws Exception {
		RouteApi.map_route(session, orgName, spaceName, domainName, host, appName);
		return ExtJSResponse.successRes();
	}
	
	/**
     * unmap a route
     * @param host
     * @param orgName
     * @param spaceName
     * @param domainName
     * @param appName
     * @return success or error
     */
	@Path("/unmap")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse unmap(@FormParam("host") String host, @FormParam("orgName") String orgName,
			@FormParam("spaceName") String spaceName, @FormParam("domainName") String domainName,
			@FormParam("appName") String appName) throws Exception {
		RouteApi.unmap_route(session, orgName, spaceName, domainName, host, appName);
		return ExtJSResponse.successRes();
	}
	
	/**
     * check a route of the same name
     * @param host
     * @param orgName
     * @param spaceName
     * @param domainName
     * @param appName
     * @return success or error
     */
	@Path("/checkRouteName")
	@GET
	public ExtJSResponse checkDomainName(@QueryParam("orgName") String orgName, @QueryParam("host") String host, @QueryParam("domain") String domain) throws Exception {
		try {
			if(RouteApi.isRouteExist(session, host, orgName, domain)) {
				return ExtJSResponse.successResWithData(false);
			}
		} catch(Exception e) {
			throw e;
		}
		return ExtJSResponse.successResWithData(true);
	}

}

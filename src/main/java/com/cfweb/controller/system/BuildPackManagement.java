package com.cfweb.controller.system;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfweb.api.BuildPackApi;
import com.cfweb.dao.CfEnvironmentMapper;
import com.cfweb.domain.CfEnvironment;
import com.cfweb.domain.CfEnvironmentExample;
import com.cfweb.util.ExtJSResponse;
import com.cfweb.util.FormUtil;

/**
 * 
 * @author dt
 *
 */
@Service
@Path("/buildpack")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BuildPackManagement {
	
	private Logger logger = Logger.getLogger(BuildPackManagement.class);
	
	@Autowired
    private CfEnvironmentMapper ceMapper;
	
	@Autowired  
	private HttpSession session; 
	
	/**
     * list all of environment
     * @param 无
     * @return list
     */
	@Path("/listall")
	@GET
	public ExtJSResponse listall() {
		List<CfEnvironment> lce = null;
		CfEnvironmentExample ceEx = new CfEnvironmentExample();
		ceEx.createCriteria().andIdIsNotNull();
		lce = ceMapper.selectByExample(ceEx);
		return ExtJSResponse.successResWithData(lce);
	}
	
	/**
     * add buildpack
     * @param dataInput
     * @return success or error
     */
	@Path("/add")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse add(MultipartFormDataInput dataInput) throws Exception {
		Map<String, List<InputPart>> uploadForm = dataInput.getFormDataMap();
		List<InputPart> fileParts = uploadForm.get("buildpackFile");
		String buildpack = FormUtil.getFormString(uploadForm.get("buildpackName"));
		String descChn = FormUtil.getFormString(uploadForm.get("descChn"));
		String descEng = FormUtil.getFormString(uploadForm.get("descEng"));
		File file;
		if (fileParts != null && !fileParts.isEmpty()) {
			InputPart inputPart = fileParts.get(0);
			InputStream inputStream = inputPart.getBody(InputStream.class, null);
			file = new File(buildpack);
			FormUtil.inputstreamtofile(inputStream, file);
		} else {
			return ExtJSResponse.errorRes();
		}
		if (BuildPackApi.createBuildpack(buildpack, file, 0 , session) != null) {
			CfEnvironment ce = new CfEnvironment();
			ce.setEnvName(buildpack);
			ce.setEnvDescChn(descChn);
			ce.setEnvDescEng(descEng);
			ceMapper.insert(ce);
			return ExtJSResponse.successRes();
		}
		return ExtJSResponse.errorRes();
	}
	
	/**
     * update buildpack
     * @param dataInput
     * @return success or error
     */
	@Path("/update")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExtJSResponse update(MultipartFormDataInput dataInput) throws Exception {
		Map<String, List<InputPart>> uploadForm = dataInput.getFormDataMap();
		List<InputPart> fileParts = uploadForm.get("buildpackFile");
		String buildpack = FormUtil.getFormString(uploadForm.get("buildpackName"));
		String descChn = FormUtil.getFormString(uploadForm.get("descChn"));
		String descEng = FormUtil.getFormString(uploadForm.get("descEng"));
		File file;
		if (fileParts != null && !fileParts.isEmpty()) {
			InputPart inputPart = fileParts.get(0);
			InputStream inputStream = inputPart.getBody(InputStream.class, null);
			file = new File(buildpack);
			FormUtil.inputstreamtofile(inputStream, file);
		} else {
			return ExtJSResponse.errorRes();
		}
		if (BuildPackApi.update_buildpack(session, buildpack,  file)) {
			CfEnvironment ce = new CfEnvironment();
			ce.setEnvName(buildpack);
			ce.setEnvDescChn(descChn);
			ce.setEnvDescEng(descEng);
			ceMapper.insert(ce);
			return ExtJSResponse.successRes();
		}
		return ExtJSResponse.errorRes();
	}
	
	/**
     * delete buildpack
     * @param buildpackName
     * @return success or error
     */
	@Path("/delete")
	@GET
	public ExtJSResponse delete(@QueryParam("buildpack") String buildpack) throws Exception {
		if (BuildPackApi.delete_buildpack(session, buildpack)) {
			CfEnvironmentExample ceEx = new CfEnvironmentExample();
			ceEx.createCriteria().andEnvNameEqualTo(buildpack);
			ceMapper.deleteByExample(ceEx);
			return ExtJSResponse.successRes();
		}
		return ExtJSResponse.errorRes();
	}

}

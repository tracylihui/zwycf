package com.cfweb.pojo;

public class AppAnalayseData {
	private String date;
	
	private double value;
	
	public AppAnalayseData(String date, double value) {
		this.date = date;
		if(Double.isNaN(value)){
			this.value=0.0;
		}else{
			this.value = value;
		}
		
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	
}

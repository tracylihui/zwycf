package com.cfweb.pojo;

import java.util.List;

public class DatabaseServiceWithBroker {
	
	private Integer id;

    private String serName;

    private String serTemImg;

    private String serPrefix;
    
    private Integer type;
    
    private List<String> brokers;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerName() {
		return serName;
	}

	public void setSerName(String serName) {
		this.serName = serName;
	}

	public String getSerTemImg() {
		return serTemImg;
	}

	public void setSerTemImg(String serTemImg) {
		this.serTemImg = serTemImg;
	}

	public String getSerPrefix() {
		return serPrefix;
	}

	public void setSerPrefix(String serPrefix) {
		this.serPrefix = serPrefix;
	}

	public List<String> getBrokers() {
		return brokers;
	}

	public void setBrokers(List<String> brokers) {
		this.brokers = brokers;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}

package com.cfweb.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class CfServiceInstanceWithType implements Serializable {
    private Integer id;

    private String serviceInstanceName;

    private String service;

    private String plan;

    private String serviceTypeImage;
    
    private Integer serviceTypeId;;
    
    private Date createTime;

    private Date updateTime;
    
    private  Object detail;
    
    private String host;
    private Integer port;
    private String user;
	private static final long serialVersionUID = 1L;
    
    

    public CfServiceInstanceWithType() {
		super();
	}

	public CfServiceInstanceWithType(String serviceInstanceName,
			String service, String plan) {
		super();
		this.serviceInstanceName = serviceInstanceName;
		this.service = service;
		this.plan = plan;
	}

	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceInstanceName() {
        return serviceInstanceName;
    }

    public void setServiceInstanceName(String serviceInstanceName) {
        this.serviceInstanceName = serviceInstanceName == null ? null : serviceInstanceName.trim();
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service == null ? null : service.trim();
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan == null ? null : plan.trim();
    }

	public String getServiceTypeImage() {
		return serviceTypeImage;
	}

	public void setServiceTypeImage(String serviceTypeImage) {
		this.serviceTypeImage = serviceTypeImage;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

    public Object getDetail() {
		return detail;
	}

	public void setDetail(Object detail) {
		this.detail = detail;
	}


	@Override
	public boolean equals(Object obj) {
		return this.serviceInstanceName.equals(obj);
	}
	
	
}
//package com.cfweb.util;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.servlet.http.HttpSession;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import com.cfweb.dao.CfAppMapper;
//import com.cfweb.domain.CfAppExternal;
//import com.cfweb.domain.CfAppQueryVo;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration("classpath:applicationContext.xml")
//public class CfAppMapperTest {
//	@Autowired
//	CfAppMapper cfAppMapper;
//
//	@Test
//	public void test() {
//		List<Integer> states = new ArrayList<Integer>();
//		states.add(0);
//		List<CfAppExternal> cfAppList = cfAppMapper.getCfAppsByParamsWithState(new CfAppQueryVo("org2", "space22", null));
//		for(CfAppExternal c : cfAppList){
//			System.out.println(c);
//		}
//	}
//
//}

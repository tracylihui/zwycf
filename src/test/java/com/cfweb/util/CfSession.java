package com.cfweb.util;

import java.net.MalformedURLException;
import java.net.URI;

import javax.servlet.http.HttpSession;

import com.cfweb.api.OauthApi;
import com.cfweb.pojo.OauthToken;

public class CfSession {
	public static HttpSession getSession(HttpSession session, String org, String space) throws Exception {
		session.setAttribute("username", "admin");
		session.setAttribute("userIsAdmin", "1");
		session.setAttribute("target", URI.create("http://api.yjs.cf").toURL());
		session.setAttribute("space", space);
		session.setAttribute("org", org);
		session.setAttribute("userid", "bae3afbe-7657-408a-9cd9-acec5502be3d");
		session.setAttribute("user_primaryid", "1");
		session.setAttribute("token", OauthApi.login("admin", "admin", session));

		System.out.println("++++++++++++++");
		OauthToken token123 = (OauthToken) session.getAttribute("token");
		System.out.println(token123.getAccessToken());
		System.out.println(token123.getTokenType());
		System.out.println(token123.getRefreshToken());
		System.out.println(token123.getExpirationIn());
		System.out.println("++++++++++++++");
		return session;
	}
}
